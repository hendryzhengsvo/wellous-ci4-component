<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Interface;

use Predis\ClientInterface;
use Predis\Response\Status;
use Wellous\Ci4Component\Libraries\Redis\WsLibRedisLock;
use Wellous\Ci4Component\Libraries\Redis\WsLibRedisQueue;

interface WsInterfaceRedis extends ClientInterface
{
	/**
	 * Creates or retrieves an instance of WsLibRedisLock for a given key.
	 * @param string $key The key used to identify the lock.
	 * @return WsLibRedisLock An instance of WsLibRedisLock for the provided key.
	 */
	public function lock(string $key = 'default'): WsLibRedisLock;

	/**
	 * Retrieves the queue associated with a given key.
	 * If a queue with the provided key does not exist, it creates a new queue and adds it to the queues array.
	 * @param string $key The key to identify the queue.
	 * @return WsLibRedisQueue The queue object associated with the provided key.
	 */
	public function queue(string $key = 'default'): WsLibRedisQueue;

	/**
	 * Retrieves the value associated with a given key from a serialized string.
	 * @param string $key The key to search for in the serialized string.
	 * @return mixed|null The value associated with the provided key, or null if the key is not found or the string is invalid.
	 */
	public function getUnserialize(string $key): mixed;

	/**
	 * Retrieves the value associated with a given key from a serialized JSON string.
	 * @param string $key     The key to search for in the serialized JSON string.
	 * @param string $jsonKey The JSON key to check for the value associated with the provided key.
	 * @return mixed|null The value associated with the provided key, or null if the key is not found or the JSON string is invalid.
	 */
	public function getUnserializeValue(string $key, string $jsonKey): mixed;

	/**
	 * Sets the serialized value for a given key in the cache.
	 * @param string $key       The key for which the value is to be set.
	 * @param mixed  $value     The value to be serialized and set.
	 * @param int    $ttl       The time-to-live in seconds for the key-value pair. Defaults to 0 (no expiration).
	 * @param bool   $notExists Flag indicating whether to set the value only if the key does not already exist. Defaults to FALSE.
	 * @return int|Status The result of the operation. If successful, returns a positive integer indicating the number of bytes written.
	 *                          If unsuccessful, returns a Status object that encapsulates the reason for failure.
	 */
	public function setSerialize(string $key, mixed $value, int $ttl, bool $notExists = FALSE): Status|int;
}