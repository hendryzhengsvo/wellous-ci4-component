<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Interface;

use Aws\Result;
use Wellous\Ci4Component\Exceptions\ServerInternalError;

/**
 * Initializes the storage system with the given bucket configuration and optional CloudFront configuration.
 * @param array $bucketConfig                             The configuration options for the storage system bucket.
 *                                                        Possible options:
 *                                                        - 'bucketName'   string  The name of the bucket (required).
 *                                                        - 'region'       string  The AWS region of the bucket (required).
 *                                                        - 'credentials'  array   The AWS credentials for accessing the bucket (optional).
 *                                                        Possible sub-options:
 *                                                        - 'key'      string  The access key ID (required).
 *                                                        - 'secret'   string  The secret access key (required).
 *                                                        - 'endpoint' string  The custom endpoint URL of the bucket (optional).
 *                                                        - 'usePathStyle' bool Whether to use path-style URLs for accessing the bucket (optional, default: false).
 * @param array $cfConfig                                 The optional CloudFront configuration options.
 *                                                        Possible options:
 *                                                        - 'enabled'           bool    Whether CloudFront is enabled for the bucket (optional, default: false).
 *                                                        - 'distributionId'    string  The CloudFront distribution ID for the bucket (optional, required if enabled is true).
 *                                                        - 'resourceUrl'       string  The CloudFront resource URL for accessing the bucket (optional, required if enabled is true).
 * @return void
 */
interface WsInterfaceCloudFiles
{
	/**
	 * Initializes the storage system with the given bucket configuration and optional CloudFront configuration.
	 * @param array $bucketConfig                                     The configuration options for the storage system bucket.
	 *                                                                Possible options:
	 *                                                                - 'bucketName'        string      The name of the bucket (required).
	 *                                                                - 'region'            string      The AWS region of the bucket (required).
	 *                                                                - 'credentials'       array       The AWS credentials for accessing the bucket (optional).
	 *                                                                Possible sub-options:
	 *                                                                - 'key'               string      The access key ID (required).
	 *                                                                - 'secret'            string      The secret access key (required).
	 *                                                                - 'endpoint'          string      The custom endpoint URL of the bucket (optional).
	 *                                                                - 'usePathStyle'      bool        Whether to use path-style URLs for accessing the bucket (optional, default: false).
	 * @param array $cfConfig                                         The optional CloudFront configuration options.
	 *                                                                Possible options:
	 *                                                                - 'enabled'           bool        Whether CloudFront is enabled for the bucket (optional, default: false).
	 *                                                                - 'distributionId'    string      The CloudFront distribution ID for the bucket (optional, required if enabled is true).
	 *                                                                - 'resourceUrl'       string      The CloudFront resource URL for accessing the bucket (optional, required if enabled is true).
	 * @return void
	 */
	public static function initial(array $bucketConfig, array $cfConfig = []): void;

	/**
	 * Returns the key file path by combining the key file and key path.
	 * If the key path is empty, the key file is returned as-is.
	 * @param string $keyFile The key file.
	 * @param string $keyPath The key path.
	 * @return string The combined key file path.
	 */
	public static function getKeyFile(string $keyFile, string $keyPath): string;

	/**
	 * Generates a URL for accessing a file in a storage system.
	 * @param string $keyFile The filename of the file to be accessed.
	 * @param string $keyPath The path of the file to be accessed.
	 * @return string            The generated URL as a string.
	 */
	public static function getUrl(string $keyFile, string $keyPath): string;

	/**
	 * Checks if a file exists in a storage system.
	 * @param string $keyFile The filename of the file to check.
	 * @param string $keyPath The path of the file (optional, default: '').
	 * @return bool              True if the file exists, false otherwise.
	 */
	public static function isExists(string $keyFile, string $keyPath = ''): bool;

	/**
	 * Get a pre-signed URL for downloading a file from the specified key file in a key path.
	 * @param string $keyFile The name of the key file.
	 * @param string $expires The expiration time for the pre-signed URL. Defaults to '+1 day'.
	 * @return string The pre-signed URL if successful.
	 */
	public static function genDownloadEndpoint(string $keyFile, string $expires = '+1 day'): string;

	/**
	 * Generates a presigned URL for uploading a single file to a storage system.
	 * @param string $fullKey    The full key (path + filename) of the file to be uploaded.
	 * @param string $expires    The expiration time of the presigned URL (optional, default: '+20 minutes').
	 * @param array  $options    Additional options for generating the presigned URL (optional).
	 *                           Possible options:
	 *                           - 'acl'          string      The access control list for the uploaded object (optional).
	 *                           - 'contentType'  string      The content type of the uploaded object (optional).
	 *                           - 'metadata'     array       The metadata for the uploaded object (optional).
	 * @return string The generated presigned URL as a string or an array of URLs if $partSize is provided.
	 */
	public static function genSingleUploadEndpoint(string $fullKey, string $expires = '+20 minutes', array $options = []): string;

	/**
	 * Generates a pre-signed URL for uploading a file in multiple parts.
	 * @param string $fullKey  The full key of the file to be uploaded.
	 * @param int    $filesize The size of the file to be uploaded.
	 * @param int    $partSize The size of each part in the multipart upload. If not provided, the method will automatically determine the part size based on the file size.
	 * @param string $expires  The expiration time of the pre-signed URL. It should be in a format that can be parsed by the strtotime() function. The default value is '+20 minutes'.
	 * @param array  $options  Additional options for generating the pre-signed URL. These options can be used to specify the permissions, ACLs, storage class, etc.
	 * @return array The generated pre-signed URL or an array containing the generated pre-signed URL along with any additional information specified in the options.
	 */
	public static function genMultipartUploadEndpoint(string $fullKey, int $filesize, int $partSize = 0, string $expires = '+20 minutes',
	                                                  array  $options = []): array;

	/**
	 * Generates a presigned URL for uploading a file to a storage system.
	 * @param string $fullKey    The full key (path + filename) of the file to be uploaded.
	 * @param int    $filesize   The size of the file in bytes.
	 * @param int    $partSize   The size of each part in bytes (optional, default: 0).
	 * @param string $expires    The expiration time of the presigned URL (optional, default: '+20 minutes').
	 * @param array  $options    Additional options for generating the presigned URL (optional).
	 *                           Possible options:
	 *                           - 'acl'          string      The access control list for the uploaded object (optional).
	 *                           - 'contentType'  string      The content type of the uploaded object (optional).
	 *                           - 'metadata'     array       The metadata for the uploaded object (optional).
	 * @return array The generated presigned URL as a string or an array of URLs if $partSize is provided.
	 */
	public static function genUploadEndpoint(string $fullKey, int $filesize, int $partSize = 0, string $expires = '+20 minutes',
	                                         array  $options = []): array;

	/**
	 * Uploads the completed multipart upload to an S3 bucket.
	 * @param string $fullKey  The full key of the uploaded file.
	 * @param string $uploadId The upload ID of the multipart upload.
	 * @param array  $parts    The array of parts for the multipart upload.
	 * @param array  $options  Additional options for the completeMultipartUpload method.
	 * @return bool Returns true if the multipart upload is successfully completed, false otherwise.
	 */
	public static function uploadMultiPartComplete(string $fullKey, string $uploadId, array $parts, array $options = []): bool;

	/**
	 * Uploads the file content to the specified key file in a key path.
	 * @param string $keyFile     The name of the key file.
	 * @param mixed  $fileContent The content of the file to be uploaded.
	 * @param string $keyPath     The path to the key file. Defaults to an empty string.
	 * @param array  $options     Additional options for the upload process.
	 * @return bool|string  Returns boolean true if the upload is successful, otherwise an error message as a string.
	 */
	public static function upload(string $keyFile, mixed $fileContent, string $keyPath = '', array $options = []): bool|string;

	/**
	 * Retrieves information about an object from the S3 bucket specified in the config.
	 * @param string $keyFile The key file of the object to retrieve information about.
	 * @param string $keyPath The path to the key file (optional).
	 * @return Result|bool The result of the headObject operation if successful, otherwise FALSE.
	 * @throws ServerInternalError
	 */
	public static function info(string $keyFile, string $keyPath = '', array $options = []): Result|bool;

	/**
	 * Retrieves an object from the S3 bucket specified in the config.
	 * @param string $keyFile The key file of the object to be retrieved.
	 * @param string $keyPath The path to the key file (optional).
	 * @return Result|bool The result of the getObject operation if successful, otherwise FALSE.
	 * @throws ServerInternalError
	 */
	public static function get(string $keyFile, string $keyPath = '', array $options = []): Result|bool;

	/**
	 * Copies a file from one location to another.
	 * @param string $oldKeyFile The key (path + filename) of the file to be copied.
	 * @param string $newKeyFile The key (path + filename) of the new destination of the copied file.
	 * @return bool  Returns true if the file is successfully copied, false otherwise.
	 */
	public static function copy(string $oldKeyFile, string $newKeyFile, array $options = []): bool;

	/**
	 * Moves a file from one location to another in the storage system.
	 * @param string $oldKeyFile The full key (path + filename) of the file to be moved.
	 * @param string $newKeyFile The full key (path + filename) of the new location for the file.
	 * @return bool                Returns true if the file was successfully moved, false otherwise.
	 */
	public static function move(string $oldKeyFile, string $newKeyFile, array $options = []): bool;

	/**
	 * Deletes a file from a storage system.
	 * @param string $keyFile The name of the file to be deleted.
	 * @param string $keyPath The path where the file is located (optional, default: '').
	 * @return bool             Returns true if the file was successfully deleted, false otherwise.
	 */
	public static function delete(string $keyFile, string $keyPath = '', array $options = []): bool;
}