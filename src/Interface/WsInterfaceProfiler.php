<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Interface;
/**
 *
 */
interface WsInterfaceProfiler
{
	/**
	 * Initializes the application.
	 * If $enable is set to TRUE, the application is enabled and can be used.
	 * If $enable is set to FALSE, the application is disabled and cannot be used.
	 * If $enable is set to NULL, the application status remains unchanged.
	 * @param bool|null $enable (optional) Whether to enable or disable the application. Defaults to NULL.
	 * @return void
	 */
	public static function initial(?bool $enable = NULL): void;

	/**
	 * Checks if the feature is enabled.
	 * @return bool Returns TRUE if the feature is enabled, and FALSE otherwise.
	 */
	public static function isEnable(): bool;

	/**
	 * Adds a log entry to the specified log file.
	 * @param string $name  The name of the log file to add the entry to.
	 * @param string $log   The log entry to be added.
	 * @param bool   $reset (optional) Whether to reset the log file before adding the entry. Defaults to TRUE.
	 **/
	public static function addLog(string $name, string $log, bool $reset = TRUE): void;

	/**
	 * Retrieves the checkpoint data for a given name.
	 * @param string $name (optional) The name of the checkpoint data to retrieve. Defaults to an empty string.
	 * @return array Returns an array containing the checkpoint data for the given name.
	 *                     The array has the following structure:
	 *                     - `name`: The name of the checkpoint.
	 *                     - `timestamp`: The timestamp when the checkpoint was created.
	 *                     - `memory_usage`: The memory usage at the checkpoint in bytes.
	 *                     - `peak_memory_usage`: The peak memory usage at the checkpoint in bytes.
	 *                     - `memory_limit`: The memory limit set for the script in bytes.
	 *                     If the given name does not exist, an empty array is returned.
	 */
	public static function getCheckPoint(string $name = ''): array;

	/**
	 * Retrieves the estimated time remaining for a specific task or function.
	 * @param string $name   (optional) The name of the task or function. Defaults to an empty string.
	 * @param bool   $detail (optional) Whether to provide detailed timing information. Defaults to FALSE.
	 * @return int|array Returns the estimated time remaining for the task or function depending on the value of $detail.
	 *                       If $detail is set to FALSE, an integer value representing the estimated time remaining in seconds is returned.
	 *                       If $detail is set to TRUE, an associative array containing detailed timing information is returned.
	 *                       The array has the following keys:
	 *                       - `estimated_time_remaining`: The estimated time remaining in seconds.
	 *                       - `elapsed_time`: The elapsed time since the task or function started in seconds.
	 *                       - `total_time`: The total expected time for the task or function in seconds.
	 */
	public static function getTimerETA(string $name = '', bool $detail = FALSE): int|array;

	public static function getHrETA(int $fromHrTime, bool $detail = FALSE, int|float|null $toHrTime = NULL): int|array;

	/**
	 * Retrieves the memory usage of a given variable or function.
	 * @param string $name   The name of the variable or function to retrieve memory usage for.
	 * @param bool   $detail (optional) Whether to provide detailed memory usage information. Defaults to FALSE.
	 * @return int|array Returns the memory usage of the variable or function depending on the value of $detail.
	 *                       If $detail is set to FALSE, an integer value representing the memory usage in bytes is returned.
	 *                       If $detail is set to TRUE, an associative array containing detailed memory usage information is returned.
	 *                       The array has the following keys:
	 *                       - `memory_usage`: The actual memory usage in bytes.
	 *                       - `peak_memory_usage`: The peak memory usage in bytes.
	 *                       - `memory_limit`: The memory limit set for the script in bytes.
	 */
	public static function getMemUse(string $name, bool $detail = FALSE): int|array;

	/**
	 * Return the raw memory usage between two memory points.
	 * @param int      $fromMem The initial memory point in bytes.
	 * @param bool     $detail  Optional. Whether to include detailed information or not. Defaults to FALSE.
	 * @param int|null $toMem   Optional. The final memory point in bytes. Defaults to NULL.
	 * @return int|array The raw memory usage between the two memory points.
	 */
	public static function getMemUseRaw(int $fromMem, bool $detail = FALSE, int|null $toMem = NULL): int|array;

	/**
	 * Adds a checkpoint with the given name.
	 * @param string $name The name of the checkpoint.
	 * @return void
	 */
	public static function addCheckPoint(string $name): void;

	/**
	 * Deletes a checkpoint with the given name.
	 * @param string $name The name of the checkpoint to be deleted.
	 * @return void
	 */
	public static function delCheckPoint(string $name): void;

	/**
	 * Debug method for printing debugging information.
	 * @param mixed ...$debug The data to be debugged
	 * @return void
	 */
	public static function debug(...$debug): void;

	/**
	 * Fetches data from a source and returns it as an array.
	 * @return array The fetched data as an array.
	 */
	public static function fetch(): array;

	/**
	 * Retrieves the usage time of the method.
	 * @return array The array containing the usage time information.
	 */
	public static function getUsageTime(): array;
}