<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Interface;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface WsInterfaceRpc
 * @package Wellous\Ci4Component\Interface
 * @property ResponseInterface|null $lastResponse
 * @method static get(string $url, array $options = [], bool $raw = FALSE)
 * @method static head(string $url, array $options = [], bool $raw = FALSE)
 * @method static post(string $url, array $options = [], bool $raw = FALSE)
 * @method static put(string $url, array $options = [], bool $raw = FALSE)
 * @method static delete(string $url, array $options = [], bool $raw = FALSE)
 * @method static connect(string $url, array $options = [], bool $raw = FALSE)
 * @method static options(string $url, array $options = [], bool $raw = FALSE)
 * @method static patch(string $url, array $options = [], bool $raw = FALSE)
 */
interface WsInterfaceRpc
{
	/**
	 * Get the last response received from a request.
	 * @return ResponseInterface|null The last response received. Returns null if no response has been received yet.
	 */
	public static function getLastResponse(): ?ResponseInterface;

	/**
	 * Magic method to handle static method calls dynamically.
	 * @param string $name The name of the method being called.
	 * @param array  $args The arguments passed to the method.
	 * @return mixed  The result of the method call.
	 */
	public static function __callStatic(string $name, array $args = []): mixed;

	/**
	 * Makes an HTTP request using the specified method, URL, options, and event parameters.
	 * @param string     $method      The HTTP method to use for the request.
	 * @param string     $url         The URL to send the request to.
	 * @param array      $options     The optional request options.
	 * @param bool       $raw         Whether to return the raw response or parse it.
	 * @param array|null $eventParams The optional event parameters.
	 * @return mixed The response of the HTTP request.
	 */
	public static function request(string $method, string $url, array $options = [], bool $raw = FALSE, ?array $eventParams = NULL): mixed;

	/**
	 * Creates a new instance of the Client class.
	 * @param array $options An associative array of options to configure the client (optional).
	 * @return Client The newly created instance of the Client class.
	 */
	public static function client(array $options = []): Client;

	/**
	 * Executes multiple HTTP requests asynchronously.
	 * @param array         $requestDatas The array of request data objects.
	 * @param callable|null $onFulfilled  (Optional) A callback function to be executed when each request is fulfilled. The callback will receive the response object as its only argument
	 *                                    .
	 * @param callable|null $onRejected   (Optional) A callback function to be executed when each request is rejected. The callback will receive the exception object as its only argument.
	 * @param array         $options      (Optional) Additional options for the requests.
	 * @param int           $concurrency  (Optional) The maximum number of simultaneous requests to be executed. Default is 10.
	 * @param array|null    $eventParams  (Optional) Additional event parameters to be passed to the onFulfilled and onRejected callbacks.
	 * @return void
	 */
	public static function batchRequestAsync(array $requestDatas, ?callable $onFulfilled = NULL, ?callable $onRejected = NULL,
	                                         array $options = [], int $concurrency = 10, ?array $eventParams = NULL): void;
}