<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Interface;

use Wellous\Ci4Component\Tool\WsToolRedisClient;

/**
 * Interface WsInterfaceLock
 * This interface defines methods for acquiring and releasing locks for resources.
 */
interface WsInterfaceLock
{

	/**
	 * Sets the Redis client for the class.
	 * @param WsToolRedisClient $redis The Redis client.
	 * @return void
	 */
	public static function initial(WsToolRedisClient $redis): void;

	/**
	 * Extend the expiration time of a lock.
	 * @param string $name The name of the lock.
	 * @param int    $ttl  The new expiration time in seconds. Default is 3600 seconds.
	 * @return int The result of the expiration extension operation. Returns 1 on success, 0 on failure.
	 */
	public function extend(string $name, int $ttl = 3600): int;

	/**
	 * Checks if a lock with the given name exists.
	 * @param string $name The name of the lock.
	 * @return bool Returns true if the lock exists, false otherwise.
	 */
	public function isLock(string $name): bool;

	/**
	 * Acquires a named lock with optional auto-release capability.
	 * @param string $name           The name of the lock.
	 * @param bool   $autoRelease    (Optional) Whether to automatically release the lock. Defaults to TRUE.
	 * @param int    $waitLockSecond (Optional) The maximum number of seconds to wait for the lock. Defaults to 600.
	 * @param int    $ttl            (Optional) The time to live for the lock in seconds. Defaults to 3600.
	 * @return bool Whether the lock was successfully acquired.
	 */
	public function acquire(string $name, bool $autoRelease = TRUE, int $waitLockSecond = 600, int $ttl = 3600): bool;

	/**
	 * Release a lock with the given name.
	 * @param string $name           The name of the lock to release.
	 * @param int    $delayReleaseMs The delay release value in milliseconds. Default is 0.
	 * @return bool Returns TRUE if the lock was released successfully, FALSE otherwise.
	 */
	public function release(string $name, int $delayReleaseMs = 0): bool;
}