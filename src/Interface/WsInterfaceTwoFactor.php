<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Interface;

use RobThree\Auth\TwoFactorAuthException;

/**
 * Interface WsInterfaceTwoFactor
 * This interface defines the methods for generating and verifying one-time passwords for two-factor authentication.
 */
interface WsInterfaceTwoFactor
{

	/**
	 * Initializes the OTP generator with the specified secret, number of digits, and time period.
	 * @param string $secret The secret used to generate the one-time password.
	 * @param int    $digits The number of digits to include in the one-time password. Default is 6.
	 * @param int    $period The time period in seconds for which the one-time password is valid. Default is 60.
	 * @return void
	 */
	public static function initial(string $secret, int $digits = 6, int $period = 60): void;

	/**
	 * Returns the digits for a given number.
	 * @return int The digits for the given number.
	 */
	public static function getDigits(): int;

	/**
	 * Returns the period as an integer.
	 * @return int The period as an integer.
	 */
	public static function getPeriod(): int;

	/**
	 * Returns the code for the given action.
	 * @param string $action The action for which to get the code. Default is an empty string.
	 * @return string The code for the given action.
	 */
	public static function getCode(string $action = ''): string;

	/**
	 * Verifies if the given code matches the expected code within a specified discrepancy range.
	 * @param string $action      The action associated with the code.
	 * @param string $code        The code to be verified.
	 * @param int    $discrepancy The acceptable discrepancy range between the expected code and the given code. Defaults to 10.
	 * @return bool True if the code is within the acceptable discrepancy range, false otherwise.
	 * @throws TwoFactorAuthException If there is an error during the verification process.
	 */
	public static function verify(string $action, string $code, int $discrepancy = 10): bool;
}