<?php
declare(strict_types = 1);

namespace Wellous\Ci4Component\Modules;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Events\Events;
use JetBrains\PhpStorm\ArrayShape;
use Throwable;
use Aws\S3\Crypto\InstructionFileMetadataStrategy;
use Wellous\Ci4Component\ArrayObject\DBSearchResult;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Libraries\WsLibUtilities;

/**
 * Represents a database report.
 */
class WsDBReport {
	/**
	 * @var WsDBReportResult[]
	 */
	private static array $cached = [];
	
	/**
	 * @var array $statement The statement variable that is initialized as an empty array.
	 *                       This variable is used to store a collection of data elements in the form of an array.
	 *                       By declaring it as an empty array, it is ready to be populated with data elements later on.
	 *                       Usage of this variable may include storing database query results, iterative data processing, or any other
	 *                       scenario where a collection of data elements is required.
	 */
	private array $statement = [];
	
	/**
	 * @var array $bindingParams The binding parameters.
	 */
	private array $bindingParams = [];
	
	/**
	 * @var string|ConnectionInterface $db The PDO object for database connection.
	 */
	protected ConnectionInterface | string $db = 'default';
	
	/**
	 * @var bool $shareInstance Whether to share the instance of the database connection.
	 */
	protected bool $shareInstance = TRUE;
	
	/**
	 * @var int $resultMode The result mode for the database query.
	 */
	protected int $resultMode = MYSQLI_STORE_RESULT;
	
	protected string $table = '';
	
	/**
	 * Class constructor.
	 * @param ConnectionInterface|string|null $db The database connection. If not provided, a default connection will be used.
	 * @param bool                            $shareInstance
	 * @param int                             $resultMode
	 */
	public function __construct(ConnectionInterface | string | null $db = NULL, bool $shareInstance = TRUE, int $resultMode = MYSQLI_STORE_RESULT)
	{
		if($db !== NULL)
			$this->db = $db;
		$this->shareInstance = $shareInstance;
		$this->resultMode    = in_array($resultMode, [MYSQLI_STORE_RESULT, MYSQLI_USE_RESULT], TRUE) ? $resultMode : MYSQLI_STORE_RESULT;
		$this->resetStatement(TRUE);
		
		Events::on('beforeDbWrite', function()
		{
			$this->clearCache(TRUE);
		});
	}
	
	/**
	 * Preprocesses the input by ensuring it is an array and trimming each element.
	 * @param array|string $input The input to preprocess.
	 * @return array The preprocessed input.
	 */
	private function inputWhere(array | string $input): array
	{
		if(!is_array($input))
			$input = [$input];
		if(is_array($input))
			$input = array_map('trim', $input);
		return $input;
	}
	
	/**
	 * Preprocesses the input by ensuring it is an array and trimming each element.
	 * @param array|string $input The input to preprocess.
	 * @return array The preprocessed input.
	 */
	private function inputSelect(array | string $input): array
	{
		if(!is_array($input))
			$input = [$input];
		$result = [];
		foreach($input as $item)
		{
			$splitItems = preg_split('/(?<!\\\),/', $item);
			foreach($splitItems as $splitItem)
			{
				$trimmedItem = trim(str_replace('\,', ',', $splitItem));
				if(!empty($trimmedItem))
					$result[] = $trimmedItem;
			}
		}
		return $result;
	}
	
	/**
	 * Resets the statement array for a new database query.
	 * Clears the bindingParams array and sets the statement array to its initial state.
	 * The statement array contains keys for select, from, where, group, having, order, and limit.
	 * Each key represents a part of the SQL query statement.
	 * @param bool   $reset
	 * @param string $name
	 * @return void
	 */
	public function resetStatement(bool $reset, string $name = ''): void
	{
		if($reset)
		{
			if(!empty($name))
				$this->statement[$name] = [];
			else
			{
				$this->bindingParams = [];
				$this->statement     = [
					'select' => [],
					'from'   => $this->table,
					'join'   => [],
					'where'  => [],
					'group'  => [],
					'having' => [],
					'order'  => [],
					'limit'  => NULL,
				];
			}
		}
	}
	
	/**
	 * Binds parameters for the database query.
	 * This method can be used to bind parameters to the database query. Parameters can be
	 * provided as a key-value pair array, or individual name-value pairs. If an array is
	 * passed as the first argument and the second argument is not provided, the array will
	 * be merged with the existing binding parameters. If a single name and value are passed,
	 * that name-value pair will be added to the binding parameters.
	 * @param string|array $name  The parameter name or an array of key-value pairs.
	 * @param mixed        $value The value of the parameter.
	 * @return static             Returns the current object instance.
	 */
	protected function bindingParams(string | array $name, mixed $value = NULL): static
	{
		if(is_array($name) && $value === NULL)
			$this->bindingParams = array_merge($this->bindingParams, $name);
		else
			$this->bindingParams[$name] = $value;
		return $this;
	}
	
	/**
	 * Executes a database query and returns a WsDBReportResult object.
	 * @param string $query  The SQL query to execute.
	 * @param array  $params The parameters for the query (default: []).
	 * @return WsDBReportResult The result of the database query.
	 * @throws DbQueryError
	 */
	protected function query(string $query, array $params = [], bool $reset = TRUE, bool $nativeType = FALSE): WsDBReportResult
	{
		try
		{
			$params    = array_merge($this->bindingParams, $params);
			$cacheName = hash('sha3-256', $query.json_encode($params));
			if(self::$cached[$cacheName] ?? FALSE)
			{
				WsLibProfiler::addCheckPoint('dbRptCached');
				$this->resetStatement($reset);
				WsLibProfiler::addLog('dbRptCached', $query, type: 'sql');
				return self::$cached[$cacheName];
			}
			$result = new WsDBReportResult($query, $params, $nativeType, $this->db, $this->shareInstance, $this->resultMode);
			$this->resetStatement($reset);
			if($this->resultMode === MYSQLI_STORE_RESULT)
			{
				$this->clearCache();
				self::$cached[$cacheName] = $result;
				return self::$cached[$cacheName];
			} else
			{
				return $result;
			}
		} catch(Throwable $ex)
		{
			$this->resetStatement($reset);
			throw new DbQueryError($ex->getMessage(), previous: $ex);
		}
	}
	
	/**
	 * Set the columns to select in the database query.
	 * @param array|string $select The columns to select (default: '*').
	 *                             If a string is provided, it will be split by the comma (,).
	 * @return static $this
	 */
	protected function select(array | string $select = '*', array $params = [], bool $reset = FALSE): static
	{
		$this->resetStatement($reset, 'select');
		$this->statement['select'] = [...$this->statement['select'], ...$this->inputSelect($select)];
		$this->bindingParams($params);
		return $this;
	}
	
	/**
	 * Set the table name in the FROM clause of the SQL query.
	 * @param string $from The table name to set in the FROM clause.
	 * @return static Returns an instance of the class on which the method is called.
	 */
	protected function from(string $from): static
	{
		$this->statement['from'] = $from;
		return $this;
	}
	
	/**
	 * Joins a table with the current query and returns the updated query builder object.
	 * @param string       $table The name of the table to join.
	 * @param array|string $on    The condition for joining the table. Can be an array of conditions or a single condition.
	 * @param string       $type  The type of join. Can be 'INNER', 'LEFT', 'RIGHT', or an empty string for a default join type (default:
	 *                            '').
	 * @return static The updated query builder object.
	 */
	protected function join(string $table, array | string $on, string $type = '', array $params = [], bool $reset = FALSE): static
	{
		$this->resetStatement($reset, 'join');
		$this->statement['join'][] = ['table' => $table, 'on' => $this->inputWhere($on), 'type' => $type];
		$this->bindingParams($params);
		return $this;
	}
	
	/**
	 * Adds a WHERE clause to the database query.
	 * @param array|string $where The WHERE clause to add.
	 * @return static The current instance of the object.
	 */
	protected function where(array | string $where, array $params = [], bool $reset = FALSE): static
	{
		$this->resetStatement($reset, 'where');
		$this->statement['where'] = [...$this->statement['where'], ...$this->inputWhere($where)];
		$this->bindingParams($params);
		return $this;
	}
	
	/**
	 * Sets the GROUP BY clause of the database query.
	 * @param array|string $group The column(s) to group by. Accepts either an array of column names or a comma-separated string (default:
	 *                            '*').
	 * @return static The current object instance.
	 */
	protected function groupBy(array | string $group, bool $reset = FALSE): static
	{
		$this->resetStatement($reset, 'group');
		$this->statement['group'] = [...$this->statement['group'], ...$this->inputSelect($group)];
		return $this;
	}
	
	/**
	 * Adds a HAVING clause to the database query.
	 * @param mixed $having The HAVING condition to add to the query.
	 * @return static Returns an instance of the current class.
	 */
	protected function having(array | string $having, array $params = [], bool $reset = FALSE): static
	{
		$this->resetStatement($reset, 'having');
		$this->statement['having'] = [...$this->statement['having'], ...$this->inputWhere($having)];
		$this->bindingParams($params);
		return $this;
	}
	
	/**
	 * Sets the order clause for the database query.
	 * @param array|string $order The order clause for the query. It can be either a string or an array of strings.
	 * @return static The current object instance.
	 */
	protected function orderBy(array | string $order, bool $reset = FALSE): static
	{
		$this->resetStatement($reset, 'order');
		$this->statement['order'] = [...$this->statement['order'], ...$this->inputSelect($order)];
		return $this;
	}
	
	/**
	 * Sets the limit and offset for the query.
	 * @param int|null $limit  The number of records to be retrieved (default: NULL).
	 * @param int      $offset The number of records to skip (default: 0).
	 * @return static Returns the current instance of the class.
	 */
	protected function limit(?int $limit = NULL, int $offset = 0): static
	{
		if(!empty($limit))
			$this->statement['limit'] = "$offset, $limit";
		return $this;
	}
	
	/**
	 * @param array $params
	 * @param bool  $reset
	 * @param bool  $nativeType
	 * @return WsDBReportResult
	 * @throws DbQueryError
	 */
	protected function execute(array $params = [], bool $reset = TRUE, bool $nativeType = FALSE): WsDBReportResult
	{
		return $this->query($this->getCompiledSelect(FALSE), $params, $reset, $nativeType);
	}
	
	/**
	 * Compiles the SELECT statement into a SQL string.
	 * @param bool $reset Whether to reset the statement (default: FALSE).
	 * @return string The compiled SQL string.
	 * @throws DbQueryError If the select or from statement is not set.
	 */
	public function getCompiledSelect(bool $reset = FALSE): string
	{
		if(empty($this->statement['select']) || empty($this->statement['from']))
			throw new DbQueryError('Execute query error: select and from statement must be set');
		$sql = "SELECT ".implode(', ', $this->statement['select'])." FROM {$this->statement['from']}";
		if(!empty($this->statement['join']))
			foreach($this->statement['join'] as $join)
				$sql .= " {$join['type']} JOIN {$join['table']} ON ".implode(' AND ', $join['on']);
		if(!empty($this->statement['where']))
			$sql .= " WHERE ".implode(' AND ', $this->statement['where']);
		if(!empty($this->statement['group']))
			$sql .= " GROUP BY ".implode(', ', $this->statement['group']);
		if(!empty($this->statement['having']))
			$sql .= " HAVING ".implode(' AND ', $this->statement['having']);
		if(!empty($this->statement['order']))
			$sql .= " ORDER BY ".implode(', ', $this->statement['order']);
		if(!empty($this->statement['limit']))
			$sql .= " LIMIT ".$this->statement['limit'];
		
		$this->resetStatement($reset);
		
		return $sql;
	}
	
	/**
	 * Calculates the sum of a column in the database.
	 * @param string $column The column name to calculate the sum.
	 * @param array  $params The parameters for the query (default: []).
	 * @param bool   $reset  Reset the statement after executing the query (default: TRUE).
	 * @return float The sum of the specified column.
	 * @throws DbQueryError
	 */
	protected function sum(string $column, array $params = [], bool $reset = TRUE): float
	{
		return $this->cell("SUM($column)", $params, 'float', $reset);
	}
	
	/**
	 * Calculates the average of a column in the database and returns the result as a float.
	 * @param string $column The column to calculate the average for.
	 * @param array  $params The parameters for the query (default: []).
	 * @param bool   $reset  Whether to reset the statement after executing the query (default: TRUE).
	 * @return float The average of the specified column.
	 * @throws DbQueryError If an error occurs while executing the query.
	 */
	protected function average(string $column, array $params = [], bool $reset = TRUE): float
	{
		return $this->cell("AVG($column)", $params, 'float', $reset);
	}
	
	/**
	 * Returns the minimum value of a specific column from the database.
	 * @param string $column The name of the column to retrieve the minimum value from.
	 * @param array  $params The parameters for the query (default: []).
	 * @param bool   $reset  Whether to reset the statement after executing the query (default: TRUE).
	 * @return float         The minimum value of the specified column.
	 * @throws DbQueryError   If an error occurs while executing the query.
	 */
	protected function min(string $column, array $params = [], bool $reset = TRUE): float
	{
		return $this->cell("MIN($column)", $params, 'float', $reset);
	}
	
	/**
	 * Returns the maximum value of a specific column in the database.
	 * @param string $column The column to find the maximum value for.
	 * @param array  $params The parameters for the query (default: []).
	 * @param bool   $reset  Whether to reset the statement after executing the query (default: TRUE).
	 * @return float The maximum value of the specified column.
	 * @throws DbQueryError
	 */
	protected function max(string $column, array $params = [], bool $reset = TRUE): float
	{
		return $this->cell("MAX($column)", $params, 'float', $reset);
	}
	
	/**
	 * Counts the number of records in the database.
	 * @param string $column The column to count (default: '*').
	 * @param array  $params The parameters for the query (default: []).
	 * @param bool   $reset  Whether to reset the statement after executing the query (default: true).
	 * @return int The number of records in the database.
	 * @throws DbQueryError
	 */
	protected function count(string $column = '*', array $params = [], bool $reset = TRUE): int
	{
		return $this->cell("COUNT($column)", $params, 'int', $reset);
	}
	
	/**
	 * Returns the value of a single cell from a database query.
	 * @param string $columnOrFnc The name of the function or column to select.
	 * @param array  $params      The parameters for the query (default: []).
	 * @param string $dataType    The data type of the cell value (default: 'string').
	 * @param bool   $reset       Whether or not to reset the statement after execution (default: true).
	 * @return int|float|string|null The value of the cell.
	 * @throws DbQueryError If an error occurs while executing the query.
	 */
	public function cell(string $columnOrFnc, array $params = [], string $dataType = 'string', bool $reset = TRUE): int | float | string | null
	{
		$sql = "SELECT $columnOrFnc FROM {$this->statement['from']}";
		if(!empty($this->statement['join']))
			foreach($this->statement['join'] as $join)
				$sql .= " {$join['type']} JOIN {$join['table']} ON ".implode(' AND ', $join['on']);
		if(!empty($this->statement['where']))
			$sql .= " WHERE ".implode(' AND ', $this->statement['where']);
		if(!empty($this->statement['group']))
			$sql .= " GROUP BY ".implode(', ', $this->statement['group']);
		if(!empty($this->statement['having']))
			$sql .= " HAVING ".implode(' AND ', $this->statement['having']);
		$sql .= " LIMIT 1";
		return $this->query($sql, $params, $reset, TRUE)->findCell($dataType);
	}
	
	/**
	 * Retrieves data with pagination.
	 * @param int|null $page       The page number to retrieve (default: 1).
	 * @param int      $pageSize   The maximum number of items per page (default: 10).
	 * @param string   $key        The column to use as the key for the dataset (default: '').
	 * @param bool     $reset      Whether to reset the query builder after execution (default: true).
	 * @return DBSearchResult  An associative array containing the count of total items and the retrieved data.
	 *                             - 'Count': The count of total items in the dataset.
	 *                             - 'Data': An array containing the retrieved data.
	 * @throws DbQueryError Thrown if there is an error executing the query.
	 */
	#[ArrayShape(['count' => "int", 'totalPage' => "int", 'page' => "int", 'data' => "array"])]
	public function findAllWithPagination(?int $page = NULL, int $pageSize = 10, string $key = '', bool $reset = TRUE): DBSearchResult
	{
		$total     = $this->count('*', [], FALSE);
		$pageSize  = max(1, (int)abs($pageSize));
		$totalPage = (int)ceil($total / $pageSize);
		if($page !== NULL)
		{
			if($page > $totalPage)
				$page = $totalPage;
			if($page < 1)
				$page = 1;
			$this->limit($pageSize, ($page - 1) * $pageSize);
		}
		$page = (int)$page;
		return new DBSearchResult([
			'count'     => $total,
			'totalPage' => $totalPage,
			'page'      => $page,
			'data'      => $this->execute([], $reset)->findAll($key),
		]);
	}
	
	/**
	 * Clears the cache of database query results.
	 * @param bool $force Whether to force clear the cache (default: FALSE).
	 * @return void
	 */
	protected function clearCache(bool $force = FALSE): void
	{
		//Clear cache if left then 10mb usable memory
		if($force || WsLibUtilities::isNearMaxMem())
		{
			foreach(static::$cached as $cached)
			{
				if($cached instanceof WsDBReportResult)
					$cached->freeResult();
			}
			static::$cached = [];
			gc_collect_cycles();
		}
	}
}
