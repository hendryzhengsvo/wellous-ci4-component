<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Modules;

use Wellous\Ci4Component\Traits\WsTraitModule;

/**
 * Class WsModules
 * This class represents a module container for components in a web service.
 */
class WsModules
{

	use WsTraitModule;

	/**
	 * Class constructor.
	 * Initializes the baseModule property with a default value.
	 * @return void
	 */
	public function __construct()
	{
		$this->baseModule = ['models' => WsModel::class];
	}

	/**
	 * Load a component into the components array.
	 * @param string $name      The name of the component.
	 * @param object $component The component object to be loaded.
	 * @return bool Returns true if the component was successfully loaded, false otherwise.
	 */
	public function load(string $name, object $component): bool
	{
		if (!empty($name) && !empty($component)) {
			$name = ucfirst($name);
			$this->components[$name] = $component;
			return TRUE;
		}
		else
			return FALSE;
	}
}