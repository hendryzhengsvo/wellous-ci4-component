<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Modules;

use Closure;
use CodeIgniter\Config\Factories;
use CodeIgniter\Database\BaseResult;
use CodeIgniter\Database\Exceptions\DataException;
use CodeIgniter\Database\Query;
use CodeIgniter\Entity\Entity;
use CodeIgniter\Model;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionException;
use stdClass;
use Throwable;
use Wellous\Ci4Component\ArrayObject\DBSearchResult;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Tool\WsStore;

/**
 * Class WsModelBase - Base class for web services models.
 * @package YourPackageName
 */
abstract class WsModelBase extends Model
{

	/**
	 * Declaration of the variable $uniqueFields
	 * @var array An empty array to hold unique fields
	 */
	protected array $uniqueFields = [];

	/**
	 * Protects the fields in the given row and throws an exception if any fields are not allowed.
	 * @param array $row The row to protect the fields for.
	 * @return array The protected row.
	 * @throws DbQueryError If any fields in the row are not allowed.
	 */
	protected function doProtectFields(array $row):array
	{
		$newRow = parent::doProtectFields($row);
		if(empty($newRow))
			throw new DbQueryError("All fields is not allowed: " . implode(", ", array_keys($row)));
		return $newRow;
	}

	/**
	 * This method is called on save to determine if entry have to be updated
	 * If this method return false insert operation will be executed
	 * @param array|object $row Data
	 */
	protected function shouldUpdate($row): bool
	{
		$id = $this->getIdValue($row);

		if (empty($id))
			return FALSE;

		if ($this->useAutoIncrement === TRUE)
			return TRUE;

		// When useAutoIncrement feature is disabled, check
		// in the database if given record already exists
		return $this->where($this->primaryKey, $id)->countAllResults() === 1;
	}

	/**
	 * Retrieve the unique fields of the object.
	 * @return bool|array Returns an array of unique fields if available, otherwise returns FALSE.
	 */
	public function getUniqueFields(): bool|array
	{
		return !empty($this->uniqueFields) ? $this->uniqueFields : FALSE;
	}

	/**
	 * Retrieves the primary key of the current instance.
	 * @return string The primary key value.
	 */
	public function getPrimaryKey(): string
	{
		return $this->primaryKey;
	}

	/**
	 * @param $id
	 * @return array|object|null
	 */
	public function find($id = NULL): object|array|null
	{
		/** @var Entity|array|object|null $result */
		$result = parent::find($id);
		return $result ?? NULL;
	}

	/**
	 * Save the given data.
	 * @param mixed $row The data to be saved.
	 * @return bool Returns TRUE if the data was successfully saved, otherwise FALSE.
	 * @throws DbQueryError Thrown if there are errors during the save process.
	 * @throws ReflectionException
	 */
	public function save($row): bool
	{
		$this->validateData($row);
		if ($row instanceof Entity && !$row->hasChanged())
			return TRUE;
		$result = parent::save($row);
		if ($errors = $this->errors())
			throw new DbQueryError(json_encode($errors));
		return $result;
	}

	/**
	 * Validates the given data.
	 * @param mixed $data The data to be validated.
	 * @return void
	 * @throws DbQueryError If the data is not valid.
	 */
	private function validateData(mixed $data): void
	{
		if (!empty($data) && ($data instanceof Entity) && !($data instanceof $this->returnType))
			throw new DbQueryError("Receive incorrect entity on save. Required:$this->returnType, Received:" . get_class($data));
		if (!empty($data) && !($data instanceof Entity) && !($data instanceof stdClass) && !is_array($data))
			throw new DbQueryError("Receive not acceptable data type, required: Entity, stdClass, array  given type:" . gettype($data));
	}

	/**
	 * Update a record with the given data.
	 * @param mixed|null $id  The ID of the record to update.
	 * @param mixed|null $row The data to update the record with.
	 * @return bool Returns `true` if the update was successful, otherwise `false`.
	 * @throws DbQueryError If there are validation errors.
	 * @throws ReflectionException
	 */
	public function update($id = NULL, $row = NULL): bool
	{
		$this->validateData($row);
		$result = parent::update($id, $row);
		if ($errors = $this->errors())
			throw new DbQueryError(json_encode($errors));
		return $result;
	}

	/**
	 * @param mixed $row      The data to be inserted into the database.
	 * @param bool  $returnID Whether to return the inserted ID or not. Default is TRUE.
	 * @return bool|int|string Returns TRUE if the insertion is successful. Returns the inserted ID as an integer if $returnID is TRUE. Returns a string if it is not possible to determine
	 *                        the inserted ID. If the insertion fails, an exception will be thrown.
	 * @throws DbQueryError Throws a DbQueryError exception if there are validation errors.
	 * @throws ReflectionException
	 */
	public function insert($row = NULL, bool $returnID = TRUE): bool|int|string
	{
		$this->validateData($row);
		$result = parent::insert($row, $returnID);
		if ($errors = $this->errors())
			throw new DbQueryError(json_encode($errors));
		return $result;
	}

	/**
	 * Replace data in the database table.
	 * @param array|null $row       The data to be replaced. Default is NULL.
	 * @param bool       $returnSQL Whether to return the generated SQL query instead of executing it. Default is FALSE.
	 * @return string|bool|Query|BaseResult Returns the generated SQL query or the result of the replace operation.
	 * @throws DbQueryError If there are any errors during the replace operation.
	 */
	public function replace(?array $row = NULL, bool $returnSQL = FALSE): string|bool|Query|BaseResult
	{
		$this->validateData($row);
		$result = parent::replace($row, $returnSQL);
		if ($errors = $this->errors())
			throw new DbQueryError(json_encode($errors));
		return $result;
	}

	/**
	 * Magic method to retrieve inaccessible properties dynamically.
	 * @param string $name The name of the property to retrieve.
	 * @return Model|null The value of the property if it exists, otherwise NULL.
	 */
	public function __get(string $name)
	{
		if ($name !== 'Modules')
			return Factories::models($name, ['getShared' => TRUE]);
		else
			return NULL;
	}

	/**
	 * Find the value of a specific cell in the database table.
	 * @param string            $columnName The name of the column to retrieve the value from.
	 * @param array|string|null $functions  Optional. The functions to apply to the column value. Default is NULL.
	 * @param string            $dataType   Optional. The desired data type of the returned value. Default is 'default'.
	 * @return float|bool|int|string|null The value of the cell, converted to the specified data type.
	 */
	public function findCell(string $columnName, array|string $functions = NULL, string $dataType = 'default'): float|bool|int|string|null
	{
		$columnNameWithFunctions = $columnName;
		if (!empty($functions)) {
			if (is_string($functions))
				$functions = explode(",", $functions);
			$functions = array_reverse($functions);
			foreach ($functions as $key => $value) {
				$params = [];
				if (!is_numeric($key)) {
					$function = $key;
					$params = is_array($value) ? $value : [$value];
				}
				else
					$function = $value;
				array_unshift($params, '{value}');
				foreach ($params as &$param)
					$param = str_replace("{value}", $columnNameWithFunctions, $param);
				$columnNameWithFunctions = "$function(" . implode(",", $params) . ")";
			}
			$columnNameWithFunctions .= " as $columnName";
		}
		$row = $this->asArray()->select($columnNameWithFunctions)->first();
		$value = $row[$columnName] ?? NULL;
		if (!empty($functions) && is_array($functions)) {
			$firstFunction = reset($functions);
			if (in_array(strtolower($firstFunction), ['count', 'sum', 'max', 'min', 'avg'], TRUE) && is_numeric($value))
				$value += 0; //force to int/float
		}
		return $this->convertType($value, $dataType);
	}

	/**
	 * Retrieve the first entity from the data source.
	 * @return object|array|null The first entity, an array of entities, or null if no entity is found.
	 */
	public function first(): object|array|null
	{
		/** @var Entity|array|object|null $result */
		$result = parent::first();
		return $result ?? NULL;
	}

	/**
	 * Converts the given value to the specified data type.
	 * @param mixed  $value    The value to be converted.
	 * @param string $dataType The data type to convert to.
	 *                         Possible values are: 'bool', 'boolean', 'int', 'integer', 'double', 'float', 'json', 'json-array'.
	 * @return mixed The converted value.
	 */
	private function convertType(mixed $value, string $dataType): mixed
	{
		return match ($dataType) {
			'bool', 'boolean' => (bool)$value,
			'int', 'integer'  => (int)$value,
			'double', 'float' => (double)$value,
			'json'            => json_decode($value),
			'json-array'      => json_decode($value, TRUE),
			default           => $value,
		};
	}

	/**
	 * Retrieves a specific column from the database table.
	 * @param string      $columnName       The name of the column to retrieve.
	 * @param string|null $columnNameForKey The name of the column to use as keys in the resulting array. Defaults to NULL.
	 * @param string      $dataType         The data type to convert the retrieved values to. Defaults to 'string'.
	 * @return array|null Returns an array containing the values from the specified column, with keys if $columnNameForKey is specified.
	 *                                      Returns NULL if the column could not be found or if an error occurred.
	 * @throws DataException Thrown if multiple columns are specified for $columnName or $columnNameForKey is specified with multiple columns.
	 */
	public function findColumn(string $columnName, ?string $columnNameForKey = NULL, string $dataType = 'string'): ?array
	{
		if (!empty($columnNameForKey) || $dataType !== 'string') {
			if (str_contains($columnName, ',') || (!empty($columnNameForKey) && str_contains($columnNameForKey, ',')))
				throw DataException::forFindColumnHaveMultipleColumns();
			$this->asArray()->select("$columnName as 'value'");
			if (!empty($columnNameForKey))
				$this->select("$columnNameForKey as 'key'");
			$result = array_column($this->findAll(), 'value', 'key');
		}
		else
			$result = parent::findColumn($columnName);
		if (is_array($result))
			foreach ($result as &$item)
				$item = $this->convertType($item, $dataType);
		return $result;
	}


	/**
	 * Find all records.
	 * @param int         $limit  (optional) The maximum number of records to retrieve. Defaults to 0 for no limit.
	 * @param int         $offset (optional) The number of records to skip before starting to retrieve. Defaults to 0 for no offset.
	 * @param string|null $key    (optional) The key used to index the result array. Defaults to NULL.
	 * @return array|null The array of records if $key is provided, otherwise returns the original data array
	 */
	public function findAll(int $limit = 0, int $offset = 0, ?string $key = NULL): ?array
	{
		$data = parent::findAll($limit, $offset);
		if (empty($key))
			return $data;
		$result = [];
		foreach ($data as $item) {
			$index = trim((string)(is_object($item) ? $item->$key : $item[$key]));
			$result[$index] = $item;
		}
		return $result;
	}

	/**
	 * Finds all records in the database table with pagination.
	 * @param int|null    $page     The current page number (default is 1).
	 * @param int         $pageSize The number of records to display per page (default is 10).
	 * @param string|null $key      The search key for filtering results (default is NULL).
	 * @return DBSearchResult The search result object containing the total count and data.
	 * @throws DbQueryError
	 */
	#[ArrayShape(['count' => "int", 'totalPage' => "int", 'page' => "int", 'data' => "array"])]
	public function findAllWithPagination(?int $page = NULL, int $pageSize = 10, ?string $key = NULL): DBSearchResult
	{
		try {
			$total = (int)static::countAllResults(FALSE);
			$pageSize = max(1, (int)abs($pageSize));
			$totalPage = (int)ceil($total / $pageSize);
			if ($page !== NULL) {
				if ($page > $totalPage)
					$page = $totalPage;
				if ($page < 1)
					$page = 1;
			}
			$page = (int)$page;
			return new DBSearchResult([
				'count'     => $total,
				'totalPage' => $totalPage,
				'page'      => $page,
				'data'      => static::findAll($pageSize, ($page - 1) * $pageSize, $key),
			]);
		} catch (Throwable $ex) {
			$this->builder()->resetQuery();
			throw new DbQueryError($ex->getMessage(), previous: $ex);
		}
	}

	/**
	 * Inserts multiple rows into the database table.
	 * @param array|null $set       An array containing the rows to be inserted. Each row should be an associative array with column names as keys and values as values.
	 * @param bool|null  $escape    Whether to escape the values before inserting them into the database.
	 * @param int        $batchSize The number of rows to insert in a single batch.
	 * @param bool       $testing   Whether to run the insert query in a testing environment.
	 * @return bool|int Returns true if the insertion was successful or the number of rows inserted.
	 * @throws DbQueryError If there are any database query errors.
	 * @throws ReflectionException
	 */
	public function insertBatch(?array $set = NULL, ?bool $escape = NULL, int $batchSize = 100, bool $testing = FALSE): bool|int
	{
		$result = parent::insertBatch($set, $escape, $batchSize, $testing);
		if ($this->errors())
			throw new DbQueryError(json_encode($this->errors()));
		return $result;
	}

	/**
	 * Updates multiple records in the database in batches.
	 * @param array|null  $set       The data to be updated for each record. (default: NULL)
	 * @param string|null $index     The column name to be used as the index for the batch update. (default: NULL)
	 * @param int         $batchSize The maximum number of records to update in a single batch. (default: 100)
	 * @param bool        $returnSQL Whether to return the generated SQL query instead of executing the update. (default: FALSE)
	 * @return array|bool|int The result of the update operation. If $returnSQL is TRUE, the generated SQL query is returned as a string. If an error occurs during the update operation,
	 *                               a DbQueryError exception is thrown. Otherwise, the number of affected rows is returned as an integer.
	 * @throws DbQueryError
	 * @throws ReflectionException
	 */
	public function updateBatch(?array $set = NULL, ?string $index = NULL, int $batchSize = 100, bool $returnSQL = FALSE): array|bool|int
	{
		$result = parent::updateBatch($set, $index, $batchSize, $returnSQL);
		if ($this->errors())
			throw new DbQueryError(json_encode($this->errors()));
		return $result;
	}

	/**
	 * Loops over records in batches, allowing you to operate on them.
	 * Works with $this->builder to get the Compiled select to
	 * determine the rows to operate on.
	 * These methods work only with dbCalls
	 * @param int      $size
	 * @param Closure  $userFunc
	 * @param int|null $limit
	 * @param bool     $returnAll
	 */
	public function chunk(int $size, Closure $userFunc, ?int $limit = NULL, bool $returnAll = FALSE): void
	{
		$builder = clone $this->builder();
		$this->builder()->resetQuery();
		$total = $builder->countAllResults(FALSE);
		if (empty($limit) || $limit > $total)
			$limit = $total;
		$offset = 0;
		$tempReturnType = $this->tempReturnType;
		$this->tempReturnType = $this->returnType;
		if (empty($size) && $limit > 0)
			$size = $limit;
		while ($offset <= $limit) {
			$result = $builder->get($size, $offset, FALSE);
			$rows = [];
			if ($result instanceof BaseResult) {
				$rows = $result->getResult($tempReturnType);
				$result->freeResult();
				$offset += count($rows);
			}
			else
				throw DataException::forEmptyDataset('chunk');
			if (empty($rows))
				break;
			else {
				if ($returnAll) {
					if ($userFunc($rows) === FALSE)
						break;
				}
				else
					foreach ($rows as $row)
						if ($userFunc($row) === FALSE)
							break 2;
			}
			WsStore::$queryCached = [];
			gc_collect_cycles();
		}
	}
}
