<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Modules;

use CodeIgniter\Config\Factories;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model as CoreModel;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use function db_connect;

/**
 * This class represents a model for handling database transactions and accessing models.
 */
class WsModel
{
	protected static bool $tranStatus = FALSE;
	protected string      $basePath   = '';

	protected ?ConnectionInterface $db = NULL;

	/**
	 * Begins a database transaction.
	 * @param ConnectionInterface|null $db The database connection to use for the transaction. If `null`, a new connection will be established.
	 * @return bool Returns `true` if the transaction was successfully started, otherwise `false`.
	 * @throws DbQueryError Thrown if the transaction fails to start.
	 */
	public static function beginTrans(?ConnectionInterface $db = NULL, bool $strict = FALSE): bool
	{
		if ($db instanceof ConnectionInterface === FALSE)
			$db = db_connect();
		if (!$db->transEnabled)
			return FALSE;
		if ($db->transBegin()) {
			$db->transStrict($strict);
			static::$tranStatus = TRUE;
			return TRUE;
		}

		throw new DbQueryError("failed to start transaction.");
	}

	/**
	 * Completes a database transaction.
	 * @param ConnectionInterface|null $db The database connection to use for the transaction. If `null`, a new connection will be established.
	 * @return bool Returns `true` if the transaction was successfully completed, otherwise `false`.
	 */
	public static function completeTrans(?ConnectionInterface $db = NULL): bool
	{
		if (!static::$tranStatus) return TRUE;
		if ($db instanceof ConnectionInterface === FALSE)
			$db = db_connect();
		if (!$db->transEnabled)
			return FALSE;
		$transDept = method_exists($db, 'getTransDept') ? $db->getTransDept() : 0;
		return !($transDept > 0) || $db->transComplete();
	}

	/**
	 * Rolls back a database transaction.
	 * @param ConnectionInterface|null $db The database connection to use for the transaction. If `null`, a new connection will be established.
	 * @return bool Returns `true` if the transaction was successfully rolled back, otherwise `false`.
	 */
	public static function rollbackTrans(?ConnectionInterface $db = NULL): bool
	{
		if (!static::$tranStatus) return TRUE;
		if ($db instanceof ConnectionInterface === FALSE)
			$db = db_connect();
		if (!$db->transEnabled)
			return FALSE;
		$transDept = method_exists($db, 'getTransDept') ? $db->getTransDept() : 0;
		return !($transDept > 0) || $db->transRollback();
	}

	/**
	 * Retrieves a CoreModel object by its name.
	 * @param string $name    The name of the CoreModel object to retrieve.
	 * @param array  $options An optional array of options to pass to the CoreModel factory.
	 * @return CoreModel|null Returns the CoreModel object if found, otherwise `null`.
	 */
	public function get(string $name, array $options = []): ?CoreModel
	{
		return Factories::models($this->basePath . ucfirst($name), $options, $this->db);
	}

	/**
	 * Magic getter method to retrieve a property value.
	 * @param string $name The name of the property to retrieve.
	 * @return CoreModel|null The value of the property.
	 */
	public function __get(string $name): ?CoreModel
	{
		return $this->get($name, ['getShared' => TRUE]);
	}

	/**
	 * Checks if a property exists.
	 * @param string $name The name of the property to check.
	 * @return bool Returns `true` if the property exists and is an instance of CoreModel, otherwise `false`.
	 */
	public function __isset(string $name)
	{
		$model = $this->get($name, ['getShared' => TRUE]);
		return $model instanceof CoreModel;
	}

	/**
	 * Returns the status of the current database transaction.
	 * @return bool Returns `true` if a transaction is currently active, otherwise `false`.
	 */
	public function getTransStatus(): bool
	{
		return static::$tranStatus;
	}

	/**
	 * Retrieves the last executed database query as a string.
	 * @return string The last executed database query.
	 */
	public function getLastQuery(?ConnectionInterface $db = NULL): string
	{
		$db = $db ?? $this->db ?? db_connect();
		return (string)$db->getLastQuery();
	}
}