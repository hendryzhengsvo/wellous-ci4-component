<?php
declare(strict_types=1);

/**
 * Class WsDBReportResult
 * Represents a result set from a database query in CodeIgniter 4.
 * The class provides various methods to fetch and manipulate the data.
 */

namespace Wellous\Ci4Component\Modules;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Database\Exceptions\DataException;
use CodeIgniter\Database\MySQLi\Connection;
use CodeIgniter\Database\ResultInterface;
use CodeIgniter\Events\Events;
use Generator;
use stdClass;
use Wellous\Ci4Component\Exceptions\DbQueryError;

class WsDBReportResult
{
	/**
	 * @var ResultInterface
	 */
	private ResultInterface $resultSet;

	private bool $nativeType;

	/**
	 * Boolean flag indicating if the result is free or not.
	 * @var bool $isFreeResult
	 */
	private bool $isFreeResult = FALSE;

	/**
	 * Represents a SQL query.
	 * @var string
	 */
	private string $query;

	/**
	 * Represents a database connection.
	 * @var ConnectionInterface
	 */
	private ConnectionInterface $db;

	private array $booleanValue = [
		'enabled' => FALSE,
		'true'    => '',
		'false'   => '',
	];
	/**
	 * @var int The result mode
	 */
	private int $resultMode;

	/**
	 * @var bool The shared instance
	 */
	private bool $useSharedInstance;

	/**
	 * Class constructor.
	 * @param string                          $query      The SQL query.
	 * @param array                           $params     The parameters for the query (default: []).
	 * @param bool                            $nativeType Flag indicating if the native data types should be used (default: true).
	 * @param ConnectionInterface|string|null $db         The database connection object (default: null).
	 * @param bool                            $useShareInstance
	 * @param int                             $resultMode
	 * @throws DbQueryError If the result set is empty and cannot be processed.
	 */
	public function __construct(string $query, array $params = [], bool $nativeType = TRUE, ConnectionInterface|string|null $db = NULL,
	                            bool   $useShareInstance = TRUE, int $resultMode = MYSQLI_STORE_RESULT)
	{
		$this->query = $query;
		$this->nativeType = $nativeType;
		$this->useSharedInstance = $useShareInstance;
		$this->resultMode = $resultMode;
		if ($db instanceof ConnectionInterface || $db === NULL)
			$this->db = $db;
		else
			$this->db = db_connect($db, $useShareInstance);

		if ($this->db instanceof Connection)
			$this->db->resultMode = $this->resultMode;

		$this->execute($params);

		Events::on('appEnd', function () {
			$this->freeResult();
		});
	}

	/**
	 * Sets boolean values.
	 * @param int|string|bool $true  The string representation of true.
	 * @param int|string|bool $false The string representation of false.
	 * @return void
	 */
	public function setBooleanValue(int|string|bool $true, int|string|bool $false): void
	{
		if ($true !== '' && $false !== '') {
			$this->booleanValue = [
				'enabled' => TRUE,
				'true'    => $true,
				'false'   => $false,
			];
		}
	}

	/**
	 * Executes a prepared query with the given parameters and sets the result set.
	 * @param array &$params The parameters to bind to the query.
	 * @return void
	 * @throws DbQueryError If the result set is empty.
	 */
	private function execute(array &$params): void
	{
		if (!empty($params)) {
			['sql' => $this->query, 'params' => $params] =
				$this->prepareQuery($this->query, $params);
			$prepareSql = $this->db->prepare(function ($db) {
				return $db->query($this->query);
			});
			$resultSet = $prepareSql->execute(...$params);
		}
		else
			$resultSet = $this->db->query($this->query);

		if ($resultSet instanceof ResultInterface)
			$this->resultSet = $resultSet;
		else {
			$this->isFreeResult = TRUE;
			throw DataException::forEmptyDataset('ReportModel');
		}
	}

	/**
	 * Checks if a SQL statement contains placeholders.
	 * @param string $sql The SQL statement to check for placeholders.
	 * @return bool Returns true if the SQL statement contains placeholders, false otherwise.
	 */
	private function containsPlaceholder(string $sql): bool
	{
		$sqlWithoutStrings = preg_replace("/'[^']*'/", '', $sql);
		return str_contains($sqlWithoutStrings, '?');
	}

	/**
	 * Prepares the SQL query with the given parameters.
	 * @param string $sql    The SQL query.
	 * @param array  $params The parameters to bind.
	 * @return array An associative array containing the prepared SQL query and the bind parameters.
	 * @throws DbQueryError if a parameter is missing for a placeholder or if the amount of placeholders
	 *                       and parameters are not equal.
	 */
	private function prepareQuery(string $sql, array $params): array
	{
		if ($this->containsPlaceholder($sql))
			return ['sql' => $sql, 'params' => $params];

		preg_match_all('/:[a-zA-Z0-9_]+:/', $sql, $matches);
		$placeholders = $matches[0];
		$values = [];

		foreach ($placeholders as $placeholder) {
			$paramName = trim($placeholder, ':');
			if (isset($params[$paramName]) && is_array($params[$paramName])) {
				$inPlaceholders = array_fill(0, count($params[$paramName]), '?');
				$inClause = implode(', ', $inPlaceholders);
				$sql = str_replace($placeholder, $inClause, $sql);
				$values = array_merge($values, $params[$paramName]);
			}
			elseif (isset($params[$paramName])) {
				$sql = preg_replace('/' . preg_quote($placeholder, '/') . '/', '?', $sql, 1);
				$values[] = $params[$paramName];
			}
			else
				throw new DbQueryError("Prepare Query is missing parameter for placeholder: $placeholder");
		}

		if (substr_count(preg_replace("/'[^']*'/", '', $sql), '?') !== count($values))
			throw new DbQueryError("Amount of placeholders and parameters are not equal");

		return ['sql' => $sql, 'params' => $values];
	}

	/**
	 * Converts the given value to the specified data type.
	 * @param mixed  $value    The value to be converted.
	 * @param string $dataType The data type to convert the value to.
	 *                         Valid options are 'bool', 'boolean', 'int', 'integer', 'double', 'float', 'json', 'json-array'.
	 * @return mixed The converted value.
	 *                         If the specified data type is 'bool' or 'boolean', returns the value converted to a boolean.
	 *                         If the specified data type is 'int' or 'integer', returns the value converted to an integer.
	 *                         If the specified data type is 'double' or 'float', returns the value converted to a double.
	 *                         If the specified data type is 'json', returns the value decoded as a JSON object or null if decoding failed.
	 *                         If the specified data type is 'json-array', returns the value decoded as a JSON array or null if decoding failed.
	 *                         If the specified data type is not recognized, returns the original value.
	 */
	private function convertType(mixed $value, string $dataType): mixed
	{
		return match ($dataType) {
			'bool', 'boolean' => (bool)$value,
			'int', 'integer'  => (int)$value,
			'double', 'float' => (double)$value,
			'json'            => json_decode($value),
			'json-array'      => json_decode($value, TRUE),
			'string'          => trim((string)$value),
			default           => is_string($value) ? trim($value) : $value,
		};
	}

	/**
	 * Converts the data type of the given data.
	 * @param stdClass|array|null $row The data to be converted.
	 */
	private function convertNativeType(null|stdClass|array $row): null|stdClass|array
	{
		if ($row === NULL || !$this->nativeType)
			return $row;
		foreach ($row as &$value) {
			if (is_numeric($value) && is_string($value) && !str_starts_with($value, '0')) {
				if ((int)$value == $value) {
					$value = (int)$value;
				}
				elseif ((float)$value == $value) {
					$value = (float)$value;
				}
			}
			else {
				if ($this->booleanValue['enabled']) {
					if ($value === $this->booleanValue['true']) {
						$value = TRUE;
					}
					elseif ($value === $this->booleanValue['false']) {
						$value = FALSE;
					}
				}
			}
		}
		return $row;
	}

	/**
	 * Finds and returns the value of the first cell in the result set.
	 * @param string $dataType The data type to convert the value to. Default is 'default'.
	 * @return float|bool|int|string|null The value of the first cell in the result set, converted to the specified data type.
	 */
	public function findCell(string $dataType = 'default'): float|bool|int|string|null
	{
		$tempNativeType = $this->nativeType;
		$this->nativeType = FALSE;
		$row = (array)$this->first();
		$this->nativeType = $tempNativeType;
		return $this->convertType(reset($row), $dataType);
	}

	/**
	 * Fetches the first row from the result set and returns it.
	 * @param string $returnType The type in which the row should be returned. Default is 'array'.
	 * @return array|null The first row from the result set as an array or null if the result set is empty.
	 */
	public function first(string $returnType = 'array'): ?array
	{
		$result = $this->resultSet->getRow(0, $returnType);
		if (!$this->isStoreResultMode())
			$this->freeResult();
		return $this->convertNativeType($result);
	}

	/**
	 * Finds a column in the result set and returns its values.
	 * @param string      $columnName The name of the column to find.
	 * @param string|null $key        Optional key to filter the results
	 */
	public function findColumn(string $columnName, ?string $key = NULL, string $dataType = 'auto'): array
	{
		if (str_contains($columnName, ',') || (!empty($key) && str_contains($key, ',')))
			throw DataException::forFindColumnHaveMultipleColumns();
		$tempNativeType = $this->nativeType;
		$this->nativeType = $dataType === 'auto';
		$data = $this->findAll($key);
		$this->nativeType = $tempNativeType;
		foreach ($data as &$row)
			$row = ($dataType === 'auto') ? $row[$columnName] : $this->convertType($row[$columnName], $dataType);
		return $data;
	}

	/**
	 * Finds all rows in the result set and returns them as an array.
	 * @param string|null $key        (Optional) The name of the column to use as the key in the resulting array.
	 * @param string      $returnType (Optional) The desired return type. Defaults to 'array'.
	 * @return array An array containing all the rows from the result set.
	 */
	public function findAll(string $key = NULL, string $returnType = 'array'): array
	{
		$result = [];
		$index = 0;
		if ($this->resultMode !== MYSQLI_USE_RESULT)
			$this->resultSet->dataSeek();
		while ($row = $this->convertNativeType($this->resultSet->getUnbufferedRow($returnType))) {
			$arykey = !empty($key) ? trim((string)$row[$key]) : $index;
			$result[$arykey] = $row;
			$index++;
		}
		if (!$this->isStoreResultMode())
			$this->freeResult();
		return $result;
	}

	/**
	 * Retrieves each row from the result set and returns it as a Generator.
	 * @param string|null $key        (Optional) The name of the column to use as the key in the resulting array.
	 * @param string      $returnType (Optional) The desired return type. Defaults to 'array'.
	 * @return Generator The rows from the result set as a Generator.
	 */
	public function __invoke(string $key = NULL, string $returnType = 'array'): Generator
	{
		if ($this->isFreeResult) return;
		$index = 0;
		while ($row = $this->convertNativeType($this->resultSet->getUnbufferedRow($returnType))) {
			$arykey = !empty($key) && isset($row[$key]) ? trim((string)$row[$key]) : $index;
			yield $arykey => $row;
			$index++;
		}
		if (!$this->isStoreResultMode())
			$this->freeResult();
	}

	/**
	 * Retrieves the query string used to fetch the result set.
	 * @return string The query string used to fetch the result set.
	 */
	public function getQuery(): string
	{
		return $this->query;
	}

	/**
	 * Retrieves the result set.
	 * @return ResultInterface The result set.
	 */
	public function getResultSet(): ResultInterface
	{
		return $this->resultSet;
	}

	/**
	 * Returns the number of rows in the result set.
	 * @return int The number of rows in the result set.
	 */
	public function getNumRows(): int
	{
		return $this->resultSet->getNumRows();
	}

	/**
	 * Returns the number of rows in the result set.
	 * @return int The number of rows in the result set.
	 */
	public function getFieldCount(): int
	{
		return $this->resultSet->getFieldCount();
	}

	/**
	 * Returns the field names of the result set.
	 * @return array An array containing the field names of the result set.
	 */
	public function getFieldNames(): array
	{
		return $this->resultSet->getFieldNames();
	}

	/**
	 * Retrieves the field data from the result set.
	 * @return array The field data.
	 */
	public function getFieldData(): array
	{
		return $this->resultSet->getFieldData();
	}

	/**
	 * Check if the query result is in store mode.
	 * @return bool Returns true if the query result is in store mode, false otherwise.
	 */
	public function isStoreResultMode(): bool
	{
		return $this->resultMode !== MYSQLI_USE_RESULT;
	}

	/**
	 * Checks if the object is free.
	 * @return bool true if the object is free, false otherwise.
	 */
	public function isFree(): bool
	{
		return $this->isFreeResult;
	}

	/**
	 * Frees the memory associated with the result set.
	 * This method should be called after you have finished working
	 * with the result set to release the memory resources.
	 * @return void
	 */
	public function freeResult(): void
	{
		if ($this->isFreeResult) return;
		$this->isFreeResult = TRUE;
		$this->resultSet->freeResult();
		if (!$this->useSharedInstance)
			$this->db->close();
	}
}
