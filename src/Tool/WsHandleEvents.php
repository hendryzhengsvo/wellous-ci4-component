<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

defined('APP_NAME') || define('APP_NAME', ($_SERVER['HTTP_HOST'] ?? ''));

/*
 * --------------------------------------------------------------------
 * Application Events
 * --------------------------------------------------------------------
 * Events allow you to tap into the execution of the program without
 * modifying or extending core files. This file provides a central
 * location to define your events, though they can always be added
 * at run-time, also, if needed.
 *
 * You create code that can execute by subscribing to events with
 * the 'on()' method. This accepts any form of callable, including
 * Closures, that will be executed when the event is triggered.
 *
 * Example:
 *      Events::on('create', [$myInstance, 'myMethod']);
 */

use Closure;
use CodeIgniter\Events\Events;
use Wellous\Ci4Component\Config\WsCfgServices;
use Wellous\Ci4Component\Libraries\WsLibUtilities;

/**
 * Class WsHandleEvents
 */
class WsHandleEvents
{
	/**
	 * The variable $triggerAppEnd indicates whether to trigger the app end process or not.
	 * @var bool
	 */
	private static bool $triggerAppEnd = FALSE;

	/**
	 * The variable $reportHighRamSize represents the value of high RAM size for reporting purposes.
	 * It is calculated by multiplying the value of 128 with the constant MB (megabyte).
	 * @var array
	 */
	private static array $reportHighRamSize = [
		'web' => 128 * MB,
		'cli' => GB,
	];

	/**
	 * Handle pre-system function,
	 * This method registers two event listeners: one for the 'pre_system' event and one for the 'post_system' event. The 'pre_system' event listener calls the 'handleShutdown' method. The 'post_system' event listener calls the 'handleShutdown' method and triggers the 'app_end' event if the 'triggerAppEnd' flag is not set.
	 * @return void
	 */
	public static function handlePreSystem(): void
	{
		Events::on('post_system', function () {
			if (!self::$triggerAppEnd) {
				Events::trigger('appEnd');
				Events::trigger('app_end');
			}
		}, Events::PRIORITY_LOW);

		Events::on('appEnd', function () {
			self::$triggerAppEnd = TRUE;

			$peakUsage = memory_get_peak_usage();
			$timeElapsed = (int)ceil(microtime(TRUE)) - REQUEST_TIME;

			//report high-ram usage
			if ($peakUsage > WsStore::$maxMemLimit) {
				$route = WsCfgServices::router();
				$request = is_cli() ? WsCfgServices::clirequest() : WsCfgServices::request();
				$controller = $route->controllerName();
				if ($controller instanceof Closure)
					$controller = '{Closure}';
				log_message('warning', json_encode([
					'status'     => 'highram_usage',
					'app'        => APP_NAME ?? $_SERVER['HTTP_HOST'] ?? '',
					'domain'     => $_SERVER['HTTP_HOST'] ?? ROOTPATH ?? '',
					'clientIP'   => WsCfgServices::getClientIp(),
					'method'     => $request->getMethod(),
					'routePath'  => WsCfgServices::getRoutePath(),
					'controller' => "$controller->{$route->methodName()}",
					'params'     => method_exists($request, 'getVar') ? $request->getVar() : [],
					'ram'        => [
						'current' => WsLibUtilities::byteToSize(memory_get_usage()),
						'peak'    => WsLibUtilities::byteToSize($peakUsage),
						'limit'   => WsLibUtilities::byteToSize(static::$reportHighRamSize[is_cli() ? 'cli' : 'web']),
					],
					'elapsedSec' => $timeElapsed,
				], JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
			}

			//report high-ram usage
			$maxLimitExecSecond = is_cli() ? 3500 : 90;
			if ($timeElapsed > $maxLimitExecSecond) {
				$route = WsCfgServices::router();
				$request = is_cli() ? WsCfgServices::clirequest() : WsCfgServices::request();
				$controller = $route->controllerName();
				if ($controller instanceof Closure)
					$controller = '{Closure}';
				log_message('warning', json_encode([
					'status'     => 'longrun_execution',
					'app'        => APP_NAME ?? $_SERVER['HTTP_HOST'] ?? '',
					'domain'     => $_SERVER['HTTP_HOST'] ?? ROOTPATH ?? '',
					'clientIP'   => WsCfgServices::getClientIp(),
					'method'     => $request->getMethod(),
					'routePath'  => WsCfgServices::getRoutePath(),
					'controller' => "$controller->{$route->methodName()}",
					'params'     => method_exists($request, 'getVar') ? $request->getVar() : [],
					'time'       => [
						'elapsedSec' => $timeElapsed,
						'max'        => $maxLimitExecSecond,
					],
				], JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
			}

			closelog();
		}, Events::PRIORITY_HIGH);
	}

	/**
	 * Get the value of the 'triggerAppEnd' flag.
	 * This method returns the current value of the 'triggerAppEnd' flag, which indicates whether the 'app_end' event has been triggered or not.
	 * @return bool The value of the 'triggerAppEnd'
	 */
	public static function getTriggerAppEnd(): bool
	{
		return self::$triggerAppEnd;
	}
}
