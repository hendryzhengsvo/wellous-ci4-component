<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use CodeIgniter\Database\ResultInterface;
use Wellous\Ci4Component\Libraries\WsLibCli;
use Wellous\Ci4Component\Libraries\WsLibProfiler;

class WsStore
{
	/**
	 * @var WsLibCli|null
	 */
	public static ?WsLibCli $cli = NULL;
	/**
	 * @var WsLibProfiler|null
	 */
	public static ?WsLibProfiler $profiler = NULL;
	/**
	 * Represents the maximum memory limit in bytes.
	 * @var int
	 */
	public static int $maxMemLimit = 0;

	/**
	 * @var ResultInterface[]
	 */
	public static array $queryCached = [];
}