<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use CodeIgniter\Autoloader\FileLocator;
use CodeIgniter\Router\RouteCollection as CoreRouteCollection;
use Config\Modules;
use Config\Routing;
use Config\Services;
use Wellous\Ci4Component\Config\WsCfgServices;

require_once SYSTEMPATH . 'Router/RouteCollection.php';

/**
 * Services Configuration file.
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class WsRouteCollection extends CoreRouteCollection
{
	private string $currentUrl;
	private string $method;

	private ?string $corsMethod = NULL;

	private string $slashedCurrentUrl;

	/**
	 * __construct
	 * Initializes a new instance of the class.
	 * @param FileLocator $locator      The file locator instance.
	 * @param Modules     $moduleConfig The module configuration instance.
	 * @param Routing     $routing      The routing instance.
	 * @return void
	 */
	public function __construct(FileLocator $locator, Modules $moduleConfig, Routing $routing)
	{
		parent::__construct($locator, $moduleConfig, $routing);
		$this->currentUrl = WsCfgServices::getRoutePath();
		$this->slashedCurrentUrl = str_replace('/', '\/', $this->currentUrl);
		$this->method = Services::request()->getMethod();
		$request = Services::request();
		if ($this->method === 'options' && $request->hasHeader('Origin') && $request->hasHeader('Access-Control-Request-Method'))
			$this->corsMethod = strtolower($request->getHeaderLine('Access-Control-Request-Method'));
	}

	/**
	 * Groups the given name and parameters.
	 * @param string $name      The name of the group.
	 * @param mixed  ...$params Additional parameters for the group.
	 * @return void
	 */
	public function group(string $name, ...$params): void
	{
		if (!$this->isMatch($this->group ? "$this->group/$name" : $name))
			return;
		parent::group($name, ...$params);
	}

	/**
	 * Checks if the given route path matches the current URL.
	 * @param string $routePath The route path to match against the current URL.
	 * @return bool Returns `true` if the route path matches the current URL, `false` otherwise.
	 */
	protected function isMatch(string $routePath): bool
	{
		$routePath = str_replace('/', '\/', $routePath);
		$regexPath = str_ireplace(
			['(:any)', '(:segment)', '(:num)', '(:alpha)', '(:alphanum)', '(:hash)'],
			['(.*)', '(.[^\/]+)', '(.[^\/]+)', '(.[^\/]+)', '(.[^\/]+)', '(.[^\/]+)'], $routePath);

		if ($routePath !== $regexPath) {
			if (preg_match("/$regexPath/i", $this->currentUrl) === FALSE)
				return FALSE;
		}
		elseif (!str_starts_with($this->slashedCurrentUrl, $routePath))
			return FALSE;
		return TRUE;
	}

	/**
	 * Creates a new route for the given verb, from and to.
	 * @param string     $verb    The HTTP verb for the route.
	 * @param string     $from    The source URL pattern or path.
	 * @param mixed      $to      The destination action or callback.
	 * @param array|null $options Additional options for the route (optional).
	 * @return void
	 */
	protected function create(string $verb, string $from, $to, array $options = NULL): void
	{
		$routePath = trim(esc(strip_tags(($this->group === NULL ? '' : $this->group . '/') . $from)), '/');
		if ($this->method === 'options' && !empty($this->corsMethod) && $this->corsMethod === $verb) //Force verb to options when has origin
			$verb = $this->method;
		if ((($this->method !== $verb && $verb !== '*') || !$this->isMatch($routePath)))
			return;
		parent::create($verb, $from, $to, $options);
	}
}
