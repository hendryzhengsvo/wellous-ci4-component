<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use CodeIgniter\I18n\Time;
use Config\Services;
use DOMDocument;
use stdClass;
use Throwable;
use Wellous\Ci4Component\Config\WsCfgServices;
use Wellous\Ci4Component\Enum\ReporterFormatType;
use Wellous\Ci4Component\Libraries\WsLibProvider;

/**
 * Class Reporter
 * @method devDeploy($type, $content, $params = [])
 * @method liveDeploy($type, $content, $params = [])
 * @method debug($type, $content, $params = [])
 * @method log($type, $content, $params = [])
 */
abstract class WsToolReporter
{
    /**
     * @var array
     */
    protected array $reporterChannel;

    /**
     * Magic method for handling dynamic method calls.
     * @param string $name      The name of the method being called.
     * @param array  $arguments The arguments passed to the method.
     * @return bool|array Returns the result of the dynamic method call.
     */
    public function __call(string $channel, array $arguments)
    {
        $type = 'normal';
        $matches = [];
        if (preg_match('/(raw|plain)_(.+)/', $channel, $matches)) {
            $type = $matches[1];
            $channel = $matches[2];
        }
        return match ($type) {
            'raw'   => $this->raw($channel, $arguments[0], $arguments[1] ?? ReporterFormatType::HTML->value),
            'plain' => $this->plain($channel, $arguments[0], $arguments[1] ?? ''),
            default => $this->report($channel, $arguments)
        };
    }

    /**
     * Sends a report to the specified channel using the specified type and arguments.
     * @param string $channel   The channel to send the report to.
     * @param array  $arguments The arguments for the report.
     * @return bool|array Returns an array if multiple reports were sent, otherwise returns a boolean indicating success.
     */
    public function report(string $channel, array $arguments): bool|array
    {
        if (!$content = $this->htmlFormatting($arguments))
            return FALSE;
        $result = [];
        foreach ($content as $i => $item)
            $result[$i] = WsLibProvider::sendTelegram($this->reporterChannel[$channel] ?? $channel, $item, ReporterFormatType::HTML->value);
        if (count($result) > 1)
            return $result;
        else
            return reset($result);
    }

    /**
     * Formats the message content and returns it as an array.
     * @param array $arguments  The arguments for formatting the message content.
     *                          - $arguments[0]: The title of the message.
     *                          - $arguments[1]: The content of the message.
     *                          - $arguments[2]: Optional parameters for the message.
     * @return array The formatted message content as an array.
     */
    protected function htmlFormatting(array $arguments): array
    {
        if (is_array($arguments[0]))
            $arguments[0] = implode(',', $arguments[0]);
        $arguments['title'] = $arguments[0];
        unset($arguments[0]);

        if (isset($arguments[1])) {
            $arguments['content'] = $arguments[1];
            unset($arguments[1]);
        }
        if (isset($arguments[2])) {
            $arguments['params'] = $arguments[2];
            unset($arguments[2]);
        }

        $arguments['content'] = $arguments['content'] ?? '';
        $current = $header = '';
        $headers = [];
        if (isset($arguments['params'])) {
            if (is_array($arguments['params']))
                $headers = $arguments['params'];
        }
        if (!empty($arguments['content']) && (is_array($arguments['content']) || is_object($arguments['content'])))
            $content = $this->json($arguments['content']);
        else
            $content = (string)$arguments['content'];
        if ((APP_NAME ?? ($_SERVER['HTTP_HOST'] ?? '')) !== '')
            $headers['Project'] = APP_NAME ?? ($_SERVER['HTTP_HOST'] ?? '');

        $request = is_cli() ? Services::clirequest() : Services::request();
        $method = strtoupper($request->getMethod());
        $headers += [
            'Environment'       => ($_ENV['ROLES'] ?? '') . (!empty($_ENV['ROLES']) ? '-' : '') . ENVIRONMENT,
            'Request IP'        => WsCfgServices::getClientIp(),
            'Server IP'         => gethostname() ?: ($_SERVER['SERVER_ADDR'] ?? '-'),
            'Request'           => "$method " . WsCfgServices::getRoutePath(),
            'Request Time'      => $this->getDateTime(defined('REQUEST_TIME') ? REQUEST_TIME : NULL),
            'Request Parameter' => method_exists($request, 'getVar') ? $request->getVar() : [],
        ];
        if (!empty($headers)) {
            $header = (!empty($arguments['title'])
                ? "<b>Title</b> : <blockquote><code>{$this->escapeContent(substr(ucwords($arguments['title']), 0, 100))}</code></blockquote>" .
                PHP_EOL . PHP_EOL : "");
            foreach ($headers as $name => $value) {
                $key = $this->escapeContent(ucwords($name));
                if (is_array($value) || is_object($value))
                    $header .= "- <i>$key</i> : <blockquote expandable>{$this->json($value)}</blockquote>" . PHP_EOL;
                else
                    $header .= "- <i>$key</i> : <code>{$this->escapeContent((string)$value)}</code>" . PHP_EOL;
            }
        }
        $body = [];
        if (!$this->validateTelegramHTML($content))
            $content = $this->escapeContent($content);
        if (!empty($content)) {
            $header .= PHP_EOL . '<b>Content</b> :';
            $data = explode(PHP_EOL, $content);
            foreach ($data as $item) {
                if (strlen($header) + strlen($current) + strlen($item) > 4050) {
                    $body[] = (!empty($header) ? $header . PHP_EOL : "") . "<blockquote expandable>$current</blockquote>";
                    $header = '';
                    $current = PHP_EOL;
                }
                $current .= $item . PHP_EOL;
            }
            if (!empty($current))
                $body[] = (!empty($header) ? $header . PHP_EOL : "") . "<blockquote expandable>$current</blockquote>";
        }
        return $body;
    }

    /**
     * Get the date and time from a timestamp.
     * @param int|null $timestamp The timestamp to convert. If not provided, the current timestamp will be used.
     * @return bool|string Returns the formatted date and time as a string. If an error occurs, it returns false.
     */
    protected function getDateTime(int $timestamp = NULL): bool|string
    {
        $timestamp = $timestamp ?? \time();
        try {
            return Time::createFromTimestamp($timestamp, config('App')->{'appTimezone'})->toDateTimeString();
        } catch (Throwable) {
            return date("Y-m-d H:i:s", $timestamp);
        }
    }

    /**
     * Converts data to a JSON string.
     * @param mixed $data   The data to be converted to JSON string.
     * @param int   $indent The indentation level for formatting the JSON string. Default is 0.
     * @return string The JSON string representation of the data.
     */
    public function json(mixed $data, int $indent = 0): string
    {
        $content = '';
        $indentSize = 4;
        if ($data instanceof stdClass)
            $data = (array)$data;
        if (is_iterable($data)) {
            $isObject = is_object($data) || (is_array($data) && !array_is_list($data));
            $content .= $isObject ? '{' : '[';
            $array = [];
            foreach ($data as $key => $value)
                $array[] .= str_repeat(' ', ($indent + 1) * $indentSize) .
                    ($isObject ? "\"$key\": " : "") .
                    $this->json($value, $indent + 1) .
                    ',';
            if (!empty($array))
                $content .= PHP_EOL . implode(PHP_EOL, $array) . PHP_EOL;
            $content .= str_repeat(' ', ($indent) * $indentSize) . ($isObject ? '}' : ']');
        }
        elseif (is_object($data))
            $content .= "<code>object[" . $this->escapeContent(get_class($data)) . "]</code>";
        elseif (is_string($data))
            $content .= '"<code>' . $this->escapeContent($data) . '</code>"';
        elseif (is_bool($data))
            $content .= $data ? '<code>true</code>' : '<code>false</code>';
        elseif (is_null($data))
            $content .= '<code>null</code>';
        elseif (is_resource($data))
            $content .= "<code>resource(" . $this->escapeContent(get_resource_type($data)) . ")</code>";
        elseif (is_callable($data))
            $content .= '<code>callable</code>';
        else
            $content .= "<code>$data</code>";
        return $content;
    }

    /**
     * Escapes HTML special characters in a given string.
     * @param string $data The string to escape.
     * @return string The escaped string.
     */
    protected function escapeContent(string $data): string
    {
        return str_replace(['<', '>'], ['&lt;', '&gt;'], $data);
    }

    /**
     * Sends plain text content to the specified Telegram channel.
     * @param string $channel The channel to send the content to.
     * @param string $content The content to send.
     * @param string $title   The optional title for the content.
     * @return bool|array     Returns true on success, or an array with error details on failure.
     */
    public function plain(string $channel, string $content, string $title = ''): bool|array
    {
        $content = (!empty($title) ? "$title\n\n" : '') . $content;
        return WsLibProvider::sendTelegram($this->reporterChannel[$channel] ?? $channel, $content, ReporterFormatType::Markdown->value);
    }

    /**
     * @param string $html
     * @return bool
     */
    private function validateTelegramHTML(string $html): bool
    {
        $oldLibXmlError = libxml_use_internal_errors(TRUE);
        $allowedTags = ['b', 'strong', 'i', 'em', 'u', 'ins', 's', 'strike', 'del', 'a', 'code', 'pre', 'p'];

        $doc = new DOMDocument();
        $doc->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $errors = libxml_get_errors();
        libxml_clear_errors();
        libxml_use_internal_errors($oldLibXmlError);

        if (!empty($errors))
            return FALSE;

        $allTags = $doc->getElementsByTagName('*');
        foreach ($allTags as $tag) {
            if (!in_array($tag->nodeName, $allowedTags, TRUE)) {
                return FALSE;
            }
            if ($tag->nodeName === 'a') {
                $href = $tag->getAttribute('href');
                if (empty($href))
                    return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * Sends plain text content to the specified Telegram channel.
     * @param string             $channel The channel to send the content to.
     * @param string             $content The content to send.
     * @param ReporterFormatType $format
     * @return bool|array     Returns true on success, or an array with error details on failure.
     */
    public function raw(string $channel, string $content, ReporterFormatType $format = ReporterFormatType::Markdown): bool|array
    {
        return WsLibProvider::sendTelegram($this->reporterChannel[$channel] ?? $channel, $content, $format->value);
    }
}
