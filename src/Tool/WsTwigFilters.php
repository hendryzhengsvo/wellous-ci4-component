<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use JetBrains\PhpStorm\Internal\TentativeType;
use ReflectionClass;
use ReflectionMethod;
use Twig\Environment as TwigEnvironment;
use Twig\TwigFilter;

class WsTwigFilters
{
	/**
	 * @var TwigEnvironment
	 */
	private TwigEnvironment $twig;
	/**
	 * @var array
	 */
	private array $methods = [];

	/**
	 * Constructor method for the class.
	 * @param TwigEnvironment $twig The Twig environment instance to be used for rendering templates.
	 */
	public function __construct(TwigEnvironment $twig)
	{
		$this->twig = $twig;
		$reflection = new ReflectionClass(__CLASS__);
		/**
		 * @var TentativeType $method
		 */
		foreach ($reflection->getMethods(ReflectionMethod::IS_STATIC) as $method)
			$this->methods[] = $method->name;
	}

	/**
	 * Invokes the object as a function.
	 * Adds the specified methods as filters to the Twig environment and returns the updated Twig environment.
	 * @return TwigEnvironment The updated Twig environment after adding the specified filters.
	 */
	public function __invoke(): TwigEnvironment
	{
		foreach ($this->methods as $method) {
			$filter = new TwigFilter($method, WsTwigFilters::class . '::' . $method);
			$this->twig->addFilter($filter);
		}
		return $this->twig;
	}

	/**
	 * Translates a given content to the specified locale using internationalization.
	 * @param string      $content The content to be translated.
	 * @param array       $args    An array of arguments to be used in the translation.
	 * @param string|null $locale  (Optional) The locale to be used for the translation. If not specified, the default locale will be used.
	 * @return array|string The translated content. If multiple translations are available, an array of translations will be returned. If no translation is available, the original content
	 *                             will be returned.
	 */
	public static function t(string $content, array $args = [], ?string $locale = NULL): array|string
	{
		return lang($content, $args, $locale);
	}

	/**
	 * Translates a given content to the specified locale using internationalization.
	 * @param string      $content The content to be translated.
	 * @param array       $args    An array of arguments to be used in the translation.
	 * @param string|null $locale  (Optional) The locale to be used for the translation. If not specified, the default locale will be used.
	 * @return array|string The translated content. If multiple translations are available, an array of translations will be returned. If no translation is available, the original content
	 *                             will be returned.
	 */
	public static function i18n(string $content, array $args = [], ?string $locale = NULL): array|string
	{
		return lang($content, $args, $locale);
	}
}