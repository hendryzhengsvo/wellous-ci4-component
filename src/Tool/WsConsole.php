<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use CodeIgniter\CLI\CLI;
use CodeIgniter\CLI\Console;
use CodeIgniter\CodeIgniter;
use CodeIgniter\Events\Events as CIEvents;
use CodeIgniter\Filters\Filters;
use Config\Filters as ConfigFilters;
use Config\Services;
use Throwable;
use Wellous\Ci4Component\Filters\WsFilterProfiler;
use Wellous\Ci4Component\Filters\WsFilterTrans;

/*
 * --------------------------------------------------------------------
 * CodeIgniter command-line tools (spark)
 * --------------------------------------------------------------------
 * Spark is a CLI tool for CodeIgniter 4. It allows basic scaffolding
 * of files, classes, and more to make your development process easier.
 * It's inspired by the `rails` command for Ruby on Rails.
 */

/**
 * Class WsConsole
 * This class represents a console application in CodeIgniter 4. It handles the execution of commands,
 * loading of filters, and triggering of events.
 */
class WsConsole
{
	private array   $allowFilters = [WsFilterTrans::class, WsFilterProfiler::class];
	private Console $console;
	private Filters $filters;
	private string  $url;

	/**
	 * Initializes the command handler by setting up error reporting, time limit, and command line parameters.
	 * @return void
	 */
	public function __construct()
	{
		// We want errors to be shown when using it from the CLI.
		error_reporting(-1);
		ini_set('display_errors', '1');

		// We allow command line run long time
		set_time_limit(0);
		ignore_user_abort(TRUE);

		// Grab our CLI instance and url segment
		$this->url = CLI::getSegments()[0] ?? 'list';

		// Create console
		$this->console = new Console();

		//Handle Private function
		$this->handleConsoleFeature();

		//Load filters
		$this->loadFilters();
	}

	/**
	 * Handle console feature, if url is list, help, or version then execute it, else continue
	 * @return void
	 */
	private function handleConsoleFeature(): void
	{
		$this->tryExecute(function () {
			if (in_array($this->url, ['list', 'help', 'version'])) {
				switch ($this->url) {
					case 'version':
						WsStore::$cli->inline(sprintf('<green>CodeIgniter <cyan>4</cyan> CLI Tool, version <yellow>%s</yellow></green>', CodeIgniter::CI_VERSION));
						break;
					default:
						$this->console->run();
						break;
				}
				exit;
			}
		});
	}

	/**
	 * Load filters from config, and create filters, only allow filters from $allowFilters
	 * @return void
	 */
	private function loadFilters(): void
	{
		/**
		 * @var ConfigFilters $configFilter
		 */
		$configFilter = config('Filters');
		$configFilter->methods = $configFilter->filters = $alias = [];
		$configFilter->globals = ['before' => [], 'after' => []];
		foreach ($this->allowFilters as $allowFilter)
			foreach ($configFilter->aliases as $name => $currentFilter)
				if (is_subclass_of($currentFilter, $allowFilter)) {
					$alias[$name] = $currentFilter;
					break;
				}
		$configFilter->aliases = $alias;
		$configFilter->globals['before'] = $configFilter->globals['after'] = array_keys($configFilter->aliases);

		// Create filters
		$this->filters = Services::filters($configFilter);
	}

	/**
	 * Runs the command by executing filters, pre-system events, the main command handling logic, post-system events, and displays the output.
	 * @return mixed Returns the exit status of the command.
	 */
	public function run(): mixed
	{
		// Execute pre-system events
		CIEvents::trigger('pre_system');

		// Start handle command
		$exit = $this->handleCommand();

		// Execute post-system events
		CIEvents::trigger('post_system');

		return $exit;
	}

	/**
	 * Handles the command by executing filters, pre-controller events, and launching the console.
	 * @return mixed Returns the exit status of the console.
	 */
	public function handleCommand(): mixed
	{
		/**
		 * Execute filter -> before
		 */
		if ($this->tryExecute([$this->filters, 'run'], [$this->url, 'before']) === EXIT_ERROR)
			return EXIT_ERROR;

		// Execute pre-controller events
		CIEvents::trigger('pre_controller');

		/**
		 * --------------------------------------------------------------------
		 * Launch the console
		 * --------------------------------------------------------------------
		 */
		$exit = $this->tryExecute([$this->console, 'run']);

		// Execute post-controller events
		CIEvents::trigger('post_controller_constructor');

		/**
		 * Execute filter -> after
		 */
		if ($this->tryExecute([$this->filters, 'run'], [$this->url, 'after']) === EXIT_ERROR)
			return EXIT_ERROR;

		return $exit;
	}

	/**
	 * Tries to execute a callable function with optional parameters and handles any exceptions that occur.
	 * @param callable $function The function to execute.
	 * @param array    $params   The optional parameters to pass to the function.
	 * @return mixed Returns the result of the function execution or EXIT_ERROR if an exception occurred.
	 */
	private function tryExecute(callable $function, array $params = []): mixed
	{
		try {
			return call_user_func_array($function, $params);
		} catch (Throwable $ex) {
			WsExceptionHandle::handleReport($ex);
			WsStore::$cli->exception($ex);
			return EXIT_ERROR;
		}
	}
}