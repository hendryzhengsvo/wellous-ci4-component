<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use CodeIgniter\Debug\ExceptionHandlerInterface;
use CodeIgniter\Events\Events;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;
use ErrorException;
use JetBrains\PhpStorm\NoReturn;
use ReflectionClass;
use Throwable;
use Wellous\Ci4Component\Exceptions\WsExcepCtrl;
use Wellous\Ci4Component\Interface\WsInterfaceProfiler;
use Wellous\Ci4Component\Libraries\WsLibCli;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Modules\WsModel;

/**
 * Class WsExceptionHandle
 * This class handles exceptions and shutdown errors in the application.
 * @package App\Exceptions
 */
class WsExceptionHandle implements ExceptionHandlerInterface
{
	/**
	 * Get the stack trace as a string for the given exception and all its previous exceptions.
	 * @param Throwable $exception The exception to retrieve the stack trace from.
	 * @return string The stack trace as a string.
	 */
	public static function getAllTraceString(Throwable $exception): string
	{
		$content = $exception->getTraceAsString();
		$prevExcetion = $exception->getPrevious();
		if ($prevExcetion)
			$content .= "\nPreviosuly Excetion: " . get_class($prevExcetion) . "\n" . static::getAllTraceString($prevExcetion);
		return $content;
	}

	/**
	 * Handle the report of a throwable exception.
	 * @param Throwable $exception The throwable exception to handle.
	 * @return array Returns an array containing the profiler, the exception message, and the exception trace.
	 */
	public static function handleReport(Throwable $exception): array
	{
		WsModel::rollbackTrans();

		$profiler = method_exists(Services::class, 'profiler') ? Services::profiler() : new WsLibProfiler();

		$reportedFile = str_replace(ROOTPATH, "(repo)", $exception->getFile()) . ":{$exception->getLine()}";
		$trace = explode("\n", str_replace(ROOTPATH, "(repo)", static::getAllTraceString($exception)));
		foreach ($trace as $index => $item)
			if (str_contains($item, "/vendor/") || str_contains($item, "\\vendor\\"))
				unset($trace[$index]);
		$sendReport = !($exception instanceof WsExcepCtrl) || $exception->needLog();
		$exMsg = str_replace(["\r\n", "\r", "\n"], " ", "{$exception->getMessage()} on $reportedFile");

		$response = Services::response();
		Events::trigger('exception', $exception, $response);

		if ($sendReport) {
			Events::trigger('exception_report', $exception, $trace, $exMsg);
			try {
				$reflect = (new ReflectionClass($exception))->getShortName();
			} catch (Throwable) {
				$reflect = '(error)';
			}
			log_message('error', json_encode([
				'status'    => $exception instanceof ErrorException ? 'error' : 'exception',
				'type'      => $reflect,
				'app'       => APP_NAME ?? $_SERVER['HTTP_HOST'] ?? '',
				'domain'    => $_SERVER['HTTP_HOST'] ?? ROOTPATH ?? '',
				'exception' => [
					'message' => $exMsg,
					'trace'   => $trace,
				],
				'profiler'  => $profiler::fetch(),
			], JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
		}

		$profiler::addLog('exception', $exMsg);
		return [
			'profiler' => $profiler,
			'exMsg'    => $exMsg,
			'trace'    => $trace,
		];
	}

	/**
	 * Handles an exception.
	 * @param Throwable         $exception  The exception to handle.
	 * @param RequestInterface  $request    The request object.
	 * @param ResponseInterface $response   The response object.
	 * @param int               $statusCode The HTTP status code.
	 * @param int               $exitCode   The exit code.
	 * @return never
	 */
	#[NoReturn]
	public function handle(Throwable $exception, RequestInterface $request, ResponseInterface $response, int $statusCode, int $exitCode): never
	{
		['profiler' => $profiler, 'exMsg' => $exMsg, 'trace' => $trace] =
			static::handleReport($exception);

		if (is_cli())
			$this->cliRender($profiler, $exception);
		else
			$this->webRender($profiler, $response, $exception, $statusCode, $trace, $exMsg);
		if (!WsHandleEvents::getTriggerAppEnd())
			Events::trigger('appEnd');
		exit($exitCode);
	}

	/**
	 * Renders the exception message for CLI environment.
	 * @param WsInterfaceProfiler $profilerClass The profiler class.
	 * @param Throwable           $ex            The exception to be rendered.
	 * @return void
	 */
	private function cliRender(WsInterfaceProfiler $profilerClass, Throwable $ex): void
	{
		$cli = method_exists(Services::class, 'cli') ? Services::cli() : new WsLibCli();
		$cli->exception($ex);
		if ($profilerClass::isEnable()) {
			$cli->border('-')
			    ->inline("<bold><yellow>Profiler</yellow></bold> -> ")
			    ->out($profilerClass::fetch());
		}
		$cli->border('-')
		    ->inline(sprintf("<cyan>Ended at <bold><light_yellow>%s</light_yellow></bold></cyan>\n", date('Y-m-d H:i:s')));
	}

	/**
	 * Renders the web response for a given exception.
	 * @param WsInterfaceProfiler $profilerClass An instance of the profiler class.
	 * @param ResponseInterface   $response      The response object.
	 * @param Throwable           $exception     The thrown exception.
	 * @param int                 $statusCode    The status code for the response.
	 * @param array               $trace         The error trace.
	 * @param string              $excepMsg      The error message.
	 * @return void
	 */
	private function webRender(WsInterfaceProfiler $profilerClass, ResponseInterface $response, Throwable $exception, int $statusCode, array $trace,
	                           string              $excepMsg): void
	{
		$message = $exception instanceof WsExcepCtrl ? $exception->getStatusText() : '';
		if (empty($message))
			$message = 'Sorry, but we have encountered an issue, please try again later.';

		$responseData = array_merge([
			'status'  => $exception instanceof WsExcepCtrl ? $exception->getStatusName() : 'error',
			'error'   => $exception instanceof WsExcepCtrl ? $exception->getErrorCode() : 'unknown',
			'message' => $message,
		], ($exception instanceof WsExcepCtrl ? $exception->getExtraData() : []));
		if ($profilerClass::isEnable() && !isset($responseData['__profiler__'])) {
			$profiler = array_merge([
				'exception' => [
					'message' => $excepMsg,
					'trace'   => $trace,
				],
			], $profilerClass::fetch());
			$responseData['__profiler__'] = !empty($data['__profiler__']) ? [$data['__profiler__'], $profiler] : $profiler;
		}
		$response->setJSON(json_encode($responseData, JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
		$response->setStatusCode($exception instanceof WsExcepCtrl ? ($exception->getCode() ?: $statusCode)
			: $statusCode, str_replace(["\r\n", "\r", "\n"], " ", $exception->getMessage()));
		$response->send();
	}
}