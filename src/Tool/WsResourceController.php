<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\RESTful\ResourceController;
use JetBrains\PhpStorm\NoReturn;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;
use Psr\Log\LoggerInterface;
use Throwable;
use Wellous\Ci4Component\Config\WsCfgServices;
use Wellous\Ci4Component\Exceptions\ClientPageNotFound;
use Wellous\Ci4Component\Exceptions\ServerInternalError;
use Wellous\Ci4Component\Exceptions\ServerUnavailable;
use Wellous\Ci4Component\Exceptions\WsExcepCtrl;
use Wellous\Ci4Component\Libraries\WsLibChunkedResponse;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Libraries\WsLibRpc;
use Wellous\Ci4Component\Libraries\WsLibUtilities;

/**
 * Class BaseController
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 * For security be sure to declare any new methods as protected or private.
 */
class WsResourceController extends ResourceController
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 * @var array
	 */
	protected $helpers = [];

	const BufferSize = 1024 * 8;
	/**
	 * @var int $pageSize page size in pagination
	 */
	protected int $pageSize = 10;
	/**
	 * @var int $page current page in pagination
	 */
	protected int $page = 1;

	/**
	 * Initializes the controller by setting the request, response, and logger objects,
	 * and optionally setting the page size and current page number.
	 * @param RequestInterface  $request  The request object.
	 * @param ResponseInterface $response The response object.
	 * @param LoggerInterface   $logger   The logger object.
	 * @return void
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger): void
	{
		parent::initController($request, $response, $logger);
		if (!is_cli()) {
			$pageSize = (int)($this->request->getVar('page_size') ?? 10);
			if (!empty($pageSize) && $pageSize > 0)
				$this->pageSize = $pageSize;
			$page = (int)($this->request->getVar('page') ?? 1);
			if (!empty($page) && $page > 0)
				$this->page = $page;
			if ($this->page < 1)
				$this->page = 1;
		}
	}

    /**
     * Creates a chunked response with the specified chunk size and transfer encoding.
     * @param int  $chunkSize               The size of each chunk in bytes (default is 4096).
     * @return WsLibChunkedResponse         A chunked response with the specified chunk size and transfer encoding.
     */
    public function chunkedResponse(int $chunkSize = 4096): WsLibChunkedResponse
    {
        return new WsLibChunkedResponse($chunkSize);
    }

	/**
	 * Calculates pagination information based on the total number of records.
	 * @param int $totalRecord The total number of records.
	 * @return array An array containing the pagination information:
	 *                         - 'record_total': The total number of records.
	 *                         - 'page_size': The number of records per page.
	 *                         - 'page_total': The total number of pages.
	 *                         - 'page': The current page.
	 *                         - 'limit': The limit of records to fetch per page.
	 *                         - 'Offset': The offset to be used in fetching records for the current page.
	 */
	public function pagination(int $totalRecord): array
	{
		$pagination = [
			'record_total' => $totalRecord,
			'page_size'    => $this->pageSize,
			'page_total'   => (int)floor($totalRecord / $this->pageSize) + (($totalRecord % $this->pageSize === 0 ? 0 : 1)),
		];
		if ($this->page > $pagination['page_total'])
			$this->page = $pagination['page_total'];
		return $pagination + [
				'page'   => $this->page,
				'limit'  => $this->pageSize,
				'offset' => ($this->page - 1) * $this->pageSize,
			];
	}

	/**
	 * Sends a response with an exception.
	 * @param Throwable $ex        The exception to be included in the response.
	 * @param int       $statuCode The HTTP status code to be included in the response (default is 500).
	 * @return ResponseInterface The response with the provided exception and status code.
	 */
	public function exception(Throwable $ex, int $statuCode = 500): ResponseInterface
	{
		if ($ex instanceof WsExcepCtrl)
			return $this->respond([
				'status'  => 'error',
				'error'   => $ex->getErrorCode(),
				'message' => $ex->getMessage(),
				'data'    => $ex->getStatusText(),
			], $statuCode);
		else
			return $this->respond([
				'status'  => 'error',
				'error'   => $ex->getCode(),
				'message' => $ex->getMessage(),
				'data'    => $ex->getMessage(),
			], 200);
	}

	/**
	 * Download a file as a response.
	 * @param mixed       $data       The file data to be downloaded.
	 * @param string|null $fileName   The name to be given to the downloaded file (optional).
	 * @param string|null $mimeType   The MIME type of the file (optional).
	 * @param bool        $attachment Whether the file should be downloaded as an attachment (default is true).
	 * @param int|null    $status     The HTTP status code to be included in the response (optional).
	 * @param string      $statusText The status text to be included in the response (optional).
	 * @return ResponseInterface           The response containing the file download.
	 */
	public function downloadFile(mixed  $data, ?string $fileName = '', ?string $mimeType = '', bool $attachment = TRUE, ?int $status = NULL,
	                             string $statusText = ''): ResponseInterface
	{
		$isRemote = FALSE;
		$response = NULL;
		$isString = is_string($data);
		$isResource = is_resource($data);
		if (!$isString && !($isResource && get_resource_type($data) === 'stream'))
			return $this->response->setStatusCode(ResponseInterface::HTTP_BAD_REQUEST, 'Invalid data type');

		WsLibProfiler::addCheckPoint('downloadFile');
		$size = 0;
		if ($isString) {

			//Guess content type from file (if applicable)
			$isFile = is_file($data);
			if (empty($mimeType) && $isFile)
				$mimeType = WsLibUtilities::guessMimeTypeByFile($data);

			//Open file as stream
			if ($isFile) {
				if (empty($fileName))
					$fileName = basename($data);
				$data = fopen($data, 'rb');
				$isString = FALSE;
				$isResource = TRUE;
			}

			if (is_string($data) && (str_starts_with($data, 'http://') || str_starts_with($data, 'https://'))) {
				try {
					$response = WsLibRpc::client()->get($data, ['stream' => TRUE]);
					if ($response->getStatusCode() >= ResponseInterface::HTTP_OK && $response->getStatusCode() < ResponseInterface::HTTP_MULTIPLE_CHOICES) {
						$isRemote = TRUE;
						if ($response->hasHeader('Content-Length'))
							$size = (int)$response->getHeaderLine('Content-Length');
						if (empty($mimeType) && $response->hasHeader('Content-Type'))
							$mimeType = $response->getHeaderLine('Content-Type');
						if (empty($fileName) && $response->hasHeader('Content-Disposition')) {
							$contentDisposition = $response->getHeaderLine('Content-Disposition');
							if (preg_match('/filename="(.*)"/', $contentDisposition, $matches))
								$fileName = $matches[1];
						}
					}
				} catch (Throwable $e) {
					WsLibProfiler::addLog('downloadFile', "Failed to get remote file: $data, Error: {$e->getMessage()}");
				}
			}

			//Guess content type from buffer
			if (empty($mimeType))
				$mimeType = WsLibUtilities::guessMimeTypeByContent($data);
		}

		//Intialize size and header
		if (!$isRemote && empty($size))
			$size = $isString ? strlen($data) : fstat($data)['size'];

		$start = 0;
		$end = $size - 1;
		$bufferSize = static::BufferSize;
		$headers = [
			'Accept-Ranges'       => 'bytes',
			'Content-Type'        => $mimeType ?: 'application/octet-stream',
			'Content-Disposition' => ($attachment ? 'attachment' : 'inline') . "; filename=\"$fileName\"",
		];
		if (!empty($size))
			$headers['Content-Length'] = $end - $start + 1;
		//Partial content
		if (!empty($size) && $this->request->hasHeader('Range')) {
			$range = $this->request->getHeaderLine('Range');
			$range = substr($range, strlen('bytes='));
			[$start, $end] = explode('-', $range);

			$start = $start ? (int)$start : 0;
			$end = $end ? (int)$end : $size - 1;
			$end = min($end, $size - 1);

			if ($start > $end) {
				WsLibProfiler::addLog('downloadFile', 'Range not satisfiable');
				return $this->response->setStatusCode(ResponseInterface::HTTP_RANGE_NOT_SATISFIABLE)
				                      ->setHeader('Content-Range', "bytes */$size");
			}

			$status = ResponseInterface::HTTP_PARTIAL_CONTENT;
			$statusText = $statusText ?: 'Partial Content';
			$headers['Content-Range'] = "bytes $start-$end/$size";
		}

		//Set header & response body
		$this->response->setStatusCode($status ?? ResponseInterface::HTTP_OK, $statusText);
		foreach ($headers as $key => $value)
			$this->response->setHeader($key, $value);

		//Respone body is empty for HEAD request
		if ($this->request->getMethod() === 'head')
			return $this->response->setBody('');

		//Send body response
		if (is_resource($data)) {
			$this->response->send();
			fseek($data, $start);
			while (!feof($data) && ($pos = ftell($data)) <= $end) {
				if ($pos + $bufferSize > $end)
					$bufferSize = $end - $pos + 1;
				echo fread($data, $bufferSize);
				flush();
			}
			if ($isResource)
				fclose($data);
			else
				rewind($data);
			$this->response->setBody('');
		}
		elseif ($isRemote && $response instanceof PsrResponseInterface) {
			try {
				$stream = $response->getBody();
				$this->response->send();
				while (!$stream->eof()) {
					echo $stream->read(static::BufferSize);
					flush();
				}
				$this->response->setBody('');
			} catch (throwable) {
				WsLibProfiler::addLog('downloadFile', 'Faild download remote file');
				return $this->response->setBody(substr($data, $start, $end - $start + 1));
			}
		}
		else
			$this->response->setBody(substr($data, $start, $end - $start + 1));

		//Complete
		WsLibProfiler::addLog('downloadFile', 'Complete procced download');
		return $this->response;
	}

	/**
	 * Sends a file as a response.
	 * @param array|string $data       The file data or file path to be included in the response.
	 * @param string       $fileName   The name of the file to be sent.
	 * @param string       $mimeType   The MIME type of the file (default is 'application/octet-stream').
	 * @param int|null     $status     The HTTP status code to be included in the response (optional).
	 * @param string       $statusText The HTTP status text to be included in the response (optional).
	 * @return ResponseInterface The response with the provided file, MIME type, status, and status text.
	 * @throws ServerUnavailable if $data is an array with a 'message' key.
	 */
	public function sendFile(array|string $data, string $fileName, string $mimeType = 'application/octet-stream', ?int $status = NULL,
	                         string       $statusText = ''): ResponseInterface
	{
		if (is_array($data))
			throw new ServerUnavailable($data['message'] ?? 'Service Unavailable');
		else
			return parent::respond([
				'status' => 'file',
				'file'   => $fileName,
				'mime'   => $mimeType,
				'data'   => base64_encode($data),
			], $status, $statusText);
	}

	/**
	 * Sends a response with a status of "ok".
	 * @param mixed        $data   The data to be included in the response.
	 * @param string|array $route  The route to be included in the response (optional).
	 * @param int          $status The HTTP status code to be included in the response (default is 200).
	 * @return ResponseInterface The response with the provided data, route and status.
	 */
	public function sendDone(mixed $data = NULL, string|array $route = '', int $status = 200): ResponseInterface
	{
		$response = [
			'status' => "ok",
			'data'   => $data,
		];
		if ($route)
			$response['route'] = $route;
		return $this->respond($response, $status, 'ok');
	}

	/**
	 * Sends a response with a status of "success".
	 * @param string       $message The message to be included in the response (required).
	 * @param string       $title   The title to be included in the response (optional).
	 * @param mixed        $data    The data to be included in the response (optional).
	 * @param string|array $route   The route to be included in the response (optional).
	 * @param int          $status  The HTTP status code to be included in the response (default is 200).
	 * @return ResponseInterface The response with the provided message, title, data, route and status.
	 * @throws ServerInternalError If the message is empty.
	 */
	public function sendSuccess(string $message, string $title = '', mixed $data = NULL, string|array $route = '',
	                            int    $status = 200): ResponseInterface
	{
		if (!$message)
			throw new ServerInternalError('send message cant be empty');

		$response = [
			'status'  => 'success',
			'message' => lang($message),
		];
		if (!empty($title))
			$response['title'] = $title;
		if ($route)
			$response['route'] = $route;
		if (!is_null($data))
			$response['data'] = $data;
		return $this->respond($response, $status, $message);
	}

	/**
	 * Sends a response with a status of "warning".
	 * @param string       $message The warning message.
	 * @param string       $title   The title of the warning (optional).
	 * @param mixed        $data    The data to be included in the response (optional).
	 * @param string|array $route   The route to be included in the response (optional).
	 * @param int          $status  The HTTP status code to be included in the response (default is 200).
	 * @return ResponseInterface The response with the provided message, title, data, route and status.
	 * @throws ServerInternalError If the message parameter is empty.
	 */
	public function sendWarning(string $message, string $title = '', mixed $data = NULL, string|array $route = '',
	                            int    $status = 200): ResponseInterface
	{
		if (!$message)
			throw new ServerInternalError('send message cant be empty');

		$response = [
			'status'  => 'warning',
			'message' => lang($message),
		];
		if (!empty($title))
			$response['title'] = $title;
		if ($route)
			$response['route'] = $route;
		if (!is_null($data))
			$response['data'] = $data;
		return $this->respond($response, $status, $message);
	}

	/**
	 * Sends a response with a status of "error".
	 * @param string       $message The error message to be included in the response.
	 * @param string       $title   The title to be included in the response (optional).
	 * @param mixed        $data    The data to be included in the response (optional).
	 * @param string|array $route   The route to be included in the response (optional).
	 * @param int          $status  The HTTP status code to be included in the response (default is 200).
	 * @return ResponseInterface The response with the provided error message, title, data, route and status.
	 * @throws ServerInternalError If the message is empty.
	 */
	public function sendError(string $message, string $title = '', mixed $data = NULL, string|array $route = '', int $status = 200): ResponseInterface
	{
		if (!$message)
			throw new ServerInternalError('send message cant be empty');

		$response = [
			'status'  => 'error',
			'message' => lang($message),
		];
		if (!empty($title))
			$response['title'] = $title;
		if ($route)
			$response['route'] = $route;
		if (!is_null($data))
			$response['data'] = $data;
		return $this->respond($response, $status, $message);
	}

	/**
	 * Sends a response with a status of "info".
	 * @param string       $message The message to be included in the response.
	 * @param string       $title   The title to be included in the response (optional).
	 * @param mixed        $data    The data to be included in the response (optional).
	 * @param string|array $route   The route to be included in the response (optional).
	 * @param int          $status  The HTTP status code to be included in the response (default is 200).
	 * @return ResponseInterface The response with the provided message, title, data, route and status.
	 * @throws ServerInternalError if the provided message is empty.
	 */
	public function sendInfo(string $message, string $title = '', mixed $data = NULL, string|array $route = '', int $status = 200): ResponseInterface
	{
		if (!$message)
			throw new ServerInternalError('send message cant be empty');

		$response = [
			'status'  => 'info',
			'message' => lang($message),
		];
		if (!empty($title))
			$response['title'] = $title;
		if ($route)
			$response['route'] = $route;
		if (!is_null($data))
			$response['data'] = $data;
		return $this->respond($response, $status, $message);
	}

	/**
	 * Sends a response with a status of "info".
	 * @param string       $message The message to be included in the response.
	 * @param string       $title   The title to be included in the response (optional).
	 * @param mixed        $data    The data to be included in the response (optional).
	 * @param string|array $route   The route to be included in the response (optional).
	 * @param int          $status  The HTTP status code to be included in the response (default is 200).
	 * @return ResponseInterface The response with the provided message, title, data, route and status.
	 * @throws ServerInternalError if the provided message is empty.
	 */
	public function sendTwig(string $twig, array $data = []): ResponseInterface
	{
		return WsCfgServices::twigResponse($twig, $data);
	}

	/**
	 * Sets the response status code to 404 (Page Not Found) and sends the response.
	 * This method is intended to be used when a requested page is not found.
	 * Usage:
	 * ```
	 * notFound();
	 * ```
	 * @return void
	 * @throws ClientPageNotFound
	 */
	#[NoReturn]
	public function pageNotFound(): void
	{
		if (!is_cli()) {
			$this->response->setStatusCode(404, "Page Not Found");
			$this->response->send();
			exit;
		}
		else
			throw new ClientPageNotFound("Page Not Found");
	}
}
