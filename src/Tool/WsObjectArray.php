<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use ArrayAccess;
use Countable;
use RuntimeException;
use Wellous\Ci4Component\Traits\WsTraitArrayAccess;

/**
 * Class WsObjectArray
 * This class implements the ArrayAccess and Countable interfaces. It allows access to an array of data with the ability to get, set, check if a field is set, and unset values at specified
 * offsets. The class also provides a toArray method to convert the object to an array and a count method to get the count of elements in the data array.
 * @package YourPackage
 */
class WsObjectArray implements ArrayAccess, Countable
{
	use WsTraitArrayAccess;

	/**
	 * @var array Allow Field
	 */
	protected array $allowField = [];

	/**
	 * @var array Array Data
	 */
	protected array $data = [];

	/**
	 * @var bool throw error if not exist
	 */
	protected bool $throwErrorIfNotExist = FALSE;

	/**
	 * Constructor method for creating a new instance of the class.
	 * @param array $data                 Data to be assigned to the instance. Defaults to an empty array.
	 * @param bool  $throwErrorIfNotExist Flag to determine if an error should be thrown if the requested key does not exist. Defaults to false.
	 * @return void
	 */
	public function __construct(array $data = [], bool $throwErrorIfNotExist = FALSE)
	{
		$this->data = array_intersect_key($data, array_flip($this->allowField));
		$this->throwErrorIfNotExist = $throwErrorIfNotExist;
	}

	/**
	 * Gets the value of a property.
	 * @param int|string $offset The name or index of the property.
	 * @return mixed The value of the property.
	 * @throws RuntimeException If the field is not allowed (only if `throwErrorIfNotExist` is `true`).
	 */
	public function &__get(int|string $offset): mixed
	{
		if (in_array($offset, $this->allowField, TRUE))
			return $this->data[$offset];
		elseif ($this->throwErrorIfNotExist)
			throw new RuntimeException("Field $offset is not allowed");
		else {
			$null = NULL;
			return $null;
		}
	}

	/**
	 * Set the value at the specified offset.
	 * @param int|string $offset The offset to set the value at.
	 * @param mixed      $value  The value to set.
	 * @return void
	 * @throws RuntimeException When the specified offset is not allowed and throwErrorIfNotExist is enabled.
	 */
	public function __set(int|string $offset, mixed $value): void
	{
		if (in_array($offset, $this->allowField, TRUE))
			$this->data[$offset] = $value;
		elseif ($this->throwErrorIfNotExist)
			throw new RuntimeException("Field $offset is not allowed");
	}

	/**
	 * Checks if a field is set in the data array.
	 * @param int|string $offset The field to check.
	 * @return bool Returns true if the field is set, false otherwise.
	 */
	public function __isset(int|string $offset): bool
	{
		if (in_array($offset, $this->allowField, TRUE))
			return isset($this->data[$offset]);
		else
			return FALSE;
	}

	/**
	 * Unset the value at the specified offset.
	 * @param int|string $offset The offset to unset the value from.
	 * @return void
	 */
	public function __unset(int|string $offset): void
	{
		unset($this->data[$offset]);
	}

	/**
	 * Get the value at the specified offset.
	 * @param mixed $offset The offset to retrieve the value from.
	 * @return mixed The value at the specified offset.
	 */
	public function &offsetGet(mixed $offset): mixed
	{
		return $this->__get($offset);
	}

	/**
	 * Converts the object to an array.
	 * @return array The array representation of the object.
	 */
	public function toArray(): array
	{
		return $this->data;
	}

	/**
	 * Returns the count of elements in the data array.
	 * @return int The count of elements.
	 */
	public function count(): int
	{
		return count($this->data);
	}
}