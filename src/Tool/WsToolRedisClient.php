<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use Wellous\Ci4Component\Interface\WsInterfaceRedis;
use Wellous\Ci4Component\Libraries\WsLibRedis;

/**
 * Class WsToolRedisClient
 * @package Wellous\Ci4Component\Tool
 * This class extends the WsLibRedis class and implements the WsInterfaceRedis interface.
 * @see     WsLibRedis
 * @see     WsInterfaceRedis
 */
abstract class WsToolRedisClient extends WsLibRedis
{
}