<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Tool;

use CodeIgniter\Log\Handlers\BaseHandler;
use CodeIgniter\Log\Handlers\HandlerInterface;

/**
 * Class Reporter
 * @method devDeploy($type, $content, $params = [])
 * @method liveDeploy($type, $content, $params = [])
 * @method debug($type, $content, $params = [])
 * @method log($type, $content, $params = [])
 */
class WsToolLogger extends BaseHandler implements HandlerInterface
{
	protected array $syslogLevelMap = [
		'emergency' => LOG_EMERG,
		'alert'     => LOG_ALERT,
		'critical'  => LOG_CRIT,
		'error'     => LOG_ERR,
		'warning'   => LOG_WARNING,
		'notice'    => LOG_NOTICE,
		'info'      => LOG_INFO,
		'debug'     => LOG_DEBUG,
	];

	/**
	 * Class constructor.
	 * @param array $config The configuration for the class. (optional)
	 * @return void
	 */
	public function __construct(array $config = [])
	{
		parent::__construct($config);
	}

	/**
	 * Checks if the given level can be handled.
	 * @param mixed $level The level to check.
	 * @return bool Returns `true` if the level can be handled, otherwise `false`.
	 */
	public function canHandle($level): bool
	{
		return in_array($level, $this->handles, TRUE);
	}

	/**
	 * Handles logging the message.
	 * If the handler returns false, then execution of handlers
	 * will stop. Any handlers that have not run, yet, will not
	 * be run.
	 * @param string $level
	 * @param string $message
	 */
	public function handle($level, $message): bool
	{
		$message = preg_replace('/[\x00-\x1F\x7F]/', '', $message);
		syslog((int)$this->syslogLevelMap[$level], $message);
		return TRUE;
	}
}