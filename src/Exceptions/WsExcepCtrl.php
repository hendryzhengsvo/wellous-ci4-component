<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

use CodeIgniter\Exceptions\HasExitCodeInterface;
use Exception;
use Throwable;

/**
 * Class WsExcepCtrl
 */
class WsExcepCtrl extends Exception implements HasExitCodeInterface, Throwable
{
	protected bool   $needLog       = FALSE;
	protected string $error         = '';
	protected int    $status        = 500;
	protected string $statusText    = 'Exceptions.Internal Server Error';
	protected string $statusName    = 'error';
	protected array  $addOnResponse = [];
	protected int    $exitCode      = EXIT_ERROR;

	/**
	 * WsExcepCtrl constructor.
	 * @param string         $message
	 * @param string         $statusText
	 * @param array          $addOnResponse
	 * @param Throwable|null $previous
	 */
	public function __construct(string $message = "", string $statusText = '', array $addOnResponse = [], Throwable $previous = NULL)
	{
		if ($statusText)
			$this->statusText = $statusText;
		$this->statusText = lang($this->statusText);
		if (!$message)
			$message = $statusText;
		$this->addOnResponse = array_merge($this->addOnResponse, $addOnResponse);
		parent::__construct($message, $this->status, $previous);
	}

	/**
	 * Get the exit code.
	 * @return int The exit code.
	 */
	public function getExitCode(): int
	{
		return $this->exitCode;

	}

	/**
	 * Set the code for the object.
	 * @param int $code The code to set.
	 * @return void
	 */
	public function setCode(int $code): void
	{
		$this->code = $code;
	}

	/**
	 * Get the status name
	 * @return string The status name
	 */
	public function getStatusName(): string
	{
		return $this->statusName;
	}

	/**
	 * Set the status name for the object.
	 * @param string $statusName The status name to set.
	 * @return Throwable The updated object with the new status name.
	 */
	public function setStatusName(string $statusName): Throwable
	{
		if (!empty($statusName))
			$this->statusName = $statusName;
		return $this;
	}

	/**
	 * Get the extra data associated with the object.
	 * @return array The additional data associated with the object.
	 */
	public function getExtraData(): array
	{
		return $this->addOnResponse;
	}

	/**
	 * Check if logging is needed.
	 * @return bool Returns true if logging is needed, false otherwise.
	 */
	public function needLog(): bool
	{
		return $this->needLog;
	}

	/**
	 * Get the error code of the object.
	 * @return string The error code.
	 */
	public function getErrorCode(): string
	{
		return $this->error;
	}

	/**
	 * Get the status text for the object.
	 * @return string The status text.
	 */
	public function getStatusText(): string
	{
		return $this->statusText;
	}

	/**
	 * Set the location of the object.
	 * @param string $file The file location to set.
	 * @param int    $line The line number to set.
	 * @return void
	 */
	public function setLocation(string $file, int $line): void
	{
		$this->file = $file;
		$this->line = $line;
	}
}