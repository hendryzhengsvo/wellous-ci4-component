<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientRequestTimeout
 * @package App\Exceptions
 */
class ClientRequestTimeout extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 408;
	protected string $error      = 'request_timeout';
	protected string $statusText = 'Exceptions.Request Timeout';
}