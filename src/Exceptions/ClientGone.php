<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientGone
 * @package App\Exceptions
 */
class ClientGone extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 410;
	protected string $error      = 'Gone';
	protected string $statusText = 'Exceptions.Gone';
}