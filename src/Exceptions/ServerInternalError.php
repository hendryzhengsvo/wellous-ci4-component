<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ServerInternalError
 * @package App\Exceptions
 */
class ServerInternalError extends WsExcepCtrl
{
	protected bool   $needLog    = TRUE;
	protected int    $status     = 500;
	protected string $error      = 'general_error';
	protected string $statusText = 'Exceptions.General error';
}