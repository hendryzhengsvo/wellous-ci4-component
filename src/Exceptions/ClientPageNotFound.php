<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientPageNotFound
 * @package App\Exceptions
 */
class ClientPageNotFound extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 404;
	protected string $error      = 'not_found';
	protected string $statusText = 'Exceptions.Not found';
}