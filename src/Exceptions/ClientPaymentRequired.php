<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientPaymentRequired
 * @package App\Exceptions
 */
class ClientPaymentRequired extends WsExcepCtrl
{
	public bool   $needLog    = FALSE;
	protected int $status     = 402;
	public string $error      = 'payment_error';
	public string $statusText = 'Exceptions.Payment error';
}