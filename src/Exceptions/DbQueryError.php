<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

use Throwable;

class DbQueryError extends WsExcepCtrl
{
	protected bool   $needLog    = TRUE;
	protected string $error      = 'dbquery_error';
	protected int    $status     = 500;
	protected string $statusText = 'Exceptions.Internal Server Error';

	/**
	 * DbQueryError constructor.
	 * @param string         $message
	 * @param string         $statusText
	 * @param array          $addOnResponse
	 * @param Throwable|null $previous
	 */
	public function __construct(string $message = "", string $statusText = '', array $addOnResponse = [], Throwable $previous = NULL)
	{
		parent::__construct($message, $statusText, $addOnResponse, $previous);
		return $this;
	}
}