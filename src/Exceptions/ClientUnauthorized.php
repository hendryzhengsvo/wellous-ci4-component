<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientBadUnauthorized
 * @package App\Exceptions
 */
class ClientUnauthorized extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 401;
	protected string $error      = 'unauthorized';
	protected string $statusText = 'Exceptions.Unauthorized';
	protected string $statusName = 'login';
}