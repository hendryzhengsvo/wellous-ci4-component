<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientForbidden
 * @package App\Exceptions
 */
class ClientForbidden extends WsExcepCtrl
{
	protected int    $status     = 403;
	protected string $error      = 'access_forbidden';
	protected string $statusText = 'Exceptions.Forbidden denied.';

}