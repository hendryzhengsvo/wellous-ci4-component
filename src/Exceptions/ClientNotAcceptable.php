<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientNotAcceptable
 * @package App\Exceptions
 */
class ClientNotAcceptable extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 406;
	protected string $error      = 'not_acceptable';
	protected string $statusText = 'Exceptions.Not acceptable';
}