<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ServerUnavailable
 * @package App\Exceptions
 */
class ServerUnavailable extends WsExcepCtrl
{
	protected bool   $needLog    = TRUE;
	protected int    $status     = 503;
	protected string $error      = 'server_unavailable';
	protected string $statusText = 'Exceptions.Service Unavailable';
}