<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientBadRequest
 * @package App\Exceptions
 */
class ClientBadRequest extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 400;
	protected string $error      = 'bad_request';
	protected string $statusText = 'Exceptions.Bad request';
}