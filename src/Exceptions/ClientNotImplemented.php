<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientNotImplemented
 * @package App\Exceptions
 */
class ClientNotImplemented extends WsExcepCtrl
{
	protected bool   $needLog    = TRUE;
	protected int    $status     = 501;
	protected string $error      = 'not_implemented';
	protected string $statusText = 'Exceptions.Not implemented';
}