<?php

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientRangeNotSatisfiable
 * @package App\Exceptions
 */
class ClientRangeNotSatisfiable extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 416;
	protected string $error      = 'range_not_satisfiable';
	protected string $statusText = 'Exceptions.Range Not Satisfiable';
}