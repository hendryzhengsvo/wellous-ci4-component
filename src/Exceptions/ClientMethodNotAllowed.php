<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientMethodNotAllowed
 * @package App\Exceptions
 */
class ClientMethodNotAllowed extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 405;
	protected string $error      = 'method_not_allowed';
	protected string $statusText = 'Exceptions.Method not allowed';
}