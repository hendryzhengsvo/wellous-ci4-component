<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientConflict
 * @package App\Exceptions
 */
class ClientConflict extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 409;
	protected string $error      = 'conflict';
	protected string $statusText = 'Exceptions.Conflict';
}