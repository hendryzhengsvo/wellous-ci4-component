<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientLengthRequired
 * @package App\Exceptions
 */
class ClientLengthRequired extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 411;
	protected string $error      = 'length_required';
	protected string $statusText = 'Exceptions.Length required';
}