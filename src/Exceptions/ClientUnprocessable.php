<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientUnprocessable
 * @package App\Exceptions
 */
class ClientUnprocessable extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 422;
	protected string $error      = 'client_unprocessable';
	protected string $statusText = 'Exceptions.Client unprocessable';
}