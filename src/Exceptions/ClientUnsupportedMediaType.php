<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientUnsupportedMediaType
 * @package App\Exceptions
 */
class ClientUnsupportedMediaType extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 415;
	protected string $error      = 'unsupported_media_type';
	protected string $statusText = 'Exceptions.Unsupport Media Type';
}