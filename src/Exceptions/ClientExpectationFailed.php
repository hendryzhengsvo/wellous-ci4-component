<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientExpectationFailed
 * @package App\Exceptions
 */
class ClientExpectationFailed extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 417;
	protected string $error      = 'expectation_failed';
	protected string $statusText = 'Exceptions.Expectation failed';
}