<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ClientTooManyRequests
 * @package App\Exceptions
 */
class ClientTooManyRequests extends WsExcepCtrl
{
	protected bool   $needLog    = FALSE;
	protected int    $status     = 429;
	protected string $error      = 'too_many_requests';
	protected string $statusText = 'Exceptions.Too many requests';
}