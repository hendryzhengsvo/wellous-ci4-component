<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Exceptions;

/**
 * Class ServerBadGateway
 * @package App\Exceptions
 */
class ServerBadGateway extends WsExcepCtrl
{
	protected bool   $needLog    = TRUE;
	protected int    $status     = 502;
	protected string $error      = 'bad_gateway';
	protected string $statusText = 'Exceptions.Bad gateway';
}