<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Entities\Cast;

use CodeIgniter\Entity\Cast\CastInterface;

abstract class WsCastImplode implements CastInterface
{
	/**
	 * @param       $value
	 * @param array $params
	 * @return array|float|object|int|bool|string|null
	 */
	public static function get($value, array $params = []): array|float|object|int|bool|string|null
	{
		$splitter = $params[0] ?? ',';
		$splitter = $splitter ?: ',';
		$type = $params[1] ?? 'string';
		$result = !empty($value) ? explode($splitter, $value) : [];
		switch ($type) {
			case 'int':
				foreach ($result as &$item)
					$item = (int)$item;
				break;
			case 'float':
			case 'double':
				foreach ($result as &$item)
					$item = (double)$item;
				break;
		}
		return $result;
	}

	/**
	 * @param       $value
	 * @param array $params
	 * @return float|object|array|bool|int|string|null
	 */
	public static function set($value, array $params = []): float|object|array|bool|int|string|null
	{
		$splitter = $params[0] ?? ',';
		return implode($splitter, (array)$value);
	}

}