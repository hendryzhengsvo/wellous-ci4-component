<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Entities\Cast;

use CodeIgniter\Entity\Cast\CastInterface;
use Wellous\Ci4Component\Libraries\WsLibUtilities;

abstract class WsCastCompress implements CastInterface
{
	static int $triggerSize = 255;

	/**
	 * @param       $value
	 * @param array $params
	 * @return mixed|string
	 */
	public static function get($value, array $params = []): mixed
	{
		if (is_string($value) && strlen($value) > 14 && substr($value, 3, 11) === 'compress://') {
			return match (substr($value, 0, 11)) {
				'gzecompress' => gzdecode(WsLibUtilities::base64UrlDecode(substr($value, 14))),
				'gzccompress' => gzuncompress(WsLibUtilities::base64UrlDecode(substr($value, 14))),
				'gzdcompress' => gzinflate(WsLibUtilities::base64UrlDecode(substr($value, 14))),
				'bzccompress' => bzdecompress(WsLibUtilities::base64UrlDecode(substr($value, 14)), TRUE),
				default       => $value,
			};
		}
		else
			return $value;
	}

	/**
	 * @param       $value
	 * @param array $params
	 * @return mixed|string
	 */
	public static function set($value, array $params = []): mixed
	{
		$triggerSize = $params[0] ?? static::$triggerSize;
		if (!is_string($value) || strlen($value) < (int)$triggerSize)
			return $value;
		$level = $params[1] ?? 9;
		$type = $params[2] ?? '';
		switch ($type) {

			case 'gzecompress':
				$result = gzencode($value, $level);
				break;

			case 'gzccompress':
				$result = gzcompress($value, $level);
				break;

			case 'gzdcompress':
				$result = gzdeflate($value, $level);
				break;
			default:
				$type = 'bzccompress';
				$result = bzcompress($value, $level);
		}
		$result = "$type://" . WsLibUtilities::base64UrlEncode($result);
		return strlen($result) > strlen($value) ? $value : $result;
	}

}
