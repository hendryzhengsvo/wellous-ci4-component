<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Entities;

use ArrayAccess;
use CodeIgniter\Entity\Entity;
use Exception;
use Throwable;
use Wellous\Ci4Component\Libraries\WsLibUtilities;

abstract class WsBaseEntity extends Entity implements ArrayAccess
{
	/**
	 * Takes an array of key/value pairs and sets them as class
	 * properties, using any `setCamelCasedProperty()` methods
	 * that may or may not exist.
	 * @param array|null $data
	 * @return $this
	 * @throws Exception
	 */
	public function fill(?array $data = NULL): static
	{
		if (!is_array($data))
			return $this;
		foreach ($data as $key => $item) {
			$newKey = WsLibUtilities::camelCaseToSnakeCase($key);
			if ($newKey !== $key && !array_key_exists($newKey, $data))
				$data[$newKey] = $item;
		}
		return parent::fill($data);
	}

	/**
	 * Magic method to retrieve the value of a requested property.
	 * @param string $key The name of the property to retrieve.
	 * @return mixed The value of the requested property.
	 * @throws Exception
	 */
	public function __get(string $key)
	{
		if (str_starts_with($key, 'format') && method_exists($this, $key))
			return $this->$key();
		return parent::__get($key);
	}

	/**
	 * Returns true if a property exists names $key, or a getter method
	 * exists named like for __get().
	 */
	public function __isset(string $key): bool
	{
		$key = $this->mapProperty($key);

		$method = 'get' . str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $key)));

		if (method_exists($this, $method)) {
			return TRUE;
		}

		return isset($this->attributes[$key]);
	}

	/**
	 * Returns the string representation of the object.
	 * This method is invoked when the object is treated as a string. It converts
	 * the object's attributes to a JSON string using the json_encode function,
	 * with options to handle Unicode characters and output partial data in case
	 * of JSON errors.
	 * @return string The string representation of the object.
	 */
	public function __toString(): string
	{
		return json_encode($this->attributes, JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR);
	}

	/**
	 * Sets the value at the specified offset.
	 * This method is invoked when using the offset set syntax to assign a value to an element of an object.
	 * It calls the magic method __set() internally to set the value at the specified offset.
	 * If an exception is thrown during the process, it will be caught and the method will return.
	 * @param mixed $offset The offset at which to set the value.
	 * @param mixed $value  The value to be set.
	 * @return void
	 */
	public function offsetSet(mixed $offset, mixed $value): void
	{
		try {
			$this->__set($offset, $value);
		} catch (Throwable) {
			return;
		}
	}

	/**
	 * Checks if the specified offset exists.
	 * This method is used to determine whether the offset exists in the data structure.
	 * It delegates the task to the __isset method to perform the actual checking.
	 * @param mixed $offset The offset to check.
	 * @return bool Returns true if the offset exists, false otherwise.
	 */
	public function offsetExists(mixed $offset): bool
	{
		return $this->__isset($offset);
	}

	/**
	 * Unsets the value at the specified offset.
	 * This method is invoked when the unset operator is used on an object implementing ArrayAccess.
	 * It internally calls the __unset() method to remove the value at the specified offset.
	 * @param mixed $offset The key or index of the value to unset.
	 * @return void
	 */
	public function offsetUnset(mixed $offset): void
	{
		$this->__unset($offset);
	}

	/**
	 * Returns the value at the specified offset.
	 * This method is invoked when retrieving a value from an object using array
	 * access syntax. It retrieves the value at the specified offset by invoking
	 * the __get method and catching any throwable exceptions that may occur. If
	 * an exception is caught, it returns NULL as the value.
	 * @param mixed $offset The offset to retrieve the value from.
	 * @return mixed|null The value at the specified offset or null if an exception occurs.
	 */
	public function offsetGet(mixed $offset): mixed
	{
		try {
			return $this->__get($offset);
		} catch (Throwable) {
			return NULL;
		}
	}
}