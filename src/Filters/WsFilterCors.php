<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;
use Wellous\Ci4Component\Libraries\WsLibProfiler;

/**
 * Class WsFilterCors
 * @package yourpackage
 */
abstract class WsFilterCors implements FilterInterface
{
    /**
     * @param RequestInterface $request
     * @param mixed|null       $arguments
     * @return void
     */
    public function before(RequestInterface $request, $arguments = NULL): void
    {
        WsLibProfiler::addCheckPoint('filter');

        if ($request->getServer('HTTP_USER_AGENT') === 'ELB-HealthChecker/2.0')
            exit;

        $response = Services::response();
        if ($request->hasHeader('Origin')) {
            $origin = $request->header('Origin')->getValue();
            $response->setHeader('Access-Control-Allow-Origin', $origin);
            $response->setHeader('Access-Control-Allow-Credentials', 'true');
            if ($request->getMethod() === 'options' && $request->hasHeader('Access-Control-Request-Method')) {
                $response->setStatusCode(204, "No Content");
                $response->setHeader('Access-Control-Allow-Methods', $request->header('Access-Control-Request-Method')->getValue());
                $response->setHeader('Access-Control-Max-Age', '3600');
                if ($request->hasHeader('Access-Control-Request-Headers'))
                    $response->setHeader('Access-Control-Allow-Headers', $request->header('Access-Control-Request-Headers')->getValue());
                $response->send();
                exit;
            }
        }
        $this->setLang($request);
        WsLibProfiler::addLog('filter', 'CorsFilter::before');
    }

    /**
     * Executes after the request has been processed and the response has been generated.
     * @param RequestInterface  $request   The request object.
     * @param ResponseInterface $response  The response object.
     * @param mixed|null        $arguments Optional arguments passed from the route.
     * @return void
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL): void {}

    /**
     * Sets the language for the application based on the 'Lang' header in the request.
     * @param RequestInterface $request The request object.
     * @return void
     */
    protected function setLang(RequestInterface $request): void
    {
        if ($request->hasHeader('Lang')) {
            $lang = $request->header('Lang')->getValue();
            $supported = config('App')->{'supportedLocales'} ?? [];
            if (!in_array($lang, $supported, TRUE))
                $lang = config('App')->{'supportedLocales'}[0] ?? config('App')->{'defaultLocale'};
            Services::language()->setLocale(ucfirst(str_replace('-', '_', $lang)));
        }
        else
            Services::language()->setLocale(ucfirst(config('App')->{'defaultLocale'} ?? 'en'));
    }
}
