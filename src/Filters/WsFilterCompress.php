<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

/**
 * Class WsFilterCompress
 * Implements the FilterInterface and provides compression functionality for the response body.
 * @package  Namespace\Of\Class
 */
class WsFilterCompress implements FilterInterface
{
	/**
	 * @var string $acceptEncoding  Represents the value of the Accept-Encoding HTTP header.
	 *                              Initially set to an empty string.
	 */
	private static string $acceptEncoding = '';

	/**
	 * Set the Accept-Encoding value from the request header.
	 * @param RequestInterface $request   The HTTP request object.
	 * @param mixed            $arguments Optional arguments.
	 * @return void
	 */
	public function before(RequestInterface $request, $arguments = NULL): void
	{
		//get accept encoding
		self::$acceptEncoding = $request->getHeaderLine('Accept-Encoding');
	}

	/**
	 * Modify the response object by compressing the response body if the client supports compression.
	 * @param RequestInterface  $request   The HTTP request object.
	 * @param ResponseInterface $response  The HTTP response object.
	 * @param mixed             $arguments Optional arguments.
	 * @return ResponseInterface The modified HTTP response object.
	 */
	public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL): ResponseInterface
	{
		if (!self::$acceptEncoding)
			return $response;

		//if support br
		$func = 'brotli_compress';
		if (str_contains(self::$acceptEncoding, 'br') && function_exists($func)) {
			$compressedOutput = $func($response->getBody());
			$response->setHeader('Content-Encoding', 'gzip')
			         ->setHeader('Content-Length', strlen($compressedOutput))
			         ->setBody($compressedOutput);

		} //if support gzip
		elseif (str_contains(self::$acceptEncoding, 'gzip')) {
			$compressedOutput = gzencode($response->getBody(), 9);
			$response->setHeader('Content-Encoding', 'gzip')
			         ->setHeader('Content-Length', strlen($compressedOutput))
			         ->setBody($compressedOutput);
		} //if support deflate
		elseif (str_contains(self::$acceptEncoding, 'deflate')) {
			$compressedOutput = gzdeflate($response->getBody(), 9);
			$response->setHeader('Content-Encoding', 'deflate')
			         ->setHeader('Content-Length', strlen($compressedOutput))
			         ->setBody($compressedOutput);
		} //if all not supported then do nothing

		return $response;
	}
}