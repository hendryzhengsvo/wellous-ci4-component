<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Filters;

defined('FCPATH') || define('FCPATH', realpath(dirname(__FILE__, 4)) . DIRECTORY_SEPARATOR);
defined('APP_NAME') || define('APP_NAME', ($_SERVER['HTTP_HOST'] ?? ''));

use Closure;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Throwable;
use Wellous\Ci4Component\Config\WsCfgServices;
use Wellous\Ci4Component\Libraries\WsLibUtilities;
use Wellous\Ci4Component\Tool\WsStore;

/**
 * Class WsFilterProfiler
 * This class implements the FilterInterface.
 * @package YourPackageName
 */
class WsFilterProfiler implements FilterInterface
{
	/**
	 * Indicates that a report request has been made.
	 * @var boolean
	 */
	protected static bool $reportRequest = TRUE;

	/**
	 * Performs tasks before executing the main logic of the method.
	 * @param RequestInterface $request   The request object.
	 * @param mixed|null       $arguments Additional arguments passed to the method.
	 * @return void
	 */
	public function before(RequestInterface $request, $arguments = NULL): void
	{
		WsStore::$profiler::addCheckPoint('filter');

		if (!is_cli() && static::$reportRequest) {
			$route = WsCfgServices::router();
			$controller = $route->controllerName();
			if ($controller instanceof Closure)
				$controller = '{Closure}';
			$log = [
				'status'     => 'request',
				'app'        => APP_NAME ?? ($_SERVER['HTTP_HOST'] ?? ''),
				'domain'     => $_SERVER['HTTP_HOST'],
				'clientIP'   => WsCfgServices::getClientIp(),
				'method'     => $request->getMethod(),
				'routePath'  => WsCfgServices::getRoutePath(),
				'controller' => "$controller->{$route->methodName()}",
				'params'     => method_exists($request, 'getVar') ? $request->getVar() : [],
			];
			$logMsg = json_encode($log, JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR);
			unset($log);
			if (!empty($logMsg) && is_string($logMsg))
				log_message('info', $logMsg);
			unset($logMsg);
		}

		WsStore::$profiler::addLog('filter', 'ProfilerFilter::before');
	}

	/**
	 * Performs tasks after executing the main logic of the method.
	 * @param RequestInterface  $request   The request object.
	 * @param ResponseInterface $response  The response object.
	 * @param mixed|null        $arguments Additional arguments passed to the method.
	 * @return void
	 */
	public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL): void
	{
		$profiler = WsCfgServices::profiler();
		$profiler::addCheckPoint('filter');
		$isJson = $response->hasHeader('content-type') && str_contains($response->header('content-type')->getValue(), '/json');
		$data = NULL;
		if ($isJson)
			$data = json_decode((string)$response->getJSON(), TRUE);
		$cli = WsStore::$cli;
		if ($cli) {
			if (!empty($data) && is_array($data))
				$cli->border('-')
				    ->inline($cli->addInfo(' ', 'cyan'))
				    ->out($data, TRUE);
			$response->setBody('');
		}

		if ($profiler::isEnable()) {
			try {
				if ($cli) {
					$profiler::addLog('filter', 'ProfilerFilter::after');
					$cli->border('-')
					    ->inline("<bold><yellow>Profiler</yellow></bold> -> ")
					    ->out($profiler::fetch(), TRUE);
				}
				else {
					$response = WsCfgServices::response();
					if (!empty($data) && is_array($data) && !array_is_list($data)) {
						$profiler::addLog('filter', 'ProfilerFilter::after');
						$data['__profiler__'] = !empty($data['__profiler__']) ? [$data['__profiler__'], $profiler::fetch()]
							: $profiler::fetch();
						$response->setJSON(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
					}
				}
			} catch (Throwable $ex) {
				log_message('error', json_encode([
					'status'    => 'exception',
					'exception' => [
						'message' => $ex->getMessage() . ' on ' . $ex->getFile() . ':' . $ex->getLine(),
						'trace'   => explode("\n", $ex->getTraceAsString()),
					],
				], JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
				$cli?->exception($ex);
			}
		}

		$cli?->border('=')
		    ->inline(sprintf("<cyan>Ended <green><bold>%s</bold></green> with <bold><light_yellow>%s</light_yellow></cyan>\n",
			    WsCfgServices::getRoutePath(), $cli->addInfo(sprintf("<light_red>Peak Mem %s</light_red>", WsLibUtilities::byteToSize(memory_get_peak_usage())), ['light_yellow', 'bold'])));
	}
}