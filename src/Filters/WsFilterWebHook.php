<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Wellous\Ci4Component\Config\WsCfgServices;
use Wellous\Ci4Component\Exceptions\ClientForbidden;
use Wellous\Ci4Component\Exceptions\DbQueryError;

/**
 * Class WsFilterTrans
 * Implements the FilterInterface and provides methods for handling operations related to database transactions and queries.
 */
class WsFilterWebHook implements FilterInterface
{
    /**
     * @param RequestInterface $request
     * @param                  $arguments
     * @return void
     * @throws ClientForbidden
     */
    public function before(RequestInterface $request, $arguments = NULL): void
    {
        $clientIp = WsCfgServices::getClientIp();
        if (ENVIRONMENT !== 'local' && !preg_match('/^10\.0\.\d+\.\d+$/', $clientIp))
            throw new ClientForbidden("You are not allow to access this page");
    }

    /**
     * Executes the "after" method of the TransFilter class.
     * This method handles the necessary operations after a request is processed.
     * It completes the database transaction and adds a log related to the filter.
     * @param RequestInterface  $request   The request object.
     * @param ResponseInterface $response  The response object.
     * @param mixed             $arguments Additional arguments (optional).
     * @return void
     * @throws DbQueryError if the database transaction fails to commit.
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL): void {}
}
