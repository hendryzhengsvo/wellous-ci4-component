<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Filters;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Events\Events;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use mysqli;
use Wellous\Ci4Component\Exceptions\DbQueryError;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Modules\WsModel;

/**
 * Class WsFilterTrans
 * Implements the FilterInterface and provides methods for handling operations related to database transactions and queries.
 */
class WsFilterTrans implements FilterInterface
{
	private static ConnectionInterface $db;

	/**
	 * Executes the "before" method of the TransFilter class.
	 * This method handles the necessary operations before a request is processed.
	 * It adds checkpoints and logs related to database transactions and queries.
	 * @param RequestInterface $request   The request object.
	 * @param mixed            $arguments Additional arguments (optional).
	 * @return void
	 */
	public function before(RequestInterface $request, $arguments = NULL): void
	{
		static::$db = db_connect();

		WsLibProfiler::addCheckPoint('filter');

		Events::on('beforeDbBegin', function () {
			WsLibProfiler::addCheckPoint('dbTrans');
		});

		Events::on('afterDbBegin', function (ConnectionInterface $db) {

			/** @var mysqli $connection */
			$threadId = 0;
			$connection = $db->getConnection();
			if ($connection instanceof mysqli)
				$threadId = $connection->thread_id;

			WsLibProfiler::addLog('dbTrans', "(#$threadId) BEGIN", type: 'sql');
		});

		Events::on('beforeDbCommit', function () {
			WsLibProfiler::addCheckPoint('dbTrans');
		});

		Events::on('afterDbCommit', function ($result, ConnectionInterface $db) {

			/** @var mysqli $connection */
			$threadId = 0;
			$connection = $db->getConnection();
			if ($connection instanceof mysqli)
				$threadId = $connection->thread_id;

			WsLibProfiler::addLog('dbTrans', "(#$threadId) COMMIT", type: 'sql');
		});

		Events::on('beforeDbRollback', function () {
			WsLibProfiler::addCheckPoint('dbTrans');
		});

		Events::on('afterDbRollback', function ($result, ConnectionInterface $db) {

			/** @var mysqli $connection */
			$threadId = 0;
			$connection = $db->getConnection();
			if ($connection instanceof mysqli)
				$threadId = $connection->thread_id;

			WsLibProfiler::addLog('dbTrans', "(#$threadId) ROLLBACK", type: 'sql');
		});

		Events::on('beforeDbQuery', function (string $query, ?ConnectionInterface $db) {
			$db = $db ?? static::$db;
			if ($db->transEnabled) {
				$selectLock = preg_match("/^(\s*)SELECT(.*)FOR UPDATE$/is", $query);
				$isWrite = $db->isWriteType($query);
				if ($selectLock || $isWrite) {
					if ($isWrite)
						Events::trigger('beforeDbWrite', $query, $db);
					WsModel::beginTrans($db);
				}
			}
			WsLibProfiler::addCheckPoint('db');
		});

		Events::on('DBQuery', function ($query) {
			Events::trigger('afterDbQuery', $query, $query->db);
		});

		Events::on('afterDbQuery', function (string $query, ?ConnectionInterface $db) {
			$db = $db ?? static::$db;
			$error = $db->error();
			/** @var mysqli $connection */
			$threadId = 0;
			$connection = $db->getConnection();
			if ($connection instanceof mysqli)
				$threadId = $connection->thread_id;
			$queryString = preg_replace('/\s+/', ' ', $query);
			if (!empty($threadId))
				$queryString = "(#$threadId) $queryString";
			if (!empty($error['message'])) {
				WsLibProfiler::addLog('db', "$queryString - {$error['message']}", type: 'sql');
				$ex = new DbQueryError("Query: $queryString \n\n{$error['message']}");
				$debug = debug_backtrace();
				foreach ($debug as $item) {
					if (!str_contains($item['file'], "/vendor/") && !str_contains($item['file'], "\\vendor\\"))
						$ex->setLocation($item['file'], $item['line']);
					throw $ex;
				}
			}
			else
				WsLibProfiler::addLog('db', $queryString, type: 'sql');
		});

		WsLibProfiler::addLog('filter', 'TransFilter::before');
	}

	/**
	 * Executes the "after" method of the TransFilter class.
	 * This method handles the necessary operations after a request is processed.
	 * It completes the database transaction and adds a log related to the filter.
	 * @param RequestInterface  $request   The request object.
	 * @param ResponseInterface $response  The response object.
	 * @param mixed             $arguments Additional arguments (optional).
	 * @return void
	 * @throws DbQueryError if the database transaction fails to commit.
	 */
	public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL): void
	{
		WsLibProfiler::addCheckPoint('filter');

		if (!WsModel::completeTrans(static::$db))
			throw new DbQueryError("Failed to commit database transaction.");

		WsLibProfiler::addLog('filter', 'TransFilter::after');
	}
}