<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Language\Ms;

if (!class_exists('Wellous\Ci4Component\Language\Ms\WsLangExceptions')) {
	class WsLangExceptions
	{
		/**
		 * @return string[]
		 */
		public static function get(): array
		{
			return [
				'Unauthorized'             => 'Tidak dibenarkan',
				'Bad request'              => 'Maaf, permintaan yang anda buat tidak sah.',
				'Conflict'                 => 'Maaf, permintaan yang anda buat bertentangan dengan keadaan semasa pelayan.',
				'Expectation failed'       => 'Maaf, permintaan yang anda buat tidak dapat diterima.',
				'Forbidden denied'         => 'Maaf, anda tidak dibenarkan mengakses halaman ini.',
				'Gone'                     => 'Maaf, sumber yang diminta tidak lagi tersedia.',
				'Length required'          => 'Maaf, permintaan yang anda buat kekurangan panjang kandungan yang diperlukan.',
				'Method not allowed'       => 'Maaf, permintaan yang anda buat tidak dibenarkan.',
				'Not acceptable'           => 'Maaf, permintaan yang anda buat tidak dapat diterima.',
				'Not authenticated'        => 'Maaf, anda tidak disahkan untuk mengakses halaman ini.',
				'Not implemented'          => 'Maaf, permintaan yang anda buat tidak dilaksanakan.',
				'Not found'                => 'Maaf, halaman yang anda cari tidak dapat ditemui.',
				'Payment required'         => 'Maaf, permintaan yang anda buat memerlukan pembayaran.',
				'Range Not Satisfiable'    => 'Maaf, permintaan yang anda buat tidak dapat memuaskan julat.',
				'Request Timeout'          => 'Maaf, permintaan anda telah tamat masa.',
				'Too many requests'        => 'Maaf, anda telah membuat terlalu banyak permintaan dalam jumlah waktu yang ditetapkan.',
				'Request entity too large' => 'Maaf, permintaan yang anda buat terlalu besar.',
				'Client unprocessable'     => 'Maaf, permintaan yang anda buat tidak dapat diproses.',
				'Unsupported Media Type'   => 'Maaf, permintaan yang anda buat mempunyai jenis media yang tidak disokong.',
				'DbQuery error'            => 'Maaf, tetapi kami telah menemui ralat pangkalan data, sila cuba lagi nanti.',
				'Bad gateway'              => 'Maaf, pintu gerbang kami kini tidak tersedia, sila cuba lagi nanti.',
				'Internal Server Error'    => 'Maaf, kami telah menghadapi isu, sila cuba lagi nanti.',
				'Service Unavailable'      => 'Maaf, tetapi perkhidmatan kini tidak tersedia, sila cuba lagi nanti.',
			];
		}
	}
}

return WsLangExceptions::get();
