<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Language\En;

if (!class_exists('Wellous\Ci4Component\Language\En\WsLangExceptions')) {
	class WsLangExceptions
	{
		/**
		 * @return string[]
		 */
		public static function get(): array
		{
			return [
				'Unauthorized'             => 'Unauthorized',
				'Bad request'              => 'Sorry, the request you made is invalid.',
				'Conflict'                 => 'Sorry, the request you made is in conflict with the current state of the server.',
				'Expectation failed'       => 'Sorry, the request you made is not acceptable.',
				'Forbidden denied'         => 'Sorry, you are not allowed to access this page.',
				'Gone'                     => 'Sorry, the requested resource is no longer available.',
				'Length required'          => 'Sorry, the request you made is missing a required content length.',
				'Method not allowed'       => 'Sorry, the request you made is not allowed.',
				'Not acceptable'           => 'Sorry, the request you made is not acceptable.',
				'Not authenticated'        => 'Sorry, you are not authenticated to access this page.',
				'Not implemented'          => 'Sorry, the request you made is not implemented.',
				'Not found'                => 'Sorry, the page you are looking for could not be found.',
				'Payment required'         => 'Sorry, the request you made is payment required.',
				'Range Not Satisfiable'    => 'Sorry, the request you made is range not satisfiable.',
				'Request Timeout'          => 'Sorry, the request you made timed out.',
				'Too many requests'        => 'Sorry, you have made too many requests in a given amount of time.',
				'Request entity too large' => 'Sorry, the request you made is too large.',
				'Client unprocessable'     => 'Sorry, the request you made is unprocessable.',
				'Unsupported Media Type'   => 'Sorry, the request you made has an unsupported media type.',
				'DbQuery error'            => 'Sorry, but we have encountered an database error, please try again later.',
				'Bad gateway'              => 'Sorry, our gateway is currently unavailable, please try again later.',
				'Internal Server Error'    => 'Sorry, we have encountered an issue, please try again later.',
				'Service Unavailable'      => 'Sorry, but service is currently unavailable, please try again later.',
			];
		}
	}
}

return WsLangExceptions::get();