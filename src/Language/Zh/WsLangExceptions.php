<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Language\Zh;

if (!class_exists('Wellous\Ci4Component\Language\Zh\WsLangExceptions')) {
	class WsLangExceptions
	{
		/**
		 * @return string[]
		 */
		// 定义一个公共的静态方法 get
		public static function get(): array
		{
			// 返回一个包含了多个键值对的数组，其中键是英文错误信息，值是对应的中文错误信息
			return [
				'Unauthorized'             => '未经授权',
				'Bad request'              => '抱歉，您的请求无效。',
				'Conflict'                 => '抱歉，您的请求与服务器当前状态冲突。',
				'Expectation failed'       => '抱歉，您的请求不被接受。',
				'Forbidden denied'         => '抱歉，您无权访问此页面。',
				'Gone'                     => '抱歉，您请求的资源已不再可用。',
				'Length required'          => '抱歉，您的请求缺少必需的内容长度。',
				'Method not allowed'       => '抱歉，您的请求方法不被允许。',
				'Not acceptable'           => '抱歉，您的请求不被接受。',
				'Not authenticated'        => '抱歉，您无权访问此页面。',
				'Not implemented'          => '抱歉，您的请求未被执行。',
				'Not found'                => '抱歉，您要找的页面不存在。',
				'Payment required'         => '抱歉，您的请求需要付费。',
				'Range Not Satisfiable'    => '抱歉，您的请求范围无法满足。',
				'Request Timeout'          => '抱歉，您的请求已超时。',
				'Too many requests'        => '抱歉，您在给定时间内的请求次数过多。',
				'Request entity too large' => '抱歉，您的请求过大。',
				'Client unprocessable'     => '抱歉，您的请求无法处理。',
				'Unsupported Media Type'   => '抱歉，您的请求包含了不支持的媒体类型。',
				'DbQuery error'            => '抱歉，我们遇到了数据库错误，请稍后再试。',
				'Bad gateway'              => '抱歉，我们的网关当前不可用，请稍后再试。',
				'Internal Server Error'    => '抱歉，我们遇到了问题，请稍后再试。',
				'Service Unavailable'      => '抱歉，服务目前不可用，请稍后再试。',
			];
		}
	}
}

// 返回执行 WsExceptions 类的 get 方法的结果
return WsLangExceptions::get();
