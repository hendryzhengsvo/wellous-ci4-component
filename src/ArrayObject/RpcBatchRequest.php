<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\ArrayObject;

use Wellous\Ci4Component\Tool\WsObjectArray;

/**
 * Class RpcBatchRequestEntity
 * @package Wellous\Ci4Component\Entities
 * @property string $method
 * @property string $url
 * @property array  $options
 * @property array  $headers
 * @property array  $params
 */
class RpcBatchRequest extends WsObjectArray
{
	protected array $allowField = ['method', 'url', 'options', 'headers', 'params'];

	/**
	 * @param string $value
	 * @return void
	 */
	public function setMethod(string $value): void
	{
		$value = trim($value);
		$this->data['method'] = strtoupper($value ?: 'GET');
	}
}