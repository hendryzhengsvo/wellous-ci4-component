<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\ArrayObject;

use Wellous\Ci4Component\Tool\WsObjectArray;

/**
 * Class DBSearchResult
 * @package Wellous\Ci4Component\ArrayObject
 * @property integer $count
 * @property integer $totalPage
 * @property integer $page
 * @property array   $data
 */
class DBSearchResult extends WsObjectArray
{
	protected array $allowField = ['count', 'data', 'totalPage', 'page'];
}