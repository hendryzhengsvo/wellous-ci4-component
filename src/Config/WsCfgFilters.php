<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

use CodeIgniter\Config\BaseConfig;
use Wellous\Ci4Component\Filters\WsFilterCors;
use Wellous\Ci4Component\Filters\WsFilterTrans;
use Wellous\Ci4Component\Libraries\WsLibProfiler;

class WsCfgFilters extends BaseConfig
{
	/**
	 * Configures aliases for Filter classes to
	 * make reading things nicer and simpler.
	 */
	public array $aliases = [
		'cors'     => WsFilterCors::class,
		'trans'    => WsFilterTrans::class,
		'profiler' => WsLibProfiler::class,
	];

	/**
	 * List of filter aliases that are always
	 * applied before and after every request.
	 */
	public array $globals = [
		'before' => [
			'cors',
			'trans',
			'profiler',
		],
		'after'  => [
			'trans',
			'profiler',
		],
	];

	/**
	 * List of filter aliases that works on a
	 * particular HTTP method (GET, POST, etc.).
	 * Example:
	 * 'post' => ['foo', 'bar']
	 * If you use this, you should disable auto-routing because auto-routing
	 * permits any HTTP method to access a controller. Accessing the controller
	 * with a method you don’t expect could bypass the filter.
	 */
	public array $methods = [];

	/**
	 * List of filter aliases that should run on any
	 * before or after URI patterns.
	 * Example:
	 * 'isLoggedIn' => ['before' => ['account/*', 'profiles/*']]
	 */
	public array $filters = [];
}
