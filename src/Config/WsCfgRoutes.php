<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

/**
 * This file is part of CodeIgniter 4 framework.
 * (c) CodeIgniter Foundation <admin@codeigniter.com>
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use CodeIgniter\Config\Routing as BaseRouting;
use CodeIgniter\Files\FileCollection;
use Config\Services;
use SplFileInfo;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Tool\WsRouteCollection as RouteCollection;

/**
 * Routing configuration
 */
class WsCfgRoutes extends BaseRouting
{
    /**
     * @param RouteCollection $routes
     * @return void
     */
    public static function routes(RouteCollection $routes): void
    {
        WsLibProfiler::addCheckPoint('routes');

        // Load the system's routing file first, so that the app and ENVIRONMENT
        // can override as needed.
        if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
            require SYSTEMPATH . 'Config/Routes.php';

        /*
         * --------------------------------------------------------------------
         * Additional Routing
         * --------------------------------------------------------------------
         *
         * There will often be times that you need additional routing and you
         * need it to be able to override any defaults in this file. Environment
         * based routes is one such time. require() additional route files here
         * to make that happen.
         *
         * You will have access to the $routes object within that file without
         * needing to reload it.
         */

        if(file_exists(APPPATH . 'Config/Route')) {
            $routeFiles = new FileCollection([APPPATH . 'Config/Route']);
            /** @var SplFileInfo $routeFile */
            foreach ($routeFiles as $routeFile) {
                $className = $routeFile->getBasename('.php');
                if (!in_array($className, ['.', '..'])) {
                    $routeClass = "App\\Config\\Route\\$className";
                    if (class_exists($routeClass))
                        $routeClass::routes($routes);
                }
            }
        }

        $request = Services::request();
        WsLibProfiler::addLog('routes', "Routes loaded with uri: " . $request->getMethod() . ' ' . WsCfgServices::getRoutePath());
    }
}
