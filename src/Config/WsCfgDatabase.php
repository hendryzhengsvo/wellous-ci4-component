<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

use CodeIgniter\Database\Config;

/**
 * Database Configuration
 */
class WsCfgDatabase extends Config
{
	/**
	 * The directory that holds the Migrations
	 * and Seeds directories.
	 */
	public string $filesPath = APPPATH . 'Database' . DIRECTORY_SEPARATOR;

	/**
	 * Lets you choose which connection group to
	 * use if no other is specified.
	 */
	public string $defaultGroup = 'default';

	/**
	 * The default database connection.
	 */
	public array $default = [
		'hostname' => 'localhost',
		'username' => 'root',
		'password' => '',
		'database' => '',
		'DBDriver' => 'App\ThirdParty\Database\MySQLi',
		'DBPrefix' => '',
		'pConnect' => FALSE,
		'DBDebug'  => FALSE,
		'charset'  => 'utf8mb4',
		'DBCollat' => 'utf8mb4_0900_ai_ci',
		'swapPre'  => '',
		'encrypt'  => FALSE,
		'compress' => TRUE,
		'strictOn' => FALSE,
		'failover' => [],
	];

}
