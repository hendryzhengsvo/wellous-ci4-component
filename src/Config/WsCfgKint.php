<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

use CodeIgniter\Config\BaseConfig;
use Kint\Renderer\AbstractRenderer;

/**
 * --------------------------------------------------------------------------
 * Kint
 * --------------------------------------------------------------------------
 * We use Kint's `RichRenderer` and `CLIRenderer`. This area contains options
 * that you can set to customize how Kint works for you.
 * @see https://kint-php.github.io/kint/ for details on these settings.
 */
class WsCfgKint extends BaseConfig
{
	/*
	|--------------------------------------------------------------------------
	| Global Settings
	|--------------------------------------------------------------------------
	*/

	public mixed $plugins;
	public int   $maxDepth          = 6;
	public bool  $displayCalledFrom = TRUE;
	public bool  $expanded          = FALSE;

	/*
	|--------------------------------------------------------------------------
	| RichRenderer Settings
	|--------------------------------------------------------------------------
	*/
	public string $richTheme  = 'aante-light.css';
	public bool   $richFolder = FALSE;
	public int    $richSort   = AbstractRenderer::SORT_FULL;
	public mixed  $richObjectPlugins;
	public mixed  $richTabPlugins;

	/*
	|--------------------------------------------------------------------------
	| CLI Settings
	|--------------------------------------------------------------------------
	*/
	public bool $cliColors      = TRUE;
	public bool $cliForceUTF8   = FALSE;
	public bool $cliDetectWidth = TRUE;
	public int  $cliMinWidth    = 40;
}
