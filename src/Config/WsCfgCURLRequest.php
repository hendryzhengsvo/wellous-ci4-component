<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

use CodeIgniter\Config\BaseConfig;

class WsCfgCURLRequest extends BaseConfig
{
	/**
	 * --------------------------------------------------------------------------
	 * CURLRequest Share Options
	 * --------------------------------------------------------------------------
	 * Whether share options between requests or not.
	 * If true, all the options won't be reset between requests.
	 * It may cause an error request with unnecessary headers.
	 */
	public bool $shareOptions = TRUE;
}
