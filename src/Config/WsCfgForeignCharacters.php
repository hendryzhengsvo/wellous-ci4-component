<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

use CodeIgniter\Config\ForeignCharacters as BaseForeignCharacters;

class WsCfgForeignCharacters extends BaseForeignCharacters
{
}
