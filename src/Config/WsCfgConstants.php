<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

/**
 * Paths
 * Holds the paths that are used by the system to
 * locate the main directories, app, system, etc.
 * Modifying these allows you to restructure your application,
 * share a system folder between multiple applications, and more.
 * All paths are relative to the project's root folder.
 */
class WsCfgConstants
{
	/**
	 * Declare method to execute initial declarations.
	 * This method performs the following tasks:
	 *  - Defines HRTIME_START constant if it is not already defined.
	 *  - Defines REQUEST_TIME_FLOAT constant with the value of $_SERVER['REQUEST_TIME_FLOAT'] if set, else with current microtime.
	 *  - Defines REQUEST_TIME constant with the value of $_SERVER['REQUEST_TIME'] if set, else with the value of REQUEST_TIME_FLOAT.
	 *  - Defines ENVIRONMENT constant with the value of 'ENVIRONMENT' environment variable if set, else with the value of $_SERVER['ENVIRONMENT'] if set, else with the default value 'local
	 *'.
	 * @return void
	 */
	public static function intialDeclare(): void
	{
		/**
		 * --------------------------------------------------------------------
		 * Initial High Resolution Time
		 * --------------------------------------------------------------------
		 */
		defined('HRTIME_START') || define('HRTIME_START', hrtime(TRUE));

		/**
		 * --------------------------------------------------------------------
		 * Request Time
		 * --------------------------------------------------------------------
		 */
		define('REQUEST_TIME_FLOAT', (double)($_SERVER['REQUEST_TIME_FLOAT'] ?? microtime(TRUE)));
		define('REQUEST_TIME', (int)($_SERVER['REQUEST_TIME'] ?? REQUEST_TIME_FLOAT));

		/**
		 * ENVIRONMENT
		 */
		defined('ENVIRONMENT') || define('ENVIRONMENT', strtolower(getenv('ENVIRONMENT') ?: ($_SERVER['ENVIRONMENT'] ?? 'local')));
	}

	/**
	 * Define system constants and exit status codes.
	 * This method is responsible for declaring system constants that are used throughout
	 * the application and defining exit status codes for different conditions.
	 * @return void
	 */
	public static function systemDeclare(): void
	{
		/*
		 | --------------------------------------------------------------------
		 | App Namespace
		 | --------------------------------------------------------------------
		 |
		 | This defines the default Namespace that is used throughout
		 | CodeIgniter to refer to the Application directory. Change
		 | this constant to change the namespace that all application
		 | classes should use.
		 |
		 | NOTE: changing this will require manually modifying the
		 | existing namespaces of App\* namespaced-classes.
		 */
		defined('APP_NAMESPACE') || define('APP_NAMESPACE', 'App');

		/*
		 | --------------------------------------------------------------------------
		 | Composer Path
		 | --------------------------------------------------------------------------
		 |
		 | The path that Composer's autoload file is expected to live. By default,
		 | the vendor folder is in the Root directory, but you can customize that here.
		 */
		defined('COMPOSER_PATH') || define('COMPOSER_PATH', ROOTPATH . 'vendor/autoload.php');

		/*
		 |--------------------------------------------------------------------------
		 | Timing Constants
		 |--------------------------------------------------------------------------
		 |
		 | Provide simple ways to work with the myriad of PHP functions that
		 | require information to be in seconds.
		 */
		defined('SECOND') || define('SECOND', 1);
		defined('MINUTE') || define('MINUTE', 60);
		defined('HOUR') || define('HOUR', 3600);
		defined('DAY') || define('DAY', 86400);
		defined('WEEK') || define('WEEK', 604800);
		defined('MONTH') || define('MONTH', 2_592_000);
		defined('YEAR') || define('YEAR', 31_536_000);
		defined('DECADE') || define('DECADE', 315_360_000);

		/*
		 |--------------------------------------------------------------------------
		 | Memory Constants
		 |--------------------------------------------------------------------------
		 |
		 | Provide simple ways to work with the myriad of PHP functions that
		 | require information to be in bytes
		 */
		defined('KB') || define('KB', 1024);
		defined('MB') || define('MB', 1048576);
		defined('GB') || define('GB', 1073741824);
		defined('TB') || define('TB', 1099511627776);
		defined('PB') || define('PB', bccomp('1125899906842624', (string)PHP_INT_MAX) <= 0 ? 1125899906842624 : '1125899906842624');
		defined('EB') || define('EB', bccomp('1152921504606846976', (string)PHP_INT_MAX) <= 0 ? 1152921504606846976 : '1152921504606846976');
		defined('ZB') || define('ZB', bccomp('1180591620717411303424', (string)PHP_INT_MAX) <= 0 ? 1180591620717411303424 : '1180591620717411303424');

		/*
		 | --------------------------------------------------------------------------
		 | Exit Status Codes
		 | --------------------------------------------------------------------------
		 |
		 | Used to indicate the conditions under which the script is exit()ing.
		 | While there is no universal standard for error codes, there are some
		 | broad conventions.  Three such conventions are mentioned below, for
		 | those who wish to make use of them.  The CodeIgniter defaults were
		 | chosen for the least overlap with these conventions, while still
		 | leaving room for others to be defined in future versions and user
		 | applications.
		 |
		 | The three main conventions used for determining exit status codes
		 | are as follows:
		 |
		 |    Standard C/C++ Library (stdlibc):
		 |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
		 |       (This link also contains other GNU-specific conventions)
		 |    BSD sysexits.h:
		 |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
		 |    Bash scripting:
		 |       http://tldp.org/LDP/abs/html/exitcodes.html
		 |
		 */
		defined('EXIT_SUCCESS') || define('EXIT_SUCCESS', 0);               // no errors
		defined('EXIT_ERROR') || define('EXIT_ERROR', 1);                   // generic error
		defined('EXIT_CONFIG') || define('EXIT_CONFIG', 3);                 // configuration error
		defined('EXIT_UNKNOWN_FILE') || define('EXIT_UNKNOWN_FILE', 4);     // file not found
		defined('EXIT_UNKNOWN_CLASS') || define('EXIT_UNKNOWN_CLASS', 5);   // unknown class
		defined('EXIT_UNKNOWN_METHOD') || define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
		defined('EXIT_USER_INPUT') || define('EXIT_USER_INPUT', 7);         // invalid user input
		defined('EXIT_DATABASE') || define('EXIT_DATABASE', 8);             // database error
		defined('EXIT__AUTO_MIN') || define('EXIT__AUTO_MIN', 9);           // lowest automatically-assigned error code
		defined('EXIT__AUTO_MAX') || define('EXIT__AUTO_MAX', 125);         // highest automatically-assigned error code
	}

	/**
	 * Declare method to execute initial and system declarations.
	 * @return void
	 */
	public static function declare(): void
	{
		self::intialDeclare();
		self::systemDeclare();
	}
}
