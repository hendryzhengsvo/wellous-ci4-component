<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

/**
 * Paths
 * Holds the paths that are used by the system to
 * locate the main directories, app, system, etc.
 * Modifying these allows you to restructure your application,
 * share a system folder between multiple applications, and more.
 * All paths are relative to the project's root folder.
 */
class WsCfgPaths
{
	/**
	 * ---------------------------------------------------------------
	 * SYSTEM FOLDER NAME
	 * ---------------------------------------------------------------
	 * This must contain the name of your "system" folder. Include
	 * the path if the folder is not in the same directory as this file.
	 */
	public string $systemDirectory = ROOTPATH . 'vendor' . DIRECTORY_SEPARATOR . 'codeigniter4' . DIRECTORY_SEPARATOR . 'framework' . DIRECTORY_SEPARATOR . 'system';

	/**
	 * ---------------------------------------------------------------
	 * APPLICATION FOLDER NAME
	 * ---------------------------------------------------------------
	 * If you want this front controller to use a different "app"
	 * folder than the default one you can set its name here. The folder
	 * can also be renamed or relocated anywhere on your server. If
	 * you do, use a full server path.
	 * @see http://codeigniter.com/user_guide/general/managing_apps.html
	 */
	public string $appDirectory = ROOTPATH . 'app';

	/**
	 * ---------------------------------------------------------------
	 * WRITABLE DIRECTORY NAME
	 * ---------------------------------------------------------------
	 * This variable must contain the name of your "writable" directory.
	 * The writable directory allows you to group all directories that
	 * need write permission to a single place that can be tucked away
	 * for maximum security, keeping it out of the app and/or
	 * system directories.
	 */
	public string $writableDirectory = ROOTPATH . 'writable';

	/**
	 * ---------------------------------------------------------------
	 * TESTS DIRECTORY NAME
	 * ---------------------------------------------------------------
	 * This variable must contain the name of your "tests" directory.
	 */
	public string $testsDirectory = ROOTPATH . 'tests';

	/**
	 * ---------------------------------------------------------------
	 * VIEW DIRECTORY NAME
	 * ---------------------------------------------------------------
	 * This variable must contain the name of the directory that
	 * contains the view files used by your application. By
	 * default this is in `app/Views`. This value
	 * is used when no value is provided to `Services::renderer()`.
	 */
	public string $viewDirectory = ROOTPATH . 'app' . DIRECTORY_SEPARATOR . 'Views';
}
