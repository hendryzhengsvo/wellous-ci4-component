<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Config;

use CodeIgniter\Config\BaseService;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\HTTP\SiteURI;
use CodeIgniter\Router\RouteCollectionInterface;
use Config\Modules;
use Config\Routing;
use Config\Services;
use Exception;
use Throwable;
use Twig\Environment as TwigEnvironment;
use Twig\Loader\FilesystemLoader as TwigFilesystemLoader;
use Wellous\Ci4Component\Exceptions\ServerInternalError;
use Wellous\Ci4Component\Interface\WsInterfaceCloudFiles;
use Wellous\Ci4Component\Interface\WsInterfaceLock;
use Wellous\Ci4Component\Interface\WsInterfaceProfiler;
use Wellous\Ci4Component\Interface\WsInterfaceRpc;
use Wellous\Ci4Component\Interface\WsInterfaceTwoFactor;
use Wellous\Ci4Component\Libraries\WsLibCli;
use Wellous\Ci4Component\Libraries\WsLibCloudFiles;
use Wellous\Ci4Component\Libraries\WsLibContainer;
use Wellous\Ci4Component\Libraries\WsLibLock;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Libraries\WsLibRpc;
use Wellous\Ci4Component\Libraries\WsLibTwoFactor;
use Wellous\Ci4Component\Tool\WsRouteCollection;
use Wellous\Ci4Component\Tool\WsTwigFilters;

/**
 * The base services instance.
 * @var BaseService
 */
class WsCfgServices extends BaseService
{
	static private array $cacheData = [];

	/**
	 * Retrieves the client IP address.
	 * The IP address is retrieved from various HTTP headers.
	 * If the IP address is not found in any header, the IP address of the client is obtained from the request object.
	 * @return string The client IP address.
	 */
	public static function getClientIp(): string
	{
		if (empty(self::$cacheData['clientIp'])) {
			if (!empty($_SERVER['HTTP_CF_CONNECTING_IP']))   //check for Cloudflare header
				self::$cacheData['clientIp'] = $_SERVER['HTTP_CF_CONNECTING_IP'];
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //check for IPs passing through proxies
				self::$cacheData['clientIp'] = trim(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])[0]); // get the first (left-most) IP address
			elseif (!empty($_SERVER['HTTP_X_REAL_IP']))   //check for Real IP
				self::$cacheData['clientIp'] = $_SERVER['HTTP_X_REAL_IP'];
			else
				self::$cacheData['clientIp'] = Services::request()->getIPAddress(); //get the IP address of the client
		}
		return self::$cacheData['clientIp'];
	}

	/**
	 * Retrieves the route path from the current request.
	 * This method checks if the route path is already cached in the static variable
	 * `self::$cacheData['routePath']`. If not, it gets the URI from the request and
	 * determines the route path based on the type of URI. It then caches the route path
	 * in the static variable for future use.
	 * @return string The route path from the current request.
	 */
	public static function getRoutePath(): string
	{
		if (empty(self::$cacheData['routePath'])) {
			$request = is_cli() ? Services::clirequest() : Services::request();
			$uri = $request->getUri();
			self::$cacheData['routePath'] = $uri instanceof SiteURI
				? $uri->getRoutePath()
				: str_replace(['/index.php', '\\', '/'], ['', DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR], $uri->getPath());
		}
		return self::$cacheData['routePath'];
	}

	/**
	 * Returns the current URL with an optional path appended.
	 * @param string $path The optional path to append to the URL. Defaults to an empty string.
	 * @return string The current URL with the optional path appended.
	 */
	public static function getCurrentUrl(string $path = ''): string
	{
		if (empty(self::$cacheData['currentUrl'])) {
			self::$cacheData['currentUrl'] = sprintf("%s://%s%s%s", Services::request()->getUri()->getScheme(),
				Services::request()->getUri()->getHost(), Services::request()->getUri()->getPort() ? ":" . Services::request()->getUri()->getPort()
					: "",
				$path
					?: str_replace(['/index.php', '\\'], ['', '/'],
					is_cli()
						? implode("/",
						Services::request()->getSegments())
						: Services::request()->getUri()->getPath()));
		}
		return self::$cacheData['currentUrl'];
	}

	/**
	 * Retrieves the method trace as an array.
	 * @return array An array containing the method trace.
	 */
	public static function trace(): array
	{
		$ex = new Exception();
		$trace = $ex->getTraceAsString();
		$trace = explode("\n", $trace);
		array_shift($trace);
		return $trace;
	}

	/**
	 * Creates a new instance of the WsLibContainer class.
	 * @param mixed $data      The data to be stored in the container.
	 * @param bool  $lock      Whether to lock the container or not (default: FALSE).
	 * @param bool  $firstOnly Whether to return only the first item in the container (default: TRUE).
	 * @return WsLibContainer The newly created instance of WsLibContainer.
	 */
	public static function container(mixed $data, bool $lock = FALSE, bool $firstOnly = TRUE): WsLibContainer
	{
		return new WsLibContainer($data, $lock, $firstOnly);
	}

	/**
	 * Get the routes collection
	 * @param bool $getShared Determine if the routes collection should be a shared instance
	 * @return RouteCollectionInterface Returns the routes collection instance
	 */
	public static function routes(bool $getShared = TRUE): RouteCollectionInterface
	{
		if ($getShared) {
			/** @var WsRouteCollection $module */
			$module = static::getSharedInstance('routes');
			return $module;
		}
		return new WsRouteCollection(static::locator(), new Modules(), new Routing());
	}

	/**
	 * Creates and returns an instance of the WsInterfaceProfiler class.
	 * @param bool $getShared (optional) Whether to get the shared instance or create a new one. Defaults to true.
	 * @return WsInterfaceProfiler The instance of WsInterfaceProfiler either retrieved from the shared instance or a new instance.
	 */
	public static function profiler(bool $getShared = TRUE): WsInterfaceProfiler
	{
		if ($getShared) {
			/** @var WsInterfaceProfiler $module */
			$module = static::getSharedInstance('profiler');
			return $module;
		}
		return new WsLibProfiler();
	}

	/**
	 * Creates and returns an instance of the WsInterfaceRpc class.
	 * @param bool $getShared (optional) Whether to get the shared instance or create a new one. Defaults to true.
	 * @return WsInterfaceRpc The instance of WsInterfaceRpc either retrieved from the shared instance or a new instance.
	 */
	public static function rpc(bool $getShared = TRUE): WsInterfaceRpc
	{
		if ($getShared) {
			/** @var WsInterfaceRpc $module */
			$module = static::getSharedInstance('rpc');
			return $module;
		}
		return new WsLibRpc();
	}

	/**
	 * Creates and returns an instance of the WsLibCli class.
	 * @param bool $getShared (optional) Whether to get the shared instance or create a new one. Defaults to true.
	 * @return WsLibCli The instance of WsLibCli either retrieved from the shared instance or a new instance.
	 */
	public static function cli(bool $getShared = TRUE): WsLibCli
	{
		if ($getShared) {
			/** @var WsLibCli $module */
			$module = static::getSharedInstance('cli');
			return $module;
		}
		return new WsLibCli();
	}

	/**
	 * Creates and returns an instance of the WsInterfaceLock class.
	 * @param bool $getShared (optional) Whether to get the shared instance or create a new one. Defaults to true.
	 * @return WsInterfaceLock The instance of WsInterfaceLock either retrieved from the shared instance or a new instance.
	 */
	public static function lock(bool $getShared = TRUE): WsInterfaceLock
	{
		if ($getShared) {
			/** @var WsInterfaceLock $module */
			$module = static::getSharedInstance('lock');
			return $module;
		}
		return new WsLibLock();
	}

	/**
	 * Creates and returns an instance of the WsInterfaceTwoFactor class.
	 * @param bool $getShared (optional) Whether to get the shared instance or create a new one. Defaults to true.
	 * @return WsInterfaceTwoFactor The instance of WsInterfaceTwoFactor either retrieved from the shared instance or a new instance.
	 */
	public static function twoFactor(bool $getShared = TRUE): WsInterfaceTwoFactor
	{
		if ($getShared) {
			/** @var WsInterfaceTwoFactor $module */
			$module = static::getSharedInstance('twoFactor');
			return $module;
		}
		return new WsLibTwoFactor();
	}

	/**
	 * Creates and returns an instance of the WsInterfaceCloudFiles class.
	 * @param bool $getShared (optional) Whether to get the shared instance or create a new one. Defaults to true.
	 * @return WsInterfaceCloudFiles The instance of WsInterfaceCloudFiles either retrieved from the shared instance or a new instance.
	 */
	public static function cloudFiles(bool $getShared = TRUE): WsInterfaceCloudFiles
	{
		if ($getShared) {
			/** @var WsInterfaceCloudFiles $module */
			$module = static::getSharedInstance('cloudFiles');
			return $module;
		}
		return new WsLibCloudFiles();
	}

	/**
	 * Creates and returns an instance of the TwigEnvironment class.
	 * @param bool  $getShared (optional) Whether to get the shared instance or create a new one. Defaults to true.
	 * @param array $paths     (optional) An array of paths to the directories where Twig templates are located.
	 * @return TwigEnvironment The instance of TwigEnvironment either retrieved from the shared instance or a new instance.
	 */
	public static function twig(bool $getShared = TRUE, array $paths = []): TwigEnvironment
	{
		if ($getShared) {
			/** @var TwigEnvironment $module */
			$module = static::getSharedInstance('twig');
			return $module;
		}
		$strictMode = TRUE;
		$func = 'isProduction';
		if (function_exists($func))
			$strictMode = !$func();

		$twigPath = [];
		if (file_exists(config('Paths')->viewDirectory) && is_dir(config('Paths')->viewDirectory))
			$twigPath[] = config('Paths')->viewDirectory;
		$viewTwig = config('Paths')->viewDirectory . DIRECTORY_SEPARATOR . 'twig';
		if (file_exists($viewTwig) && is_dir($viewTwig))
			$twigPath[] = $viewTwig;
		$twigPath = array_reverse($twigPath);

		$filter = new WsTwigFilters(new TwigEnvironment(new TwigFilesystemLoader(array_merge($paths, $twigPath)), [
			'cache'            => FALSE,
			'debug'            => $strictMode,
			'auto_reload'      => $strictMode,
			'strict_variables' => $strictMode,
			'autoescape'       => 'html',
		]));
		return $filter();
	}

	/**
	 * Renders a Twig template with the given name and context.
	 * @param string $name    The name of the Twig template to render.
	 * @param array  $context The context data to pass to the Twig template.
	 * @return string The rendered content of the Twig template.
	 * @throws ServerInternalError If an error occurs while rendering the Twig template.
	 */
	public static function twigRender(string $name, array $context = []): string
	{
		try {
			return self::twig()->render($name, $context);
		} catch (Throwable $e) {
			throw new ServerInternalError($e->getMessage(), previous: $e);
		}
	}

	/**
	 * Creates and returns a Twig response with the provided template name and context.
	 * @param string $name    The name of the Twig template to render.
	 * @param array  $context (optional) The context variables to pass to the Twig template. Defaults to an empty array.
	 * @return ResponseInterface The response object containing the rendered Twig template.
	 * @throws ServerInternalError
	 */
	public static function twigResponse(string $name, array $context = []): ResponseInterface
	{
		return static::response()->setBody(static::twigRender($name, $context));
	}
}

