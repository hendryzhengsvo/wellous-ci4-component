<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Database\MySQLi;

use CodeIgniter\Database\MySQLi\PreparedQuery as BasePreparedQuery;
use CodeIgniter\Events\Events;

/**
 * Class WsDbPreparedQuery
 * Represents a prepared query against the database with connection info saved.
 */
class WsDbPreparedQuery extends BasePreparedQuery
{
	/**
	 * Prepares the query against the database, and saves the connection
	 * info necessary to execute the query later.
	 * NOTE: This version is based on SQL code. Child classes should
	 * override this method.
	 * @param array $options Passed to the connection's prepare statement.
	 *                       Unused in the MySQLi driver.
	 */
	public function _prepare(string $sql, array $options = []): BasePreparedQuery
	{
		return parent::_prepare($sql, $options);
	}

	/**
	 * Takes a new set of data and runs it against the currently
	 * prepared query. Upon success, will return a Results object.
	 */
	public function _execute(array $data): bool
	{
		$this->query->setBinds($data);
		Events::trigger('beforeDbQuery', $this->query->getQuery(), $this->db);
		return parent::_execute($data);
	}
}