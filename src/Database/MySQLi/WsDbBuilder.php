<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Database\MySQLi;

use CodeIgniter\Database\MySQLi\Builder as BaseBuilder;
use CodeIgniter\Database\ResultInterface;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Libraries\WsLibUtilities;
use Wellous\Ci4Component\Tool\WsStore;

class WsDbBuilder extends BaseBuilder
{

	/**
	 * Retrieves data from the parent class using a specified limit, offset, and reset option.
	 * @param int|null $limit  The maximum number of rows to retrieve. Use NULL to retrieve all rows.
	 * @param int      $offset The offset from where to start retrieving the rows.
	 * @param bool     $reset  Specifies whether to reset the select after retrieval.
	 * @return bool|string|ResultInterface Returns the retrieved data as a boolean, string, or a ResultInterface object.
	 */
	public function get(?int $limit = NULL, int $offset = 0, bool $reset = TRUE): bool|string|ResultInterface
	{
		if ($limit !== NULL)
			$this->limit($limit, $offset);
		$query = parent::getCompiledSelect(FALSE);
		$cacheName = hash('sha3-256', $query);

		/** @var ResultInterface $result */
		if (!empty(WsStore::$queryCached[$cacheName])) {
			$result = WsStore::$queryCached[$cacheName];
			if ($result->resultID !== FALSE) {
				WsLibProfiler::addCheckPoint('dbCached');
				if ($reset === TRUE) {
					$this->resetSelect();
					$this->binds = [];
				}
				WsLibProfiler::addLog('dbCached', preg_replace('/\s+/', ' ', $query), type: 'sql');
				return WsStore::$queryCached[$cacheName];
			}
			else
				unset(WsStore::$queryCached[$cacheName]);
		}

		$result = parent::get($limit, $offset, $reset);

		//Clear cache if left then 10mb usable memory
		if (WsLibUtilities::isNearMaxMem()) {
			WsStore::$queryCached = [];
			gc_collect_cycles();
		}

		if ($result instanceof ResultInterface)
			WsStore::$queryCached[$cacheName] = $result;

		return $result;
	}

	/**
	 * Resets the query builder "write" values.
	 * Called by the insert() update() insertBatch() updateBatch() and delete() functions
	 */
	protected function resetWrite(): void
	{
		WsStore::$queryCached = [];
		gc_collect_cycles();
		parent::resetWrite();
	}
}
