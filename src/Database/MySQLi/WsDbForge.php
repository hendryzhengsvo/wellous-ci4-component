<?php
declare(strict_types=1);

/**
 * This file is part of CodeIgniter 4 framework.
 * (c) CodeIgniter Foundation <admin@codeigniter.com>
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Wellous\Ci4Component\Database\MySQLi;

use CodeIgniter\Database\MySQLi\Forge as BaseForge;

/**
 * Forge for MySQLi
 */
class WsDbForge extends BaseForge
{
}