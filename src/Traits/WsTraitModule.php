<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Traits;

use Wellous\Ci4Component\Modules\WsModules;

/**
 * Trait WsTraitModule
 * @property WsModules $modules
 */
trait WsTraitModule
{
	/**
	 * @var string
	 */
	protected string $componentPath = '';
	/**
	 * @var array
	 */
	protected array $components = [];
	/**
	 * @var array
	 */
	protected array $baseModule = [];

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function __get(string $name)
	{
		$className = ucfirst($name);
		if (isset($this->components[$className]))
			return $this->components[$className];
		else {
			$classPath = ($this->baseModule[$name] ?? FALSE) ? $this->baseModule[$name] : "$this->componentPath\\$className";
			if (class_exists($classPath))
				$this->components[$className] = ($this instanceof WsModules) ? new $classPath($this)
					: new $classPath(property_exists($this, 'modules') ? $this->modules : $this, $this);
			return $this->components[$className] ?? NULL;
		}
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function __isset(string $name): bool
	{
		$className = ucfirst($name);
		if (isset($this->components[$className]))
			return TRUE;
		else {
			$classPath = ($this->baseModule[$name] ?? FALSE) ? $this->baseModule[$name] : "$this->componentPath\\$className";
			return class_exists($classPath);
		}
	}

	/**
	 * @param string $name
	 * @param string $classPath
	 * @return mixed
	 */
	public function __getWithFullPath(string $name, string $classPath): mixed
	{
		$name = ucfirst($name);
		if (isset($this->components[$name])) {
			return $this->components[$name];
		}
		else {
			if (class_exists($classPath))
				$this->components[$name] = new $classPath($this->modules, $this);
			return $this->components[$name] ?? NULL;
		}
	}
}