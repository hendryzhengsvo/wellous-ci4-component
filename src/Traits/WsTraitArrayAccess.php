<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Traits;

use Throwable;
use Wellous\Ci4Component\Modules\WsModules;

/**
 * Trait WsTraitModule
 * @property WsModules $modules
 * @method __unset(mixed $offset)
 * @method __set(mixed $offset, mixed $value)
 * @method __get(mixed $offset)
 * @method __isset(mixed $offset)
 */
trait WsTraitArrayAccess
{

	/**
	 * @param mixed $offset
	 * @return bool
	 */
	public function offsetExists(mixed $offset): bool
	{
		return $this->__isset($offset);
	}

	/**
	 * @param mixed $offset
	 * @return mixed
	 */
	public function offsetGet(mixed $offset): mixed
	{
		return $this->__get($offset);
	}

	/**
	 * @param mixed $offset
	 * @param mixed $value
	 * @return void
	 * @throws Throwable
	 */
	public function offsetSet(mixed $offset, mixed $value): void
	{
		$this->__set($offset, $value);
	}

	/**
	 * @param mixed $offset
	 * @return void
	 */
	public function offsetUnset(mixed $offset): void
	{
		$this->__unset($offset);
	}
}