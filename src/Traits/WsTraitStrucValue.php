<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Traits;

/**
 * Trait WsEntityFlagSet
 * @package Wellous\Ci4Component\Traits
 * @property  array $attributes
 */
trait WsTraitStrucValue
{
	/**
	 * Determines the type of a given value.
	 * @param mixed $value The value to determine the type of.
	 * @return string The detected type of the value.
	 */
	private function _getTypeName(mixed $value): string
	{
		$type = gettype($value);
		if (in_array($type, ['resource', 'resource (closed)', 'object', 'NULL', 'unknown type'], TRUE)) {
			return 'json';
		}
		elseif ($type === 'array') {
			if (!array_is_list($value)) {
				return 'json-array';
			}
			else {
				if (count($value) === count(array_filter($value, function ($val) {
						return preg_match('/^-?[1-9][0-9]*$/', (string)$val) === 1;
					}))) {
					return 'number-array';
				}
				else {
					return 'list-array';
				}
			}
		}
		else {
			if (is_bool($value)) {
				return 'boolean';
			}
			elseif (preg_match('/^-?[1-9][0-9]*$/', (string)$value) === 1) {
				return 'integer';
			}
			elseif (is_numeric($value)) {
				return 'double';
			}
			else {
				return $type;
			}
		}
	}

	/**
	 * Saves the value based on the given type.
	 * @param mixed  $value The value to be saved.
	 * @param string $type  The type of the value.
	 * @return string The saved value based on the given type.
	 */
	private function _getTypeValue(mixed $value, string $type): string
	{
		return match ($type) {
			'json', 'json-array' => json_encode($value, JSON_UNESCAPED_SLASHES & JSON_UNESCAPED_UNICODE),
			'number-array'       => implode(',', array_map('intval', $value)),
			'list-array'         => implode(',', $value),
			'boolean'            => $value ? '1' : '0',
			'date'               => date("Y-m-d", strtotime($value)),
			'time'               => date("H:i:s", strtotime($value)),
			'datetime'           => date("Y-m-d H:i:s", strtotime($value)),
			'timestamp'          => (int)($this->_isValidTimeStamp($value) ? $value : strtotime($value)),
			default              => (string)$value,
		};
	}

	/**
	 * Returns the value converted to the specified data type.
	 * @param mixed  $value The value to be converted.
	 * @param string $type  The data type to convert the value to. Defaults to 'string' if not provided.
	 * @return mixed The converted value based on the specified data type.
	 */
	private function _returnByType(mixed $value, string $type): mixed
	{
		$type = empty($type) ? 'string' : $type;
		return match ($type) {
			'json'            => json_decode($value),
			'json-array'      => json_decode($value, TRUE),
			'number-array'    => array_map('intval', explode(',', $value)),
			'list-array'      => explode(',', $value),
			'integer'         => (int)$value,
			'boolean'         => boolval($value),
			'date'            => date("Y-m-d", strtotime($value)),
			'time'            => date("H:i:s", strtotime($value)),
			'datetime'        => date("Y-m-d H:i:s", strtotime($value)),
			'timestamp'       => $this->_isValidTimeStamp($value) ? $value : strtotime($value),
			'double', 'float' => (double)$value,
			default           => (string)$value,
		};
	}

	/**
	 * Checks if the given value is a valid timestamp.
	 * @param mixed $timestamp The timestamp value to be checked.
	 * @return bool True if the given value is a valid timestamp, false otherwise.
	 */
	private function _isValidTimeStamp(int|string $timestamp): bool
	{
		return ((string)(int)$timestamp === $timestamp)
			&& ($timestamp <= PHP_INT_MAX)
			&& ($timestamp >= ~PHP_INT_MAX);
	}
	/**
	 * Retrieves the value based on the type specified in the attributes.
	 * @return mixed The retrieved value based on the specified type.
	 */
	public function getValue(string $field = 'value'): mixed
	{
		if (isset($this->casts[$field]))
			unset($this->casts[$field]);
		return $this->_returnByType($this->attributes[$field], $this->attributes['type']);
	}

	/**
	 * Sets the value for an attribute.
	 * @param mixed $value The value to be set.
	 * @return static Returns an instance of the current class.
	 */
	public function setValue(mixed $value, string $field = 'value'): static
	{
		$this->attributes[$field] = $this->_getTypeValue($value, $this->attributes['type'] = $this->_getTypeName($value));
		return $this;
	}

	/**
	 * Returns the object properties as a raw display array.
	 * @return array The object properties as a raw display array.
	 */
	public function toRawDisplayArray(): array
	{
		return [
			'type'  => $this->attributes['type'],
			'key'   => $this->attributes['key'],
			'value' => $this->attributes['value'],
		];
	}
}
