<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Traits;

/**
 * Trait WsEntityFlagSet
 * @package Wellous\Ci4Component\Traits
 * @property array $attributes
 */
trait WsTraitFlagSet
{
	protected array $flagSets = [];

	/**
	 * @param string $flag
	 * @param string $field
	 * @return $this
	 */
	public function addFlag(string $flag, string $field = 'flag'): static
	{
		if (!empty($this->flagSets) && array_is_list($this->flagSets))
			$this->flagSets[$field] = $this->flagSets;
		$flags = !empty($this->attributes[$field]) ? explode(',', $this->attributes[$field]) : [];
		if (in_array($flag, $this->flagSets[$field] ?? [], TRUE)) {
			$flags[] = $flag;
			$flags = array_unique($flags);
			usort($flags, function ($a, $b) use ($field) {
				foreach ($this->flagSets[$field] ?? [] as $value) {
					if ($a == $value)
						return 0;
					if ($b == $value)
						return 1;
				}
				return -1;
			});
		}
		$this->attributes[$field] = implode(',', $flags);
		return $this;
	}

	/**
	 * @param string $flag
	 * @param string $field
	 * @return $this
	 */
	public function removeFlag(string $flag, string $field = 'flag'): static
	{
		$flags = !empty($this->attributes[$field]) ? explode(',', $this->attributes[$field]) : [];
		$key = array_search($flag, $flags);
		if ($key !== FALSE)
			unset($flags[$key]);
		$this->attributes[$field] = implode(',', $flags);
		return $this;
	}

	/**
	 * @param string $flag
	 * @param string $field
	 * @return bool
	 */
	public function haveFlag(string $flag, string $field = 'flag'): bool
	{
		$flags = !empty($this->attributes[$field]) ? explode(',', $this->attributes[$field]) : [];
		return in_array($flag, $flags, TRUE);
	}

	/**
	 * @param string $field
	 * @return array
	 */
	public function listFlag(string $field = 'flag'): array
	{
		return !empty($this->attributes[$field]) ? explode(',', $this->attributes[$field]) : [];
	}

	/**
	 * @param string $field
	 * @return array
	 */
	public function allFlag(string $field = 'flag'): array
	{
		return $this->flagSets[$field] ?? [];
	}
}