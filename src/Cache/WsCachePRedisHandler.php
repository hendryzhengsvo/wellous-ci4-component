<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Cache;

/**
 * This file is third party for CodeIgniter 4 framework.
 */

use CodeIgniter\Cache\Handlers\PredisHandler;

class WsCachePRedisHandler extends PredisHandler
{
}
