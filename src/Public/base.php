<?php
declare(strict_types=1);

// Check PHP version.
use Config\Services;
use Wellous\Ci4Component\Config\WsCfgServices;
use Wellous\Ci4Component\Libraries\WsLibCli;
use Wellous\Ci4Component\Libraries\WsLibUtilities;
use Wellous\Ci4Component\Tool\WsStore;

$minPhpVersion = '8.0'; // If you update this, don't forget to update `spark`.
if (version_compare(PHP_VERSION, $minPhpVersion, '<')) {
	$message = sprintf(
		'Your PHP version must be %s or higher to run CodeIgniter. Current version: %s',
		$minPhpVersion,
		PHP_VERSION
	);
	die($message);
}

// Refuse to run when called from php-cgi
if (defined('SPARKED') && str_starts_with(PHP_SAPI, 'cgi'))
	die("The cli tool is not supported when running php-cgi. It needs php-cli to function!\n\n");

openlog('php', LOG_ODELAY, LOG_USER);

defined('CI_DEBUG') || define('CI_DEBUG', FALSE);
defined('SHOW_DEBUG_BACKTRACE') || define('SHOW_DEBUG_BACKTRACE', FALSE);

/*
 |--------------------------------------------------------------------------
 | Memory Constants
 |--------------------------------------------------------------------------
 |
 | Provide simple ways to work with the myriad of PHP functions that
 | require information to be in bytes
 */
defined('KB') || define('KB', 1024);
defined('MB') || define('MB', 1048576);
defined('GB') || define('GB', 1073741824);
defined('TB') || define('TB', 1099511627776);
defined('PB') || define('PB', bccomp('1125899906842624', (string)PHP_INT_MAX) <= 0 ? 1125899906842624 : '1125899906842624');
defined('EB') || define('EB', bccomp('1152921504606846976', (string)PHP_INT_MAX) <= 0 ? 1152921504606846976 : '1152921504606846976');
defined('ZB') || define('ZB', bccomp('1180591620717411303424', (string)PHP_INT_MAX) <= 0 ? 1180591620717411303424 : '1180591620717411303424');

/*
 *---------------------------------------------------------------
 * BOOTSTRAP THE APPLICATION
 *---------------------------------------------------------------
 * This process sets up the path constants, loads and registers
 * our autoloader, along with Composer's, loads our constants
 * and fires up an environment-specific bootstrapping.
 */

// Define the absolute root paths for ease of use
defined('ROOTPATH') || define('ROOTPATH', realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR);

// Define the libary path for ease of use
defined('WSPATH') || define('WSPATH', realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR);

// Path to the front controller (this file)
defined('FCPATH') || define('FCPATH', ROOTPATH . 'public' . DIRECTORY_SEPARATOR);

// Ensure the current directory is pointing to the front controller's directory
if (getcwd() . DIRECTORY_SEPARATOR !== FCPATH)
	chdir(FCPATH);

// Load our paths config file
// This is the line that might need to be changed, depending on your folder structure.
require_once FCPATH . '../app/Config/Paths.php';
// ^^^ Change this line if you move your application folder

$paths = new Config\Paths();

// Location of the framework bootstrap file.
require_once rtrim($paths->systemDirectory, '\\/ ') . DIRECTORY_SEPARATOR . 'bootstrap.php';

// Load environment settings from .env files into $_SERVER and $_ENV
require_once SYSTEMPATH . 'Config/DotEnv.php';
(new CodeIgniter\Config\DotEnv(ROOTPATH))->load();

// Define ENVIRONMENT
if (!defined('ENVIRONMENT'))
	define('ENVIRONMENT', env('CI_ENVIRONMENT', 'production'));

if (is_file(APPPATH . 'Config/Boot/' . ENVIRONMENT . '.php'))
	require_once APPPATH . 'Config/Boot/' . ENVIRONMENT . '.php';

WsStore::$profiler = WsCfgServices::profiler();

$maxMemoryLimit = WsLibUtilities::unitToInt(ini_get('memory_limit'));
if (is_cli()) {

	$remove_verbose_argvs = function (array &$argv): void {
		$verbose = ['-v', '--verbose', '-p', '--profiler'];
		foreach ($argv as $key => $value)
			if (in_array($value, $verbose, TRUE)) {
				unset($argv[$key]);
				if (isset($argv[$key + 1]) && str_starts_with($argv[$key + 1], '-') === FALSE)
					unset($argv[$key + 1]);
			}
		$argv = array_values($argv);
	};

	global $argv;
	WsStore::$cli = method_exists('Config\\Services', 'cli') ? Services::cli() : new WsLibCli();
	WsStore::$cli->parse([
		'verbose'  => [
			'prefix'      => 'v',
			'longPrefix'  => 'verbose',
			'description' => 'Verbose output',
			'noValue'     => TRUE,
		],
		'profiler' => [
			'prefix'      => 'p',
			'longPrefix'  => 'profiler',
			'description' => 'Profiler output',
			'noValue'     => TRUE,
		],
	]);
	WsStore::$profiler::initial(WsStore::$cli->arguments->get('verbose') && WsStore::$cli->arguments->get('profiler'));
	$remove_verbose_argvs($argv);
	$remove_verbose_argvs($_SERVER['argv']);
	unset($remove_verbose_argvs);

	WsStore::$maxMemLimit = GB;
	if ($maxMemoryLimit < WsStore::$maxMemLimit)
		ini_set('memory_limit', '1G');

	WsStore::$cli->inline(sprintf("<cyan>Started <green><bold>%s</bold></green> with <bold><light_yellow>%s</light_yellow></cyan>\n",
		WsCfgServices::getRoutePath(), WsStore::$cli->addInfo("", ['light_yellow', 'bold'])))
	             ->border('=');
}
else {
	WsStore::$maxMemLimit = 128 * MB;
	if ($maxMemoryLimit < WsStore::$maxMemLimit)
		ini_set('memory_limit', '128M');
}

/*
 *---------------------------------------------------------------
 * BOOTSTRAP THE APPLICATION
 *---------------------------------------------------------------
 * This process sets up the path constants, loads and registers
 * our autoloader, along with Composer's, loads our constants
 * and fires up an environment-specific bootstrapping.
 */

// Grab our CodeIgniter
$app = Config\Services::codeigniter();
$app->initialize();

return $app;