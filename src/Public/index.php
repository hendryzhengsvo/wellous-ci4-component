<?php
declare(strict_types=1);

/**
 * @var CodeIgniter\CodeIgniter $app
 */

$app = require_once 'base.php';

$context = is_cli() ? 'php-cli' : 'web';
$app->setContext($context);

/**
 *---------------------------------------------------------------
 * LAUNCH THE APPLICATION
 *---------------------------------------------------------------
 * Now that everything is set up, it's time to actually fire
 * up the engines and make this app do its thang.
 * @throws Throwable
 */
$app->run();

// Save Config Cache
// $factoriesCache->save('config');
// ^^^ Uncomment this line if you want to use Config Caching.

// Exits the application, setting the exit code for CLI-based applications
// that might be watching.
exit(EXIT_SUCCESS);
