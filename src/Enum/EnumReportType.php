<?php

namespace Wellous\Ci4Component\Enum;

enum EnumReportType: string
{
	case JSON = 'json';
	case CSV = 'csv';
	case XML = 'xml';
    case HTML = 'html';
    case XLSX = 'xlsx';
}
