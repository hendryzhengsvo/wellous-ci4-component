<?php

namespace Wellous\Ci4Component\Enum;

enum ReporterFormatType: string
{
    case HTML       = 'HTML';
    case Markdown   = 'Markdown';
    case MarkdownV2 = 'MarkdownV2';
}
