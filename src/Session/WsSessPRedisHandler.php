<?php
declare(strict_types=1);

/**
 * This file is part of CodeIgniter 4 framework.
 * (c) CodeIgniter Foundation <admin@codeigniter.com>
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Wellous\Ci4Component\Session;

use CodeIgniter\Session\Handlers\BaseHandler;
use Config\Session;
use Throwable;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Libraries\WsLibUtilities;
use Wellous\Ci4Component\Tool\WsToolRedisClient;

/**
 * Session handler using Redis for persistence
 */
class WsSessPRedisHandler extends BaseHandler
{
	/**
	 * phpRedis instance
	 * @var WsToolRedisClient|null
	 */
	protected ?WsToolRedisClient $redis;

	/**
	 * Key prefix
	 * @var string
	 */
	protected string $keyPrefix = '';

	/**
	 * Lock key
	 * @var string|null
	 */
	protected ?string $lockKey = NULL;

	/**
	 * Key exists flag
	 * @var bool
	 */
	protected bool $keyExists = FALSE;

	/**
	 * Number of seconds until the session ends.
	 * @var int
	 */
	protected int $sessionExpiration = 7200;

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		/** @var $config Session $config */
		$config = config('Session');
		parent::__construct($config, WsLibUtilities::getClientIp());

		if ($this->matchIP === TRUE)
			$this->keyPrefix .= $this->ipAddress . ':';

		$this->sessionExpiration = empty($config->{'sessionExpiration'})
			? (int)ini_get('session.gc_maxlifetime')
			: $config->{'sessionExpiration'};
	}

	/**
	 * Re-initialize existing session, or creates a new one.
	 * @param string $path The path where to store/retrieve the session
	 * @param string $name The session name
	 */
	public function open(string $path, string $name): bool
	{
		return TRUE;
	}

	/**
	 * Reads the session data from the session storage, and returns the results.
	 * @param string $id    The session ID
	 * @return string Returns an encoded string of the read data.
	 *                      If nothing was read, it must return false.
	 */
	public function read(string $id): string
	{
		if (isset($this->redis) && $this->lockSession($id)) {
			if (!isset($this->sessionID)) {
				$this->sessionID = $id;
			}

			$data = $this->redis->get($this->keyPrefix . $id);

			if (is_string($data)) {
				$this->keyExists = TRUE;
			}
			else {
				$data = '';
			}

			$this->fingerprint = sha1($data);
			return $data;
		}

		return '';
	}

	/**
	 * Acquires an emulated lock.
	 * @param string $sessionID Session ID
	 */
	protected function lockSession(string $sessionID): bool
	{
		// PHP 7 reuses the SessionHandler object on regeneration,
		// so we need to check here if the lock key is for the
		// correct session ID.
		if ($this->lockKey === $this->keyPrefix . $sessionID . ':lock')
			return (bool)$this->redis->expire($this->lockKey, 300);

		$lockKey = $this->keyPrefix . $sessionID . ':lock';
		$attempt = 0;
		WsLibProfiler::addCheckPoint('session');
		$success = FALSE;
		do {
			if (!$this->redis->setnx($lockKey, (string)time()))
				usleep(300000);
			else {
				$this->redis->expire($lockKey, 300);
				$success = TRUE;
				break;
			}
		} while (++$attempt < 30);
		WsLibProfiler::addLog('session', 'Aquired lock for ' . $lockKey . ' after ' . $attempt . ' attempts.');

		$this->lockKey = $lockKey;
		if (!$success)
			log_message('error', 'Session: Unable to obtain lock for ' . $this->keyPrefix . $sessionID . ' after ' . $attempt . ' attempts, aborting.');
		return $this->lock = $success;
	}

	/**
	 * Writes the session data to the session storage.
	 * @param string $id   The session ID
	 * @param string $data The encoded session data
	 */
	public function write(string $id, string $data): bool
	{
		if (!isset($this->redis))
			return FALSE;
		if ($this->sessionID !== $id) {
			if (!$this->releaseLock() || !$this->lockSession($id))
				return FALSE;
			$this->keyExists = FALSE;
			$this->sessionID = $id;
		}
		if (isset($this->lockKey)) {
			if ($this->fingerprint !== ($fingerprint = sha1($data)) || $this->keyExists === FALSE) {
				if ($this->redis->setex($this->keyPrefix . $id, $this->sessionExpiration, $data)) {
					$this->fingerprint = $fingerprint;
					$this->keyExists = TRUE;
					$this->releaseLock();
					return TRUE;
				}
				return FALSE;
			}
			$this->releaseLock();
			return (bool)$this->redis->expire($this->keyPrefix . $id, $this->sessionExpiration);
		}
		return FALSE;
	}

	/**
	 * Releases a previously acquired lock
	 */
	protected function releaseLock(): bool
	{
		WsLibProfiler::addCheckPoint('session');
		if (isset($this->redis, $this->lockKey) && $this->lock) {
			if (!$this->redis->del($this->lockKey)) {
				$this->logger->error('Session: Error while trying to free lock for ' . $this->lockKey);
				return FALSE;
			}
			$this->lockKey = NULL;
			$this->lock = FALSE;
		}
		WsLibProfiler::addLog('session', 'Released lock.');
		return TRUE;
	}

	/**
	 * Closes the current session.
	 */
	public function close(): bool
	{
		WsLibProfiler::addCheckPoint('session');
		if (isset($this->redis)) {
			try {
				$pingReply = $this->redis->ping();
				if (($pingReply === TRUE) || ($pingReply === '+PONG')) {
					if (isset($this->lockKey))
						$this->redis->del($this->lockKey);
				}
			} catch (Throwable $e) {
				log_message('error', 'Session: Got RedisException on close(): ' . $e->getMessage());
			}
			$this->redis = NULL;
		}
		WsLibProfiler::addLog('session', 'Closed session.');
		return TRUE;
	}

	/**
	 * Destroys a session
	 * @param string $id The session ID being destroyed
	 */
	public function destroy(string $id): bool
	{
		if (isset($this->redis, $this->lockKey)) {
			if (($result = $this->redis->del($this->keyPrefix . $id)) !== 1)
				$this->logger->debug('Session: Redis::del() expected to return 1, got ' . var_export($result, TRUE) . ' instead.');
			return $this->destroyCookie();
		}
		return FALSE;
	}

	/**
	 * Cleans up expired sessions.
	 * @param int $max_lifetime Sessions that have not updated
	 *                          for the last max_lifetime seconds will be removed.
	 * @return int Returns the number of deleted sessions on success, or false on failure.
	 */
	public function gc(int $max_lifetime): int
	{
		return 1;
	}
}
