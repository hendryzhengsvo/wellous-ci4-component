<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

defined('SHARE_PATH') || define('SHARE_PATH', env('EFS_PATH', ''));

use Wellous\Ci4Component\Libraries\Provider\WsLibProviderAi;
use Config\Services;
use ReflectionClass;
use Throwable;

/**
 *
 */
class WsLibProvider
{
    protected const MISC_URL                = 'https://service.fiv5s.com/';
    protected const NODE_URL                = 'https://ws-service.fiv5s.com/';
    public const    IMG_RESIZE_SIZE_COVER   = 'cover';
    public const    IMG_RESIZE_SIZE_CONTAIN = 'contain';
    public const    IMG_RESIZE_SIZE_STRETCH = 'stretch';

    protected static array $config = [];

    /**
     * Initializes the software with the specified configuration.
     * @param array|null $config An array of configuration options.
     * @return void
     */
    public static function initial(?array $config = []): void
    {
        if (!empty($config))
            static::$config = $config;
    }

    /**
     * Returns an instance of the Ai class.
     * This method creates a new instance of the Ai class using the fully qualified class name of the current class.
     * @return WsLibProviderAi Returns an instance of the Ai class.
     */
    public static function ai(): WsLibProviderAi {
        return new WsLibProviderAi(static::class);
    }

    /**
     * Send an email with the specified subject, content, and other parameters.
     * @param string       $subject     The subject of the email.
     * @param string       $content     The content of the email.
     * @param array        $fromEmail   An array containing the email address and name of the sender.
     * @param array|string $toEmails    An array or a string containing the email addresses of the recipients.
     * @param array        $ccEmails    An array containing the email addresses of the CC recipients.
     * @param array        $bccEmails   An array containing the email addresses of the BCC recipients.
     * @param string|array $attachments An array or a string containing the paths or URLs of the attachments.
     * @param string       $replyTo     The email address to be used as the reply-to address.
     * @return bool|string|array Returns the response from the API endpoint, or FALSE if an error occurred.
     */
    public static function sendEmail(
        string $subject, string $content, array $fromEmail, array|string $toEmails,
        array  $ccEmails = [], array $bccEmails = [], string|array $attachments = [],
        string $replyTo = ''): bool|string|array
    {
        $fromEmail = static::getEmailAddress($fromEmail, FALSE);
        $toEmails = static::getEmailAddress($toEmails);

        if (empty($fromEmail) || empty($toEmails))
            return FALSE;

        if ($attachments && !isset($attachments[0]))
            $attachments = [$attachments];

        return static::postApi('send/email', [
            'subject'     => $subject,
            'content'     => $content,
            'from'        => $fromEmail,
            'to'          => $toEmails,
            'cc'          => static::getEmailAddress($ccEmails),
            'bcc'         => static::getEmailAddress($bccEmails),
            'replyTo'     => $replyTo,
            'attachments' => $attachments ?? [],
        ]);
    }

    /**
     * Get the email address(es) from the given input.
     * @param array|string|null $emails   The input email(s).
     * @param bool              $needList Whether the result should be returned as a list.
     * @return array The email address(es).
     */
    protected static function getEmailAddress(array|string|null $emails, bool $needList = TRUE): array
    {
        $result = [];
        switch (gettype($emails)) {
            case 'array':
                if (array_is_list($emails)) {
                    foreach ($emails as $email)
                        if (!empty($email) && $email = static::getEmailAddress($email, FALSE))
                            $result[] = $email;
                }
                elseif (!empty($emails['Email'])) {
                    $result = $emails;
                }
                break;
            default:
                $result = ["Email" => $emails];
                break;
        }
        if ($needList && !array_is_list($result))
            $result = [$result];
        return $result;
    }

    /**
     * Send a POST request to a specified API endpoint.
     * @param string       $url    The URL of the API endpoint.
     * @param string|array $params An array of parameters to be sent in the request.
     * @param bool         $toNode Flag indicating whether to send the request to a node URL or a miscellaneous URL.
     * @return bool|string|array Returns the response from the API endpoint, or FALSE if an error occurred.
     */
    public static function postApi(string $url, string|array $params = [], bool $toNode = FALSE): bool|string|array
    {
        $result = FALSE;
        $baseUrl = static::$config[$toNode ? 'node_url' : 'misc_url'] ?? static::$config['url'] ?? ($toNode ? static::NODE_URL : static::MISC_URL);
        try {
            $request = [
                'base_uri' => $baseUrl,
                'timeout'  => static::$config['timeout'] ?? 100,
                'verify'   => FALSE,
                'headers'  => [
                    'Authorization' => WsLibUtilities::generateToken(static::$config['apiKey'] ?? '', static::$config['apiSecret'] ?? ''),
                ],
            ];
            $request['headers'] = array_change_key_case($request['headers']);
            if (is_array($params)) {
                $request['headers']['content-type'] = 'application/json';
                $request['json'] = $params;
            }
            else {
                if (is_string($params)) {
                    $request['headers']['content-type'] = 'text/plain';
                    if (self::isBinaryData($params))
                        $request['headers']['content-type'] = 'application/octet-stream';
                }
                $request['body'] = $params;
            }
            $result = WsLibRpc::post($url, $request);
        } catch (Throwable $ex) {
            static::handleException($ex);
        } finally {
            return $result;
        }
    }

    /**
     * Handle an exception and log the error details.
     * @param Throwable $ex The exception that occurred.
     * @return void
     */
    protected static function handleException(Throwable $ex): void
    {
        $content = FALSE;
        if ($response = WsLibRpc::getLastResponse()) {
            $content = $response->getBody()->getContents();
            if ($response->hasHeader('content-type') && str_contains($response->getHeader('content-type')[0], '/json'))
                $content = json_decode($content, TRUE) ?: $content;
        }
        $reflect = new ReflectionClass($ex);
        $error = [
                'status'    => 'exception',
                'app'       => APP_NAME ?? ($_SERVER['HTTP_HOST'] ?? ''),
                'domain'    => $_SERVER['HTTP_HOST'] ?? '',
                'type'      => $reflect->getShortName(),
                'exception' => [
                    'message' => $ex->getMessage(),
                    'trace'   => $ex->getTraceAsString(),
                ],
            ] + ($response ? ['response' => $content] : []);
        log_message('error', json_encode($error, JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
    }

    /**
     * Check if the share path is available for writing.
     * @return bool TRUE if the share path is available for writing, FALSE otherwise.
     */
    protected static function checkSharPathAvaliable(): bool
    {
        if (!empty(SHARE_PATH) && is_writable(SHARE_PATH) && is_dir(SHARE_PATH))
            return TRUE;
        return FALSE;
    }

    /**
     * Send an SMS message to the specified mobile number.
     * @param string $mobilePrefix The prefix of the mobile number.
     * @param string $mobile       The mobile number.
     * @param string $content      The content of the SMS message.
     * @return bool|string|array Returns the response from the API endpoint, or FALSE if an error occurred.
     */
    public static function sendSms(string $mobilePrefix, string $mobile, string $content): bool|string|array
    {
        return static::postApi('send/sms', [
            'mobilePrefix' => $mobilePrefix,
            'mobile'       => $mobile,
            'content'      => $content,
        ]);
    }

    /**
     * Send a Telegram message to a specified channel.
     * @param string $channel The channel to send the message to.
     * @param string $content The content of the message.
     * @param string $format  The format of the message.
     * @return bool Returns TRUE if the message was successfully sent, otherwise FALSE.
     */
    public static function sendTelegram(string $channel, string $content, string $format): bool
    {
        return (bool)static::postApi('send/telegram', [
            'channel' => $channel,
            'content' => $content,
            'format'  => $format,
        ], TRUE);
    }

    /**
     * Get geolocation information for a given IP address.
     * @param string|null $ip The IP address. If not provided, the client's IP address will be used.
     * @return string|array|bool Returns the geolocation information for the IP address, or FALSE if an error occurred.
     */
    public static function geoIp(string $ip = NULL): string|array|bool
    {
        if (empty($ip))
            $ip = WsLibUtilities::getClientIp();
        return static::postApi("pull/geoip/$ip");
    }

    /**
     * Save a log entry.
     * @param string       $platform       The platform of the log entry.
     * @param string       $module         The module of the log entry.
     * @param string       $requestUrl     The URL of the request.
     * @param array|string $requestParams  The parameters of the request.
     * @param array        $requestHeaders The headers of the request.
     * @param mixed        $response       The response from the request.
     * @param int          $timeUsage      The time taken for the request.
     * @return bool Returns TRUE if the log entry was saved successfully, otherwise returns FALSE.
     */
    public static function saveLog(
        string $platform, string $module, string $requestUrl, array|string $requestParams,
        array  $requestHeaders, mixed $response, int $timeUsage): bool
    {
        if (str_contains($requestUrl, '?')) {
            $url = explode('?', $requestUrl);
            $requestUrl = $url[0];
            if (!empty($url[1])) {
                if (is_array($requestParams)) {
                    $requestParams['url_params'] = implode('?', $url);
                }
                else {
                    $params = $requestParams;
                    $requestParams = ['request_params' => $params, 'url_params' => implode('?', $url)];
                }
            }
        }
        return (bool)static::postApi('log/add', [
            'env'            => ENVIRONMENT,
            'platform'       => $platform,
            'module'         => $module,
            'requestUrl'     => $requestUrl,
            'requestParams'  => $requestParams,
            'requestHeaders' => $requestHeaders,
            'response'       => $response,
            'timeUsage'      => $timeUsage,
        ]);
    }

    /**
     * Collect a specific address.
     * @param string $address The address to collect.
     * @return bool|string|array Returns the response from the API endpoint, or FALSE if an error occurred.
     */
    public static function collectAddress(string $address): bool|string|array
    {
        return static::postApi('push/location', ['receiver_address' => $address]);
    }

    /**
     * Generate an Excel file and return the result.
     * @param array $sheetsData An array of data for each sheet in the Excel file.
     * @param bool  $showInline Flag indicating whether to show the Excel file inline in the browser.
     * @return bool|string|array Returns the generated Excel file if $showInline is FALSE. If $showInline is TRUE, the Excel file will be sent as a response and the script will exit.
     */
    public static function generateXlsx(array $sheetsData = [], bool $showInline = FALSE): bool|string|array
    {
        $result = static::postApi('generate/excel', ['data' => $sheetsData]);
        if ($showInline) {
            Services::response()->noCache()->setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")->setBody($result)
                    ->send();
            exit;
        }
        return $result;
    }

    /**
     * Merge multiple PDFs into one.
     * @param string|array $pdfs       The PDF files to merge. Can be an array of file paths or base64 encoded strings.
     * @param array        $config     Additional configuration parameters for the merging process.
     * @param bool         $showInline Flag indicating whether to display the merged PDF inline in the browser.
     * @return bool|string|array      Returns the merged PDF as a string if $showInline is FALSE,
     *                                 or a boolean value indicating the success of the merging process.
     * @throws Throwable              If an exception occurs during the merging process.
     */
    public static function mergePdf(string|array $pdfs = '', array $config = [], bool $showInline = FALSE): bool|string|array
    {
        foreach ($pdfs as &$pdf) {
            if (file_exists($pdf))
                $pdf = base64_encode(file_get_contents($pdf));
            elseif (!base64_decode($pdf, TRUE))
                $pdf = base64_encode($pdf);
        }
        $result = static::postApi('generate/pdf/merge', ['pdfs' => $pdfs, 'config' => $config]);
        if ($result && $showInline) {
            Services::response()->noCache()->setContentType("application/pdf")->setBody($result)->send();
            exit;
        }
        return $result;
    }

    /**
     * Generate a PDF document from HTML content.
     * @param string|array $htmls      The HTML content or an array of HTML content to be converted into a PDF.
     * @param array        $config     Additional configuration options for generating the PDF (optional).
     * @param bool         $showInline Flag indicating whether to display the generated PDF inline in the browser (optional).
     * @return bool|string|array Returns the generated PDF content as a string, or FALSE if an error occurred.
     */
    public static function generatePdf(string|array $htmls = '', array $config = [], bool $showInline = FALSE): bool|string|array
    {
        $result = static::postApi('generate/pdf', ['html' => $htmls, 'config' => $config]);
        if ($result && $showInline) {
            Services::response()->noCache()->setContentType("application/pdf")->setBody($result)->send();
            exit;
        }
        return $result;
    }

    /**
     * Downgrade a PDF file.
     * @param mixed  $filePath   The path to the PDF file to be downgraded. It can be a file path or a stream resource.
     * @param string $baseName   The base name of the downgraded file. If not specified, it will be set to 'file'.
     * @param bool   $showInline Flag indicating whether to show the downgraded PDF inline in the browser. Default is FALSE.
     * @return string|false        Returns the downgraded PDF file as a string, or FALSE if an error occurred.
     */
    public static function downgradePdf(mixed $filePath, string $baseName = '', bool $showInline = FALSE): string|false
    {
        if (empty($filePath))
            return FALSE;
        clearstatcache();
        if (is_resource($filePath) && get_resource_type($filePath) === 'stream')
            rewind($filePath);
        elseif (is_string($filePath) && is_file($filePath))
            $filePath = file_get_contents($filePath);
        elseif (!is_string($filePath))
            return FALSE;
        if (self::checkSharPathAvaliable()) {
            $sharePath = SHARE_PATH . DIRECTORY_SEPARATOR . uniqid('temp_', TRUE);
            if (is_resource($filePath))
                file_put_contents($sharePath, stream_get_contents($filePath));
            else
                file_put_contents($sharePath, $filePath);
            $result = static::postApi('pdf/downgrade', [
                'mode'     => 'share',
                'filename' => $baseName ?: 'file',
                'share'    => $sharePath,
                'path'     => SHARE_PATH,
            ]);
        }
        else {
            $result = static::postMultiparts('pdf/downgrade', [
                [
                    'name'     => 'mode',
                    'contents' => 'upload',
                ],
                [
                    'name'     => 'file',
                    'filename' => $baseName ?: 'file',
                    'contents' => $filePath,
                ],
            ]);
        }
        if ($result && $showInline) {
            Services::response()->noCache()->setContentType("application/pdf")->setBody($result)->send();
            exit;
        }
        return $result;
    }

    /**
     * Generate a QR code with the given text and options.
     * @param string $text                  The text to be encoded in the QR code.
     * @param int    $imgSize               The size of the output image in pixels. Default is 512.
     * @param int    $padding               The padding around the QR code. Default is 1.
     * @param bool   $transparentBackground Flag indicating whether the background of the QR code should be transparent. Default is FALSE.
     * @param string $type                  The file type of the output image. Default is 'png'.
     * @param bool   $showInline            Flag indicating whether to show the QR code inline in the response. Default is FALSE.
     * @return bool|string|array            Returns the response from the API endpoint, or FALSE if an error occurred.
     */
    public static function generateQr(
        string $text, int $imgSize = 512, int $padding = 1, bool $transparentBackground = FALSE,
        string $type = 'png', bool $showInline = FALSE): bool|string|array
    {
        $result = static::postApi('generate/qrcode', ['type' => $type, 'text' => $text, 'size' => $imgSize, 'padding' => $padding, 'transparent' => $transparentBackground]);
        if ($result && $showInline) {
            Services::response()->noCache()->setContentType("image/$type")->setBody($result)->send();
            exit;
        }
        return $result;
    }

    /**
     * Generate a barcode image or data for a given text.
     * @param string $text        The text to be encoded in the barcode.
     * @param string $code        The barcode format code. Default is C128.
     * @param int    $height      The height of the barcode image. Default is 80.
     * @param int    $widthFactor The factor to scale the width of the barcode. Default is 2.
     * @param string $color       The color of the barcode. Default is #000.
     * @param string $type        The type of barcode image to generate. Default is png.
     * @param bool   $showInline  Flag indicating whether to display the generated barcode inline. Default is FALSE.
     * @return bool|string|array    Returns the barcode image or data if successful, or FALSE if an error occurred.
     */
    public static function generateBarcode(
        string $text, string $code = 'C128', int $height = 80, int $widthFactor = 2,
        string $color = '#000', string $type = 'png', bool $showInline = FALSE): bool|string|array
    {
        $result = static::postApi('generate/barcode', ['type' => $type, 'code' => $code, 'text' => $text, 'height' => $height, 'widthFactor' => $widthFactor, 'color' => $color]);
        if ($result && $showInline) {
            Services::response()->noCache()->setContentType("image/$type")->setBody($result)->send();
            exit;
        }
        return $result;
    }

    /**
     * Resize an image.
     * @param mixed  $image      The image data or image file path.
     * @param int    $width      The desired width of the resized image.
     * @param int    $height     The desired height of the resized image.
     * @param string $size       The resizing method to use ('stretch', 'fit', 'crop', etc.).
     * @param string $align      The alignment of the image within the resized area ('center', 'top', 'bottom', etc.).
     * @param string $type       The file type of the resized image ('png', 'jpg', 'gif', etc.).
     * @param bool   $showInline Flag indicating whether to show the resized image inline or not.
     * @return bool|string|array    Returns the resized image data as a string, or FALSE if an error occurred.
     */
    public static function resizeImage(
        mixed  $image, int $width = 0, int $height = 0, string $size = 'stretch',
        string $align = 'cc', string $type = 'png', bool $showInline = FALSE): bool|string|array
    {
        if (is_resource($image)) {
            $resourceType = get_resource_type($image);
            switch ($resourceType) {
                case 'gd':
                    ob_start();
                    imagepng($image);
                    $image = ob_get_clean();
                    break;
                case 'stream':
                    $image = stream_get_contents($image);
                    break;
            }
        }
        elseif (is_file($image) && file_exists($image)) {
            $image = file_get_contents($image);
        }

        $result = static::postApi('image/resize', [
            'type'   => $type,
            'width'  => $width,
            'height' => $height,
            'size'   => $size,
            'align'  => $align,
            'image'  => base64_encode($image),
        ]);
        if ($result && $showInline) {
            Services::response()->noCache()->setContentType("image/$type")->setBody($result)->send();
            exit;
        }
        return $result;
    }

    /**
     * Organize and process an avatar image.
     * @param mixed $image The image to be processed. Can be a GD resource, a file path or a binary string.
     * @param int   $size  The desired size of the processed image.
     */
    public static function organizeAvatar(mixed $image, int $size = 0, string $type = 'png', bool $showInline = FALSE): bool|string|array
    {
        if (is_resource($image)) {
            $resourceType = get_resource_type($image);
            switch ($resourceType) {
                case 'gd':
                    ob_start();
                    imagepng($image);
                    $image = ob_get_clean();
                    break;
                case 'stream':
                    $image = stream_get_contents($image);
                    break;
            }
        }
        elseif (is_file($image) && file_exists($image)) {
            $image = file_get_contents($image);
        }

        $result = static::postApi('image/organizeAvatar', ['type' => $type, 'size' => $size, 'image' => base64_encode($image)]);
        if ($result && $showInline) {
            Services::response()->noCache()->setContentType("image/$type")->setBody($result)->send();
            exit;
        }
        return $result;
    }

    /**
     * Subscribe to an event by sending a POST request to the API endpoint*/
    public static function subscribeEvent(string $event, string $url, string $method = 'POST'): bool|array|string
    {
        return static::postApi('broker/subscribe', ['event' => $event, 'method' => $method, 'url' => $url]);
    }

    /**
     * Unsubscribes from a specified event by sending a POST request to the event/unsubscribe API endpoint.
     * @param string $event  The name of the event to unsubscribe from.
     * @param string $url    The URL to be notified when the event occurs.
     * @param string $method The HTTP method to be used for the notification request. Default is 'POST'.
     * @return bool|array|string Returns the response from the API endpoint, or FALSE if an error occurred.
     */
    public static function unsubscribeEvent(string $event, string $url, string $method = 'POST'): bool|array|string
    {
        return static::postApi('broker/unsubscribe', ['event' => $event, 'method' => $method, 'url' => $url]);
    }
	
	/**
	 * Publishes an event to the API endpoint.
	 * @param string       $event       The name of the event to publish.
	 * @param string|array $params      An array of parameters to be sent along with the event.
	 * @param array        $headers     An array of additional headers to be included in the request.
	 * @param bool         $privateOnly Flag indicating whether the event should be published only to private channels.
	 * @return bool|array|string|null Returns the response from the API endpoint, or NULL if an error occurred.
	 */
    public static function publishEvent(string $event, string|array $params = [], array $headers = [], bool $privateOnly = FALSE):
    bool|array|string|null
    {
        return static::postApi('broker/publish', ['event' => $event, 'params' => $params, 'headers' => $headers, 'private' => $privateOnly], TRUE);
    }

    /**
     * Queue a message for sending.
     * @param string       $method      The HTTP method to use for the request.
     * @param string       $url         The URL of the API endpoint.
     * @param array        $queryParams An array of query parameters to be sent in the request.
     * @param array        $headers     An array of headers to be sent in the request.
     * @param array        $form        An array of form data to be sent in the request.
     * @param array        $json        An array of JSON data to be sent in the request.
     * @param string|array $body        The body of the request.
     * @return array Returns the response from the API endpoint.
     */
    public static function queueMessage(
        string $method, string $url, array $queryParams = [], array $headers = [], array $form = [],
        array  $json = [], string|array $body = '', string $name = ''): array
    {
        return static::postApi('queue/message', [
            'method'  => $method,
            'url'     => $url,
            'headers' => $headers,
            'query'   => $queryParams,
            'json'    => $json,
            '$form'   => $form,
            'body'    => $body,
            'name'    => $name,
        ], TRUE);
    }

    /**
     * Queue messages for processing.
     * @param array $queue An array of messages to be queued.
     * @return bool|array|null Returns the response from the API endpoint, or NULL if an error occurred.
     */
    public static function queueMessages(array $queue): bool|array|null
    {
        return static::postApi('queue/message', [
            'queue' => $queue,
        ], TRUE);
    }

    /**
     * Converts a video file to a specified format.
     * @param string $keyFile  The key file to be converted.
     * @param int    $duration The duration of the converted video (default is 120 seconds).
     * @param string $outpath  The path where the converted video should be saved (default is 'converted').
     * @return array|string|false Returns the response from the conversion, or FALSE if an error occurred.
     */
    public static function convertVideo(
        string $keyFile, int $duration = 120, string $expire = '+20 minutes',
        string $outpath = 'converted'): array|string|false
    {
        if (empty($keyFile))
            return FALSE;

        return static::postApi('video/convert', [
            'keyFile'  => $keyFile,
            'duration' => $duration,
            'expire'   => $expire,
            'outpath'  => $outpath,
        ]);
    }

    /**
     * Send a POST request with multipart form data to a specified API endpoint.
     * @param string $url       The URL of the API endpoint.
     * @param array  $multipart An array of multipart data to be sent in the request.
     * @param bool   $toNode    Flag indicating whether to send the request to a node URL or a miscellaneous URL.
     * @return bool|string|array Returns the response from the API endpoint, or FALSE if an error occurred.
     */
    protected static function postMultiparts(string $url, array $multipart = [], bool $toNode = FALSE): false|string|array
    {
        $result = FALSE;
        $baseUrl = static::$config[$toNode ? 'node_url' : 'misc_url'] ?? static::$config['url'] ?? ($toNode ? static::NODE_URL : static::MISC_URL);
        try {
            $result = WsLibRpc::post($url, [
                'base_uri'  => $baseUrl,
                'timeout'   => static::$config['timeout'] ?? 100,
                'verify'    => FALSE,
                'headers'   => [
                    'Authorization' => WsLibUtilities::generateToken(static::$config['apiKey'] ?? '', static::$config['apiSecret'] ?? ''),
                ],
                'multipart' => $multipart,
            ]);
        } catch (Throwable $ex) {
            static::handleException($ex);
        } finally {
            return $result;
        }
    }

    /**
     * Convert an image file to a different format.
     * @param mixed  $filePath The path to the image file, or a file resource.
     * @param string $baseName The base name of the converted image file (optional).
     * @return array|string|false Returns the response from the conversion API, or FALSE if an error occurred.
     */
    public static function convertImage(mixed $filePath, string $baseName = ''): array|string|false
    {
        if (empty($filePath))
            return FALSE;
        clearstatcache();
        if (is_resource($filePath) && get_resource_type($filePath) === 'stream')
            rewind($filePath);
        elseif (is_string($filePath) && is_file($filePath))
            $filePath = file_get_contents($filePath);
        elseif (!is_string($filePath))
            return FALSE;
        if (self::checkSharPathAvaliable()) {
            $sharePath = SHARE_PATH . DIRECTORY_SEPARATOR . uniqid('image_', TRUE);
            if (is_resource($filePath))
                file_put_contents($sharePath, stream_get_contents($filePath));
            else
                file_put_contents($sharePath, $filePath);
            return static::postApi('image/convert', [
                'mode'     => 'share',
                'filename' => $baseName ?: 'file',
                'share'    => $sharePath,
                'path'     => SHARE_PATH,
            ]);
        }
        else {
            return static::postMultiparts('image/convert', [
                [
                    'name'     => 'mode',
                    'contents' => 'upload',
                ],
                [
                    'name'     => 'image',
                    'filename' => $baseName ?: 'file',
                    'contents' => $filePath,
                ],
            ]);
        }
    }

    /**
     * Upload a file to a temporary location for sharing or processing.
     * @param mixed  $filePath      The path or resource of the file to be uploaded.
     * @param string $baseName      The base name of the file. If empty, 'file' will be used as the default name.
     * @param bool   $forceDownload Flag indicating whether the file should be forced to download or displayed inline.
     * @param string $mimeType      The MIME type of the file. If empty, it will be determined automatically.
     * @return array|false Returns an array containing the response from the API endpoint, or FALSE if an error occurred.
     */
    public static function uploadTmp(mixed $filePath, string $baseName = '', bool $forceDownload = TRUE, string $mimeType = ''): array|false
    {
        if (empty($filePath))
            return FALSE;
        clearstatcache();
        if (is_resource($filePath) && get_resource_type($filePath) === 'stream')
            rewind($filePath);
        elseif (is_string($filePath) && is_file($filePath))
            $filePath = file_get_contents($filePath);
        elseif (!is_string($filePath))
            return FALSE;
        if (static::checkSharPathAvaliable()) {
            $sharePath = SHARE_PATH . DIRECTORY_SEPARATOR . uniqid('temp_', TRUE);
            if (is_resource($filePath))
                file_put_contents($sharePath, stream_get_contents($filePath));
            else
                file_put_contents($sharePath, $filePath);
            return static::postApi('file/upload', [
                'mode'        => 'share',
                'filename'    => $baseName ?: 'file',
                'share'       => $sharePath,
                'mime'        => $mimeType,
                'disposition' => $forceDownload ? 'attachment' : 'inline',
                'path'        => SHARE_PATH,
            ]);
        }
        else {
            return static::postMultiparts('file/upload', [
                [
                    'name'     => 'mode',
                    'contents' => 'upload',
                ],
                [
                    'name'     => 'disposition',
                    'contents' => $forceDownload ? 'attachment' : 'inline',
                ],
                [
                    'name'     => 'mime',
                    'contents' => $mimeType,
                ],
                [
                    'name'     => 'file',
                    'filename' => $baseName ?: 'file',
                    'contents' => $filePath,
                ],
            ]);
        }
    }

    /**
     * Generates the upload temporary endpoint for a file.
     * @param string $filename The name of the file to be uploaded.
     * @param int    $fileSize The size of the file in bytes.
     * @param string $mimeType The MIME type of the file.
     * @param string $expire   The expiration time for the upload endpoint.
     * @param int    $partSize The size of each part to be uploaded, in bytes.
     * @return array|bool       Returns the generated upload temporary endpoint as an array, or FALSE if an error occurred.
     */
    public static function genUploadTmpEndpoint(
        string $filename, int $fileSize, string $mimeType = 'application/octet-stream',
        string $expire = '+20 minutes', int $partSize = 0): array|bool
    {
        return static::postApi('file/upload/endpoint', [
            'filename' => $filename ?: 'file',
            'filesize' => $fileSize ?: 0,
            'mime'     => $mimeType ?? 'application/octet-stream',
            'expire'   => $expire,
            'partsize' => $partSize ?? 5 * 1024 * 1024,
        ]);
    }

    /**
     */
    public static function uploadTmpComplete(string $key, string $uploadId, array $parts): array|bool
    {
        return static::postApi('file/upload/complete', [
            'key'      => $key ?: 'file',
            'uploadId' => $uploadId ?: 'upload-id',
            'parts'    => $parts,
        ]);
    }

    /**
     * Flushes the contents of a file from the server and returns the file contents.
     * @param string $fileUrl The URL of the file to flush.
     * @return string|bool Returns the file contents if the file was successfully flushed, or FALSE if an error occurred.
     */
    public static function flushFromShare(string $fileUrl): string|bool
    {
        $data = file_get_contents($fileUrl);
        if (is_file($fileUrl))
            @unlink($fileUrl);
        return $data;
    }

    /**
     * Save file data to a share location.
     * @param mixed $fileData The file data to be saved. This can either be a file path, a resource stream or a string containing the file contents.
     * @return string|bool Returns the share path of the saved file if successful, otherwise returns an empty string or FALSE.
     */
    public static function saveToShare(mixed $fileData): string|bool
    {
        if (empty($fileData))
            return FALSE;
        if (static::checkSharPathAvaliable()) {
            clearstatcache();
            if (is_resource($fileData) && get_resource_type($fileData) === 'stream')
                rewind($fileData);
            elseif (is_string($fileData) && is_file($fileData))
                $fileData = file_get_contents($fileData);
            elseif (!is_string($fileData))
                return FALSE;
            $sharePath = SHARE_PATH . DIRECTORY_SEPARATOR . uniqid('share_', TRUE);
            if (is_resource($fileData))
                file_put_contents($sharePath, stream_get_contents($fileData));
            else
                file_put_contents($sharePath, $fileData);
            return $sharePath;
        }
        else
            return static::uploadTmp($fileData, uniqid('share_', TRUE))['url'] ?? '';
    }

    /**
     * Check whether a string contains any non-printable characters.
     * @param string $string The string to be checked.
     * @return bool Returns TRUE if the string contains non-printable characters, otherwise returns FALSE.
     */
    private static function containsNonPrintableCharacters(string $string): bool
    {
        return preg_match('~[^\x20-\x7E\t\r\n]~', $string) > 0;
    }

    /**
     * Check if a given string is a valid UTF-8 encoded string.
     * @param string $string The string to check.
     * @return bool Returns true if the string is a valid UTF-8 encoded string, false otherwise.
     */
    private static function isValidUtf8(string $string): bool
    {
        return (boolean)mb_check_encoding($string, 'UTF-8');
    }

    /**
     * Checks if the given data is binary.
     * @param string $data The data to be checked.
     * @return bool Returns TRUE if the data is binary, FALSE otherwise.
     */
    private static function isBinaryData(string $data): bool
    {
        if (self::containsNonPrintableCharacters($data))
            return TRUE;
        if (!self::isValidUtf8($data))
            return TRUE;
        return FALSE;
    }

}
