<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use CodeIgniter\Database\BaseConnection;

defined('APPPATH') || define('APPPATH', env('APPPATH', ''));

/**
 * Class WsLibBuilder
 * This class provides methods for creating and updating model and entity files based on database tables.
 */
class WsLibBuilder
{
	protected string $sectionPatterns = "#//private-section-begin.*?private-section-end#s";

	private array $mapEntityColumn = [];
	private array $mapTableColumn  = [];

	protected string $indent = '        ';

	protected BaseConnection $db;

	public function __construct()
	{
		helper(['inflector']);
		$this->db = db_connect();
	}

	/**
	 * Create a model file based on the given table name.
	 * @param string $modulePath
	 * @param string $modelName
	 * @return self
	 */
	public function createAll(string $modulePath = 'Modules', string $modelName = 'Models'): self
	{
		$tables = $this->db->listTables();
		foreach ($tables as $table)
			$this->createModel($table)
			     ->createEntity($table);
		return $this->rebuildModule($modulePath, $modelName);
	}

	/**
	 * Sets the mapping between the entity columns and the database columns.
	 * @param array $mapColumn An associative array that maps the entity columns to the database columns.
	 *                         The keys are the names of the entity columns and the values are the names of the respective database columns.
	 *                         Example: ['entity_column1' => 'database_column1', 'entity_column2' => 'database_column2']
	 * @return void
	 */
	public function setMapEntityColumn(array $mapColumn): void
	{
		$this->mapEntityColumn = $mapColumn;
	}

	/**
	 * Sets the mapping between the table columns and the database columns.
	 * This method allows you to specify the mapping between the table columns and the respective database columns.
	 * The mapping is stored in the $mapTableColumn property of the class.
	 * @param array $tableColumn An associative array that maps the table columns to the database columns.
	 *                           The keys are the names of the table columns and the values are the names of the respective database columns.
	 *                           Example: ['table' => ['table_column1' => 'database_column1', 'table_column2' => 'database_column2']]
	 *                           The array should not be empty.
	 * @return void
	 */
	public function setMapTableColumn(array $tableColumn): void
	{
		$this->mapTableColumn = $tableColumn;
	}

	/**
	 * Create a model file based on the given table name.
	 * @param string $table                      The name of the table.
	 * @param bool   $combatibleParamsUpdateOnly Determines if the model should only be updated if it already exists.
	 * @return self
	 */
	public function createModel(string $table, bool $combatibleParamsUpdateOnly = FALSE): self
	{
		return $this->buildModel($table, $combatibleParamsUpdateOnly);
	}

	/**
	 * Create an entity based on the given table.
	 * @param string $table                      The name of the table.
	 * @param bool   $combatibleParamsUpdateOnly Determines if the entity should only be created if it already exists.
	 * @return self
	 */
	public function createEntity(string $table, bool $combatibleParamsUpdateOnly = FALSE): self
	{
		return $this->buildEntity($table, $combatibleParamsUpdateOnly);
	}

	/**
	 * Updates all models.
	 * This method retrieves a list of tables from the database and iterates over each table.
	 * For each table, it creates a model and an entity.
	 * Finally, it rebuilds the models using the rebuildModels method.
	 * @return self The current instance of the class.
	 */
	public function updateAll(string $modulePath = 'Modules', string $modelName = 'Models'): self
	{
		$tables = $this->db->listTables();
		foreach ($tables as $table)
			$this->updateModel($table)
			     ->updateEntity($table);
		return $this->rebuildModule($modulePath, $modelName);
	}

	/**
	 * Update the model file for the given table.
	 * @param string $table The name of the table.
	 * @return self
	 */
	public function updateModel(string $table): self
	{
		return $this->buildModel($table, TRUE);
	}

	/**
	 * Update the entity file based on the given table name.
	 * @param string $table The name of the table.
	 * @return self
	 */
	public function updateEntity(string $table): self
	{
		return $this->buildEntity($table, TRUE);
	}

	/**
	 * Build an entity based on the given table.
	 * @param string $table      The name of the table.
	 * @param bool   $updateOnly Indicates whether to only update the entity if it already exists.
	 * @return self
	 */
	public function buildEntity(string $table, bool $updateOnly = FALSE): self
	{
		if (!defined('APPPATH'))
			return $this;

		$class = pascalize($table) . 'Entity';
		$casts = $attrs = $datamaps = $props = [];
		$listData = [
			'attrs'    => [],
			'casts'    => [],
			'dataMaps' => [],
		];
		$path = APPPATH . DIRECTORY_SEPARATOR . 'Entities' . DIRECTORY_SEPARATOR . "$class.php";
		if (!file_exists($path) && $updateOnly)
			return $this;

		$fields = $this->db->getFieldData($table);
		$nameLength = 0;
		foreach ($fields as $field) {
			$name = $field->name;
			$commentType = 'string';
			$dataType = 'string';
			switch ($field->type) {
				case 'tinyint':
				case 'smallint':
				case 'mediumint':
				case 'int':
				case 'integer':
				case 'bigint':
				case 'bit':
				case 'numeric':
					$dataType = 'integer';
					$commentType = 'integer';
					$val = strval((int)$field->default);
					break;
				case 'real':
				case 'double':
				case 'float':
				case 'decimal':
					$dataType = 'double';
					$commentType = 'double';
					$val = strval((double)$field->default);
					break;
				case 'date':
					$val = date("Y-m-d", $field->default > 0 ? strtotime($field->default) : 0);
					$val = "'$val'";
					break;
				case 'time':
					$val = date("H:i:s", $field->default > 0 ? strtotime($field->default) : 0);
					$val = "'$val'";
					break;
				case 'datetime':
				case 'timestamp':
					if ($field->default === 'CURRENT_TIMESTAMP' || $field->default === 'current_timestamp()')
						$val = date("Y-m-d H:i:s", 0);
					else
						$val = date("Y-m-d H:i:s", $field->default > 0 ? strtotime($field->default) : 0);
					$val = "'$val'";
					break;
				case 'year':
					$dataType = 'integer';
					$commentType = 'int';
					$val = (int)$field->default;
					if ($val < 1970)
						$val = 1970;
					$val = strval($val);
					break;
				case 'json':
					$dataType = 'json-array';
					$commentType = 'array';
					$val = json_decode($field->default ?? '', TRUE);
					if (!is_array($val))
						$val = [];
					$val = "'" . json_encode($val) . "'";
					break;
				case 'char':
				case 'varchar':
				case 'text':
				default:
					$val = "'$field->default'";
			}
			$op = '';
			if (($field->nullable && $field->default == NULL) || $field->primary_key) {
				$val = NULL;
				$op = "?";
			}
			$mapName = $this->mapTableColumn[$table][$name] ?? camelize(preg_replace(
				array_map(function ($value) { return "/\b$value\b/"; }, array_keys($this->mapEntityColumn)),
				array_values($this->mapEntityColumn),
				$name));
			if ($val === NULL)
				$val = "NULL";

			$listData['attrs'][$name] = $val;
			$listData['casts'][$name] = "$op$dataType";
			if ($nameLength < strlen($name))
				$nameLength = strlen($name);
			if ($mapName !== $name)
				$listData['dataMaps'][$mapName] = "$name";
			$props[] = " * @property $commentType \$$mapName";
		}

		$length = max(array_map('strlen', array_keys($listData['attrs']))) + 2;
		foreach ($listData['attrs'] as $key => $val)
			$attrs[] = sprintf("%-{$length}s => %s", "'$key'", $val);

		$length = max(array_map('strlen', array_keys($listData['casts']))) + 2;
		foreach ($listData['casts'] as $key => $val)
			$casts[] = sprintf("%-{$length}s => '%s'", "'$key'", $val);

		if (!empty($listData['dataMaps'])) {
			$length = max(array_map('strlen', array_keys($listData['dataMaps']))) + 2;
			foreach ($listData['dataMaps'] as $key => $val)
				$datamaps[] = sprintf("%-{$length}s => '%s'", "'$key'", $val);
		}

		$template = $this->getEntityTemplate();
		$template = strtr($template, [
			'{class}'    => rtrim($class),
			'{attr}'     => rtrim(implode(",\r\n$this->indent", $attrs)),
			'{casts}'    => rtrim(implode(",\r\n$this->indent", $casts)),
			'{datamaps}' => rtrim(implode(",\r\n$this->indent", $datamaps)),
			'{prop}'     => rtrim(implode("\r\n", $props)),
		]);
		if (file_exists($path)) {
			$string = file_get_contents($path);
			if (preg_match($this->sectionPatterns, $template, $matches)) {
				$string = preg_replace($this->sectionPatterns, $matches[0], $string);
				file_put_contents($path, $string);
			}
		}
		else
			file_put_contents($path, $template);
		return $this;
	}

	/**
	 * Build a model file based on the given table name.
	 * @param string      $table      The name of the table.
	 * @param bool        $updateOnly Determines if the model should only be updated if it already exists.
	 * @param string|null $modelPath  The path where the model file should be stored. If null, the default model path will be used.
	 * @return self
	 */
	public function buildModel(string $table, bool $updateOnly = FALSE, ?string $modelPath = NULL): self
	{
		$model = pascalize($table);
		$path = $modelPath ? APPPATH . $modelPath . DIRECTORY_SEPARATOR . $model . ".php"
			: APPPATH . DIRECTORY_SEPARATOR . "Models" . DIRECTORY_SEPARATOR . "$model.php";
		if (!file_exists($path) && $updateOnly)
			return $this;

		$db = db_connect();
		$pk = '';
		$model = pascalize($table);
		$entity = pascalize($table) . 'Entity';
		$createdField = 'created_at';
		$updatedField = 'updated_at';
		$deletedField = 'deleted_at';
		$allowedFields = $validationRules = $validateRecord = [];
		$fields = $db->getFieldData($table);
		foreach ($fields as $field) {
			$allowedFields[] = "'$field->name'";
			$validationRules[$field->name] = [];
			if ($field->nullable || $field->primary_key || $field->default !== NULL) $validationRules[$field->name][] = "permit_empty";
			switch ($this->getPhpNativeType($field->type)) {
				case 'double':
				case 'integer':
					$validationRules[$field->name][] = "numeric";
					break;
				case 'json':
					$validationRules[$field->name][] = "valid_json";
					break;
				case 'string':
					$validationRules[$field->name][] = "string";
					switch ($field->type) {
						case 'datetime':
							$field->max_length = 25;
							break;
						case 'date':
							$field->max_length = 10;
							break;
						case 'time':
							$field->max_length = 15;
							break;
					}
					break;
				case 'text':
					$validationRules[$field->name][] = "string";
					$field->max_length = 65535;
					break;
				case 'mediumtext':
					$validationRules[$field->name][] = "string";
					$field->max_length = 16777215;
					break;
				case 'longtext':
					$validationRules[$field->name][] = "string";
					$field->max_length = 4294967295;
					break;
			}

			if ($field->max_length) $validationRules[$field->name][] = "max_length[$field->max_length]";
			if ($field->primary_key) $pk = "$field->name";
			if ($field->name === 'created') $createdField = 'created';
			if ($field->name === 'updated') $updatedField = 'updated';
			if ($field->name === 'deleted') $deletedField = 'deleted';
		}

		$length = max(array_map('strlen', array_keys($validationRules))) + 2;
		foreach ($validationRules as $name => $rules)
			$validateRecord [] = sprintf("%-{$length}s => '%s'", "'$name'", implode("|", $rules));

		$template = $this->getModelTemplate();
		$template = strtr($template, [
			'{table}'           => $table,
			'{pk}'              => $pk,
			'{model}'           => $model,
			'{entity}'          => $entity,
			'{createdField}'    => $createdField,
			'{updatedField}'    => $updatedField,
			'{deletedField}'    => $deletedField,
			'{allowedFields}'   => rtrim(implode(",\r\n$this->indent", $allowedFields)),
			'{validationRules}' => rtrim(implode(",\r\n$this->indent", $validateRecord)),
		]);

		if (file_exists($path)) {
			$string = file_get_contents($path);
			if (preg_match($this->sectionPatterns, $template, $matches)) {
				$string = preg_replace($this->sectionPatterns, $matches[0], $string);
				file_put_contents($path, $string);
			}
		}
		else
			file_put_contents($path, $template);
		return $this;
	}

	/**
	 * Retrieves the PHP native type based on the given field type.
	 * @param string $fieldType The field type to determine the PHP native type for.
	 * @return string The PHP native type corresponding to the given field type.
	 */
	protected function getPhpNativeType(string $fieldType): string
	{
		return match ($fieldType) {
			'tinyint', 'smallint', 'mediumint', 'int', 'integer', 'bigint', 'bit', 'numeric' => 'integer',
			'real', 'double', 'float', 'decimal'                                             => 'double',
			'json'                                                                           => 'json',
			'text', 'mediumtext', 'longtext'                                                 => $fieldType,
			default                                                                          => 'string',
		};
	}

	/**
	 * Retrieves the model template used for building models.
	 * This method reads the content of the ModelTemplate.txt file that is located in the Builder directory.
	 * The template is used as a starting point for creating models in an application.
	 * @return string The content of the model template.
	 */
	protected function getModelTemplate(): string
	{
		return file_get_contents(__DIR__ . "/Builder/Model.template");
	}

	/**
	 * Retrieves the model template used for building models.
	 * This method reads the content of the ModelTemplate.txt file that is located in the Builder directory.
	 * The template is used as a starting point for creating models in an application.
	 * @return string The content of the model template.
	 */
	protected function getModuleModelsTemplate(): string
	{
		return file_get_contents(__DIR__ . "/Builder/ModuleModels.template");
	}

	/**
	 * Retrieves the entity template used for building entities.
	 * @return string The content of the entity template.
	 */
	protected function getEntityTemplate(): string
	{
		return file_get_contents(__DIR__ . "/Builder/Entity.template");
	}

	/**
	 * Rebuilds the models.
	 * This method connects to the database and retrieves a list of tables.
	 * @return self The current instance of the class.
	 */
	public function rebuildModule(string $modulePath = 'Modules', string $modelName = 'Models'): self
	{
		$path = APPPATH . $modulePath . DIRECTORY_SEPARATOR . "$modelName.php";

		if (file_exists($path)) {
			$content = file_get_contents($path);
			$useContent = $propertyContent = [];
			$tables = $this->db->listTables();
			foreach ($tables as $table) {
				$model = pascalize($table);
				$useContent[] = "use App\\$modelName\\$model;";
				$propertyContent[] = " * @property $model \$" . lcfirst($model);
			}
			$template = strtr($this->getModuleModelsTemplate(), [
				'{useContent}'  => rtrim(implode("\r\n", $useContent)),
				'{prosContent}' => rtrim(implode("\r\n", $propertyContent)),
			]);
			if (preg_match($this->sectionPatterns, $template, $matches)) {
				$content = preg_replace($this->sectionPatterns, $matches[0], $content);
				file_put_contents($path, $content);
			}
		}

		return $this;
	}
}