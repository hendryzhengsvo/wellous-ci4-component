<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use RuntimeException;

/**
 * Class ChunkedResponse
 */
class WsLibChunkedResponse
{
    private string $buffer = '';
    private int    $chunkSize;

    /**
     * Class constructor
     * @param int $chunkSize - The size of each chunk in bytes (default: 4096)
     */
    public function __construct(int $chunkSize = 4096)
    {
        ini_set('output_buffering', 'off');
        ob_implicit_flush();
        $this->chunkSize = $chunkSize;
        if ($this->chunkSize < 1)
            $this->chunkSize = 4096;
        $this->noCacheImmediate();
    }

    /**
     * Sets a header for the response
     * @param string       $name   - Name of the header
     * @param string|array $values - Value(s) of the header (can be string or array)
     * @return static
     */
    public function header(string $name, string|array $values): static
    {
        if (!is_array($values))
            $values = [$values];
        header($name . ': ' . implode(', ', $values), TRUE, 200);
        return $this;
    }

    /**
     * Set multiple headers for the response
     * @param array $headers - An array containing the header name as the key and the header value(s) as the value
     *                       Example: ['Content-Type' => 'application/json', 'X-Custom-Header' => 'value']
     * @return static
     */
    public function headers(array $headers): static
    {
        foreach ($headers as $name => $values)
            $this->header($name, $values);
        return $this;
    }

    /**
     * Sets the headers to disable caching immediately.
     * @return static
     */
    public function noCacheImmediate(): static
    {
        $this->headers([
                           'Pragma'        => 'no-cache',
                           'Expires'       => '0',
                           'Last-Modified' => gmdate('D, d M Y H:i:s') . ' GMT',
                           'Cache-Control' => ['no-store', 'no-cache', 'must-revalidate', 'max-age=0'],
                       ]);
        return $this;
    }

    /**
     * Sends a resource to the output.
     * @param resource $resource The resource to send.
     * @param string   $fileName The name of the file to be downloaded. Optional.
     * @param string   $mineType The MIME type of the resource. Optional.
     * @return $this
     */
    public function sendResource($resource, string $fileName = '', string $mineType = ''): static
    {
        if (!empty($fileName))
            $this->header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
        if (!empty($mineType))
            $this->header('Content-Type', $mineType);
        rewind($resource);
        while (!feof($resource)) {
            $this->buffer .= fread($resource, $this->chunkSize);
            $this->flush();
        }
        fclose($resource);
        $this->flush();
        return $this;
    }

    /**
     * Sends a file to the output.
     * @param string $sourceFile The path to the source file.
     * @param string $fileName   The name of the file to be downloaded (optional).
     * @param string $mineType   The mime type of the file (optional).
     * @param bool   $attachment
     * @return static
     */
    public function sendFile(string $sourceFile, string $fileName = '', string $mineType = ''): static
    {
        if (!file_exists($sourceFile))
            throw new RuntimeException('File not found: ' . $sourceFile);
        if (!empty($fileName))
            $this->header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
        if (!empty($mineType))
            $this->header('Content-Type', $mineType);
        $this->buffer = '';
        $fp = fopen($sourceFile, 'rb');
        while (!feof($fp)) {
            $this->buffer .= fread($fp, $this->chunkSize);
            $this->flush();
        }
        $this->flush();
        return $this;
    }

    /**
     * Appends data to the buffer and flushes the buffer if it reaches or exceeds the chunk size.
     * @param string $data The data to be appended to the buffer.
     * @return static
     */
    public function send(string $data): static
    {
        $this->buffer .= $data;
        if (strlen($this->buffer) >= $this->chunkSize)
            $this->flush();
        return $this;
    }

    /**
     * Flushes the buffer to the output.
     * @return static
     */
    public function flush(): static
    {
        if (empty($this->buffer))
            return $this;
        echo $this->buffer;
        $this->buffer = '';
        return $this;
    }
}
