<?php

namespace Wellous\Ci4Component\Libraries;

use Closure;
use InvalidArgumentException;

class WxLibBcMath
{
	public const COMPARE_EQUAL = 0;

	public const COMPARE_LEFT_GRATER = 1;

	public const COMPARE_RIGHT_GRATER = -1;

	protected const DEFAULT_SCALE = 100;

	protected const MAX_BASE = 256;

	protected const BIT_OPERATOR_AND = 'and';

	protected const BIT_OPERATOR_OR = 'or';

	protected const BIT_OPERATOR_XOR = 'xor';
	/**
	 * @var bool
	 */
	protected static bool $trimTrailingZeroes = TRUE;

	/**
	 * @param string $min
	 * @param string $max
	 * @return string
	 */
	public static function rand(string $min, string $max): string
	{
		$max = static::convertScientificNotationToString($max);
		$min = static::convertScientificNotationToString($min);
		$difference = static::add(static::sub($max, $min), '1');
		$randPercent = static::div((string)mt_rand(), (string)mt_getrandmax(), 8);
		return static::add($min, static::mul($difference, $randPercent, 8), 0);
	}

	/**
	 * Converts a number in scientific notation to a string representation.
	 * @param string $number The number in scientific notation.
	 * @return string The number as a string representation.
	 */
	public static function convertScientificNotationToString(string $number): string
	{
		// check if number is in scientific notation, first use stripos as is faster than preg_match
		if (stripos($number, 'E') !== FALSE &&
			preg_match('/(-?(\d+\.)?\d+)E([+-]?)(\d+)/i', $number, $regs)
		) {
			// calculate final scale of number
			$scale = (int)$regs[4] + static::getDecimalsLength($regs[1]);
			$pow = static::pow('10', $regs[4], $scale);
			$number = static::formatTrailingZeroes(
				$regs[3] === '-' ? static::div($regs[1], $pow, $scale) : static::mul($pow, $regs[1], $scale)
			);
		}
		return static::parseNumber($number);
	}

	/**
	 * Calculate the length of the decimal part of a given number.
	 * @param string $number The number to get the decimals length from.
	 * @return int The length of the decimal part of the number.
	 */
	public static function getDecimalsLength(string $number): int
	{
		if (static::isFloat($number))
			return strcspn(strrev($number), '.');
		return 0;
	}

	/**
	 * Check if a given number is a float.
	 * @param string $number The number to check.
	 * @return bool Returns true if the number is a float, false otherwise.
	 */
	protected static function isFloat(string $number): bool
	{
		return str_contains($number, '.');
	}

	/**
	 * Calculates the power of a number.
	 * This method calculates the power of a number using the given base and exponent.
	 * If the exponent is a floating-point number, it uses a different algorithm to calculate the power.
	 * @param string   $base     The base number.
	 * @param string   $exponent The exponent.
	 * @param int|null $scale    Optional. The scale to use for the result.
	 * @return string The result of raising $base to the power of $exponent.
	 */
	public static function pow(string $base, string $exponent, ?int $scale = NULL): string
	{
		$base = static::convertScientificNotationToString($base);
		$exponent = static::convertScientificNotationToString($exponent);
		return static::formatTrailingZeroes(static::isFloat($exponent)
			?
			static::powFractional($base, $exponent, $scale)
			:
			($scale === NULL ? bcpow($base, $exponent) : bcpow($base, $exponent, $scale))
		);
	}

	/**
	 * Format trailing zeroes of a given number.
	 * @param string $number The number to format trailing zeroes for.
	 * @return string The formatted number.
	 */
	protected static function formatTrailingZeroes(string $number): string
	{
		if (static::$trimTrailingZeroes)
			return static::trimTrailingZeroes($number);
		return $number;
	}

	/**
	 * Remove trailing zeroes from a given number.
	 * @param string $number The number to trim the trailing zeroes from.
	 * @return string The trimmed number.
	 */
	protected static function trimTrailingZeroes(string $number): string
	{
		if (static::isFloat($number))
			$number = rtrim($number, '0');
		return rtrim($number, '.') ?: '0';
	}

	/**
	 * Calculates the fractional power of a number.
	 * @param string   $base       The base number.
	 * @param string   $exponent   The exponent number.
	 * @param int|null $scale      The scale to use for the calculation.
	 *                             If not provided, the default scale will be used.
	 * @return string              The calculated result as a string.
	 */
	protected static function powFractional(string $base, string $exponent, ?int $scale = NULL): string
	{
		// we need to increased scale to get correct results and avoid rounding error
		$currentScale = $scale ?? static::getScale();
		$increasedScale = $currentScale * 2;

		// add zero to trim scale
		return static::parseNumber(static::add(
			static::exp(
				static::mul($exponent, static::log($base), $increasedScale)), '0', $currentScale)
		);
	}

	/**
	 * Returns the current scale used by the bcmath functions.
	 * @return int The current scale.
	 */
	public static function getScale(): int
	{
		return bcscale();
	}

	/**
	 * Parses a given number and returns a string representation of it.
	 * @param string $number The number to be parsed.
	 * @return string The parsed number as a string.
	 */
	protected static function parseNumber(string $number): string
	{
		$number = str_replace('+', '', (string)filter_var($number, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION));
		if ($number === '-0' || !is_numeric($number))
			return '0';
		return $number;
	}

	/**
	 * Adds two operands and returns the result as a string.
	 * @param string   $leftOperand  The left operand.
	 * @param string   $rightOperand The right operand.
	 * @param int|null $scale        The scale for the result. Optional.
	 * @return string The result of adding the two operands as a string.
	 */
	public static function add(string $leftOperand, string $rightOperand, ?int $scale = NULL): string
	{
		$leftOperand = static::convertScientificNotationToString($leftOperand);
		$rightOperand = static::convertScientificNotationToString($rightOperand);
		return static::formatTrailingZeroes(($scale === NULL) ? bcadd($leftOperand, $rightOperand) : bcadd($leftOperand, $rightOperand, $scale));
	}

	/**
	 * Calculates the exponential value of the given number.
	 * @param string $number The number to calculate the exponential value for.
	 * @return string The exponential value as a string.
	 **/
	public static function exp(string $number): string
	{
		$scale = static::DEFAULT_SCALE;
		$result = '1';
		for ($i = 299; $i > 0; --$i)
			$result = static::add(static::mul(static::div($result, (string)$i, $scale), $number, $scale), '1', $scale);
		return $result;
	}

	/**
	 * Multiplies two operands and returns the result as a string.
	 * @param string   $leftOperand  The left operand.
	 * @param string   $rightOperand The right operand.
	 * @param int|null $scale        The scale for the result. Optional.
	 * @return string The product of the two operands.
	 */
	public static function mul(string $leftOperand, string $rightOperand, ?int $scale = NULL): string
	{
		$leftOperand = static::convertScientificNotationToString($leftOperand);
		$rightOperand = static::convertScientificNotationToString($rightOperand);
		return static::formatTrailingZeroes(($scale === NULL) ? bcmul($leftOperand, $rightOperand) : bcmul($leftOperand, $rightOperand, $scale));
	}

	/**
	 * Performs division of two given string operands with optional scale.
	 * @param string   $dividend The string representation of the dividend.
	 * @param string   $divisor  The string representation of the divisor.
	 * @param int|null $scale    Optional. The scale of the result. If not provided, the result will be returned as is.
	 * @return string The result of the division as a string.
	 * @throws InvalidArgumentException If the divisor is equal to zero.
	 */
	public static function div(string $dividend, string $divisor, ?int $scale = NULL): string
	{
		$dividend = static::convertScientificNotationToString($dividend);
		$divisor = static::convertScientificNotationToString($divisor);
		if (static::trimTrailingZeroes($divisor) === '0')
			throw new InvalidArgumentException('Division by zero');
		return static::formatTrailingZeroes(($scale === NULL) ? bcdiv($dividend, $divisor) : bcdiv($dividend, $divisor, $scale));
	}

	/**
	 * Calculates the logarithm of a number and returns the result as a string.
	 * @param string $number The number to calculate the logarithm for.
	 * @return string The logarithm of the number.
	 */
	public static function log(string $number): string
	{
		$number = static::convertScientificNotationToString($number);
		if ($number === '0')
			return '-INF';
		if (static::COMPARE_RIGHT_GRATER === static::comp($number, '0'))
			return 'NAN';
		$scale = static::DEFAULT_SCALE;
		$m = (string)log((float)$number);
		$x = static::sub(static::div($number, static::exp($m), $scale), '1', $scale);
		$res = '0';
		$pow = '1';
		$i = 1;
		do {
			$pow = static::mul($pow, $x, $scale);
			$sum = static::div($pow, (string)$i, $scale);
			$res = ($i % 2 === 1) ? static::add($res, $sum, $scale) : static::sub($res, $sum, $scale);
			++$i;
		} while (static::comp($sum, '0', $scale));
		return static::add($res, $m, $scale);
	}

	/**
	 * Compares two operands and returns the result as an integer.
	 * If the scale parameter is not provided, the comparison is done using the maximum number of significant digits between the two operands.
	 * @param string   $leftOperand  The left operand.
	 * @param string   $rightOperand The right operand.
	 * @param int|null $scale        The scale for the comparison. Optional.
	 * @return int The result of the comparison:
	 *                               -1 if the left operand is less than the right operand,
	 *                               0 if the left operand is equal to the right operand,
	 *                               1 if the left operand is greater than the right operand.
	 */
	public static function comp(string $leftOperand, string $rightOperand, ?int $scale = NULL): int
	{
		$leftOperand = static::convertScientificNotationToString($leftOperand);
		$rightOperand = static::convertScientificNotationToString($rightOperand);
		if ($scale === NULL)
			return bccomp($leftOperand, $rightOperand, max(strlen($leftOperand), strlen($rightOperand)));
		return bccomp($leftOperand, $rightOperand, $scale);
	}

	/**
	 * Subtracts two operands and returns the result as a string.
	 * @param string   $leftOperand  The left operand.
	 * @param string   $rightOperand The right operand.
	 * @param int|null $scale        The scale for the result. Optional.
	 * @return string The difference of the two operands.
	 */
	public static function sub(string $leftOperand, string $rightOperand, ?int $scale = NULL): string
	{
		$leftOperand = static::convertScientificNotationToString($leftOperand);
		$rightOperand = static::convertScientificNotationToString($rightOperand);
		return static::formatTrailingZeroes(($scale === NULL) ? bcsub($leftOperand, $rightOperand) : bcsub($leftOperand, $rightOperand, $scale));
	}

	/**
	 * Calculates the square root of a number and returns the result as a string.
	 * @param string   $number The number for which to calculate the square root.
	 * @param int|null $scale  The scale for the result. Optional.
	 * @return string The square root of the number.
	 */
	public static function sqrt(string $number, ?int $scale = NULL): string
	{
		$number = static::convertScientificNotationToString($number);
		return static::formatTrailingZeroes(($scale === NULL) ? bcsqrt($number) : bcsqrt($number, $scale));
	}

	/**
	 * Sets the flag to trim trailing zeroes.
	 * @param bool $flag The flag to determine if trailing zeroes should be trimmed or not.
	 * @return void
	 */
	public static function setTrimTrailingZeroes(bool $flag): void
	{
		static::$trimTrailingZeroes = $flag;
	}

	/**
	 * Finds the maximum value from a list of values and returns it as a string.
	 * @param mixed ...$values The values to compare and find the maximum from.
	 * @return string|null The maximum value from the list of values, or null if the list is empty.
	 */
	public static function max(...$values): ?string
	{
		$max = NULL;
		foreach (static::parseValues($values) as $number) {
			$number = static::convertScientificNotationToString((string)$number);
			if ($max === NULL)
				$max = $number;
			elseif (static::comp($max, $number) === static::COMPARE_RIGHT_GRATER)
				$max = $number;
		}
		return $max;
	}

	/**
	 * Parses the input values and returns them as an array.
	 * If the input values are nested in an array, the method will flatten the array and return the flattened values.
	 * @param array $values The input values.
	 * @return array The parsed values.
	 */
	protected static function parseValues(array $values): array
	{
		if (is_array($values[0]))
			$values = $values[0];
		return $values;
	}

	/**
	 * Finds the minimum value among the given values and returns it as a string.
	 * @param mixed ...$values The values to compare.
	 * @return string|null The minimum value among the given values, or null if no values are provided.
	 */
	public static function min(...$values): ?string
	{
		$min = NULL;
		foreach (static::parseValues($values) as $number) {
			$number = static::convertScientificNotationToString((string)$number);
			if ($min === NULL)
				$min = $number;
			elseif (static::comp($min, $number) === static::COMPARE_LEFT_GRATER)
				$min = $number;
		}
		return $min;
	}

	/**
	 * Calculates the power of a number modulo a given modulus and returns the result as a string.
	 * @param string   $base     The base number.
	 * @param string   $exponent The exponent.
	 * @param string   $modulus  The modulus.
	 * @param int|null $scale    The scale for the result. Optional.
	 * @return string The result of raising the base to the power of the exponent modulo the modulus.
	 * @throws InvalidArgumentException If the exponent is negative or if the modulus is zero.
	 */
	public static function powMod(string $base, string $exponent, string $modulus, ?int $scale = NULL): string
	{
		$base = static::convertScientificNotationToString($base);
		$exponent = static::convertScientificNotationToString($exponent);
		if (static::isNegative($exponent))
			throw new InvalidArgumentException('Exponent can\'t be negative');
		if (static::trimTrailingZeroes($modulus) === '0')
			throw new InvalidArgumentException('Modulus can\'t be zero');
		// bcpowmod don't support floats

		return static::formatTrailingZeroes((
			static::isFloat($base)
			|| static::isFloat($exponent)
			|| static::isFloat($modulus)
		)
			? static::mod(static::pow($base, $exponent, $scale), $modulus, $scale)
			:
			($scale === NULL
				?
				bcpowmod($base, $exponent, $modulus)
				:
				bcpowmod($base, $exponent, $modulus, $scale)
			)
		);
	}

	/**
	 * Checks if a number is negative.
	 * @param string $number The number to check.
	 * @return bool Returns true if the number is negative, false otherwise.
	 */
	protected static function isNegative(string $number): bool
	{
		return strncmp('-', $number, 1) === 0;
	}

	/**
	 * Performs the modulus operation on two operands and returns the result as a string.
	 * @param string   $dividend The dividend.
	 * @param string   $divisor  The divisor.
	 * @param int|null $scale    The scale for the result. Optional.
	 * @return string     The remainder of the division.
	 */
	public static function mod(string $dividend, string $divisor, ?int $scale = NULL): string
	{
		// bcmod is not working properly - for example bcmod(9.9999E-10, -0.00056, 9) should return '-0.000559999' but returns 0.0000000
		// let use this $x - floor($x/$y) * $y;
		return static::formatTrailingZeroes(
			static::sub(
				$dividend,
				static::mul(static::floor(static::div($dividend, $divisor, $scale)), $divisor, $scale),
				$scale
			)
		);
	}

	/**
	 * Rounds a number down to the nearest integer and returns the result as a string.
	 * @param string $number The number to be rounded down.
	 * @return string The rounded down value of the number.
	 */
	public static function floor(string $number): string
	{
		$number = static::trimTrailingZeroes(static::convertScientificNotationToString($number));
		if (static::isFloat($number)) {
			$result = 0;
			if (static::isNegative($number))
				--$result;
			$number = static::add($number, (string)$result, 0);
		}
		return static::parseNumber($number);
	}

	/**
	 * Calculates the factorial of a given number and returns the result as a string.
	 * @param string $number The number to calculate the factorial for.
	 * @return string The factorial of the given number.
	 */
	public static function fact(string $number): string
	{
		$number = static::convertScientificNotationToString($number);
		if (static::isFloat($number))
			throw new InvalidArgumentException('Number has to be an integer');
		if (static::isNegative($number))
			throw new InvalidArgumentException('Number has to be greater than or equal to 0');
		$return = '1';
		for ($i = 2; $i <= $number; ++$i)
			$return = static::mul($return, (string)$i);
		return $return;
	}

	/**
	 * Converts a hexadecimal string to its decimal equivalent.
	 * @param string $hex The hexadecimal string to convert.
	 * @return string The decimal equivalent of the hexadecimal string.
	 */
	public static function hexdec(string $hex): string
	{
		$remainingDigits = str_replace('0x', '', substr($hex, 0, -1));
		$lastDigitToDecimal = (string)hexdec(substr($hex, -1));
		if ($remainingDigits === '')
			return $lastDigitToDecimal;
		return static::add(static::mul('16', static::hexdec($remainingDigits)), $lastDigitToDecimal, 0);
	}

	/**
	 * Converts a decimal number to a hexadecimal string representation.
	 * @param string $decimal The decimal number to convert.
	 * @return string The hexadecimal representation of the decimal number.
	 */
	public static function dechex(string $decimal): string
	{
		$quotient = static::div($decimal, '16', 0);
		$remainderToHex = dechex((int)static::mod($decimal, '16'));
		if (static::comp($quotient, '0') === static::COMPARE_EQUAL)
			return $remainderToHex;
		return static::dechex($quotient) . $remainderToHex;
	}

	/**
	 * Performs a bitwise AND operation on two operands and returns the result as a string.
	 * @param string $leftOperand  The left operand.
	 * @param string $rightOperand The right operand.
	 * @return string The result of the bitwise AND operation.
	 */
	public static function bitAnd(
		string $leftOperand,
		string $rightOperand
	): string
	{
		return static::bitOperatorHelper($leftOperand, $rightOperand, static::BIT_OPERATOR_AND);
	}

	/**
	 *
	 */
	protected static function bitOperatorHelper(string $leftOperand, string $rightOperand, string $operator): string
	{
		$leftOperand = static::convertScientificNotationToString($leftOperand);
		$rightOperand = static::convertScientificNotationToString($rightOperand);
		if (static::isFloat($leftOperand))
			throw new InvalidArgumentException('Left operator has to be an integer');
		if (static::isFloat($rightOperand))
			throw new InvalidArgumentException('Right operator has to be an integer');
		$leftOperandNegative = static::isNegative($leftOperand);
		$rightOperandNegative = static::isNegative($rightOperand);
		$leftOperand = static::dec2bin(static::abs($leftOperand));
		$rightOperand = static::dec2bin(static::abs($rightOperand));
		$maxLength = max(strlen($leftOperand), strlen($rightOperand));
		$leftOperand = static::alignBinLength($leftOperand, $maxLength);
		$rightOperand = static::alignBinLength($rightOperand, $maxLength);
		if ($leftOperandNegative)
			$leftOperand = static::recalculateNegative($leftOperand);
		if ($rightOperandNegative)
			$rightOperand = static::recalculateNegative($rightOperand);
		$isNegative = FALSE;
		$result = '';
		if (static::BIT_OPERATOR_AND === $operator) {
			$result = $leftOperand & $rightOperand;
			$isNegative = ($leftOperandNegative and $rightOperandNegative);
		}
		elseif (static::BIT_OPERATOR_OR === $operator) {
			$result = $leftOperand | $rightOperand;
			$isNegative = ($leftOperandNegative or $rightOperandNegative);
		}
		elseif (static::BIT_OPERATOR_XOR === $operator) {
			$result = $leftOperand ^ $rightOperand;
			$isNegative = ($leftOperandNegative xor $rightOperandNegative);
		}
		if ($isNegative)
			$result = static::recalculateNegative($result);
		$result = static::bin2dec($result);
		return $isNegative ? '-' . $result : $result;
	}

	/**
	 * Converts a decimal number to a binary number.
	 * @param string $number The decimal number to convert.
	 * @param int    $base   The base to convert to (default is self::MAX_BASE).
	 * @return string The binary representation of the decimal number.
	 */
	public static function dec2bin(string $number, int $base = self::MAX_BASE): string
	{
		return static::decBaseHelper(
			$base,
			static function (int $base) use ($number) {
				$value = '';
				if ($number === '0')
					return chr((int)$number);
				while (static::comp($number, '0') !== static::COMPARE_EQUAL) {
					$rest = static::mod($number, (string)$base);
					$number = static::div($number, (string)$base);
					$value = chr((int)$rest) . $value;
				}
				return $value;
			}
		);
	}

	/**
	 * Executes a closure with a specified base and returns the result as a string.
	 * @param int     $base    The base to be used.
	 * @param Closure $closure The closure to be executed.
	 * @return string The result of the closure execution.
	 * @throws InvalidArgumentException if the base is invalid.
	 */
	protected static function decBaseHelper(int $base, Closure $closure): string
	{
		if ($base < 2 || $base > static::MAX_BASE)
			throw new InvalidArgumentException('Invalid Base: ' . $base);
		$orgScale = static::getScale();
		static::setScale(0);
		$value = $closure($base);
		static::setScale($orgScale);
		return $value;
	}

	/**
	 * Sets the scale for mathematical operations using the BC Math functions.
	 * @param int $scale The scale to set.
	 * @return void
	 */
	public static function setScale(int $scale): void
	{
		bcscale($scale);
	}

	/**
	 * Returns the absolute value of a number as a string.
	 * @param string $number The number to get the absolute value of.
	 * @return string The absolute value of the number.
	 */
	public static function abs(string $number): string
	{
		$number = static::convertScientificNotationToString($number);
		if (static::isNegative($number))
			$number = substr($number, 1);
		return static::parseNumber($number);
	}

	/**
	 * Aligns the binary length of a string by padding it with zeros to a specified length.
	 * @param string $string The string to align the binary length of.
	 * @param int    $length The desired length of the binary representation.
	 * @return string The string with the aligned binary length.
	 */
	protected static function alignBinLength(string $string, int $length): string
	{
		return str_pad($string, $length, static::dec2bin('0'), STR_PAD_LEFT);
	}

	/**
	 * Recalculates the negative of a given number and returns the result as a string.
	 * @param string $number The number to recalculate.
	 * @return string The negative value of the input number.
	 */
	protected static function recalculateNegative(string $number): string
	{
		$xor = str_repeat(static::dec2bin((string)(static::MAX_BASE - 1)), strlen($number));
		$number ^= $xor;
		for ($i = strlen($number) - 1; $i >= 0; --$i) {
			$byte = ord($number[$i]);
			if (++$byte !== static::MAX_BASE) {
				$number[$i] = chr($byte);
				break;
			}
		}

		return $number;
	}

	/**
	 * Converts a binary number to its decimal representation.
	 * @param string $binary The binary number to convert.
	 * @param int    $base   The base of the binary number. Optional, defaults to self::MAX_BASE.
	 * @return string The decimal representation of the binary number.
	 */
	public static function bin2dec(string $binary, int $base = self::MAX_BASE): string
	{
		return static::decBaseHelper($base,
			static function (int $base) use ($binary) {
				$size = strlen($binary);
				$return = '0';
				for ($i = 0; $i < $size; ++$i) {
					$element = ord($binary[$i]);
					$power = static::pow((string)$base, (string)($size - $i - 1));
					$return = static::add($return, static::mul((string)$element, $power));
				}
				return $return;
			}
		);
	}

	/**
	 * Performs a bitwise OR operation on two operands and returns the result as a string.
	 * @param string $leftOperand  The left operand.
	 * @param string $rightOperand The right operand.
	 * @return string The result of the bitwise OR operation.
	 */
	public static function bitOr(string $leftOperand, string $rightOperand): string
	{
		return static::bitOperatorHelper($leftOperand, $rightOperand, static::BIT_OPERATOR_OR);
	}

	/**
	 * Performs a bitwise XOR operation on two operands and returns the result as a string.
	 * @param string $leftOperand  The left operand.
	 * @param string $rightOperand The right operand.
	 * @return string              The result of the bitwise XOR operation.
	 */
	public static function bitXor(string $leftOperand, string $rightOperand): string
	{
		return static::bitOperatorHelper($leftOperand, $rightOperand, static::BIT_OPERATOR_XOR);
	}

	/**
	 * Rounds a number to a specified precision using the Round Half Even algorithm.
	 * @param string $number    The number to be rounded.
	 * @param int    $precision The number of decimal places to round to. Optional, defaults to 0.
	 * @return string The rounded number.
	 */
	public static function roundHalfEven(string $number, int $precision = 0): string
	{
		$number = static::convertScientificNotationToString($number);
		if (!static::isFloat($number))
			return $number;
		$precessionPos = strpos($number, '.') + $precision + 1;
		if (strlen($number) <= $precessionPos)
			return static::round($number, $precision);
		if ($number[-1] !== '5')
			return static::round($number, $precision);
		$isPrevEven = $number[$precessionPos - 1] === '.'
			? (int)$number[$precessionPos - 2] % 2 === 0
			: (int)$number[$precessionPos - 1] % 2 === 0;
		$isNegative = static::isNegative($number);
		if ($isPrevEven === $isNegative)
			return static::roundUp($number, $precision);
		return static::roundDown($number, $precision);
	}

	/**
	 * Rounds a number to a specified precision and returns the result as a string.
	 * @param string $number    The number to round.
	 * @param int    $precision The number of decimal places to round to. Optional, defaults to 0.
	 * @return string The rounded number.
	 */
	public static function round(string $number, int $precision = 0): string
	{
		$number = static::convertScientificNotationToString($number);
		if (static::isFloat($number)) {
			$number = static::isNegative($number)
				?
				static::sub($number, '0.' . str_repeat('0', $precision) . '5', $precision)
				:
				static::add($number, '0.' . str_repeat('0', $precision) . '5', $precision);
		}
		return static::parseNumber($number);
	}

	/**
	 * Rounds up a number to a specified precision and returns the result as a string.
	 * @param string $number    The number to round up.
	 * @param int    $precision The number of decimal places to round up to. Optional, default is 0.
	 * @return string The rounded up number.
	 */
	public static function roundUp(string $number, int $precision = 0): string
	{
		$number = static::convertScientificNotationToString($number);
		if (!static::isFloat($number))
			return $number;
		$multiply = static::pow('10', (string)abs($precision));
		return static::parseNumber($precision < 0
			?
			static::mul(
				static::ceil(static::div($number, $multiply, static::getDecimalsLength($number))),
				$multiply,
				(int)abs($precision)
			)
			:
			static::div(
				static::ceil(static::mul($number, $multiply, static::getDecimalsLength($number))),
				$multiply,
				$precision
			)
		);
	}

	/**
	 * Rounds up a number to the nearest integer greater than or equal to it and returns the result as a string.
	 * @param string $number The number to round up.
	 * @return string The rounded up number.
	 */
	public static function ceil(string $number): string
	{
		$number = static::trimTrailingZeroes(static::convertScientificNotationToString($number));
		if (static::isFloat($number)) {
			$result = 1;
			if (static::isNegative($number))
				--$result;
			$number = static::add($number, (string)$result, 0);
		}
		return static::parseNumber($number);
	}

	/**
	 * Rounds down a number to a specified decimal precision and returns it as a string.
	 * @param string $number    The number to round down.
	 * @param int    $precision The decimal precision to round down to. Optional, defaults to 0.
	 * @return string The rounded down number.
	 */
	public static function roundDown(string $number, int $precision = 0): string
	{
		$number = static::convertScientificNotationToString($number);
		if (!static::isFloat($number))
			return $number;
		$multiply = static::pow('10', (string)abs($precision));
		return static::parseNumber($precision < 0
			?
			static::mul(
				static::floor(static::div($number, $multiply, static::getDecimalsLength($number))),
				$multiply,
				(int)abs($precision)
			)
			:
			static::div(
				static::floor(static::mul($number, $multiply, static::getDecimalsLength($number))),
				$multiply,
				$precision
			)
		);
	}
}