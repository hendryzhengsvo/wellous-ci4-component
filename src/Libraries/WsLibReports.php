<?php
declare(strict_types = 1);

namespace Wellous\Ci4Component\Libraries;

use Generator;
use ReflectionClass;
use Throwable;
use Wellous\Ci4Component\Enum\EnumReportType;

/**
 * Class WsLibReports
 */
class WsLibReports {
	protected static array $config = [];
	protected const REPORT_URL = 'https://report-api.fiv5s.com/';
	
	/**
	 * Initializes the software with the specified configuration.
	 * @param array|null $config An array of configuration options.
	 * @return void
	 */
	public static function initial(?array $config = []): void
	{
		if(!empty($config))
			static::$config = $config;
	}
	
	/**
	 * Get the base URL for the application.
	 * This method returns the base URL for the application. If the configuration
	 * array has a 'url' key, its value will be returned. Otherwise, the constant
	 * REPORT_URL will be returned.
	 * @return string The base URL for the application.
	 */
	private static function getBaseUrl(bool $external = FALSE): string
	{
		if($external)
			return static::$config['url'] ?? static::REPORT_URL;
		else
			return static::$config['internal-url'] ?? static::$config['url'] ?? static::REPORT_URL;
	}
	
	/**
	 * Generate a signature for API authentication with an optional expiration time.
	 * @param int $expires The expiration time in seconds. Default is 300 seconds (5 minutes).
	 * @return string The generated signature.
	 */
	private static function signature(string $url, int $expires): string
	{
		$secret = sha1(str_replace(['https://', 'http://'], '', self::getBaseUrl()).$url);
		return WsLibUtilities::encrypt(
			WsLibUtilities::generateToken(static::$config['apiKey'] ?? '', static::$config['apiSecret'] ?? '').
			' expires='.
			(time() + $expires),
			$secret);
	}
	
	/**
	 * Send a GET request to a specified API endpoint.
	 * @param string       $url    The URL of the API endpoint.
	 * @param string|array $params An array of parameters to be sent in the request.
	 * @return bool|string|array Returns the response from the API endpoint, or FALSE if an error occurred.
	 */
	protected static function getApi(string $url, string | array $params = [], int $expires = 300): bool | string | array
	{
		$result                     = FALSE;
		$params['report-signature'] = self::signature($url, $expires);
		try
		{
			$request = [
				'base_uri' => self::getBaseUrl(),
				'timeout'  => static::$config['timeout'] ?? 100,
				'verify'   => FALSE,
				'query'    => $params,
			];
			$result  = WsLibRpc::get($url, $request);
		} catch(Throwable $ex)
		{
			static::handleException($ex);
		} finally
		{
			return $result;
		}
	}
	
	/**
	 * Handle an exception and log the error details.
	 * @param Throwable $ex The exception that occurred.
	 * @return void
	 */
	protected static function handleException(Throwable $ex): void
	{
		$content = FALSE;
		if($response = WsLibRpc::getLastResponse())
		{
			$content = $response->getBody()->getContents();
			if($response->hasHeader('content-type') && str_contains($response->getHeader('content-type')[0], '/json'))
				$content = json_decode($content, TRUE) ?: $content;
		}
		$reflect = new ReflectionClass($ex);
		$error   = [
				'status'    => 'exception',
				'app'       => APP_NAME ?? ($_SERVER['HTTP_HOST'] ?? ''),
				'domain'    => $_SERVER['HTTP_HOST'] ?? '',
				'type'      => $reflect->getShortName(),
				'exception' => [
					'message' => $ex->getMessage(),
					'trace'   => $ex->getTraceAsString(),
				],
			] + ($response ? ['response' => $content] : []);
		log_message('error', json_encode($error, JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
	}
	
	/**
	 * Get a report from the API.
	 * @param string $code          The code of the report.
	 * @param array  $params        Additional parameters to pass to the API.
	 * @param int    $page          The page number of the report.
	 * @param int    $pageSize      The number of items per page.
	 * @return bool|string|array    Returns the report data.
	 *                              - If the API response is JSON, it is decoded and returned as an array.
	 *                              - If the API response is not JSON, it is returned as a string.
	 */
	public static function getReport(string $code, array $params = [], int $page = 1, int $pageSize = 50): bool | string | array
	{
		WsLibProfiler::addCheckPoint('reports');
		$result = static::getApi("report/$code", [
			...$params,
			...[
				'report-page-size'   => $pageSize,
				'report-page-number' => $page,
			],
		]);
		if(isset($result['sql']))
			WsLibProfiler::addLog('reports', $result['sql']);
		return $result;
	}
	
	/**
	 * Get a report from the API.
	 * @param string $code          The code of the report.
	 * @param array  $params        Additional parameters to pass to the API.
	 * @return Generator Returns the report data.
	 *                              - If the API response is JSON, it is decoded and returned as an array.
	 *                              - If the API response is not JSON, it is returned as a string.
	 */
	public static function retrieveReport(string $code, array $params = [], $bufferSize = 50): Generator
	{
		$page = 1;
		do
		{
			WsLibProfiler::addCheckPoint('reports');
			$result = static::getReport($code, $params, $page, $bufferSize);
			if(isset($result['sql']))
				WsLibProfiler::addLog('reports', $result['sql']);
			foreach($result['data'] as $row)
				yield $row;
		} while($page++ < $result['pagination']['max_page'] ?? 1);
	}
	
	/**
	 * Get statistics for a specific code.
	 * @param string $code   The code for which to retrieve statistics.
	 * @param array  $params Additional parameters for the API request.
	 * @return bool|string|array Returns the statistics data if successful, otherwise returns false.
	 */
	public static function getStatistics(string $code, array $params = []): bool | string | array
	{
		WsLibProfiler::addCheckPoint('statistics');
		$result = static::getApi("statistics/$code", $params);
		if(isset($result['sql']))
			WsLibProfiler::addLog('statistics', $result['sql']);
		return $result;
	}
	
	/**
	 * Download a report of a specified type and code.
	 * @param EnumReportType $type   The type of the report.
	 * @param string         $code   The code of the report.
	 * @param array          $params Additional parameters for the report.
	 * @param bool           $raw    Generate styled/raw report. Default is FALSE.
	 * @return bool|string|array Returns the downloaded report. Can be of type bool, string, or array depending on the report.
	 */
	public static function downloadReport(EnumReportType $type, string $code, array $params = [], bool $raw = FALSE): bool | string | array
	{
		return static::getApi("report/$code.$type->value", [...$params, 'report-raw' => $raw ? '1' : '0']);
	}
	
	/**
	 * Download a report of a specified type and code.
	 * @param EnumReportType $type   The type of the report.
	 * @param string         $code   The code of the report.
	 * @param array          $params Additional parameters for the report.
	 * @return bool|string|array Returns the downloaded report. Can be of type bool, string, or array depending on the report.
	 */
	public static function downloadStatistics(EnumReportType $type, string $code, array $params = []): bool | string | array
	{
		return static::getApi("statistics/$code.$type->value", $params);
	}
	
	/**
	 * Get the report link for a given report type and code.
	 * @param EnumReportType $type    The report type.
	 * @param string         $code    The report code.
	 * @param array          $params  Additional parameters for the report query string. Default is an empty array.
	 * @param int            $expiras The expiration time for the signature in seconds. Default is 300 seconds.
	 * @return bool|string|array The report link.
	 */
	public static function getReportLink(
		EnumReportType $type, string $code, array $params = [], string $fileName = NULL, bool $raw = FALSE,
		int            $expiras = 300): bool | string | array
	{
		$params = [
			...$params,
			'report-raw'       => $raw ? '1' : '0',
			'report-signature' => self::signature("report/$code.$type->value", $expiras),
		];
		if(!empty($fileName))
			$params['report-filename'] = $fileName;
		return self::getBaseUrl(TRUE)."report/$code.$type->value?".http_build_query($params);
	}
	
	/**
	 * Get the report link for a given report type and code.
	 * @param EnumReportType $type    The report type.
	 * @param string         $code    The report code.
	 * @param array          $params  Additional parameters for the report query string. Default is an empty array.
	 * @param int            $expiras The expiration time for the signature in seconds. Default is 300 seconds.
	 * @return bool|string|array The report link.
	 */
	public static function getStatisticsLink(
		EnumReportType $type, string $code, array $params = [], string $fileName = NULL, bool $raw = FALSE,
		int            $expiras = 300): bool | string | array
	{
		$params = [
			...$params,
			'report-raw'       => $raw ? '1' : '0',
			'report-signature' => self::signature("statistics/$code.$type->value", $expiras),
		];
		if(!empty($fileName))
			$params['report-filename'] = $fileName;
		return self::getBaseUrl(TRUE)."statistics/$code.$type->value?".http_build_query($params);
	}
}
