<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use Predis\Client;
use Predis\ClientException;
use Predis\Command\CommandInterface;
use Predis\Connection\Cluster\RedisCluster;
use Predis\Connection\NodeConnectionInterface;
use Predis\Connection\ParametersInterface;
use Predis\Connection\Replication\SentinelReplication;
use Predis\Connection\StreamConnection;
use Predis\Response\Status;
use Throwable;
use Wellous\Ci4Component\Interface\WsInterfaceRedis;
use Wellous\Ci4Component\Libraries\Redis\WsLibRedisLock;
use Wellous\Ci4Component\Libraries\Redis\WsLibRedisQueue;

/**
 * Class WsToolRedisClient
 * This class extends the Client class and provides additional functionality for connecting to a Redis server,
 * executing Redis commands, and managing Redis data.
 * @package Namespace\Here
 */
abstract class WsLibRedis extends Client implements WsInterfaceRedis
{
	/**
	 * @var array WsLibRedisLock[] The array of locks associated with the Redis client.
	 */
	private array $locks = [];
	/**
	 * @var array WsLibRedisQueue[] The array of queues associated with the Redis client.
	 */
	private array $queues = [];

	private string $prefix = '';

	/**
	 * Class constructor.
	 * Initializes a new Redis instance with optional parameters and options.
	 * @param array $parameters The optional parameters to be passed to the parent constructor.
	 * @param array $options    The optional options to be passed to the parent constructor.
	 * @return void
	 */
	public function __construct(array $parameters = [], array $options = [])
	{
		WsLibProfiler::addCheckPoint("redis");
		try {
			$this->prefix = $options['prefix'] ?? '';
			parent::__construct($parameters, $options);
			$this->logConnection();
		} catch (Throwable $ex) {
			WsLibProfiler::addLog("redis", "connect failed, " . $ex->getMessage());
		}
	}

	public function prefix()
	{
		return $this->prefix;
	}

	public function __destruct()
	{
		$this->disconnect();
	}

	/**
	 * Creates or retrieves an instance of WsLibRedisLock for a given key.
	 * @param string $key The key used to identify the lock.
	 * @return WsLibRedisLock An instance of WsLibRedisLock for the provided key.
	 */
	public function lock(string $key = 'default'): WsLibRedisLock
	{
		if (!isset($this->locks[$key]))
			$this->locks[$key] = new WsLibRedisLock($this, $key);
		return $this->locks[$key];
	}

	/**
	 * Retrieves the queue associated with a given key.
	 * If a queue with the provided key does not exist, it creates a new queue and adds it to the queues array.
	 * @param string $key The key to identify the queue.
	 * @return WsLibRedisQueue The queue object associated with the provided key.
	 */
	public function queue(string $key = 'default'): WsLibRedisQueue
	{
		if (!isset($this->queues[$key]))
			$this->queues[$key] = new WsLibRedisQueue($this, $key);
		return $this->queues[$key];
	}

	/**
	 * Logs the connection details for the Redis connection.
	 * This method logs the connection details for the Redis connection based on the type of connection object.
	 * @return void
	 * @throws ClientException
	 */
	private function logConnection(): void
	{
		/** @var ParametersInterface $parameters */
		/** @var StreamConnection|SentinelReplication|RedisCluster $connection */
		$connection = $this->getConnection();
		if ($connection instanceof StreamConnection) {
			$parameters = $connection->getParameters();
			WsLibProfiler::addLog("redis", "connect redis://$parameters->host:$parameters->port/$parameters->database");
		}
		elseif ($connection instanceof SentinelReplication) {
			/** @var NodeConnectionInterface $connect */
			$parameters = $connection->getSentinelConnection()->getParameters();
			WsLibProfiler::addLog("redis", "connect redis://$parameters->host:$parameters->port/$parameters->database");
		}
		elseif ($connection instanceof RedisCluster) {
			/** @var NodeConnectionInterface $connect */
			foreach ($connection->getIterator() as $connect) {
				$parameters = $connect->getParameters();
				WsLibProfiler::addLog("redis", "connect redis://$parameters->host:$parameters->port/$parameters->database");
			}
		}
	}

	/**
	 * Connects to the Redis server and logs the connection status.
	 */
	public function connect(): void
	{
		WsLibProfiler::addCheckPoint("redis");
		try {
			parent::connect();
			$this->logConnection();
		} catch (Throwable $ex) {
			WsLibProfiler::addLog("redis", "connect failed, " . $ex->getMessage());
		}
		WsLibProfiler::addLog("redis", "connect");
	}

	/**
	 * Disconnects from the Redis server.
	 * This method disconnects from the Redis server by adding a checkpoint to the WsLibProfiler,
	 * calling the parent class's disconnect() method, and adding a log entry to the WsLibProfiler.
	 * @return void
	 */
	public function disconnect(): void
	{
		WsLibProfiler::addCheckPoint("redis");
		parent::disconnect();
		WsLibProfiler::addLog("redis", "Disconnect");
	}

	/**
	 * Executes a command by calling the parent executeCommand method and logs the command details.
	 * @param CommandInterface $command The command to be executed.
	 * @return mixed The result of executing the command.
	 */
	public function executeCommand(CommandInterface $command): mixed
	{
		WsLibProfiler::addCheckPoint("redis");
		$result = parent::executeCommand($command);
		WsLibProfiler::addLog("redis", "{$command->getId()} " . $this->argsToString($command->getArguments()), type: "redis");
		return $result;
	}

	/**
	 * Converts an array of arguments into a string representation.
	 * @param array $arguments The array of arguments to be converted.
	 * @return string The string representation of the provided arguments.
	 */
	protected function argsToString(array $arguments = []): string
	{
		$result = '';
		$maxSize = 100;
		foreach ($arguments as $item) {
			switch (gettype($item)) {
				case 'string':
					$item = strlen($item) > $maxSize ? substr($item, 0, $maxSize) . '...' : $item;
					$item = "'$item'";
					break;
				case 'boolean':
					$item = ($item ? "true" : "false");
					break;
				case 'array':
					$item = '(array)';
					break;
				case 'object':
					$item = '(object)';
					break;
				case 'resource':
				case 'resource (closed)':
					$item = '(resource)';
					break;
				case 'NULL':
					$item = '(null)';
					break;
				case 'unknown type':
					$item = '(unknown)';

			}
			$result .= (!empty($result) ? ' ' : '') . $item;
		}
		return $result;
	}

	/**
	 * Retrieves the value associated with a given key from a serialized string.
	 * @param string $key The key to search for in the serialized string.
	 * @return mixed|null The value associated with the provided key, or null if the key is not found or the string is invalid.
	 */
	public function getUnserialize(string $key): mixed
	{
		if ($result = $this->get($key)) {
			try {
				return unserialize($result);
			} catch (Throwable) {
				return NULL;
			}
		}
		else
			return NULL;
	}

	/**
	 * Retrieves the value associated with a given key from a serialized JSON string.
	 * @param string $key     The key to search for in the serialized JSON string.
	 * @param string $jsonKey The JSON key to check for the value associated with the provided key.
	 * @return mixed|null The value associated with the provided key, or null if the key is not found or the JSON string is invalid.
	 */
	public function getUnserializeValue(string $key, string $jsonKey): ?array
	{
		if ($result = $this->get($key)) {
			try {
				$data = unserialize($result);
				return WsLibUtilities::arrayGetValue($data ?? [], $jsonKey);
			} catch (Throwable) {
				return NULL;
			}
		}
		else
			return NULL;
	}

	/**
	 * Sets the serialized value for a given key in the cache.
	 * @param string $key       The key for which the value is to be set.
	 * @param mixed  $value     The value to be serialized and set.
	 * @param int    $ttl       The time-to-live in seconds for the key-value pair. Defaults to 0 (no expiration).
	 * @param bool   $notExists Flag indicating whether to set the value only if the key does not already exist. Defaults to FALSE.
	 * @return int|Status The result of the operation. If successful, returns a positive integer indicating the number of bytes written.
	 *                          If unsuccessful, returns a Status object that encapsulates the reason for failure.
	 */
	public function setSerialize(string $key, mixed $value, int $ttl, bool $notExists = FALSE): Status|int
	{
		$value = serialize($value);
		if ($notExists) {
			$result = $this->setnx($key, $value);
			if (!empty($ttl))
				return $this->expire($key, $ttl);
			else
				return $result;
		}
		elseif (!empty($ttl))
			return $this->setex($key, $ttl, $value);
		else
			return $this->set($key, $value);
	}

	/**
	 * Removes and returns the member with the highest score from a sorted set.
	 * @param string $key   The key of the sorted set.
	 * @param int    $count The maximum number of members to remove and return. Default is 1.
	 * @return mixed        The member(s) with the highest score, or null if the sorted set is empty or does not exist.
	 */
	public function zPopMax(string $key, int $count = 1): mixed
	{
		return $this->__call(__FUNCTION__, [$this->prefix . $key, $count]);
	}

	/**
	 * Removes and returns the smallest scored elements from a sorted set.
	 * @param string $key   The key of the sorted set.
	 * @param int    $count (optional) The number of elements to remove and return. Default is 1.
	 * @return mixed The smallest scored elements from the sorted set. Returns NULL if the sorted set
	 *                      is empty or the key does not exist.
	 */
	public function zPopMin(string $key, int $count = 1): mixed
	{
		return $this->__call(__FUNCTION__, [$this->prefix . $key, $count]);
	}
}