<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

defined('REQUEST_TIME') || define('REQUEST_TIME', (int)microtime(TRUE));
defined('REQUEST_TIME_FLOAT') || define('REQUEST_TIME_FLOAT', microtime(TRUE));

use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeImmutable;
use Throwable;

class WsLibDateTimeMethod
{
	private static ?int   $timestamp      = NULL;
	private static ?float $timestampFloat = NULL;

	/**
	 * Set a checkpoint for a given timestamp.
	 * If no timestamp is provided, the current time will be used.
	 * @param float|null $timestamp The timestamp to set as the checkpoint. Defaults to the current time.
	 * @return void
	 */
	public static function setCheckpoint(float $timestamp = NULL): void
	{
		if (empty($timestamp))
			$timestamp = microtime(TRUE);
		static::$timestamp = (int)$timestamp;
		static::$timestampFloat = $timestamp;
	}

	/**
	 * Clear the checkpoint timestamps.
	 * @return void
	 */
	public static function clearCheckpoint(): void
	{
		static::$timestamp = NULL;
		static::$timestampFloat = NULL;
	}

	/**
	 * Get the current Unix timestamp with microseconds.
	 * @param bool $current (optional) If TRUE, return the current microtime. Default to FALSE.
	 * @return float The Unix timestamp with microseconds.
	 */
	public static function microtime(bool $current = FALSE): float
	{
		return $current === TRUE ? microtime(TRUE) : static::$timestampFloat ?? REQUEST_TIME_FLOAT;
	}

	/**
	 * Get the current timestamp or a specific timestamp.
	 * @param bool $current Whether to get the current timestamp or not. Default is FALSE.
	 * @return int The current timestamp if $current is TRUE, otherwise the stored timestamp or the default REQUEST_TIME.
	 */
	public static function timestamp(bool $current = FALSE): int
	{
		return $current === TRUE ? time() : static::$timestamp ?? REQUEST_TIME;
	}

	/**
	 * Get the current date or a specific date in the format Y-m-d.
	 * @param bool $current Optional. Determines whether to return the current date or a specific date. Defaults to FALSE.
	 * @return string The date in the format Y-m-d.
	 */
	public static function date(bool $current = FALSE): string
	{
		return date("Y-m-d", static::timestamp($current));
	}

	/**
	 * Get the current time or a specified time as a string.
	 * @param bool $current Whether to get the current time or a specified time. Default is FALSE.
	 * @return string The time string in the format HH:MM:SS.
	 */
	public static function time(bool $current = FALSE): string
	{
		return date("H:i:s", static::timestamp($current));
	}

	/**
	 * Get the current datetime or a formatted datetime from a given timestamp.
	 * @param bool $current Indicates whether to return the current datetime or a formatted datetime from a given timestamp.
	 * @return string The datetime in the format "Y-m-d H:i:s".
	 */
	public static function datetime(bool $current = FALSE): string
	{
		return date("Y-m-d H:i:s", static::timestamp($current));
	}

	/**
	 * Convert a number of seconds to a time string.
	 * The time string can be formatted in three different sizes:
	 * - 1: Format the time as MM:SS (minutes:seconds).
	 * - 2: Format the time as HH:MM (hours:minutes).
	 * - Default: Format the time as HH:MM:SS (hours:minutes:seconds).
	 * @param int      $seconds The number of seconds to convert.
	 * @param int|null $size    The size of the time string. Optional. Default is null.
	 *                          - 1 for MM:SS format.
	 *                          - 2 for HH:MM format.
	 *                          - null for HH:MM:SS format.
	 * @return string The formatted time string.
	 */
	public static function secondToTime(int $seconds = 0, ?int $size = NULL): string
	{
		switch ($size) {
			case 1:
				$secs = floor($seconds / 60);
				return sprintf('%02d:%02d', $secs, $secs);
			case 2:
				$minutes = floor($seconds / 60);
				$secs = floor($seconds % 60);
				return sprintf('%02d:%02d', $minutes, $secs);
			default:
				$hours = floor($seconds / 3600);
				$minutes = floor(intval($seconds / 60) % 60);
				$secs = floor($seconds % 60);
				return sprintf('%02d:%02d:%02d', $hours, $minutes, $secs);
		}
	}

	/**
	 * Convert a time string to seconds.
	 * @param string $time The time string in the format HH:MM:SS.
	 * @return int The number of seconds.
	 */
	public static function timeToSecond(string $time): int
	{
		$hours = 0;
		$minutes = 0;
		$seconds = 0;
		$time = preg_replace("/^(\d{1,2}):(\d{2})$/", "00:$1:$2", $time);
		sscanf($time, "%d:%d:%d", $hours, $minutes, $seconds);
		return $hours * 3600 + $minutes * 60 + $seconds;
	}

	/**
	 * Calculate the difference between two dates
	 * @param string $fromDate   The starting date (Y-m-d H:i:s format)
	 * @param string $toDate     The ending date (Y-m-d H:i:s format)
	 * @param bool   $absolute   Whether to return the absolute value of the difference
	 * @param string $returnType The unit of measurement for the difference. Valid options are 'seconds', 'minutes', 'hours', and 'days'. Default is 'days'.
	 * @return bool|int The difference between the two dates, in the specified unit of measurement. Returns FALSE if an error occurs.
	 */
	public static function dateDiff(string $fromDate, string $toDate, bool $absolute = FALSE, string $returnType = 'days'): bool|int
	{
		try {
			$fromDate = new DateTimeImmutable($fromDate);
			$toDate = new DateTimeImmutable($toDate);
			$diff = $toDate->diff($fromDate, $absolute);

			return (int)match ($returnType) {
					'seconds' => $diff->s + (int)(($diff->i + (int)(($diff->days * 24 + $diff->h) * 60)) * 60),
					'minutes' => $diff->i + (int)(($diff->h + (int)($diff->days * 24)) * 60),
					'hours'   => $diff->h + (int)($diff->days * 24),
					default   => $diff->days,
				} * (int)$diff->format('%R1');
		} catch (Throwable) {
			return FALSE;
		}
	}

	/**
	 * Generate an array of dates between two given dates (inclusive)
	 * @param string $start Start date in Y-m-d format
	 * @param string $end   End date in Y-m-d format
	 * @return array Array of dates in Y-m-d format
	 */
	public static function dateRange(string $start, string $end): array
	{
		$result = [];
		try {
			$begin = new DateTimeImmutable($start);
			$end = new DateTimeImmutable($end);
			$end = $end->modify('+1 day');
			$period = new DatePeriod($begin, new DateInterval('P1D'), $end);
			foreach ($period as $day)
				/** @var DateTime $day */
				$result[] = $day->format("Y-m-d");
		} catch (Throwable) {
			return [];
		}
		return $result;
	}

	/**
	 * Get the number of months between two dates
	 * @param string $startDate Start date in any format supported by strtotime()
	 * @param string $endDate   End date in any format supported by strtotime()
	 * @return int Number of months between the two dates
	 */
	public static function getMonthBetweenTwoDates(string $startDate, string $endDate): int
	{
		$ts1 = strtotime($startDate);
		$ts2 = strtotime($endDate);

		$year1 = (int)date('Y', $ts1);
		$year2 = (int)date('Y', $ts2);

		$month1 = (int)date('m', $ts1);
		$month2 = (int)date('m', $ts2);

		return (int)((($year2 - $year1) * 12) + ($month2 - $month1));
	}

	/**
	 * Check if a given datetime is valid
	 * @param string $datetime The datetime to check
	 * @return bool Returns true if the datetime is valid, false otherwise
	 */
	public static function checkValidDate(string $datetime): bool
	{
		return ($datetime && strtotime($datetime) && strtotime($datetime) > 0);
	}

	/**
	 * Parse the given value into a string representation using the specified format
	 * @param null|int|string|DateTime|DateTimeImmutable|bool $datetime The value to parse. Can be null, integer, string, DateTime, DateTimeImmutable or boolean.
	 * @param string                                          $format   The format to use for parsing. Default is 'Y-m-d H:i:s'.
	 * @return string The parsed value as a string representation.
	 */
	public static function parse(null|int|string|DateTime|DateTimeImmutable|bool $datetime, string $format = 'Y-m-d H:i:s'): string
	{
		return (string)static::format($format, $datetime);
	}

	/**
	 * Format a date or time string using a given format
	 * @param string                                          $format   The desired format for the date or time (default: 'Y-m-d H:i:s')
	 * @param null|int|string|DateTime|DateTimeImmutable|bool $fromTime The date or time to format. If NULL, the current date and time will be used.
	 *                                                                  If a DateTime or DateTimeImmutable object is provided, it will be formatted using the specified format.
	 *                                                                  If a bool TRUE is provided, the current timestamp will be used.
	 *                                                                  If a string is provided, it will be parsed as a timestamp.
	 *                                                                  If an integer is provided, it will be treated as a timestamp.
	 * @return string|false    The formatted date or time string. FALSE if the formatting failed.
	 */
	public static function format(string $format = 'Y-m-d H:i:s', null|int|string|DateTime|DateTimeImmutable|bool $fromTime = NULL): string|false
	{
		if ($fromTime instanceof DateTime || $fromTime instanceof DateTimeImmutable)
			return $fromTime->format($format);
		elseif ($fromTime === TRUE)
			$fromTime = static::timestamp(TRUE);
		$timestamp = empty($fromTime) ? static::timestamp() : (is_string($fromTime) ? strtotime($fromTime) : $fromTime);
		return $timestamp !== FALSE ? date($format, $timestamp) : FALSE;
	}
}

