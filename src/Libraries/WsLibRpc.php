<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use CodeIgniter\Events\Events;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Wellous\Ci4Component\ArrayObject\RpcBatchRequest;
use Wellous\Ci4Component\Exceptions\ServerBadGateway;
use Wellous\Ci4Component\Exceptions\ServerUnavailable;
use Wellous\Ci4Component\Interface\WsInterfaceRpc;

/**
 * @package Wellous\Ci4Component\Libraries
 * @method static get(string $url, array $options = [], bool $raw = FALSE)
 * @method static head(string $url, array $options = [], bool $raw = FALSE)
 * @method static post(string $url, array $options = [], bool $raw = FALSE)
 * @method static put(string $url, array $options = [], bool $raw = FALSE)
 * @method static delete(string $url, array $options = [], bool $raw = FALSE)
 * @method static connect(string $url, array $options = [], bool $raw = FALSE)
 * @method static options(string $url, array $options = [], bool $raw = FALSE)
 * @method static patch(string $url, array $options = [], bool $raw = FALSE)
 */
class WsLibRpc implements WsInterfaceRpc
{
	protected static ?Client $client = NULL;

	private static bool                 $trigger      = TRUE;
	protected static ?ResponseInterface $lastResponse = NULL;

	/**
	 * Get the last response received from a HTTP request.
	 * @return ResponseInterface|null The last response object or null if no response has been received yet.
	 */
	public static function getLastResponse(): ?ResponseInterface
	{
		return static::$lastResponse;
	}

	/**
	 * @param string $name The name of the method being called.
	 * @param array  $args An array of arguments that are passed to the method being called.
	 * @return mixed The result of the method being called.
	 * @throws GuzzleException
	 * @throws ServerBadGateway
	 * @throws ServerUnavailable
	 * @throws Throwable
	 * @package Wellous\Ci4Component\Libraries
	 *                     This method is a magic method in PHP that allows the calling of static methods dynamically.
	 *                     It is used to handle HTTP requests using different HTTP methods like GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, and PATCH.
	 */
	public static function __callStatic(string $name, array $args = []): mixed
	{
		return static::request(strtoupper($name), ...$args);
	}

	/**
	 * Sends an HTTP request.
	 * @param string     $method      The HTTP method to use (e.g., GET, POST, PUT, DELETE).
	 * @param string     $url         The URL to send the request to.
	 * @param array      $options     (optional) Additional options for the request.
	 * @param bool       $raw         (optional) Whether to return the raw response content. Default is FALSE.
	 * @param array|null $eventParams (optional) Additional parameters for event triggering.
	 * @return mixed The response content. If $raw is FALSE and the response
	 *                                expires JSON content, it will be decoded and returned as an array,
	 *                                otherwise it will be returned as is.
	 * @throws ServerBadGateway
	 * @throws ServerUnavailable
	 * @throws Throwable
	 * @throws GuzzleException
	 * @package Wellous\Ci4Component\Libraries
	 */
	public static function request(string $method, string $url, array $options = [], bool $raw = FALSE, ?array $eventParams = NULL): mixed
	{
		WsLibProfiler::addCheckPoint('rpc');
		static::$lastResponse = NULL;
		$method = strtoupper($method);
		static::prepareOption($method, $options);
		$trigger = static::$trigger;
		try {
			$response = match ($method) {
				"HEAD"   => static::client()->head($url, $options),
				"GET"    => static::client()->get($url, $options),
				"POST"   => static::client()->post($url, $options),
				"PUT"    => static::client()->put($url, $options),
				"DELETE" => static::client()->delete($url, $options),
				default  => static::client()->request($method, $url, $options),
			};
			if (!str_starts_with($url, 'http'))
				$url = ($options['base_uri'] ?? '') . $url;
			WsLibProfiler::addLog('rpc', "$method $url" . (!empty($options['query']) ? '?' . http_build_query($options['query'])
					: '') . (!empty($options['query']) ? ' form: ' . http_build_query($options['query'])
					: '') . ' responseCode: ' . $response->getStatusCode());
			$content = $response->getBody()->getContents();
			$response->getBody()->rewind();
			if ($trigger) {
				static::$trigger = FALSE;
				Events::trigger('rpcSuccess', clone $response, ['url' => $url, 'options' => $options], $eventParams);
				static::$trigger = TRUE;
			}
			static::$lastResponse = $response;
		} catch (ConnectException $ex) {
			$context = $ex->getHandlerContext();
			$url = $context['url'] ?? '';
			$timeout = $context['appconnect_time'] ?? 0;
			WsLibProfiler::addLog('rpc', "$method $url" . (!empty($options['query']) ? '?' . http_build_query($options['query'])
					: '') . (!empty($options['query']) ? ' form: ' . http_build_query($options['query'])
					: '') . ' error: Connection timeout' . (!empty($timeout) ? "($timeout seconds)" : ""));
			if ($trigger) {
				static::$trigger = FALSE;
				Events::trigger('rpcFailed', $ex, ['url' => $url, 'options' => $options, 'timeout' => $timeout], $eventParams);
				static::$trigger = TRUE;
			}
			throw new ServerBadGateway("Connection timeout for $url", previous: $ex);
		} catch (ClientException|ServerException $ex) {
			$responseCode = 0;
			if ($ex->hasResponse()) {
				static::$lastResponse = $ex->getResponse();
				$responseCode = static::$lastResponse->getStatusCode();
			}
			WsLibProfiler::addLog('rpc', "$method $url" . (!empty($options['query']) ? '?' . http_build_query($options['query'])
					: '') . ' error: ' . $ex->getMessage() . " responseCode: $responseCode");
			$content = static::$lastResponse->getBody()->getContents();
			if ($trigger) {
				static::$trigger = FALSE;
				Events::trigger('rpcFailed', $ex, ['url' => $url, 'options' => $options, 'content' => $content], $eventParams);
				static::$trigger = TRUE;
			}
			if (empty($content))
				throw new ServerUnavailable($ex->getMessage(), "Server unavailable to $url", previous: $ex);
		} catch (Throwable $ex) {
			WsLibProfiler::addLog('rpc', "$method $url" . (!empty($options['query']) ? '?' . http_build_query($options['query'])
					: '') . (!empty($options['query']) ? ' form: ' . http_build_query($options['query'])
					: '') . ' error: ' . $ex->getMessage());
			if ($trigger) {
				static::$trigger = FALSE;
				Events::trigger('rpcFailed', $ex, ['url' => $url, 'options' => $options], $eventParams);
				static::$trigger = TRUE;
			}
			throw $ex;
		}
		if (!$raw && static::$lastResponse->hasHeader('content-type') && str_contains(static::$lastResponse->getHeader('content-type')[0], '/json'))
			return json_decode($content, TRUE) ?: $content;
		else
			return $content;
	}

	/**
	 * Prepare options for HTTP request method.
	 * @param string $method  The HTTP request method.
	 * @param array  $options The options for the HTTP request.
	 * @return void
	 */
	protected static function prepareOption(string &$method, array &$options): void
	{
		$method = strtoupper($method);
		if (!empty($options['headers']))
			$options['headers'] = array_change_key_case($options['headers']);
		if (($method === 'JSON' || in_array(($options['headers']['content-type'] ?? ''), ['text/json', 'application/json'], TRUE)) && !empty($options['form_params'])) {
			if ($method === 'JSON')
				$method = 'POST';
			$options['json'] = $options['form_params'];
			unset($options['form_params']);
		}
		if (in_array($method, ['GET', 'HEAD', 'OPTIONS'], TRUE) && (
				(!empty($options['form_params']) && is_array($options['form_params']))
				|| (!empty($options['json']) && is_array($options['json']))
			)
		) {
			if (!empty($options['form_params']) && is_array($options['form_params'])) {
				$options['query'] = (!empty($options['query']) && is_array($options['query']))
					?
					array_merge($options['query'], $options['form_params'])
					:
					$options['form_params'];
				unset($options['form_params']);
			}
			if (!empty($options['json']) && is_array($options['json'])) {
				$options['query'] = (!empty($options['query']) && is_array($options['query']))
					?
					array_merge($options['query'], $options['json'])
					:
					$options['json'];
				unset($options['json']);
			}
		}
	}

	/**
	 * @package Wellous\Ci4Component\Libraries
	 * @method static get(string $url, array $options = [], bool $raw = FALSE)
	 * @method static head(string $url, array $options = [], bool $raw = FALSE)
	 * @method static post(string $url, array $options = [], bool $raw = FALSE)
	 * @method static put(string $url, array $options = [], bool $raw = FALSE)
	 * @method static delete(string $url, array $options = [], bool $raw = FALSE)
	 * @method static connect(string $url, array $options = [], bool $raw = FALSE)
	 * @method static options(string $url, array $options = [], bool $raw = FALSE)
	 * @method static patch(string $url, array $options = [], bool $raw = FALSE)
	 */
	public static function client(array $options = []): Client
	{
		if (static::$client === NULL) {
			$options = array_merge([
				'connect_timeout' => 10,
				'timeout'         => 100,
			], $options);
			static::$client = new Client($options);
		}
		return static::$client;
	}

	/**
	 * Send asynchronous batch requests.
	 * @param array         $requestDatas An array of request data. Each element can be a string URL or an associative array with the following properties:
	 *                                    - url: The URL to send the request to.
	 *                                    - method: The HTTP method to use (default: 'GET').
	 *                                    - headers: An array of headers to include in the request.
	 *                                    - params: An array of parameters to include in the request.
	 *                                    - options: Additional options to pass to the request.
	 * @param callable|null $onFulfilled  (Optional) A callback function to be called when a request is fulfilled.
	 * @param callable|null $onRejected   (Optional) A callback function to be called when a request is rejected.
	 * @param array         $options      (Optional) Additional options for the requests.
	 * @param int           $concurrency  (Optional) The number of requests to send concurrently (default: 10).
	 * @param array|null    $eventParams  (Optional) Additional parameters to pass to the event triggers.
	 * @return void
	 */
	public static function batchRequestAsync(array $requestDatas, ?callable $onFulfilled = NULL, ?callable $onRejected = NULL,
	                                         array $options = [], int $concurrency = 10, ?array $eventParams = NULL): void
	{
		$requestList = [];
		$trigger = static::$trigger;
		foreach ($requestDatas as $key => $requestData) {
			if (is_string($requestData))
				$requestData = ['url' => $requestDatas, 'options' => [], 'method' => 'GET', 'headers' => [], 'params' => []];
			elseif (is_array($requestData) && !empty($requestData['url'])) {
				$requestData['method'] = $requestData['method'] ?? 'GET';
				$requestData['headers'] = $requestData['headers'] ?? [];
				$requestData['params'] = $requestData['params'] ?? [];
				$requestData['options'] = $requestData['options'] ?? [];
			}
			else {
				unset($requestDatas[$key]);
				continue;
			}

			$requestData['options']['headers'] = array_change_key_case($requestData['headers']);
			if (!empty($requestData['params'])) {
				switch ($requestData['options']['headers']['content-type'] ?? '') {
					case 'text/json':
					case 'application/json':
						$requestData['options']['json'] = $requestData['params'];
						break;
					case 'multipart/form-data':
						$requestData['options']['multipart'] = [];
						if (!empty($requestData['options']['headers']['content-type']))
							unset($requestData['options']['headers']['content-type']);
						foreach ($requestData['params'] as $name => $value) {
							if (is_array($value) && !empty($value['name']) && !empty($value['contents']))
								$requestData['options']['multipart'][] = $value;
							else
								$requestData['options']['multipart'][] = [
									'name'     => $name,
									'contents' => $value,
								];
						}
						break;
					default:
						if (gettype($requestData['params']) === 'string') {
							if (empty($requestData['options']['headers']['content-type']))
								$requestData['options']['headers']['content-type'] = 'text/plain';
							$requestData['options']['body'] = $requestData['params'];
						}
						else
							$requestData['options']['form_params'] = $requestData['params'];
				}
			}
			unset($requestData['headers'], $requestData['params']);
			static::prepareOption($requestData['method'], $requestData['options']);
			$requestList[$key] = new RpcBatchRequest($requestData);
			unset($requestDatas[$key]);
		}

		$client = static::client([
				'connect_timeout' => 10,
				'timeout'         => 100,
			] + $options);

		$requests = function ($requestList) use ($client) {
			/** @var RpcBatchRequest $requestItem */
			foreach ($requestList as $key => $requestItem) {
				yield $key => function () use ($client, $requestItem) {
					return $client->requestAsync($requestItem->method, $requestItem->url, $requestItem->options);
				};
			}
		};

		$pool = new Pool($client, $requests($requestList), [
			'options'     => [
					'connect_timeout' => 10,
					'timeout'         => 100,
				] + $options,
			'concurrency' => $concurrency,
			'fulfilled'   => function (Response $response, $key) use ($onFulfilled, $requestList, $trigger, $eventParams) {
				/** @var RpcBatchRequest $requestData */
				$requestData = $requestList[$key];
				WsLibProfiler::addLog('rpc', "$requestData->method $requestData->url" . (!empty($options['query'])
						? '?' . http_build_query($options['query'])
						: '') . (!empty($options['query']) ? ' form: ' . http_build_query($options['query'])
						: '') . ' responseCode: ' . $response->getStatusCode());
				if ($trigger) {
					static::$trigger = FALSE;
					Events::trigger('rpcSuccess', $response, $requestData->toArray(), $eventParams);
					static::$trigger = TRUE;
				}
				if (!empty($onFulfilled))
					$onFulfilled($response, $key);
			},
			'rejected'    => function (Throwable $ex, $key) use ($onRejected, $requestList, $trigger, $eventParams) {
				/** @var RpcBatchRequest $requestData */
				$requestData = $requestList[$key];

				$responseCode = 0;
				if (($ex instanceof ClientException || $ex instanceof ServerException) && $ex->hasResponse()) {
					static::$lastResponse = $ex->getResponse();
					$responseCode = static::$lastResponse->getStatusCode();
				}
				WsLibProfiler::addLog('rpc', "$requestData->method $requestData->url" . (!empty($options['query'])
						? '?' . http_build_query($options['query'])
						: '') . (!empty($options['query']) ? ' form: ' . http_build_query($options['query'])
						: '') . ' error: ' . $ex->getMessage() . " responseCode: $responseCode");

				if ($trigger) {
					static::$trigger = FALSE;
					Events::trigger('rpcFailed', $ex, $requestData->toArray(), $eventParams);
					static::$trigger = TRUE;
				}
				if (!empty($onRejected))
					$onRejected($ex, $key);
			},
		]);
		$promise = $pool->promise();
		$promise->wait();
	}
}