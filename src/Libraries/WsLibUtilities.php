<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use CodeIgniter\Events\Events;
use Config\Services;
use Exception;
use finfo;
use Throwable;
use Traversable;
use Wellous\Ci4Component\Config\WsCfgServices;
use Wellous\Ci4Component\Tool\WsStore;

/**
 * Class Utilities
 * @package Utils
 */
class WsLibUtilities
{
    /**
     * Get method trace
     * This method returns an array of the current method trace.
     * @return array An array containing the method trace.
     */
    public static function trace(): array
    {
        $ex = new Exception();
        $trace = $ex->getTraceAsString();
        $trace = explode("\n", $trace);
        array_shift($trace);
        return $trace;
    }

    /**
     * Converts the given number of bytes to a human-readable size with an optional space between the number and the unit.
     * @param int  $bytes     The number of bytes to convert.
     * @param int  $precision The decimal precision to round the result to. Defaults to 2.
     * @param bool $space     Whether to include a space between the number and the unit. Defaults to false.
     * @return string The human-readable size representation.
     */
    public static function byteToSize(int $bytes, int $precision = 2, bool $space = FALSE): string
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= (1 << (10 * $pow));
        return sprintf('%0.2f', round($bytes, $precision)) . ($space ? ' ' : '') . $units[$pow];
    }

    /**
     * Retrieves information about the caller of the getCallerInfo method.
     * This method uses the debug_backtrace function to retrieve the caller information. The retrieved information includes
     * the class name, function name, arguments, file, and line number. The information is stored in the $info parameter as
     * an associative array.
     * @param array $info [optional] Reference to an array where the caller information will be stored. The array is
     *                    updated with the following keys:
     *                    - class: The name of the class that called the getCallerInfo method.
     *                    - function: The name of the function or method that called the getCallerInfo method.
     *                    - args: The arguments passed to the caller function or method.
     *                    - file: The file name where the caller function or method is defined.
     *                    - line: The line number where the caller function or method is defined.
     * @return string The name of the caller in the format "class::function" if available, otherwise just the function name.
     */
    public static function getCallerInfo(array &$info = [], bool $functionOnly = FALSE): string
    {
        $result = '{root}';
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 3);
        if (isset($backtrace[2])) {
            $info += [
                'class'    => $backtrace[2]['class'] ?? FALSE,
                'function' => $backtrace[2]['function'] ?? FALSE,
                'args'     => $backtrace[2]['args'] ?? FALSE,
                'file'     => $backtrace[1]['file'] ?? FALSE,
                'line'     => $backtrace[1]['line'] ?? FALSE,
            ];
            if (isset($backtrace[2]['class']) && isset($backtrace[2]['function'])) {
                $result = $functionOnly ? $backtrace[2]['function'] : $backtrace[2]['class'] . '::' . $backtrace[2]['function'];
            }
            elseif (isset($backtrace[2]['function'])) {
                $result = $backtrace[2]['function'];
            }
        }

        if ($result === '{root}') {
            $info += [
                'class'    => $backtrace[0]['class'] ?? FALSE,
                'function' => $result,
                'file'     => $backtrace[0]['file'] ?? FALSE,
                'line'     => $backtrace[0]['line'] ?? FALSE,
            ];
        }
        return $result;
    }

    /**
     * Checks if the memory usage is approaching the maximum memory limit.
     * @param int  $bufferMB The buffer size in megabytes. Defaults to 10MB.
     * @param bool $peak     Determines whether to check the peak memory usage. Defaults to FALSE.
     * @return bool Returns TRUE if the memory usage is nearing the maximum memory limit,
     *                       otherwise returns FALSE.
     */
    public static function isNearMaxMem(int $bufferMB = 10, bool $peak = FALSE): bool
    {
        return ($peak ? memory_get_peak_usage() : memory_get_usage()) + $bufferMB * MB > WsStore::$maxMemLimit;
    }

    /**
     * Generates a version 4 UUID (Universally Unique Identifier) using random integer values.
     * @return string The generated UUID.
     */
    public static function genUuidV4(): string
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            static::randomInt(0, 0xffff),
            static::randomInt(0, 0xffff),
            static::randomInt(0, 0xffff),
            static::randomInt(0, 0x0fff) | 0x4000,
            static::randomInt(0, 0x3fff) | 0x8000,
            static::randomInt(0, 0xffff),
            static::randomInt(0, 0xffff),
            static::randomInt(0, 0xffff)
        );
    }

    /**
     * Generates a random integer between the specified minimum and maximum values.
     * If the random_int function is available, it will be used. Otherwise, the mt_rand function will be used.
     * If an error occurs during the random number generation, FALSE will be returned.
     * @param int $min The minimum value of the random integer.
     * @param int $max The maximum value of the random integer.
     * @return bool|int The generated random integer or FALSE if an error occurs.
     */
    public static function randomInt(int $min, int $max): bool|int
    {
        try {
            if (function_exists('random_int')) {
                return random_int($min, $max);
            }
            else
                return @mt_rand($min, $max);
        } catch (Throwable) {
            return FALSE;
        }
    }

    /**
     * Converts the characters in the given content from half-width (半角) to full-width (全角) or vice versa.
     * @param string $content       The content to convert.
     * @param bool   $toFullAngle   Whether to convert to full-angle or not. Defaults to false.
     * @param bool   $includeSymbol Whether to include symbol characters in the conversion. Defaults to false.
     * @return array|string The converted content, with characters either in full-angle or half-angle, based on the provided parameters.
     */
    public static function convertUtfAngle(string $content, bool $toFullAngle = FALSE, bool $includeSymbol = FALSE): array|string
    {
        $fullAngle = [
            'alphanumeric' => [
                '０', '１', '２', '３', '４', '５', '６', '７', '８', '９',
                'Ａ', 'Ｂ', 'Ｃ', 'Ｄ', 'Ｅ', 'Ｆ', 'Ｇ', 'Ｈ', 'Ｉ', 'Ｊ',
                'Ｋ', 'Ｌ', 'Ｍ', 'Ｎ', 'Ｏ', 'Ｐ', 'Ｑ', 'Ｒ', 'Ｓ', 'Ｔ',
                'Ｕ', 'Ｖ', 'Ｗ', 'Ｘ', 'Ｙ', 'Ｚ', 'ａ', 'ｂ', 'ｃ', 'ｄ',
                'ｅ', 'ｆ', 'ｇ', 'ｈ', 'ｉ', 'ｊ', 'ｋ', 'ｌ', 'ｍ', 'ｎ',
                'ｏ', 'ｐ', 'ｑ', 'ｒ', 'ｓ', 'ｔ', 'ｕ', 'ｖ', 'ｗ', 'ｘ',
                'ｙ', 'ｚ',
            ],
            'symbol'       => [
                '－', '　', '：', '．', '，', '／', '％', '＃', '！', '＠',
                '＆', '（', '）', '＜', '＞', '＂', '＇', '？', '［', '］',
                '｛', '｝', '＼', '｜', '＋', '＝', '＿', '＾', '￥', '￣',
                '｀',
            ],
        ];
        $semiAngle = [
            'alphanumeric' => [ //半角
                                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                                'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
                                'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                                'y', 'z',
            ],
            'symbol'       => [
                '-', ' ', ':', '.', ',', '/', '%', '#', '!', '@',
                '&', '(', ')', '<', '>', '"', '\'', '?', '[', ']',
                '{', '}', '\\', '|', '+', '=', '_', '^', '$', '~',
                '`',
            ],
        ];
        if ($includeSymbol) {
            $fullAngle = array_merge($fullAngle['alphanumeric'], $fullAngle['symbol']);
            $semiAngle = array_merge($semiAngle['alphanumeric'], $semiAngle['symbol']);
        }
        else {
            $fullAngle = $fullAngle['alphanumeric'];
            $semiAngle = $semiAngle['alphanumeric'];
        }
        if ($toFullAngle === FALSE)
            return str_replace($fullAngle, $semiAngle, $content);  //全角到半角
        else
            return str_replace($semiAngle, $fullAngle, $content);  //半角到全角
    }

    /**
     * Compresses the given content if its size is greater than or equal to the minimum size specified.
     * @param string $content The content to compress.
     * @param int    $minSize The minimum size at which the content should be compressed. Defaults to 13000 bytes.
     * @return string The compressed content if it meets the minimum size requirement, otherwise the original content.
     */
    public static function compressContent(string $content, int $minSize = 13000): string
    {
        if (strlen($content) >= $minSize)
            return 'base64://' . static::base64UrlEncode(@gzdeflate($content, 9));
        else
            return $content;
    }

    /**
     * Decompress String
     * @param string $compressed The compressed string to decompress
     * @return string The decompressed string
     */
    public static function decompressContent(string $compressed): string
    {
        if (str_starts_with($compressed, 'base64://')) {
            $compressed = static::base64UrlDecode(substr($compressed, 9));
            return @gzinflate($compressed);
        }
        return $compressed;
    }

    /**
     * Apply data to a template string
     * @param string     $template The template string
     * @param array      $data     The data to be applied to the template
     * @param mixed|null $default  The default value if the data key is not found
     * @param string     $pattern  The pattern to match in the template string
     * @return string The template string with data applied
     */
    public static function strTemplateApply(string $template, array $data, mixed $default = NULL, string $pattern = '%([\w]+)[^%]*%'): string
    {
        $matches = [];
        if ($default !== NULL && !is_string($default))
            $default = json_encode($default);
        if (preg_match_all("/($pattern)/", $template, $matches, PREG_PATTERN_ORDER)) {
            $search = [];
            $replace = [];
            foreach ($matches[0] as $index => $match) {
                $keyword = $matches[2][$index];
                if (isset($data[$keyword])) {
                    if (!is_string($data[$keyword]))
                        $data[$keyword] = json_encode($data[$keyword]);
                    $search[] = $match;
                    $replace[] = $data[$keyword];
                }
                elseif ($default !== NULL) {
                    $search[] = $match;
                    $replace[] = $default;
                }
            }
            $template = str_replace($search, $replace, $template);
        }
        return $template;
    }

    /**
     * Get temporary file path
     * @param string $prefix The prefix of the temporary file name (optional)
     * @return bool|string The path to the temporary file or false if an error occurred
     */
    public static function getTempFile(string $prefix = ''): bool|string
    {
        $tempFileName = tempnam(sys_get_temp_dir(), $prefix);
        if ($tempFileName === FALSE)
            return FALSE;
        Events::on('appEnd', function () use ($tempFileName) {
            if (file_exists($tempFileName))
                @unlink($tempFileName);
        });
        return $tempFileName;
    }

    /**
     * Get the full URL including scheme, host, port and path.
     * @param string $path The path to append to the URL (optional).
     * @return string The full URL.
     */
    public static function getFullUrl(string $path = ''): string
    {
        $port = Services::request()->getUri()->getPort();
        $port = $port ? ":$port" : "";
        return sprintf("%s://%s%s/%s", Services::request()->getUri()->getScheme(),
            Services::request()->getUri()->getHost(), $port, $path ?: Services::request()->getUri()->getPath());
    }

    /**
     * Get Client IP
     * Returns the client's IP address. It checks for Cloudflare headers, IPs passing through proxies,
     * Real IP, and falls back to REMOTE_ADDR if no IP address is found.
     * @return string The client's IP address
     */
    public static function getClientIp(): string
    {
        return WsCfgServices::getClientIp();
    }

    /**
     * Shuffle associative array
     * @param array $array The associative array to be shuffled
     * @return bool Returns TRUE on success, FALSE on failure
     */
    public static function shuffleAssoc(array &$array): bool
    {
        $keys = array_keys($array);
        shuffle($keys);
        $new = [];
        foreach ($keys as $key)
            $new[$key] = $array[$key];
        $array = $new;
        return TRUE;
    }

    /**
     * Search for a value in an array and return all occurrences
     * @param bool|int|float|string $needle        The value to search for
     * @param array                 $arrayHaystack The array to search in
     * @return array|int|string Returns an array of keys where the value was found, or the key of the first occurrence if there is only one, or false if the value was not found
     */
    public static function arraySearchAll(bool|int|float|string $needle, array $arrayHaystack): array|int|string
    {
        $result = [];
        foreach ($arrayHaystack as $key => $value)
            if ($value === $needle)
                $result[] = $key;
        return (count($result) > 1 ? $result : $result[0]);
    }

    /**
     * Get a column from the array
     * @param array           $data      The input array
     * @param int|string|null $columnKey The column key to extract from each array element
     * @param int|string|null $indexKey  The key to use as the index/key for the resulting array
     * @return array                The array containing the extracted column values
     */
    public static function arrayColumn(array $data, int|string|null $columnKey, int|string|null $indexKey = NULL): array
    {
        $result = [];
        array_walk($data, function ($item) use (&$result, $columnKey, $indexKey) {
            $value = is_object($item) ? ($item->$columnKey ?? NULL) : ($item[$columnKey] ?? NULL);
            $key = $indexKey === NULL ? NULL : (is_object($item) ? ($item->$indexKey ?? NULL) : ($item[$indexKey] ?? NULL));
            if ($key !== NULL)
                $result[$key] = $value;
            else
                $result[] = $value;
        });
        return $result;
    }

    /**
     * Get Random Values from Array
     * @param array $array
     * @param int   $picked
     * @return mixed
     */
    public static function arrayRandVal(array $array, int $picked = 1): mixed
    {
        shuffle($array);
        $picked = min($picked, count($array));
        $result = [];
        for ($i = 0; $i < $picked; $i++)
            $result[] = $array[$i];
        return $picked === 1 ? $result[0] : $result;
    }

    /**
     * Calculate operations on array values
     * @param mixed  $array   The input array
     * @param float  $number  The number used for operations
     * @param string $operate The operation to perform on array values, default is '*'
     * @return array  The modified array after performing the operations
     */
    public static function arrayCalcNumber(mixed $array, float $number, string $operate = '*'): array
    {
        if (!is_array($array) || empty($array))
            return $array;

        foreach ($array as $key => $value) {
            switch (gettype($value)) {

                case 'array':
                    $array[$key] = static::arrayCalcNumber($value, $number, $operate);
                    break;

                case 'integer':
                case 'double':
                    switch ($operate) {
                        case '+':
                            $array[$key] += $number;
                            break;
                        case '-':
                            $array[$key] -= $number;
                            break;
                        case '*':
                            $array[$key] *= $number;
                            break;
                        case '/':
                            $array[$key] /= $number;
                            break;
                        case '\\':
                            $array[$key] = round($value / $number);
                            break;
                        case '^':
                            $array[$key] = pow($value, $number);
                            break;
                        case '%':
                            $array[$key] %= $number;
                            break;
                        case '`':
                            $array[$key] = pow($value, 1 / $number);
                            break;
                        case '<<':
                            $array[$key] <<= $number;
                            break;
                        case '>>':
                            $array[$key] >>= $number;
                            break;
                    }
                    break;
            }
        }
        return $array;
    }

    /**
     * Adds a value to an array or performs arithmetic addition on a numeric value.
     * The method recursively traverses the array and adds the values from $append to corresponding keys,
     * excluding keys in $expectedKey array. If the value of a key is not an array or an object,
     * the method performs arithmetic addition on $data and $append.
     * @param mixed $data        The array or value to add/perform addition on.
     * @param mixed $append      The value to add or perform addition with.
     * @param array $expectedKey Array of keys to exclude from addition. Defaults to an empty array.
     * @param int   $deep        The current depth of the recursion. Defaults to 0.
     * @param int   $maxDeep     The maximum depth of the recursion. Defaults to 10.
     * @return void If the data is an array, the updated array with added values.
     *                           If the data is a number, the result of addition.
     */
    public static function arrayAddValue(mixed &$data, mixed $append, array $expectedKey = [], int $deep = 0, int $maxDeep = 10): void
    {
        switch (gettype($append)) {
            case 'array':
            case 'object':
                if (is_array($data) || $data instanceof Traversable) {
                    if (!is_array($expectedKey)) {
                        $expectedKey = (string)$expectedKey;
                        $expectedKey = !empty($expectedKey) ? [$expectedKey] : [];
                    }
                    if (!is_array($data))
                        $data = [];
                    foreach ($append as $label => $value) {
                        if (!in_array($label, $expectedKey, TRUE)) {
                            if ($deep >= $maxDeep)
                                $data[$label] = $value;
                            else {
                                if (!isset($data[$label]))
                                    $data[$label] = [];
                                static::arrayAddValue($data[$label], $value, $expectedKey, ++$deep, $maxDeep);
                            }
                        }
                    }
                }
                else
                    $data = $append;
                break;
            case 'integer':
                $data = (int)$data;
                $data += (int)$append;
                break;
            case 'double':
                $data = (double)$data;
                $data += (double)$append;
                break;
            default:
                $data = $append;
                break;
        }
    }

    /**
     * Subtracts a value from the given data, depending on the type of the value.
     * @param mixed  &$data        The data from which the value should be subtracted. This parameter is passed by reference.
     * @param mixed   $subtrahend  The value to subtract from the data.
     * @param bool    $negative    (Optional) Whether negative subtraction is allowed. Defaults to FALSE.
     * @param bool    $removeEmpty (Optional) Whether empty values should be removed. Defaults to TRUE.
     * @param array   $expectedKey (Optional) An array of expected keys. Only values with keys not present in this array will be subtracted.
     * @param int     $deep        (Optional) The recursion depth. Defaults to 0.
     * @param int     $maxDeep     (Optional) The maximum recursion depth allowed. Defaults to 10.
     * @return void
     */
    public static function arraySubtractValue(mixed &$data, mixed $subtrahend, bool $negative = FALSE, bool $removeEmpty = TRUE,
                                              array $expectedKey = [], int $deep = 0, int $maxDeep = 10): void
    {
        switch (gettype($subtrahend)) {
            case 'array':
            case 'object':
                if (is_array($data) || $data instanceof Traversable) {
                    if (!is_array($expectedKey)) {
                        $expectedKey = (string)$expectedKey;
                        $expectedKey = !empty($expectedKey) ? [$expectedKey] : [];
                    }
                    if (!is_array($data))
                        $data = [];
                    foreach ($subtrahend as $label => $value) {
                        if (!in_array($label, $expectedKey, TRUE)) {
                            if ($deep >= $maxDeep)
                                $data[$label] = $value;
                            else {
                                if (!isset($data[$label]))
                                    $data[$label] = [];
                                static::arraySubtractValue($data[$label], $value, $negative, $removeEmpty, $expectedKey, ++$deep, $maxDeep);
                                if ($removeEmpty && empty($result))
                                    unset($data[$label]);
                            }
                        }
                    }
                }
                else
                    $data = $subtrahend;
                break;
            case 'integer':
                $data = (int)$data;
                $data -= (int)$subtrahend;
                if ($negative && $data < 0)
                    $data = 0;
                break;
            case 'double':
                $data = (double)$data;
                $data -= (double)$subtrahend;
                if ($negative && $data < 0)
                    $data = 0;
                break;
            default:
                $data = $subtrahend;
                break;
        }
    }

    /**
     *
     */
    public static function arrayCombination(array $array, int $choose): array
    {
        $composer = function (&$combination, &$composed, $start, $_choose, $arr, $n) use (&$composer) {
            if ($_choose == 0)
                $combination[] = $composed;
            else
                for ($i = $start; $i <= $n - $_choose; ++$i) {
                    $composed[] = $arr[$i];
                    if ($_choose - 1 == 0)
                        $combination[] = $composed;
                    else
                        $composer($combination, $composed, $i + 1, $_choose - 1, $arr, $n);
                    array_pop($composed);
                }
        };

        $n = count($array);
        $combination = [];
        $composed = [];
        $composer($combination, $composed, 0, $choose, $array, $n);
        return $combination;
    }

    /**
     * Sorts a multidimensional array based on the specified columns and sort flags.
     * The array will be sorted in ascending order by default unless specified otherwise by the sort flags.
     * If $maintainKey is set to TRUE, the keys of the array will be preserved during sorting.
     * @param array &      $array           The array to be sorted.
     * @param string|array $orderColumns    The column(s) to sort the array by. Can be a single column or an array of columns.
     * @param bool         $maintainKey     (optional) Whether to preserve the keys of the array during sorting. Default is FALSE.
     * @param int|array    $defaultSortFlag (optional) The default sort flag(s) to be used for columns that don't have a specific sort flag defined.
     *                                      Can be a single sort flag or an array of sort flags. Default is SORT_ASC.
     * @return void
     */
    public static function arrayMultiSort(array     &$array, string|array $orderColumns, bool $maintainKey = FALSE,
                                          int|array $defaultSortFlag = SORT_ASC): void
    {
        $args = [];
        if (is_string($orderColumns))
            $orderColumns = [$orderColumns];
        foreach ($orderColumns as $field => $orderFlags) {
            if (is_string($orderFlags)) {
                $field = $orderFlags;
                $orderFlags = $defaultSortFlag;
            }
            if (!is_array($orderFlags))
                $orderFlags = [$orderFlags];
            $orderBy = [];
            foreach ($orderFlags as $orderMethod)
                if (in_array($orderMethod, [SORT_ASC, SORT_DESC, SORT_REGULAR, SORT_NUMERIC,
                                            SORT_STRING, SORT_LOCALE_STRING, SORT_NATURAL, SORT_STRING | SORT_FLAG_CASE, SORT_NATURAL | SORT_FLAG_CASE], TRUE))
                    $orderBy[] = $orderMethod;
            if (!empty($orderBy)) {
                $sortCol = [];
                $incensitive = in_array(SORT_FLAG_CASE, $orderFlags, TRUE);
                foreach ($array as $key => $row) {
                    if (isset($row[$field]))
                        $sortCol[$key] = $incensitive ? strtolower($row[$field]) : $row[$field];
                    else
                        continue 2;
                }
                $args[] = $sortCol;
                foreach ($orderBy as $flag)
                    $args[] = $flag;
            }
        }
        if ($maintainKey) {
            $keys = array_keys($array);
            $args[] = &$array;
            $args[] = &$keys;
            call_user_func_array('array_multisort', $args);
            $array = array_combine($keys, $array);
        }
        else {
            $args[] = &$array;
            call_user_func_array('array_multisort', $args);
        }
    }

    /**
     * Get value from nested array
     * @param array  $array     The array to search in
     * @param string $key       The key to look for in the nested array
     * @param string $delimiter The delimiter used to separate nested array keys
     * @return mixed The value corresponding to the provided key, NULL if not found
     */
    public static function arrayGetValue(array $array, string $key, string $delimiter = '.'): mixed
    {
        return array_reduce(explode($delimiter, $key), function ($current, $key) {
            return $current[$key] ?? NULL;
        }, $array);
    }

    /**
     * Generate Token
     * Generates a secure token using the provided API key, API secret, and algorithm.
     * The default algorithm is 'sha256'.
     * @param string $apiKey    The API key
     * @param string $apiSecret The API secret
     * @param string $algorithm The algorithm to use for generating the token (default: 'sha256')
     * @return string The generated token
     */
    public static function generateToken(string $apiKey, string $apiSecret, string $algorithm = 'sha256'): string
    {
        $ts = time();
        $method = "hmac-$algorithm";
        $nonce = strtolower(static::genCode(20));
        $signature = hash_hmac(substr($method, 5), $ts . $apiKey . $nonce, $nonce . $apiSecret);
        return sprintf("%s apikey=%s ts=%s nonce=%s signature=%s", $method, $apiKey, $ts, $nonce, $signature);
    }

    /**
     * Generate Code
     * @param int    $length
     * @param string $replaceCodeBase
     * @return string
     */
    public static function genCode(int $length = 8, string $replaceCodeBase = ''): string
    {
        $result = '';
        $codeBase = is_string($replaceCodeBase) && !empty($replaceCodeBase)
            ? $replaceCodeBase
            :
            '23456789abcdefghjklmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        $max = strlen($codeBase) - 1;
        if ($max < 1)
            return '';
        for ($i = 0; $i < $length; ++$i)
            $result .= $codeBase[static::randomInt(0, $max)];
        return $result;
    }

    /**
     * Parse Token
     * Parses a token and extracts the parameters from it.
     * @param string $token           The token to parse.
     * @param array  $allowAlgorithms An optional array of allowed algorithms. Defaults to an empty array.
     * @return array|bool Returns an array of parsed parameters if successful, or false if the token is invalid.
     */
    public static function parseToken(string $token, array $allowAlgorithms = [], $preKeys = ['apikey', 'ts', 'nonce', 'signature']): array|bool
    {
        if (str_starts_with($token, 'Bearer '))
            $token = substr($token, 7);
        if (!str_contains($token, " "))
            return FALSE;
        [$algorithm, $content] = explode(" ", $token, 2);
        $algorithm = strtolower($algorithm);
        if (!empty($allowAlgorithms) && !in_array(str_replace('hmac-', '', $algorithm), $allowAlgorithms, TRUE))
            return FALSE;
        else
            $algorithm = substr($algorithm, 5);
        $result = [];
        $params = explode(" ", $content);
        foreach ($params as $item) {
            if (str_contains($item, "=")) {
                [$name, $value] = explode("=", $item, 2);
                if (in_array($name, $preKeys, TRUE))
                    $result[trim($name)] = trim($value);
            }
        }
        sort($preKeys);
        ksort($result);
        $keys = array_keys($result);
        if ($preKeys !== $keys)
            return FALSE;
        $result['hash'] = $algorithm;
        return $result;
    }

    /**
     * Validate Rules
     * @param string|array $patterns The patterns to validate against
     * @param array        $data     The data to be validated
     * @return array An array of keys that failed validation along with their corresponding values and validations
     */
    public static function validateRules(string|array $patterns, array $data): array
    {
        if (!is_array($patterns))
            $patterns = [$patterns];
        $needed = [];
        foreach ($patterns as $key => $validate) {
            if (is_int($key)) {
                $key = $validate;
                $validate = 'must';
            }
            $value = $data[$key] ?? NULL;
            if (!empty($validate) && !static::validate($value, $validate))
                $needed[$key] = ['validate' => $validate, 'given' => $value];
        }
        return $needed;
    }

    /**
     * Validate data using specified validation methods
     * @param mixed        $data
     * @param array|string $validateMethods
     * @return bool
     */
    public static function validate(mixed $data, array|string $validateMethods): bool
    {
        $pass = TRUE;
        $optional = FALSE;
        if (!is_array($validateMethods))
            $validateMethods = !empty($validateMethods) ? [$validateMethods] : [];
        foreach ($validateMethods as $key => $validate) {
            $extend = '';
            if (!is_int($key)) {
                $type = $key;
                $extend = $validate;
            }
            else {
                if (!str_contains($validate, ":"))
                    $type = $validate;
                else
                    [$type, $extend] = explode(":", $validate, 2);
            }
            $type = strtolower($type);
            switch ($type) {

                case 'in':
                    if (!is_array($extend))
                        $extend = !empty($extend) ? [$extend] : [];
                    $pass = in_array($data, $extend, TRUE);
                    break;

                case 'like':
                    if (!is_array($extend))
                        $extend = !empty($extend) ? [$extend] : [];
                    $pass = in_array($data, $extend);
                    break;

                case 'date':
                    $subfix = '';
                    $extend = (string)$extend;
                    $data = (string)$data;
                    if ($extend === 'time') $subfix = '\s\d{2}:\d{2}:\d{2}';
                    $pass = preg_match("/^((19[7-9]\d|2\d{3})-\d{2}-\d{2})$subfix$/", $data) && strtotime($data) > 0;
                    break;

                case 'match':
                    if ($extend != NULL)
                        $pass = $data === $extend;
                    break;

                case 'array':
                    $pass = is_array($data);
                    break;

                case 'array+':
                    $pass = is_array($data) && !empty($data);
                    break;

                case 'boolean':
                    $pass = (boolean)filter_var($data, FILTER_VALIDATE_BOOLEAN);
                    break;

                case 'number':
                    $pass = is_numeric($data);
                    break;

                case 'number+':
                    $pass = is_numeric($data) && !empty($data);
                    break;

                case 'alphabet':
                    $pass = match ($extend) {
                        'upper' => ctype_upper($data),
                        'lower' => ctype_lower($data),
                        default => ctype_alpha($data),
                    };
                    break;

                case 'alphanumber':
                    $pass = ctype_alnum($data);
                    break;

                case 'ip':
                    $pass = match ($extend) {
                        'v4'    => (boolean)filter_var($data, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4),
                        'v6'    => (boolean)filter_var($data, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6),
                        default => (boolean)filter_var($data, FILTER_VALIDATE_IP),
                    };
                    break;

                case 'email':
                    $pass = (boolean)filter_var($data, FILTER_VALIDATE_EMAIL);
                    break;

                case 'url':
                    $pass = (boolean)filter_var($data, FILTER_VALIDATE_URL);
                    break;

                case 'must':
                    $pass = !($data === NULL);
                    break;

                case 'must+':
                    $pass = !empty($data);
                    break;

                case 'min':
                    $extend = (int)$extend;
                    if (!empty($extend)) {
                        if (is_array($data) || is_object($data))
                            $pass = count($data) >= $extend;
                        else
                            $pass = strlen($data) >= $extend;
                    }
                    break;

                case 'mbmin':
                    $extend = (int)$extend;
                    if (!empty($extend)) {
                        if (is_array($data) || is_object($data))
                            $pass = count($data) >= $extend;
                        else
                            $pass = mb_strlen($data) >= $extend;
                    }
                    break;

                case 'max':
                    $extend = (int)$extend;
                    if (!empty($extend)) {
                        if (is_array($data) || is_object($data))
                            $pass = count($data) <= $extend;
                        else
                            $pass = strlen($data) <= $extend;
                    }
                    break;

                case 'mbmax':
                    $extend = (int)$extend;
                    if (!empty($extend)) {
                        if (is_array($data) || is_object($data))
                            $pass = count($data) <= $extend;
                        else
                            $pass = mb_strlen($data) <= $extend;
                    }
                    break;

                case 'len':
                    $extend = (int)$extend;
                    if (!empty($extend)) {
                        if (is_array($data) || is_object($data))
                            $pass = count($data) === $extend;
                        else
                            $pass = strlen($data) === $extend;
                    }
                    break;

                case 'mblen':
                    $extend = (int)$extend;
                    if (!empty($extend)) {
                        if (is_array($data) || is_object($data))
                            $pass = count($data) === $extend;
                        else
                            $pass = mb_strlen($data) === $extend;
                    }
                    break;

                case 'printable':
                    $pass = (boolean)preg_match("/^[[:print:]]+$/u", $data);
                    break;

                case 'content':
                    $extend = (string)$extend;
                    $extend = preg_match("/^\s+$/", $extend) ? $extend : '';
                    $pass = static::safeUtf8($data, $extend);
                    break;

                case 'optional':
                    if ($data === NULL)
                        $optional = TRUE;
                    continue 2;

                case 'regexp':
                case 'regex':
                    $pass = (boolean)preg_match($extend, (string)$data);
                    break;

                default:
                    if (is_array($validate))
                        $pass = in_array($data, $validate, TRUE);
                    else
                        $pass = (boolean)preg_match($extend, $data);
            }
            if (!$pass)
                return $optional;
        }
        return TRUE;
    }

    /**
     * Validate if a string is safe UTF-8 encoded.
     * @param string $content The content to validate.
     * @param string $space   The space character to allow (default: '\x20').
     * @return bool True if the string is safe UTF-8 encoded, false otherwise.
     */
    public static function safeUtf8(string $content, string $space = '\x20'): bool
    {
        return boolval(preg_match('/^(?:'
            . '[\x21-\x7E' . $space . ']'
            . '|[\xC2-\xDF][\x80-\xBF]'                // non-overlong 2-byte
            . '|\xE0[\xA0-\xBF][\x80-\xBF]'           // excluding overlongs
            . '|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}'    // straight 3-byte
            . '|\xED[\x80-\x9F][\x80-\xBF]'           // excluding surrogates
            . '|\xF0[\x90-\xBF][\x80-\xBF]{2}'        // planes 1-3
            . '|[\xF1-\xF3][\x80-\xBF]{3}'            // planes 4-15
            . '|\xF4[\x80-\x8F][\x80-\xBF]{2}'        // plane 16
            . ')+$/xs', $content));
    }

    /**
     * Guess MIME Type by Content
     * @param string $content The content to be analyzed
     * @return string The guessed MIME type based on the content
     */
    public static function guessMimeTypeByContent(string $content): string
    {
        $fInfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_buffer($fInfo, $content);
        finfo_close($fInfo);
        return $mimeType;
    }

    /**
     * Guess the Mime Type of a File
     * @param string $fileName The name or path of the file
     * @return string The guessed Mime Type of the file
     */
    public static function guessMimeTypeByFile(string $fileName): string
    {
        $fInfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($fInfo, $fileName);
        finfo_close($fInfo);
        return $mimeType;
    }

    /**
     * Convert Unit to Integer
     * This method takes a unit string and converts it to an integer representing the size in bytes.
     * @param string $unit The unit string to convert.
     * @return int The integer representation of the size in bytes.
     */
    public static function unitToInt(string $unit): int
    {
        return (int)preg_replace_callback('/(-?\d+)(.?)/', function ($m) {
            return $m[1] * pow(1024, strpos('BKMG', $m[2]));
        }, strtoupper($unit));
    }

    /**
     * Convert Camel Case to Snake Case.
     * Converts a string from camel case to snake case by inserting underscores
     * before each capital letter (except at the beginning of the string).
     * @param string $input The input string in camel case format.
     * @return string The converted string in snake case format.
     */
    public static function camelCaseToSnakeCase(string $input): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }

    /**
     * Convert snake_case to camelCase
     * @param string $input               The input string in snake_case format
     * @param bool   $capitalizeFirstChar Set to true if you want to capitalize the first character of the output string
     * @return string The converted string in camelCase format
     */
    public static function snakeCaseToCamelCase(string $input, bool $capitalizeFirstChar = FALSE): string
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $input)));
        if (!$capitalizeFirstChar)
            $str = lcfirst($str);
        return $str;
    }

    /**
     * Encodes the given data using Base64 URL encoding.
     * @param string $data The data to be encoded.
     * @return string The Base64 URL encoded data.
     */
    public static function base64UrlEncode(string $data): string
    {
        $b64 = base64_encode($data);
        $url = strtr($b64, '+/', '-_');
        return rtrim($url, '=');
    }

    /**
     * Decodes the given base64url-encoded data.
     * @param string $data The base64url-encoded data to decode.
     * @return string The original decoded data.
     */
    public static function base64UrlDecode(string $data): string
    {
        if (str_contains($data, '-') || str_contains($data, '_'))
            $data = strtr($data, '-_', '+/');
        $padding = 4 - strlen($data) % 4;
        if ($padding < 4)
            $data .= str_repeat('=', $padding);
        return base64_decode($data);
    }

    /**
     * Replaces unprintable characters in the given string with the specified replacement string.
     * Optionally, whitespace characters can also be replaced.
     * @param string $string     The input string in which to replace unprintable characters.
     * @param string $replaceTo  The string to replace unprintable characters with. Defaults to an empty string.
     * @param bool   $whitespace Whether to also replace whitespace characters. Defaults to FALSE.
     * @return string The resulting string with unprintable characters replaced.
     */
    public static function replaceUnprintableChars(string $string, string $replaceTo = '', bool $whitespace = FALSE): string
    {
        return preg_replace('/[\x00-\x1F\x7F' . ($whitespace ? '\s' : '') . ']/u', $replaceTo, $string);
    }

    /**
     * Encrypts the given data using the specified key and cipher algorithm.
     * @param string $data      The data to encrypt.
     * @param string $key       The encryption key.
     * @param string $algorithm The cipher algorithm to use. Defaults to 'bf-ofb'.
     * @return string The encrypted data encoded in base64url format.
     */
    public static function encrypt(string $data, string $key, string $algorithm = 'aes-256-gcm', int $tagLength = 4, string $aad = ''): string
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($algorithm));
        if (!str_ends_with($algorithm, 'gcm') && !str_ends_with($algorithm, 'ccm') && !str_ends_with($algorithm, 'ocb'))
            $tagLength = 0;
        else {
            if ($tagLength % 2 !== 0)
                $tagLength--;
            $tagLength = max(4, min(16, $tagLength));
        }
        if ($aad === '')
            $aad = $iv;
        $encrypted = openssl_encrypt($data, $algorithm, $key, OPENSSL_RAW_DATA, $iv, $tag, $aad, $tagLength);
        return static::base64UrlEncode($iv . $tag . $encrypted);
    }

    /**
     * Decrypts the given data using the specified key and algorithm.
     * @param string $encrypted
     * @param string $key       The key used for decryption.
     * @param string $algorithm The algorithm to use for decryption. Defaults to 'bf-ofb'.
     * @param int    $tagLength
     * @param string $aad
     * @return bool|string The decrypted data, or false if decryption fails.
     */
    public static function decrypt(string $encrypted, string $key, string $algorithm = 'aes-256-gcm', int $tagLength = 4,
                                   string $aad = ''): bool|string
    {
        $data = static::base64UrlDecode($encrypted);
        $ivLength = openssl_cipher_iv_length($algorithm);
        if (!str_ends_with($algorithm, 'gcm') && !str_ends_with($algorithm, 'ccm') && !str_ends_with($algorithm, 'ocb'))
            $tagLength = 0;
        else {
            if ($tagLength % 2 !== 0)
                $tagLength--;
            $tagLength = max(4, min(16, $tagLength));
        }
        $iv = substr($data, 0, $ivLength);
        $tag = substr($data, $ivLength, $tagLength);
        $data = substr($data, $ivLength + $tagLength);
        if ($aad === '')
            $aad = $iv;
        return openssl_decrypt($data, $algorithm, $key, OPENSSL_RAW_DATA, $iv, $tag, $aad);
    }

    public static function fifo(int $flag = 0, string $libDb = '/usr/share/misc/magic'): finfo
    {
        return new finfo($flag, !is_file($libDb) || !is_readable($libDb) ? NULL : $libDb);
    }

    /**
     * Converts the file at the specified path to a Data URL.
     * If a MIME type is not provided, it will attempt to determine the MIME type
     *     automatically using the file information database at the provided path.
     * @param string      $filePath The path to the file.
     * @param string|null $mimeType The MIME type of the file. Defaults to NULL.
     * @param string      $libDb    The path to the file information database. Defaults to '/usr/share/misc/magic'.
     * @return string The Data URL representation of the file.
     */
    public static function fileToDataUrl(string $filePath, ?string $mimeType = NULL, string $libDb = '/usr/share/misc/magic'): string
    {
        if (empty($mimeType)) {
            $fifo = static::fifo(FILEINFO_MIME);
            $mimeType = !is_file($filePath) || !is_readable($filePath) ? $fifo->buffer($filePath) : $fifo->file($filePath);
        }
        return "data:$mimeType;base64," . base64_encode(file_get_contents($filePath));
    }

    /**
     * Converts a data URL to a string.
     * This method takes a data URL and extracts the string data from it. The data URL should follow the format
     * "data:[mediatype][;base64],<data>". If the URL does not match this format or does not include the "base64"
     * specifier, the method will return false. Otherwise, it will decode the base64 data and return it as a string.
     * @param string $url The data URL to convert to a string.
     * @return string|false The decoded string from the data URL, or false if the URL is invalid.
     */
    public static function dataUrlToString(string $url): string|false
    {
        $parts = explode(',', $url);
        if (count($parts) !== 2 || !str_contains($parts[0], 'base64'))
            return FALSE;
        return base64_decode($parts[1]);
    }
}
