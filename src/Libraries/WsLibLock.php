<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use CodeIgniter\Events\Events;
use Wellous\Ci4Component\Interface\WsInterfaceLock;
use Wellous\Ci4Component\Tool\WsToolRedisClient;

class WsLibLock implements WsInterfaceLock
{
	/**
	 * @var WsToolRedisClient
	 */
	protected static WsToolRedisClient $redis;

	/**
	 * Sets the Redis client for the class.
	 * @param WsToolRedisClient $redis The Redis client.
	 * @return void
	 */
	public static function initial(WsToolRedisClient $redis): void
	{
		static::$redis = $redis;
	}

	/**
	 * Extend the expiration time of a lock.
	 * @param string $name The name of the lock.
	 * @param int    $ttl  The new expiration time in seconds. Default is 3600 seconds.
	 * @return int The result of the expiration extension operation. Returns 1 on success, 0 on failure.
	 */
	public function extend(string $name, int $ttl = 3600): int
	{
		$key = "lock:$name";
		return static::$redis->expire($key, $ttl);
	}

	/**
	 * Checks if a lock with the given name exists.
	 * @param string $name The name of the lock.
	 * @return bool Returns true if the lock exists, false otherwise.
	 */
	public function isLock(string $name): bool
	{
		$key = "lock:$name";
		return (boolean)static::$redis->exists($key);
	}

	/**
	 * Acquires a named lock with optional auto-release capability.
	 * @param string $name           The name of the lock.
	 * @param bool   $autoRelease    (Optional) Whether to automatically release the lock. Defaults to TRUE.
	 * @param int    $waitLockSecond (Optional) The maximum number of seconds to wait for the lock. Defaults to 600.
	 * @param int    $ttl            (Optional) The time to live for the lock in seconds. Defaults to 3600.
	 * @return bool Whether the lock was successfully acquired.
	 */
	public function acquire(string $name, bool $autoRelease = TRUE, int $waitLockSecond = 600, int $ttl = 3600): bool
	{
		$key = "lock:$name";
		WsLibProfiler::addCheckPoint('lock');
		WsLibProfiler::addCheckPoint($key);
		while (!static::$redis->setnx($key, time())) {
			usleep(WsLibUtilities::randomInt(250000, 500000));
			if (WsLibProfiler::getTimerETA($key) > $waitLockSecond * 1000) {
				WsLibProfiler::addLog('lock', "Lock timeout: $key");
				return FALSE;
			}
		}
		static::$redis->expire($key, $ttl);
		WsLibProfiler::addLog("lock", "Lock acquired: $key");
		if ($autoRelease) {
			Events::on('appEnd', function () use ($name) {
				$this->release($name);
			});
		}
		return TRUE;
	}

	/**
	 * Release a lock with the given name.
	 * @param string $name           The name of the lock to release.
	 * @param int    $delayReleaseMs The delay release value in milliseconds. Default is 0.
	 * @return bool Returns TRUE if the lock was released successfully, FALSE otherwise.
	 */
	public function release(string $name, int $delayReleaseMs = 0): bool
	{
		$key = "lock:$name";
		WsLibProfiler::addLog('lock', "Lock release: $key");
		if ($delayReleaseMs > 0)
			return static::$redis->pexpire($key, $delayReleaseMs) > 0;
		else
			return static::$redis->del($key) > 0;
	}
}
