<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries\Provider;

use Wellous\Ci4Component\Libraries\WsLibUtilities;

/**
 * Class Utilities
 * @package Utils
 */
class WsLibProviderAi
{
    private static string $provider;

    /**
     * Constructor method for the class.
     * @param string $provider The provider to be set.
     * @return void
     */
    public function __construct(string $provider)
    {
        static::$provider = $provider;
    }

    /**
     * Makes a call to the provider using the given method name and parameters.
     * @param mixed ...$params The parameters to pass to the method.
     * @return mixed The result of the provider method call.
     */
    private static function providerPost(...$params): mixed
    {
        return call_user_func_array([static::$provider, 'postApi'], $params);
    }

    /**
     * Makes a prompt request to the AI API.
     * @param string            $code     The code to prompt the AI with.
     * @param array|string|null $contents The contents used for the prompt.
     * @param array|string|null $images   The images used for the prompt.
     * @param string|null       $model    The model used for the prompt (optional).
     * @return array|string The response from the API.
     */
    public static function prompt(string       $code, array|string|null $contents = NULL, array|string|null $images = NULL,
                                  string|array $systemPrompt = NULL,
                                  ?string      $model = NULL): array|string
    {
        if (is_array($images))
            foreach ($images as &$image)
                if (is_file($image) && is_readable($image))
                    $image = WsLibUtilities::fileToDataUrl($image);

        $response = self::providerPost('ai/prompt', [
            'code'     => $code,
            'system'   => $systemPrompt,
            'contents' => $contents,
            'images'   => $images,
            'model'    => $model,
        ]);
        return $response['data'] ?? $response;
    }

    /**
     * Makes a prompt request to the AI API.
     * @param array       $message
     * @param array       $responseFormat
     * @param string|null $model The model used for the prompt (optional).
     * @return array|string The response from the API.
     */
    public static function chat(array $message, array $responseFormat = [], ?string $model = NULL): array|string
    {
        return self::providerPost('ai/chat', [
            'message' => $message,
            'format'  => $responseFormat,
            'model'   => $model,
        ]);
    }

    /**
     * Speech method.
     * This method sends a request to the Provider API to convert text to speech using the specified voice, format, and model.
     * @param string      $input  The text input to be converted to speech.
     * @param string|null $voice  Optional. The voice to be used for speech synthesis. Default is NULL.
     * @param string|null $format Optional. The format of the speech output. Default is NULL.
     * @param string|null $model  Optional. The model to be used for speech synthesis. Default is NULL.
     * @return array The response from the Provider API, which includes the converted speech.
     */
    public static function speech(string $input, ?string $voice = NULL, ?string $format = NULL, ?string $model = NULL): array
    {
        return self::providerPost('ai/speech', [
            'input'  => $input,
            'voice'  => $voice,
            'format' => $format,
            'model'  => $model,
        ]);
    }

    /**
     * Makes a prompt request to the AI API.
     * @param string      $input
     * @param string|null $mimeType
     * @param string|null $model The model used for the prompt (optional).
     * @return array The response from the API.
     */
    public static function transcription(string $input, string $mimeType = NULL, ?string $model = NULL): array
    {
        return self::providerPost('ai/transcription', [
            'input'     => $input,
            'mime_type' => $mimeType,
            'model'     => $model,
        ]);
    }

    /**
     * Makes an embeddings request to the AI API.
     * @param string      $input The input to generate embeddings for.
     * @param string|null $model The model used for the embeddings (optional).
     * @return array The response from the API.
     */
    public static function embeddings(string $input, ?string $model = NULL): array
    {
        return self::providerPost('ai/prompt', [
            'input' => $input,
            'model' => $model,
        ]);
    }

    /**
     * Makes an embeddings request to the AI API.
     * @param string      $input The input to generate embeddings for.
     * @param string|null $model The model used for the embeddings (optional).
     * @return array The response from the API.
     */
    public static function moderation(string $input, ?string $model = NULL): array
    {
        return self::providerPost('ai/moderation', [
            'input' => $input,
            'model' => $model,
        ]);
    }
}
