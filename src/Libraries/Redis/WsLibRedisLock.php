<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries\Redis;

use CodeIgniter\Events\Events;
use Wellous\Ci4Component\Libraries\WsLibProfiler;
use Wellous\Ci4Component\Libraries\WsLibRedis;
use Wellous\Ci4Component\Libraries\WsLibUtilities;

class WsLibRedisLock
{
	/**
	 * @var WsLibRedis $redis The Redis client instance.
	 */
	private WsLibRedis $redis;
	/**
	 * @var string $key The key used to access a specific value in the Redis database.
	 */
	private string $key;

	/**
	 * Constructor for the class.
	 * @param WsLibRedis $redis The Redis client object.
	 * @param string     $key   The name parameter.
	 */
	public function __construct(WsLibRedis $redis, string $key)
	{
		$this->redis = $redis;
		$this->key = "lock:$key";
	}

	/**
	 * Extend the expiration time of a lock.
	 * @param int $ttl The new expiration time in seconds. Default is 3600 seconds.
	 * @return int The result of the expiration extension operation. Returns 1 on success, 0 on failure.
	 */
	public function extend(int $ttl = 3600): int
	{
		return $this->redis->expire($this->key, $ttl);
	}

	/**
	 * Checks if a lock with the given name exists.
	 * @return bool Returns true if the lock exists, false otherwise.
	 */
	public function isLock(): bool
	{
		return (boolean)$this->redis->exists($this->key);
	}

	/**
	 * Acquires a named lock with optional auto-release capability.
	 * @param bool $autoRelease    (Optional) Whether to automatically release the lock. Defaults to TRUE.
	 * @param int  $waitLockSecond (Optional) The maximum number of seconds to wait for the lock. Defaults to 600.
	 * @param int  $ttl            (Optional) The time to live for the lock in seconds. Defaults to 3600.
	 * @return bool Whether the lock was successfully acquired.
	 */
	public function acquire(bool $autoRelease = TRUE, int $waitLockSecond = 600, int $ttl = 3600): bool
	{
		WsLibProfiler::addCheckPoint('lock');
		WsLibProfiler::addCheckPoint($this->key);
		while (!$this->redis->setnx($this->key, time())) {
			usleep(WsLibUtilities::randomInt(250000, 500000));
			if (WsLibProfiler::getTimerETA($this->key) > $waitLockSecond * 1000) {
				WsLibProfiler::addLog('lock', "Lock timeout: $this->key");
				return FALSE;
			}
		}
		$this->redis->expire($this->key, $ttl);
		WsLibProfiler::addLog("lock", "Lock acquired: $this->key");
		if ($autoRelease) {
			Events::on('appEnd', function () {
				$this->release();
			});
		}
		return TRUE;
	}

	/**
	 * Release a lock with the given name.
	 * @param int $delayReleaseMs The delay release value in milliseconds. Default is 0.
	 * @return bool Returns TRUE if the lock was released successfully, FALSE otherwise.
	 */
	public function release(int $delayReleaseMs = 0): bool
	{
		WsLibProfiler::addLog('lock', "Lock release: $this->key");
		if ($delayReleaseMs > 0)
			return $this->redis->pexpire($this->key, $delayReleaseMs) > 0;
		else
			return $this->redis->del($this->key) > 0;
	}
}
