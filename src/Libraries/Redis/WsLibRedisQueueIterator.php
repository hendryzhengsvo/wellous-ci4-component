<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries\Redis;

use Iterator;

class WsLibRedisQueueIterator implements Iterator
{
	private WsLibRedisQueue   $queue;
	private null|bool|int|float|string|array|object $current = NULL;

	public function __construct($queue)
	{
		$this->queue = $queue;
	}

	/**
	 * Gets the current value of the iterator.
	 * This method returns the current value of the iterator. The current value
	 * can be any string value or NULL. If the current value is NULL, it means
	 * that the iterator has reached the end of the collection.
	 * @return bool|int|float|string|array|object|null Returns the current value of the iterator. If the
	 * current value is NULL, it means that the iterator has reached the end of
	 * the collection.
	 */
	public function current(): null|bool|int|float|string|array|object
	{
		return $this->current;
	}

	/**
	 * Returns the current key.
	 * @return string|null The current key. If there is no key or the key is invalid, returns NULL.
	 */
	public function key(): ?string
	{
		return NULL;
	}

	/**
	 * Advances the iterator to the next item.
	 * @return void
	 */
	public function next(): void
	{
		$this->current = $this->queue->dequeue();
	}

	/**
	 * Determines if the current value of the iterator is valid.
	 * This method checks if the current value of the iterator is not NULL.
	 * If the current value is NULL, it tries to fetch the next value from
	 * the queue and assigns it to the current value. It then checks if the
	 * current value is not NULL again, and returns the result as a boolean.
	 * @return bool Returns true if the current value of the iterator is valid; false otherwise.
	 */
	public function valid(): bool
	{
		if ($this->current === NULL) {
			$this->current = $this->queue->dequeue();
		}
		return $this->current !== NULL;
	}

	/**
	 * Rewinds the iterator to the first element.
	 * This method does not perform any action and is meant to be overridden
	 * by concrete iterator classes that need to reset their internal state
	 * to the initial position.
	 * @return void
	 */
	public function rewind(): void {}
}