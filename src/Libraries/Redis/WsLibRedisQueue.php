<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries\Redis;

use Iterator;
use IteratorAggregate;
use Wellous\Ci4Component\Libraries\WsLibRedis;

/**
 * Class WsLibRedisQueue
 * This class represents a queue implemented using Redis sorted set.
 */
class WsLibRedisQueue implements IteratorAggregate
{
	/**
	 * @var WsLibRedis The Redis client object.
	 */
	private WsLibRedis $redis;

	/**
	 * @var string The key of the Redis sorted set.
	 */
	private string $key;

	/**
	 * @var int The time-to-live (TTL) of the sorted set.
	 */
	private int $ttl;

	/**
	 * @var int The index of the queue.
	 */
	private int $queueIndex = 0;

	/**
	 * Class constructor.
	 * Initializes a new Redis queue with the specified key and optional TTL.
	 * @param WsLibRedis $redis The Redis client object.
	 * @param string $key The key of the Redis sorted set.
	 * @param int $ttl The time-to-live (TTL) of the sorted set. Default is 86400 seconds.
	 * @return void
	 */

	public function __construct(WsLibRedis $redis, string $key, int $ttl = 86400)
	{
		$this->redis = $redis;
		$this->key = "queue:$key";
		$this->ttl = $ttl;
		$this->queueIndex = $this->redis->zCard($this->key);
	}

	/**
	 * Adds an item to the Redis sorted set with optional priority.
	 * @param string $item The item to be added to the sorted set.
	 * @param int $priority The priority of the item. Default is 0.
	 * @return void
	 */
	public function enqueue(mixed $item, int $priority = 0): void
	{
		$this->queueIndex++;
		$score = ($priority + 1) * 1000000 - $this->queueIndex;
		$item = serialize($item);
		$this->redis->zAdd($this->key, [$item => $score]);
		if($this->ttl > 0)
			$this->redis->expire($this->key, $this->ttl);
	}

	/**
	 * Removes and returns one or more elements from the sorted set stored in Redis.
	 * @param int $count The number of elements to dequeue. Defaults to 1.
	 * @return bool|int|float|string|array|object|null The dequeued element(s) or NULL if the set is empty.
	 */
	public function dequeue(int $count = 1): null|bool|int|float|string|array|object
	{
		$items = $this->redis->zPopMax($this->key, $count);
		return $this->processData($items, true);
	}

	private function processData(?array $items, bool $useKey = false): null|bool|int|float|string|array|object
	{
		if(empty($items))
			return NULL;
		if($useKey)
			$items = array_keys($items);
		$items = array_map('unserialize', $items);
		if(count($items) === 1)
			return reset($items);
		else
			return $items;
	}

	/**
	 * Removes an item from the Redis sorted set.
	 * @param string $item The item to be removed from the sorted set.
	 * @return bool True if the item was successfully removed, false otherwise.
	 */
	public function remove(string $item): bool
	{
		return (boolean)$this->redis->zrem($this->key, $item);
	}

	/**
	 * Returns the first element of the sorted set identified by the key.
	 * @return bool|int|float|string|array|object|null The first element of the sorted set, or NULL if the sorted set is empty.
	 */
	public function peek(int $count = 1): null|bool|int|float|string|array|object
	{
		$items = $this->redis->zRange($this->key, -1 * $count, -1);
		$items = array_reverse($items);
		return $this->processData($items);
	}

	/**
	 * Returns the first element of the sorted set identified by the key.
	 * @return bool|int|float|string|array|object|null The first element of the sorted set, or NULL if the sorted set is empty.
	 */
	public function all(int $count = 1): null|bool|int|float|string|array|object
	{
		$items = $this->redis->zRevRange($this->key, 0, -1);
		return $this->processData($items);
	}

	/**
	 * Gets the size of the set stored in Redis.
	 * @return int The size of the set.
	 */
	public function size(): int
	{
		return $this->redis->zCard($this->key);
	}

	/**
	 * Determines whether the set stored in Redis is empty or not.
	 * @return bool True if the set is empty, false otherwise.
	 */
	public function isEmpty(): bool
	{
		return $this->size() == 0;
	}

	public function getIterator(): Iterator
	{
		return new WsLibRedisQueueIterator($this);
	}
}
