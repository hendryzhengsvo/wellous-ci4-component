<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries\Cli;

use League\CLImate\Util\Writer\StdErr;
use League\CLImate\Util\Writer\WriterInterface;

class WsCliError extends StdErr implements WriterInterface
{
	protected bool $verbose;

	public function __construct(bool $verbose)
	{
		$this->verbose = $verbose;
	}

	/**
	 * @param string $content
	 * @return void
	 */
	public function write($content): void
	{
		if ($this->verbose && is_cli())
			parent::write($content);
	}
}