<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries\Cli;

use League\CLImate\CLImate;
use function defined;
use const DIRECTORY_SEPARATOR;
use const STDIN;
use const STDOUT;

class WsCliCursor
{
	private CLImate $cli;
	private         $input;

	/**
	 * @param resource|null $input
	 */
	public function __construct(CLImate $cli, $input = NULL)
	{
		$this->cli = $cli;
		$this->input = $input ?? (defined('STDIN') ? STDIN : fopen('php://input', 'r+'));
	}

	/**
	 * @return $this
	 */
	public function moveUp(int $lines = 1): static
	{
		$this->cli->inline(sprintf("\e[%dA", $lines));
		return $this;
	}

	/**
	 * @return $this
	 */
	public function moveDown(int $lines = 1): static
	{
		$this->cli->inline(sprintf("\e[%dB", $lines));
		return $this;
	}

	/**
	 * @return $this
	 */
	public function moveRight(int $columns = 1): static
	{
		$this->cli->inline(sprintf("\e[%dC", $columns));
		return $this;
	}

	/**
	 * @return $this
	 */
	public function moveLeft(int $columns = 1): static
	{
		$this->cli->inline(sprintf("\e[%dD", $columns));
		return $this;
	}

	/**
	 * @return $this
	 */
	public function moveToColumn(int $column): static
	{
		$this->cli->inline(sprintf("\e[%dG", $column));
		return $this;
	}

	/**
	 * @return $this
	 */
	public function moveToPosition(int $column, int $row): static
	{
		$this->cli->inline(sprintf("\e[%d;%dH", $row + 1, $column));
		return $this;
	}

	/**
	 * @return $this
	 */
	public function savePosition(): static
	{
		$this->cli->inline("\e7");
		return $this;
	}

	/**
	 * @return $this
	 */
	public function restorePosition(): static
	{
		$this->cli->inline("\e8");
		return $this;
	}

	/**
	 * @return $this
	 */
	public function hide(): static
	{
		$this->cli->inline("\e[?25l");
		return $this;
	}

	/**
	 * @return $this
	 */
	public function show(): static
	{
		$this->cli->inline("\e[?25h\e[?0c");

		return $this;
	}

	/**
	 * Clears all the output from the current line.
	 * @return $this
	 */
	public function clearLine(): static
	{
		$this->cli->inline("\e[2K");
		return $this;
	}

	/**
	 * Clears all the output from the current line after the current position.
	 */
	public function clearLineAfter(): self
	{
		$this->cli->inline("\e[K");
		return $this;
	}

	/**
	 * Clears all the output from the cursors' current position to the end of the screen.
	 * @return $this
	 */
	public function clearOutput(): static
	{
		$this->cli->inline("\e[0J");
		return $this;
	}

	/**
	 * Clears the entire screen.
	 * @return $this
	 */
	public function clearScreen(): static
	{
		$this->cli->inline("\e[2J");
		return $this;
	}

	/**
	 * Returns the current cursor position as x,y coordinates.
	 */
	public function getCurrentPosition(): array
	{
		static $isTtySupported;
		if (!$isTtySupported ??= '/' === DIRECTORY_SEPARATOR && stream_isatty(STDOUT))
			return [1, 1];
		$sttyMode = shell_exec('stty -g');
		shell_exec('stty -icanon -echo');
		@fwrite($this->input, "\e[6n");
		$code = trim(fread($this->input, 1024));
		shell_exec(sprintf('stty %s', $sttyMode));
		sscanf($code, "\e[%d;%dR", $row, $col);
		return [$col, $row];
	}
}