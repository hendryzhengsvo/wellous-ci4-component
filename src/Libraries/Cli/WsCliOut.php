<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries\Cli;

use League\CLImate\Util\Writer\StdOut;
use League\CLImate\Util\Writer\WriterInterface;

class WsCliOut extends StdOut implements WriterInterface
{
	protected bool $verbose;

	public function __construct(bool $verbose)
	{
		$this->verbose = $verbose;
	}

	/**
	 * @param string $content
	 * @return void
	 */
	public function write($content): void
	{
		if ($this->verbose && is_cli())
			parent::write($content);
	}
}