<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries\Cli;

use CodeIgniter\Events\Events;
use Countable;
use League\CLImate\CLImate;
use Wellous\Ci4Component\Libraries\WsLibCli;
use function count;

class WsCliProgress
{
	/**
	 * @var CLImate
	 */
	private CLImate $cli;
	/**
	 * @var WsLibCli
	 */
	private WsLibCli $libCli;
	/**
	 * @var int
	 */
	private int $max;
	/**
	 * @var int
	 */
	private int $step = 0;
	/**
	 * @var int
	 */
	private int $maxBar;
	/**
	 * @var int
	 */
	private int $curBar = -1;

	private bool $finished = FALSE;

	public function __construct(WsLibCli $libCli, int $max = 0, int $maxBar = 100, $now = 0)
	{
		$this->libCli = $libCli;
		$this->cli = $libCli->getCli();
		if ($maxBar > 200)
			$maxBar = 200;
		elseif ($maxBar <= 0)
			$maxBar = 100;
		$this->maxBar = $maxBar;
		$this->start($max)
		     ->set($now);

		Events::on('wslib:cli:before', function () {
			if (!$this->finished)
				$this->finish();
		});
	}

	public function status(string $status): WsCliProgress
	{
		return $this->set($this->step, $status);
	}

	public function start($max = 0, string $status = ''): WsCliProgress
	{
		if ($this->finished)
			return $this;
		$this->max = $max;
		$this->libCli->cursor->hide();
		$this->set(0, $status, FALSE);
		return $this;
	}

	public function finish(): WsCliProgress
	{
		if ($this->finished)
			return $this;

		$this->libCli->cursor
			->moveDown(2)
			->moveToColumn(1)
			->show();
		$this->cli->inline("\n");
		$this->finished = TRUE;
		return $this;
	}

	/**
	 * @param int    $step
	 * @param string $status
	 * @param bool   $autoFinish
	 * @return $this
	 */
	public function set(int $step, string $status = '', bool $autoFinish = TRUE): WsCliProgress
	{
		if ($this->finished)
			return $this;

		if ($step > $this->max)
			$this->max = $step;
		elseif ($step < 0)
			$step = 0;

		$cusor = $this->libCli->cursor;
		$this->step = $step;

		//Calculate percent
		$percent = $step > 0 && $this->max > 0 ? (double)@number_format($step / $this->max * 100, 2) : 0;
		if ($percent > 100)
			$percent = 100;
		$bar = $percent > 0 ? (int)min(ceil(ceil($percent) * ($this->maxBar / 100)), $this->maxBar) : 0;

		//Draw progress info
		$cusor->moveToColumn(1)
		      ->clearLineAfter();
		$this->cli->inline($this->libCli->addInfo(number_format($step) . ' of ' . number_format($this->max) . " ($percent%) $status"));
		if ($this->curBar !== $bar) {

			$cusor->moveDown()
			      ->moveToColumn(1);

			//Draw base bar (first init or reset)
			if ($this->curBar <= 0 || $bar < $this->curBar) {
				$this->curBar = 0;
				$cusor->clearLineAfter();
				$this->cli->inline("[ <blue>" . str_repeat('░', $this->maxBar) . "</blue> ]");
			}

			//Draw progress bar
			$cusor->moveToColumn(3 + $this->curBar);
			for ($i = $this->curBar; $i < $bar; $i++)
				$this->cli->inline("\e[38;2;0;" .
					min(55 + (int)ceil($i * 200 / $this->maxBar), 255) .
					";0m█");
			$cusor->moveUp();

			//Draw ETA and move cursor
			$this->curBar = $bar;
		}

		//Move back and show cursor
		if ($autoFinish && $step >= $this->max)
			$this->finish();

		return $this;
	}

	public function advance(string $status = '', int $number = 1, bool $autoFinish = TRUE): WsCliProgress
	{
		return $this->set($this->step + $number, $status, $autoFinish);
	}

	/**
	 * @param Countable|iterable $iterable
	 * @param int|NULL           $max
	 * @return iterable
	 */
	public function iterate(Countable|iterable $iterable, int $max = NULL): iterable
	{
		$this->start($max ?? (is_countable($iterable) ? count($iterable) : 0));
		foreach ($iterable as $key => $value) {
			yield $key => $value;
			$this->advance(autoFinish: FALSE);
		}
	}
}