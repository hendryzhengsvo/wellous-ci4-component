<?php
declare(strict_types=1);

/**
 * This class provides a profiling functionality for measuring the execution time and memory usage of code sections.
 * @package Wellous\Ci4Component\Libraries
 */

namespace Wellous\Ci4Component\Libraries;

defined('HRTIME_START') || define('HRTIME_START', hrtime(TRUE));

use Closure;
use Config\Services;
use ReflectionFunction;
use Throwable;
use Wellous\Ci4Component\Config\WsCfgServices;
use Wellous\Ci4Component\Interface\WsInterfaceProfiler;
use Wellous\Ci4Component\Tool\WsStore;

/**
 * Class WsLibProfiler
 */
class WsLibProfiler implements WsInterfaceProfiler
{
	protected static array $maxLog     = [
		'enable'  => 50000,
		'disable' => 1000,
	];
	protected static ?bool $enable     = NULL;
	protected static array $debug      = [];
	protected static array $log        = [];
	protected static array $checkPoint = [];
	protected static array $elapsed    = [];

	private static array $lastCheckPoint = [
		'timer'  => HRTIME_START,
		'memory' => 0,
	];

	/**
	 * @param bool|null $enable
	 * @return void
	 */
	public static function initial(?bool $enable = NULL): void
	{
		if (static::$enable === NULL && $enable !== NULL)
			static::$enable = $enable;
	}

	/**
	 * Checks if the feature is enabled.
	 * @return bool Returns a boolean value indicating whether the feature is enabled or not.
	 */
	public static function isEnable(): bool
	{
		return static::$enable === NULL ? TRUE : static::$enable;
	}

	/**
	 * Adds a log entry to the log array.
	 * @param string $name  The name of the log entry.
	 * @param string $log   The log message.
	 * @param bool   $reset Whether to reset the check point or not. Default is TRUE.
	 * @param string $type
	 * @return void
	 */
	public static function addLog(string $name, string $log, bool $reset = TRUE, string $type = ''): void
	{
		$checkPoint = static::getCheckPoint($name);
		static::addUsageTime($name, $checkPoint['timer']['eta']);

		if (!WsStore::$cli && !static::isEnable()) {
			$currentCheckPoint = static::$checkPoint[$name] ?? [
				'timer'  => hrtime(TRUE),
				'memory' => memory_get_usage(),
			];
			$lastCheckPoint = [
				'timer'  => static::getHrETA(static::$lastCheckPoint['timer'], TRUE, $currentCheckPoint['timer'] ?? HRTIME_START),
				'memory' => static::getMemUseRaw(static::$lastCheckPoint['memory'], TRUE, $currentCheckPoint['memory'] ?? static::$lastCheckPoint['memory']),
			];
			if ($lastCheckPoint['timer']['to'] - $lastCheckPoint['timer']['from'] > 0 && !WsStore::$cli) {
				$timer = "Time({$lastCheckPoint['timer']['from']}~{$lastCheckPoint['timer']['to']}:{$lastCheckPoint['timer']['eta']}ms)";
				if ($lastCheckPoint['memory']['use'] < 0)
					$lastCheckPoint['memory']['use'] = 0;
				$memory = "Mem({$lastCheckPoint['memory']['from']}~{$lastCheckPoint['memory']['to']}:{$lastCheckPoint['memory']['use']})";
				static::$log[] = count(static::$log) > 0 ? "$timer, $memory - code executed in {$lastCheckPoint['timer']['eta']}ms"
					: "$timer, $memory - code intialized in {$lastCheckPoint['timer']['eta']}ms";
			}

			static::$lastCheckPoint = [
				'timer'  => hrtime(TRUE),
				'memory' => memory_get_usage(),
			];
		}

		if ($reset)
			static::addCheckPoint($name);
		else
			static::delCheckPoint($name);

		$timer = "Time({$checkPoint['timer']['from']}~{$checkPoint['timer']['to']}:{$checkPoint['timer']['eta']}ms)";
		$memory = "Mem({$checkPoint['memory']['from']}~{$checkPoint['memory']['to']}:{$checkPoint['memory']['use']})";
		if (WsStore::$cli) {
			if (!static::isEnable())
				return;
			if (strlen($log) < 1000)
				WsStore::$cli->inline("<dim>[$timer, $memory]</dim><magenta>$name</magenta>: ")
				             ->out(self::cliHighlight($log, $type), TRUE);
			else
				WsStore::$cli->inline("<dim>[$timer, $memory]</dim><magenta>$name</magenta>: ")
				             ->out($log, TRUE);
		}
		else {
			$log = "$timer, $memory - $name: $log";
			static::$log[] = $log;
			if (count(static::$log) >= static::$maxLog[static::isEnable() ? 'enable' : 'disable'])
				array_shift(static::$log);
		}
	}

	/**
	 * Highlights specific text in a CLI environment.
	 * Uses regular expressions to apply colors to different components of the input text.
	 * @param string $content The text to be highlighted.
	 * @return string The highlighted text.
	 */
	private static function cliHighlight(string $content, string $type = ''): string
	{
		$content = preg_replace(
			"/\b(?:https?|ftp|file|rediss?|mysqls?):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]/i",
			"<cyan>$0</cyan>",
			preg_replace("/`(.*?)`/", "<bold>$0</bold>",
				preg_replace("/(['\"])(.*?)\\1/",
					WsStore::$cli->format("$0", TRUE, 'string'),
					preg_replace("/(?<=\s|^)(\d+)(?=\s|$)|(')(\d+)(')|(\")(\d+)(\")/",
						WsStore::$cli->format("$0", TRUE, 'double'), $content)
				)
			));
		switch ($type) {
			case 'sql':
				static::cliHighlightSQL($content);
				break;
			case 'redis':
				static::cliHighlightRedis($content);
				break;
		}
		return $content;
	}

	/**
	 * Highlights SQL keywords, functions, and special characters in a SQL string.
	 * @param string $content The SQL string to highlight.
	 */
	private static function cliHighlightSQL(string &$content): void
	{
		//highlight sql keywords
		$keywords = '/\b(SELECT|FROM|WHERE|LIMIT|OFFSET|ORDER BY|GROUP BY|JOIN|LEFT JOIN|RIGHT JOIN|INNER JOIN|OUTER JOIN|ON|LIKE|AND|OR|NOT|IN|AS|NULL|IS|COUNT|AVG|SUM|MIN|MAX|UPDATE|SET|INSERT INTO|VALUES|DELETE FROM|SHOW COLUMNS|SHOW TABLES|SHOW DATABASES|BEGIN|COMMIT|ROLLBACK|START TRANSACTION|SAVEPOINT|RELEASE SAVEPOINT|LOCK TABLES|UNLOCK TABLES|ASC|DESC)\b/i';
		$content = preg_replace($keywords, '<blue>$1</blue>', $content);

		//highlight sql common function
		$functions = '/\b(MAX|MIN|AVG|CAST|REPEAT|ASIN|ACOS|ATAN|SHA1|RANK|NOW|CURDATE|CURTIME|DATE_ADD|DATE_SUB|DATEDIFF|SUBSTRING|LENGTH|REPLACE|GROUP_CONCAT)\b/i';
		$content = preg_replace($functions, '<magenta>$1</magenta>', $content);

		//highlight sql special character
		$special = '/([*,;()])/';
		$content = preg_replace($special, '<bold>$1</bold>', $content);
	}

	/**
	 * Highlights Redis commands in a given string.
	 * @param string $content The Redis command string to highlight.
	 */
	private static function cliHighlightRedis(string &$content): void
	{
		$parttern = '/\b(SET|GET|DEL|EXPIRE|TTL|LPUSH|RPUSH|LPOP|RPOP|SADD|SCARD|SMEMBERS|ZADD|ZRANGE|HSET|HGETALL|SUBSCRIBE|PUBLISH|INCR|DECR|LLEN|SREM|ZREM|HMSET|HGET|BLPOP|BRPOP|ZUNIONSTORE|ZUNION|ZSCORE|ZPOPMAX|ZPOPMIN|ZMPOP|ZRANK|ZSCAN|ZREVRANK|ZINCRBY|ZCOUNT|ZREVRANGE|PING|ECHO|MGET|BITOP|BITCOUNT|BITPOS|SAVE|BGSAVE|LASTSAVE|SHUTDOWN|INFO|MONITOR|SLAVEOF|CONFIG|SLOWLOG|SYNC|PSYNC|REPLCONF|FLUSHDB|FLUSHALL|DBSIZE|DEBUG|PFADD|PFCOUNT|PFMERGE|SETEX|SETNX|MSET|MSETNX|GETSET|STRLEN|APPEND|GETRANGE|SETRANGE|INCRBY|DECRBY|INCRBYFLOAT)\b/i';
		$content = preg_replace($parttern, '<blue>$1</blue>', $content);
	}

	/**
	 * Retrieves the checkpoint values for the specified name.
	 * @param string $name The name of the checkpoint. If not specified, retriev first checkpoints.
	 * @return array The checkpoint values for the specified name.
	 */
	public static function getCheckPoint(string $name = ''): array
	{
		return [
			'timer'  => static::getTimerETA($name, TRUE),
			'memory' => static::getMemUse($name, TRUE),
		];
	}

	/**
	 * Get the estimated time of arrival (ETA) for a timer.
	 * @param string $name   The name of the timer. (Optional)
	 * @param bool   $detail Whether to return detailed information. (Default: FALSE)
	 * @return int|array     The estimated time of arrival in seconds, or an array with detailed information.
	 */
	public static function getTimerETA(string $name = '', bool $detail = FALSE): int|array
	{
		return static::getHrETA(static::$checkPoint[$name]['timer'] ?? HRTIME_START, $detail);
	}

	/**
	 * Calculate the estimated time of arrival in milliseconds or an array with detailed information.
	 * @param int            $fromHrTime The starting time in high-resolution format.
	 * @param bool           $detail     Indicates whether to include detailed information in the output array.
	 * @param int|float|null $toHrTime   The ending time in high-resolution format. If not provided, the current time will be used.
	 * @return int|array The estimated time of arrival in milliseconds or an array with detailed information.
	 */
	public static function getHrETA(int $fromHrTime, bool $detail = FALSE, int|float|null $toHrTime = NULL): int|array
	{
		$fromHrTime = (int)floor(($fromHrTime - HRTIME_START) / 1e+6);
		$toHrTime = (int)floor((($toHrTime ?? hrtime(TRUE)) - HRTIME_START) / 1e+6);
		$eta = $toHrTime - $fromHrTime;
		return $detail ? [
			'from' => $fromHrTime,
			'to'   => $toHrTime,
			'eta'  => $eta,
		] : $eta;
	}

	/**
	 * Retrieves the memory usage of a specific checkpoint.
	 * @param string $name   The name of the checkpoint.
	 * @param bool   $detail (optional) Whether to include detailed memory information.
	 * @return int|array The memory usage, or an array of detailed memory information if $detail is TRUE.
	 */
	public static function getMemUse(string $name, bool $detail = FALSE): int|array
	{
		return static::getMemUseRaw(static::$checkPoint[$name]['memory'] ?? 0, $detail);
	}

	/**
	 * Retrieves the raw memory usage between two given memory points.
	 * @param int      $fromMem The starting memory point.
	 * @param bool     $detail  (Optional) Whether to include detailed memory information.
	 * @param int|null $toMem   (Optional) The ending memory point. If not provided, the current memory usage will be used.
	 * @return int|array The memory usage, or an array of detailed memory information if $detail is TRUE.
	 */
	public static function getMemUseRaw(int $fromMem, bool $detail = FALSE, int|null $toMem = NULL): int|array
	{
		$toMem = $toMem ?? memory_get_usage();
		$useMem = $toMem - $fromMem;
		return $detail ? [
			'from' => WsLibUtilities::byteToSize($fromMem),
			'to'   => WsLibUtilities::byteToSize($toMem),
			'use'  => WsLibUtilities::byteToSize($useMem),
		] : $useMem;
	}

	/**
	 * Adds the usage time to a specific checkpoint.
	 * @param string $name      The name of the checkpoint.
	 * @param int    $usageTime The usage time to be added.
	 * @return void
	 */
	protected static function addUsageTime(string $name, int $usageTime): void
	{
		if (!isset(static::$elapsed[$name]))
			static::$elapsed[$name] = 0;
		static::$elapsed[$name] += $usageTime;
	}

	/**
	 * Adds a checkpoint with the given name.
	 * @param string $name The name of the checkpoint.
	 * @return void
	 */
	public static function addCheckPoint(string $name): void
	{
		static::$checkPoint[$name] = [
			'timer'  => hrtime(TRUE),
			'memory' => memory_get_usage(),
		];
	}

	/**
	 * Deletes a specific checkpoint.
	 * @param string $name The name of the checkpoint to delete.
	 */
	public static function delCheckPoint(string $name): void
	{
		if (isset(static::$checkPoint[$name]))
			unset(static::$checkPoint[$name]);
	}

	/**
	 * Appends debug information to the existing debug log.
	 * @param mixed ...$debug The debug information to be appended.
	 * @return void
	 */
	public static function debug(...$debug): void
	{
		$backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
		$caller = $backtrace[0];

		$callerFile = str_replace(ROOTPATH, '', $caller['file'] ?? '');
		$callerLine = $caller['line'] ?? '?';
		if (WsStore::$cli) {
			if (!static::isEnable())
				return;
			foreach ($debug as $item) {
				WsStore::$cli->inline("<yellow><bold>DEBUG</bold></yellow> <dim>$callerFile:$callerLine</dim>: ");
				WsStore::$cli->json($item);
			}
		}
		else
			foreach ($debug as $item)
				static::$debug[count(static::$debug) . ". $callerFile:$callerLine"] = $item;
	}

	/**
	 * Fetches the profiler information.
	 * @return array The profiler information.
	 */
	public static function fetch(): array
	{
		$router = Services::router();
		$request = is_cli() ? Services::clirequest() : Services::request();
		$controller = $router->controllerName();
		if ($controller instanceof Closure) {
			try {
				$ref = new ReflectionFunction($controller);
				$controller = $ref->getFileName() . ':' . $ref->getStartLine();
			} catch (Throwable) {
				$controller = 'Closure';
			}
		}
		$method = $router->methodName();
		$profiler = [
			'environment' => ENVIRONMENT,
			'request'     => $request->getMethod() . ' ' . WsCfgServices::getRoutePath(),
			'controller'  => "$controller->$method",
			'memory'      => [
				'current' => WsLibUtilities::byteToSize(memory_get_usage()),
				'peak'    => WsLibUtilities::byteToSize(memory_get_peak_usage()),
			],
			'usageTime'   => static::getUsageTime(),
		];
		if (!is_cli()) {
			$params = $request->getVar();
			$profiler = [
					'host'     => $request->getServer('HTTP_HOST'),
					'serverIp' => $request->getServer('SERVER_ADDR'),
					'clientIp' => WsCfgServices::getClientIp(),
				] + $profiler;
			if (!empty($params))
				$profiler['params'] = $params;
		}
		else
			$profiler['params'] = $request->getSegments();
		if (!empty($_COOKIE))
			$profiler['cookies'] = $_COOKIE;
		if (!empty(static::$debug))
			$profiler['debug'] = static::$debug;
		if (!empty(static::$log))
			$profiler['log'] = static::$log;
		return $profiler;
	}

	/**
	 * Retrieves the usage time of the application.
	 * @return array An associative array containing the elapsed time in different parts of the application.
	 */
	public static function getUsageTime(): array
	{
		$total = static::getHrETA(HRTIME_START);
		if (isset(static::$elapsed['total']))
			unset(static::$elapsed['total']);
		if (isset(static::$elapsed['other']))
			unset(static::$elapsed['other']);
		$usedTime = array_sum(static::$elapsed);
		static::$elapsed['total'] = $total;
		static::$elapsed['other'] = $total - $usedTime;
		return static::$elapsed;
	}

}