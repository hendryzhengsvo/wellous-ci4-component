<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use ArrayAccess;
use Wellous\Ci4Component\Traits\WsTraitArrayAccess;

/**
 * Class Container
 */
class WsLibContainer implements ArrayAccess
{
	use WsTraitArrayAccess;

	/**
	 * Data Storage
	 */
	protected array $data = [];

	protected bool $lock;

	protected bool $__firstOnly = FALSE;

	/**
	 * Constructor for creating a new instance of the class.
	 * @param mixed $data        The data to import into the object.
	 * @param bool  $lock        Whether to lock the object after importing the data. Defaults to FALSE.
	 * @param bool  $__firstOnly Whether to only import the first element of the data. Defaults to FALSE.
	 */
	public function __construct(mixed $data = NULL,
	                            bool  $lock = FALSE,
	                            bool  $__firstOnly = FALSE)
	{
		$this->__firstOnly = $__firstOnly;
		$this->lock = FALSE;
		$this->import($data);
		$this->lock = $lock;
	}

	/**
	 * Import data into the object
	 * @param mixed $data          The data to import. Can be an object, an array, or a string in JSON format.
	 * @param bool  $notExistsOnly Optional. If set to TRUE, only set values for keys that do not already exist in the object.
	 * @return self
	 */
	public function import(mixed $data, bool $notExistsOnly = FALSE): self
	{
		if ($this->lock || NULL === $data)
			return $this;
		if (!empty($data)) {
			switch (gettype($data)) {
				case 'object':
					$data = $data instanceof WsLibContainer ? $data() : (array )$data;
					foreach ($data as $key => $value)
						$this->set($key, $value, $notExistsOnly);
					break;

				case 'array':
					foreach ($data as $key => $value)
						$this->set($key, $value, $notExistsOnly);
					break;

				case 'string':
					$data = json_decode($data, TRUE);
					if (json_last_error() !== 0)
						$data = [];
					foreach ($data as $key => $value)
						$this->set($key, $value, $notExistsOnly);
					break;
			}
		}
		return $this;
	}

	/**
	 * Set a value in the data array.
	 * @param int|string $offset        The key or index.
	 * @param mixed|null $value         The value to be set.
	 * @param bool       $notExistsOnly If true, the value will be set only if the key does not exist.
	 * @return self Returns an instance of the current object.
	 */
	public function set(int|string $offset, mixed $value = NULL, bool $notExistsOnly = FALSE): self
	{
		if (($notExistsOnly && !isset($this->data[$offset])) || !$notExistsOnly)
			$this->__set($offset, $value);
		return $this;
	}

	/**
	 * Set the state of the object based on the given arguments
	 * @param array $args The arguments to set the state of the object
	 * @return self The updated object with the new state
	 */
	public static function __set_state(array $args): self
	{
		return new WsLibContainer($args['__data'], $args['__lock']);
	}

	/**
	 * Cleanup method that is automatically called when the object is destroyed
	 */
	public function __destruct()
	{
		$this->data = [];
	}

	/**
	 * Checks if a key is set in the data array.
	 * @param int|string $offset The key to check.
	 * @return bool Returns true if the key is set, false otherwise.
	 */
	public function __isset(int|string $offset): bool
	{
		return isset($this->data[$offset]);
	}

	/**
	 * Unset a value at the specified offset
	 * @param int|string $offset The offset to unset
	 * @return void
	 */
	public function __unset(int|string $offset): void
	{
		if (!$this->lock && isset($this->data[$offset]))
			unset($this->data[$offset]);
	}

	/**
	 * Method to invoke the object as a function
	 * @param null|int|string $offset The offset to get or NULL to dump the entire data
	 * @return mixed If $offset is NULL, returns the dumped data, otherwise returns the value at the specified offset
	 */
	public function __invoke(null|int|string $offset = NULL): mixed
	{
		return $offset === NULL ? $this->__dump() : $this->__get($offset);
	}

	/**
	 * Dump the data as an array recursively.
	 * @return array The dumped data as an array.
	 */
	protected function __dump(): array
	{
		$entire = [];
		foreach ($this->data as $key => $data)
			$entire[$key] = $data instanceof WsLibContainer ? $data->__dump() : $data;
		return $entire;
	}

	/**
	 * Get the value of the specified offset.
	 * @param int|string $offset The offset key to retrieve the value for.
	 * @return mixed The value associated with the specified offset.
	 */
	public function __get(int|string $offset)
	{
		return $this->get($offset);
	}

	/**
	 * Set a value to a specified offset in the data container.
	 * @param int|string $offset The offset where the value will be set.
	 * @param mixed      $value  The value to be set.
	 */
	public function __set(int|string $offset, mixed $value)
	{
		if ($this->lock || NULL === $value)
			return;
		if (!$this->__firstOnly && is_array($value) && $this->__isAssoc($value))
			$this->data[$offset] = new WsLibContainer($value);
		else
			$this->data[$offset] = $value;
	}

	/**
	 * Get the value for the given offset/key in the data array.
	 * If the offset/key is '*', return the entire data array.
	 * If the offset/key does not exist in the data array, return the default value.
	 * @param int|string $offset  The offset/key in the data array to get the value from
	 * @param mixed      $default (optional) The default value to return if the offset/key does not exist (default: NULL)
	 * @return mixed The value for the given offset/key in the data array, or the default value
	 */
	public function get(int|string $offset, mixed $default = NULL): mixed
	{
		return $offset === '*' ? $this->data : ($this->data[$offset] ?? $default);
	}

	/**
	 *
	 */
	protected function __isAssoc(array $arr): bool
	{
		return array_keys($arr) !== range(0, count($arr) - 1);
	}

	/**
	 * Returns the string representation of the object.
	 * @return string The string representation of the object in JSON format.
	 */
	public function __toString(): string
	{
		return json_encode($this->__dump());
	}

	/**
	 * Calls the specified method on the object.
	 * @param string $method    The name of the method to call.
	 * @param array  $arguments The arguments to pass to the method.
	 * @return mixed|false The result of the method call if the method exists and is callable,
	 *                          otherwise FALSE if the method does not exist or is not callable.
	 */
	public function __call(string $method, array $arguments)
	{
		if (isset($this->data[$method]) && is_callable($this->data[$method]))
			return call_user_func_array($this->data[$method], $arguments);
		else
			return FALSE;
	}

	/**
	 * Flattens the data array with the given prefix and postfix.
	 * @param string $prefix  The prefix to be added to each flattened key.
	 * @param string $postfix The postfix to be added to each flattened key.
	 * @return array The flattened data array with prefixed and postfixed keys.
	 */
	public function flattenData(string $prefix = '', string $postfix = ''): array
	{
		return $this->__flattenData($this->data, $prefix, $postfix);
	}

	/**
	 * Flattens a nested data structure into a flat array.
	 * @param mixed  $data   The data structure to flatten.
	 * @param string $prefix (Optional) The prefix to add to each flattened key.
	 * @param string $postfix
	 * @param string $keyPrefix
	 * @return array
	 */
	protected function __flattenData(mixed  $data,
	                                 string $prefix = '',
	                                 string $postfix = '',
	                                 string $keyPrefix = ''): array
	{
		$result = [];
		foreach ($data as $key => $item) {
			$strKey = $keyPrefix . (empty($keyPrefix) || is_int($key) ? '' : '.') . (is_int($key) ? "[$key]" : $key);
			if (is_array($item) || is_object($item)) {
				$result += $this->__flattenData($item, $prefix, $postfix, $strKey);
			}
			else {
				$result["$prefix$strKey$postfix"] = $item;
			}
		}
		return $result;
	}

	/**
	 * Deletes a value from the object.
	 * @param string $offset The key
	 */
	public function del(string $offset): self
	{
		if (isset($this->data[$offset]))
			unset($this->data[$offset]);
		return $this;
	}

	/**
	 * @return WsLibContainer
	 */
	public function clear(): self
	{
		$this->data = [];
		return $this;
	}

	/**
	 * Lock setup to lock the content.
	 * @param bool $lock
	 * @return WsLibContainer
	 */
	protected function lock(bool $lock = TRUE): self
	{
		$this->lock = $lock;
		return $this;
	}
}