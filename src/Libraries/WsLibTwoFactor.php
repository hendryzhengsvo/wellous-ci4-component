<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use Base32\Base32;
use InvalidArgumentException;
use RobThree\Auth\TwoFactorAuth;
use RobThree\Auth\TwoFactorAuthException;
use Wellous\Ci4Component\Interface\WsInterfaceTwoFactor;

/**
 * Class WsLibTwoFactor
 * Implements WsInterfaceTwoFactor
 * @package
 */
class WsLibTwoFactor implements WsInterfaceTwoFactor
{
	protected static int    $digits = 6;
	protected static int    $period = 30;
	protected static string $secret = '';

	/**
	 * Sets the initial configuration for the OTP generation.
	 * @param string $secret The secret used for generating OTP.
	 * @param int    $digits The number of digits in the OTP. Default is 6.
	 * @param int    $period The time period for which the OTP is valid, in seconds. Default is 60.
	 * @return void
	 * @throws InvalidArgumentException When the secret is empty.
	 */
	public static function initial(string $secret, int $digits = 6, int $period = 60): void
	{
		if (empty($secret))
			throw new InvalidArgumentException('Secret cannot be empty');
		static::$secret = $secret;
		if (!empty($digits))
			static::$digits = $digits;
		if (!empty($period))
			static::$period = $period;
	}

	/**
	 * Retrieves the number of digits used in the generated OTP codes.
	 * @return int The number of digits
	 */
	public static function getDigits(): int
	{
		return static::$digits;
	}

	/**
	 * Retrieves the period for generating Time-based One-Time Passwords (TOTP).
	 * @return int The period in seconds
	 */
	public static function getPeriod(): int
	{
		return static::$period;
	}

	/**
	 * Retrieves the authentication code for a given action.
	 * @param string $action The action to append to the secret (optional)
	 * @return string The authentication code for the action
	 * @throws TwoFactorAuthException
	 */
	public static function getCode(string $action = ''): string
	{
		return (new TwoFactorAuth(APP_NAME ?? ($_SERVER['HTTP_HOST'] ?? ''), static::$digits, static::$period))->getCode(static::getSecret($action));
	}

	/**
	 * Retrieves the secret for a given action.
	 * @param string $action The action to append to the secret (optional)
	 * @return string The encoded secret with the action appended
	 */
	protected static function getSecret(string $action = ''): string
	{
		return Base32::encode(static::$secret . ':' . $action);
	}

	/**
	 * Verifies the given action and code against the stored secret.
	 * @param string $action      The action associated with the code.
	 * @param string $code        The code to be verified.
	 * @param int    $discrepancy The allowed time discrepancy in seconds (default: 10).
	 * @return bool Returns true if the code is verified, false otherwise.
	 * @throws TwoFactorAuthException
	 */
	public static function verify(string $action, string $code, int $discrepancy = 10): bool
	{
		return (new TwoFactorAuth(APP_NAME ?? ($_SERVER['HTTP_HOST'] ?? ''), static::$digits, static::$period))->verifyCode(static::getSecret($action), $code, $discrepancy);
	}
}
