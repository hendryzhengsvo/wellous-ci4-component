<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use CodeIgniter\Events\Events;
use League\CLImate\Argument\Manager;
use League\CLImate\CLImate;
use League\CLImate\Decorator\Style;
use League\CLImate\TerminalObject\Dynamic\Input;
use League\CLImate\TerminalObject\Dynamic\Padding;
use League\CLImate\TerminalObject\Dynamic\Spinner;
use League\CLImate\Util\Output;
use Throwable;
use Wellous\Ci4Component\Libraries\Cli\WsCliCursor;
use Wellous\Ci4Component\Libraries\Cli\WsCliError;
use Wellous\Ci4Component\Libraries\Cli\WsCliOut;
use Wellous\Ci4Component\Libraries\Cli\WsCliProgress;
use Wellous\Ci4Component\Tool\WsExceptionHandle;

/**
 * @method WsLibCli br()
 * @method WsLibCli black(string $str = NULL)
 * @method WsLibCli red(string $str = NULL)
 * @method WsLibCli green(string $str = NULL)
 * @method WsLibCli yellow(string $str = NULL)
 * @method WsLibCli blue(string $str = NULL)
 * @method WsLibCli magenta(string $str = NULL)
 * @method WsLibCli cyan(string $str = NULL)
 * @method WsLibCli lightGray(string $str = NULL)
 * @method WsLibCli darkGray(string $str = NULL)
 * @method WsLibCli lightRed(string $str = NULL)
 * @method WsLibCli lightGreen(string $str = NULL)
 * @method WsLibCli lightYellow(string $str = NULL)
 * @method WsLibCli lightBlue(string $str = NULL)
 * @method WsLibCli lightMagenta(string $str = NULL)
 * @method WsLibCli lightCyan(string $str = NULL)
 * @method WsLibCli white(string $str = NULL)
 * @method WsLibCli backgroundBlack(string $str = NULL)
 * @method WsLibCli backgroundRed(string $str = NULL)
 * @method WsLibCli backgroundGreen(string $str = NULL)
 * @method WsLibCli backgroundYellow(string $str = NULL)
 * @method WsLibCli backgroundBlue(string $str = NULL)
 * @method WsLibCli backgroundMagenta(string $str = NULL)
 * @method WsLibCli backgroundCyan(string $str = NULL)
 * @method WsLibCli backgroundLightGray(string $str = NULL)
 * @method WsLibCli backgroundDarkGray(string $str = NULL)
 * @method WsLibCli backgroundLightRed(string $str = NULL)
 * @method WsLibCli backgroundLightGreen(string $str = NULL)
 * @method WsLibCli backgroundLightYellow(string $str = NULL)
 * @method WsLibCli backgroundLightBlue(string $str = NULL)
 * @method WsLibCli backgroundLightMagenta(string $str = NULL)
 * @method WsLibCli backgroundLightCyan(string $str = NULL)
 * @method WsLibCli backgroundWhite(string $str = NULL)
 * @method WsLibCli bold(string $str = NULL)
 * @method WsLibCli dim(string $str = NULL)
 * @method WsLibCli underline(string $str = NULL)
 * @method WsLibCli blink(string $str = NULL)
 * @method WsLibCli invert(string $str = NULL)
 * @method WsLibCli hidden(string $str = NULL)
 * @method WsLibCli success(string $str = NULL)
 * @method WsLibCli error(string $str = NULL)
 * @method WsLibCli critical(string $str = NULL)
 * @method WsLibCli warn(string $str = NULL)
 * @method WsLibCli comment(string $str = NULL)
 * @method WsLibCli whisper(string $str = NULL)
 * @method WsLibCli shout(string $str = NULL)
 * @method WsLibCli inline(string $str)
 * @method WsLibCli table(array $data)
 * @method WsLibCli draw(string $art)
 * @method WsLibCli border(string $char = NULL, integer $length = NULL)
 * @method WsLibCli dump(mixed $var)
 * @method WsLibCli flank(string $output, string $char = NULL, integer $length = NULL)
 * @method Padding padding(integer $length = 0, string $char = '.')
 * @method Input input(string $prompt)
 * @method Input confirm(string $prompt)
 * @method Input password(string $prompt)
 * @method Input checkboxes(string $prompt, array $options)
 * @method Input radio(string $prompt, array $options)
 * @method WsLibCli columns(array $data, $column_count = NULL)
 * @method WsLibCli clear()
 * @method Spinner spinner(string $label = NULL, string ...$characters = NULL)
 * @property Style   $style
 * @property Manager $arguments
 * @property Output  $output
 */
class WsLibCli
{
	protected static array $argv     = [];
	protected CLImate      $cli;
	protected bool         $isCli;
	protected bool         $showInfo = TRUE;
	public WsCliCursor     $cursor;

	/**
	 * Constructor method.
	 */
	public function __construct()
	{
		$this->isCli = is_cli();
		$this->cli = new CLImate;
		$this->cursor = new WsCliCursor($this->cli);
		global $argv;
		if (empty(static::$argv) && !empty($argv) && is_array($argv))
			static::$argv = $argv;
		if (is_cli()) {
			$this->cli->arguments->add([
				'verbose' => [
					'prefix'      => 'v',
					'longPrefix'  => 'verbose',
					'description' => 'Verbose output',
					'noValue'     => TRUE,
				],
			]);
			$this->cli->arguments->parse(static::$argv);
		}
		$this->cli->output
			->add('error', new WsCliError($this->isCli && $this->cli->arguments->get('verbose')))
			->add('verbose', new WsCliOut($this->isCli && $this->cli->arguments->get('verbose')))->defaultTo('verbose');
		$this->cli->style->addCommand('success', 'green');
		$this->cli->style->addCommand('critical', ['red', 'bold']);
		$this->cli->style->addCommand('warn', 'light_yellow');
	}

	/**
	 * Parse the command line arguments.
	 * @param array $argument The arguments to be added.
	 * @param array $argv     The command line arguments to be parsed. Defaults to an empty array.
	 * @return static Returns the parsed object.
	 */
	public function parse(array $argument, array $argv = []): static
	{
		if (empty($argv))
			$argv = static::$argv;
		$this->cli->arguments->add($argument);
		$this->cli->arguments->parse($argv);
		return $this;
	}

	/**
	 * Gets the value of a property using magic __get method.
	 * @param string $name The name of the property to get.
	 * @return mixed The value of the requested property.
	 */
	public function __get(string $name)
	{
		return $this->cli->$name;
	}

	/**
	 * Returns the CLImate object used for interacting with the command line interface.
	 * @return CLImate The CLImate object.
	 */
	public function getCli(): CLImate
	{
		return $this->cli;
	}

	/**
	 * Handles dynamic method calls in the class.
	 * @param string $name      The name of the method being called.
	 * @param array  $arguments The arguments passed to the method.
	 * @return WsLibCli The current object instance.
	 */
	public function __call(string $name, array $arguments): WsLibCli
	{
		if (!$this->isCli)
			return $this;
		$name = match ($name) {
			'write' => 'out',
			default => $name,
		};
		$feature = [
			'inline', 'br', 'table', 'json', 'draw', 'border', 'dump',
			'flank', 'spinner', 'padding', 'input', 'confirm', 'password',
			'checkboxes', 'radio', 'columns', 'clear', 'style', 'out'];
		if (in_array(strtolower($name), $feature, TRUE)) {
			Events::trigger('wslib:cli:before', $name, $arguments);
			$this->cli->{$name}(...$arguments);
		}
		else {
			if (!empty($arguments[0]) && is_string($arguments[0]) && count($arguments) === 1)
				$arguments[0] = $this->addInfo($arguments[0]);
			Events::trigger('wslib:cli:before', $name, $arguments);
			$this->cli->{$name}($arguments);
		}
		return $this;
	}

	/**
	 * Adds additional information to the given content.
	 * @param string $content The content to add information to.
	 * @return string The modified content with additional information.
	 */
	public function addInfo(string $content = '', array|string $format = 'dim'): string
	{
		$elapsed = 'ET ' . WsLibDateTimeMethod::secondToTime((int)(WsLibProfiler::getTimerETA() / 1e+3));
		$memory = 'Mem ' . WsLibUtilities::byteToSize(memory_get_usage());
		$format = is_array($format) ? $format : [$format];
		$result = "[$elapsed, $memory]";
		if ($this->showInfo) {
			$result = (!empty($format) ? "<" . implode("><", $format) . ">" : "") .
				$result .
				(!empty($format) ? "</" . implode("></", array_reverse($format)) . ">" : "");
		}
		return $result . (!empty($content) ? " " . trim($content) : "");
	}

	/**
	 * Starts the execution of a task identified by the given name.
	 * @param string $caller
	 * @return WsLibCli The current WsLibCli object.
	 */
	public function start(string $caller = ''): WsLibCli
	{
		if (!empty($caller))
			$caller = WsLibUtilities::getCallerInfo();
		WsLibProfiler::addCheckPoint($caller);
		$this->cli->br();
		$this->info("Start : <underline><bold>$caller</bold></underline>");
		return $this;
	}

	/**
	 * Displays informational content with blue color on the command line interface.
	 * @param string|null $content The content to display. If NULL, the method will not display anything.
	 * @return WsLibCli The WsLibCli object.
	 */
	public function info(?string $content = NULL): WsLibCli
	{
		$this->blue($content);
		return $this;
	}

	/**
	 * Ends a given process and prints information about it.
	 * @param string $caller The name of the process to end.
	 * @return WsLibCli The current object.
	 */
	public function end(string $caller = ''): WsLibCli
	{
		if (!empty($caller))
			$caller = WsLibUtilities::getCallerInfo();
		$this->info("End : <underline><bold>$caller</bold></underline>, elapsed " . WsLibDateTimeMethod::secondToTime((int)(WsLibProfiler::getTimerETA($caller) / 1e+3)))
		     ->br();
		return $this;
	}

	/**
	 * Sets the status message and displays it on the command line interface.
	 * @param string $content The content of the status message.
	 * @return WsLibCli The WsLibCli object.
	 */
	public function status(string $content = ''): WsLibCli
	{
		$this->comment()->inline("\r\e[K" . $this->addInfo("Status: $content"));
		return $this;
	}

	/**
	 * Logs and displays an exception.
	 * @param Throwable $ex The exception to log and display.
	 * @return WsLibCli The current instance of WsLibCli.
	 */
	public function exception(Throwable $ex): WsLibCli
	{
		$reportedFile = str_replace(ROOTPATH, "(repo)", $ex->getFile()) . ":{$ex->getLine()}";
		$trace = explode("\n", str_replace(ROOTPATH, "(repo)", WsExceptionHandle::getAllTraceString($ex)));
		$fullClassName = get_class($ex);
		$shortClassName = str_contains($fullClassName, '\\') ? ltrim(strrchr($fullClassName, '\\'), '\\') : $fullClassName;
		$this->cli->output->defaultTo('error');
		$this->border('-')
		     ->rawOut("<background_light_red><white><bold> -= Exception =- </bold></white></background_light_red>")
		     ->rawOut("<magenta>Exception</magenta> - <red><bold>$shortClassName</bold></red><bold>\n<cyan>" .
			     $ex->getMessage() . '</cyan> on <yellow>' . $reportedFile .
			     "</yellow>\n<bold><magenta>Trace</magenta>:</bold><dark_gray>\n" . str_repeat(' ', 4) . implode("\n" . str_repeat(' ', 4), $trace) . "</dark_gray>");
		$this->cli->output->defaultTo('error');
		return $this;
	}

	/**
	 * Outputs raw content to the command line interface.
	 * @param string $content The content to output.
	 * @return WsLibCli The current instance of the WsLibCli class.
	 */
	public function rawOut(string $content): WsLibCli
	{
		$this->cli->out($content);
		return $this;
	}

	/**
	 * Outputs the specified content to the command line interface.
	 * @param string|null $content The content to be outputted. Defaults to NULL.
	 * @return WsLibCli Returns the current WsLibCli object for method chaining.
	 */
	public function out(mixed $content = NULL, bool $raw = FALSE): WsLibCli
	{
		if (!is_cli() || $content === NULL)
			return $this;
		if (is_string($content))
			$this->cli->out($raw ? $content : $this->addInfo($content));
		else {
			if (!$raw)
				$this->inline($this->addInfo(' '));
			$this->json($content);
		}
		return $this;
	}

	/**
	 * Returns a new instance of WsCliProgress used for displaying progress bar in CLI interface.
	 * @param int $total  The total progress value.
	 * @param int $maxBar The maximum progress bar length.
	 * @return WsCliProgress The WsCliProgress object.
	 */
	public function progress(int $total, int $maxBar = 100): WsCliProgress
	{
		return new WsCliProgress($this, $total, $maxBar);
	}

	/**
	 * Prints the structure of the given data to the command line interface.
	 * @param mixed $data       The data to print the structure of.
	 * @param int   $indent     (Optional) The level of indentation. Default is 0.
	 * @param int   $indentSize (Optional) The size of each indentation. Default is 2.
	 * @return WsLibCli The WsLibCli object.
	 */
	public function json(mixed $data, int $indent = 0, int $indentSize = 2): WsLibCli
	{
		$indent++;
		if (is_array($data) || is_object($data)) {
			$isList = is_array($data) && array_is_list($data);
			$this->inline("\e[K" . ($isList ? "[" : "{"));
			$started = FALSE;
			foreach ($data as $key => $value) {
				if ($started)
					$this->inline(",");
				else
					$started = TRUE;
				$this->br()->inline("\e[K");
				if (!$isList)
					$this->inline(str_repeat(" ", $indent * $indentSize) . '<magenta>"' . $key . '"</magenta> : ');
				if (is_object($value) || is_array($value))
					$this->json($value, $indent, $indentSize);
				else
					$this->value($value, $isList ? $indent : 0);
			}
			$this->br()->inline("\e[K" . str_repeat(" ", (max(($indent - 1), 0)) * $indentSize) . ($isList ? "]" : "}"));
			if ($indent === 1)
				$this->inline("\e[K");
		}
		else
			$this->value($data, 0, $indentSize);
		if ($indent === 1)
			$this->br();
		return $this;
	}

	/**
	 * Formats the given data into a string.
	 * @param mixed  $data The data to format.
	 * @param bool   $wrap (optional) Whether to wrap the string or not. Default is FALSE.
	 * @param string $type (optional) The type of the data. If empty, the type will be determined using gettype().
	 * @return string The formatted data as a string.
	 */
	public function format(mixed $data, bool $wrap = FALSE, string $type = '', bool $strQuate = FALSE): string
	{
		if (empty($type))
			$type = gettype($data);
		$strQuate = $strQuate ? '"' : '';
		switch ($type) {
			case "NULL":
				$result = '<dark_gray>(NULL)</dark_gray>';
				break;
			case 'boolean':
				$result = '<yellow>' . ($data ? 'TRUE' : 'FALSE') . '</yellow>';
				break;
			case 'integer':
			case 'double':
				$result = '<green>' . $data . '</green>';
				break;
			case 'string':
				$result = "<red>$strQuate" . str_replace(["\r\n", "\r", "\n"], $wrap ? "\n" : "<magenta>\\n</magenta>", $data) . "$strQuate</red>";
				break;
			default:
				try {
					if (is_resource($data))
						$result = '<cyan>(Resource ' . get_resource_type($data) . ')</cyan>';
					else
						$result = '<blue>$strQuate' . $data . '$strQuate</blue>';
				} catch (Throwable) {
					$result = '<dark_gray>(Non-displayable type : ' . ucfirst($type) . ')</dark_gray>';
				}
				break;
		}
		return $result;
	}

	/**
	 * Prints the value of the given data to the command line interface with optional indentation.
	 * @param mixed $data       The data to be printed.
	 * @param int   $indent     The number of indentations. Defaults to 0.
	 * @param int   $indentSize The size of each indentation in spaces. Defaults to 2.
	 * @return WsLibCli The current instance of the WsLibCli object.
	 */
	public function value(mixed $data, int $indent = 0, int $indentSize = 2): WsLibCli
	{
		if ($indent > 0)
			$this->inline(str_repeat(" ", $indent * $indentSize));
		$this->inline(static::format($data, strQuate: TRUE));
		return $this;
	}
}