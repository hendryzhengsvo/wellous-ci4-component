<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use Aws\CloudFront\CloudFrontClient;
use Aws\Command;
use Aws\Credentials\Credentials;
use Aws\Exception\MultipartUploadException;
use Aws\Result;
use Aws\S3\MultipartUploader;
use Aws\S3\S3Client;
use GuzzleHttp\RequestOptions;
use ReflectionClass;
use Throwable;
use Wellous\Ci4Component\Exceptions\ServerInternalError;
use Wellous\Ci4Component\Interface\WsInterfaceCloudFiles;

class WsLibCloudFiles implements WsInterfaceCloudFiles
{
	// The minimum part size for multipart uploads is 5 MB
	private const MIN_PART_SIZE_MB = 5;
	// The minimum part size for multipart uploads in bytes
	private const MIN_PART_SIZE = self::MIN_PART_SIZE_MB * MB;
	// The configuration for the S3 bucket
	protected static array $bucketConfig;
	// The configuration for CloudFront
	protected static array $cfConfig = [];
	// The S3 client instance
	private static ?S3Client $client = NULL;

	/**
	 * Initializes the configuration for the bucket and CloudFront.
	 * @param array $bucketConfig   The configuration for the bucket.
	 *                              - region: The region of the bucket.
	 *                              - bucket: The name of the bucket.
	 *                              - access: The access credentials.
	 *                              - key: The access key.
	 *                              - secret: The secret key.
	 * @param array $cfConfig       The configuration for CloudFront.
	 *                              - cfDistributionId: The CloudFront distribution ID.
	 *                              - domain: The domain associated with the distribution.
	 *                              - access: The access credentials.
	 *                              - key: The access key.
	 *                              - secret: The secret key.
	 * @return void
	 */
	public static function initial(array $bucketConfig, array $cfConfig = []): void
	{
		if (!empty($bucketConfig['region']) && !empty($bucketConfig['bucket']) && !empty($bucketConfig['access'])
			&& !empty($bucketConfig['access']['key']) && !empty($bucketConfig['access']['secret'])
		)
			static::$bucketConfig = $bucketConfig;

		if (!empty($cfConfig['cfDistributionId']) && !empty($cfConfig['domain'])
			&& !empty($cfConfig['access']['key']) && !empty($cfConfig['access']['secret'])
		)
			static::$cfConfig = $cfConfig;
	}

	/**
	 * Returns the URL for a given key file and key path.
	 * @param string $keyFile The key file to retrieve the URL for.
	 * @param string $keyPath The key path associated with the key file.
	 * @return string The URL of the key file, or an empty string if an exception occurs.
	 */
	public static function getUrl(string $keyFile, string $keyPath): string
	{
		$keyFile = static::getKeyFile($keyFile, $keyPath);
		try {
			$result = static::isExists($keyFile) ? static::getLink($keyFile) : '';
		} catch (Throwable $ex) {
			static::handleException($ex, "s3::getUrl($keyFile)");
			$result = '';
		}
		return $result;
	}

	/**
	 * Returns the key file path by combining the key file and key path.
	 * If the key path is empty, the key file is returned as-is.
	 * @param string $keyFile The key file.
	 * @param string $keyPath The key path.
	 * @return string The combined key file path.
	 */
	public static function getKeyFile(string $keyFile, string $keyPath): string
	{
		if (empty($keyPath))
			return $keyFile;
		$parsePath = pathinfo("$keyPath/$keyFile");
		return ((!empty($parsePath['dirname']) && $parsePath['dirname'] !== '/') ? "{$parsePath['dirname']}/" : "") .
			"{$parsePath['filename']}" . (!empty($parsePath['extension']) ? ".{$parsePath['extension']}" : '');
	}

	/**
	 * Determines if a file exists in an Amazon S3 bucket.
	 * @param string $keyFile The key of the file.
	 * @param string $keyPath The path of the file (optional).
	 * @return bool Returns true if the file exists, false otherwise.
	 * @throws ServerInternalError
	 */
	public static function isExists(string $keyFile, string $keyPath = ''): bool
	{
		$s3Client = static::getS3Client();
		$keyFile = static::getKeyFile($keyFile, $keyPath);
		$keyFile = str_starts_with($keyFile, '/') ? substr($keyFile, 1) : $keyFile;
		return $s3Client->doesObjectExist(static::$bucketConfig['bucket'], $keyFile);
	}

	/**
	 * Retrieves the S3 client instance to interact with AWS S3.
	 * @return S3Client The S3 client instance.
	 * @throws ServerInternalError If the S3 bucket config is empty.
	 */
	protected static function getS3Client(): S3Client
	{
		if (empty(static::$bucketConfig))
			throw new ServerInternalError('S3 bucket config is empty');
		if (empty(static::$client))
			static::$client = new S3Client([
				'region'      => static::$bucketConfig['region'] ?? '',
				'version'     => 'latest',
				'credentials' => new Credentials(static::$bucketConfig['access']['key'] ?? '', static::$bucketConfig['access']['secret'] ?? ''),
				'verify'      => FALSE,
			]);
		return static::$client;
	}

	/**
	 * Returns the link of a file based on the provided key file.
	 * @param string $keyFile The key file of the file.
	 * @return string The generated link of the file.
	 */
	protected static function getLink(string $keyFile): string
	{
		$keyFile = str_starts_with($keyFile, '/') ? $keyFile : "/$keyFile";
		$domain = !empty(static::$cfConfig['domain'])
			?
			static::$cfConfig['domain']
			:
			(static::$bucketConfig['bucket'] ?? '') . '.s3.' . (static::$bucketConfig['region'] ?? '') . '.amazonaws.com';
		return "https://$domain$keyFile";
	}

	/**
	 * Handles an exception by logging it and formatting the data into a JSON string.
	 * @param Throwable $ex     The exception to handle.
	 * @param string    $method The method where the exception occurred.
	 * @return void
	 */
	protected static function handleException(Throwable $ex, string $method): void
	{
		$reflect = new ReflectionClass($ex);
		log_message('error', json_encode([
			'status'    => 'exception',
			'app'       => APP_NAME ?? ($_SERVER['HTTP_HOST'] ?? ''),
			'domain'    => $_SERVER['HTTP_HOST'] ?? '',
			'type'      => $reflect->getShortName(),
			'exception' => [
				'message' => "$method: " . $ex->getMessage() . " on {$ex->getFile()}:{$ex->getLine()}",
				'trace'   => $ex->getTraceAsString(),
			],
			'profiler'  => WsLibProfiler::fetch(),
		], JSON_UNESCAPED_UNICODE | JSON_PARTIAL_OUTPUT_ON_ERROR));
	}

	/**
	 * Retrieves a pre-signed URL for a file stored in an S3 bucket.
	 * @param string $keyFile The path to the file in the bucket.
	 * @param string $keyPath The base path of the bucket.
	 * @param string $expires The expiration time of the URL (default: '+1 day').
	 * @return string            The pre-signed URL for the file.
	 * @throws ServerInternalError
	 * @deprecated Use genDownloadPreSignUrl instead.
	 */
	public static function getPreSignUrl(string $keyFile, string $keyPath = '', string $expires = '+1 day', array $options = []): string
	{
		return static::genDownloadEndpoint(static::getKeyFile($keyFile, $keyPath), $expires, $options);
	}

	/**
	 * Retrieves a pre-signed URL for a file stored in an S3 bucket.
	 * @param string $keyFile The path to the file in the bucket.
	 * @param string $expires The expiration time of the URL (default: '+1 day').
	 * @return string            The pre-signed URL for the file.
	 * @throws ServerInternalError
	 */
	public static function genDownloadEndpoint(string $keyFile, string $expires = '+1 day', array $options = []): string
	{
		$s3 = static::getS3Client();
		$fileExist = static::isExists($keyFile);
		if ($fileExist) {
			$command = $s3->getCommand('GetObject', [
				'Bucket' => static::$bucketConfig['bucket'],
				'Key'    => $keyFile,
				...$options,
			]);
			$presignedRequest = $s3->createPresignedRequest($command, $expires);
			$presignedUrl = (string)$presignedRequest->getUri();
		}
		else
			$presignedUrl = '';
		return $presignedUrl;
	}

	/**
	 * Generates a presigned URL for a single file upload.
	 * @param string $fullKey The full key of the object to be uploaded.
	 * @param string $expires The expiration time of the presigned URL. Default is '+20 minutes'.
	 * @param array  $options Additional options to be passed to the 'PutObject' command.
	 * @return string The generated presigned URL.
	 * @throws ServerInternalError
	 */
	public static function genSingleUploadEndpoint(string $fullKey, string $expires = '+1 day', array $options = []): string
	{
		$s3Client = static::getS3Client();
		$cmd = $s3Client->getCommand('PutObject', [
			'Bucket' => static::$bucketConfig['bucket'],
			'Key'    => $fullKey,
			...$options,
		]);
		$request = $s3Client->createPresignedRequest($cmd, $expires);
		return (string)$request->getUri();
	}

	/**
	 * Generates a multipart upload endpoint for S3 using the given parameters.
	 * @param string $fullKey  The full key (path) for the object to be uploaded.
	 * @param int    $filesize The size of the file in bytes.
	 * @param int    $partSize The size of each part in bytes. Defaults to 0.
	 * @param string $expires  The expiration time for the presigned URLs. Defaults to '+20 minutes'.
	 * @param array  $options  Additional options to be passed to the 'UploadPart' command.
	 * @return array           An array of presigned URLs for uploading the file.
	 * @throws ServerInternalError
	 */
	public static function genMultipartUploadEndpoint(string $fullKey, int $filesize, int $partSize = 0, string $expires = '+20 minutes',
	                                                  array  $options = []): array
	{
		$s3Client = static::getS3Client();
		$result = $s3Client->createMultipartUpload([
			'Bucket' => static::$bucketConfig['bucket'],
			'Key'    => $fullKey,
		]);
		$presignedUrls = [
			'uploadId'     => $result['UploadId'],
			'endpointUrls' => [],
		];
		$totalParts = ceil($filesize / $partSize);
		for ($partNumber = 1; $partNumber <= $totalParts; $partNumber++) {
			$cmd = $s3Client->getCommand('UploadPart', [
				'Bucket'     => static::$bucketConfig['bucket'],
				'Key'        => $fullKey,
				'UploadId'   => $presignedUrls['uploadId'],
				'PartNumber' => $partNumber,
				...$options,
			]);
			$request = $s3Client->createPresignedRequest($cmd, $expires);
			$presignedUrls['endpointUrls'][] = (string)$request->getUri();
		}
		return $presignedUrls;
	}

	/**
	 * Generates the upload endpoint based on the given parameters.
	 * @param string $fullKey  The full key of the upload.
	 * @param int    $filesize The size of the file in bytes.
	 * @param int    $partSize The size of each part in bytes. Default is 0.
	 * @param string $expires  The expiration time of the upload endpoint. Default is '+20 minutes'.
	 * @param array  $options  Additional options for the upload. Default is an empty array.
	 * @return array The generated upload endpoint details.
	 * @throws ServerInternalError
	 */
	public static function genUploadEndpoint(string $fullKey, int $filesize, int $partSize = 0, string $expires = '+20 minutes',
	                                         array  $options = []): array
	{
		if ($partSize <= 0)
			$partSize = static::MIN_PART_SIZE;
		if ($filesize > $partSize && $partSize >= static::MIN_PART_SIZE)
			$presignedUrls = static::genMultipartUploadEndpoint($fullKey, $filesize, $partSize, $expires, $options);
		else {
			$partSize = $filesize;
			$presignedUrls = static::genMultipartUploadEndpoint($fullKey, $filesize, $partSize, $expires, $options);
		}
		return [
				'expires'  => $expires,
				'partSize' => $partSize,
				'key'      => $fullKey,
			] + $presignedUrls;
	}

	/**
	 * Uploads the completed multipart upload to an S3 bucket.
	 * @param string $fullKey  The full key of the uploaded file.
	 * @param string $uploadId The upload ID of the multipart upload.
	 * @param array  $parts    The array of parts for the multipart upload.
	 * @param array  $options  Additional options for the completeMultipartUpload method.
	 * @return bool Returns true if the multipart upload is successfully completed, false otherwise.
	 * @throws ServerInternalError
	 */
	public static function uploadMultiPartComplete(string $fullKey, string $uploadId, array $parts, array $options = []): bool
	{
		//Sorting PartNumber, must follow sequential order
		usort($parts, function ($a, $b) {
			return $a['PartNumber'] - $b['PartNumber'];
		});
		$s3Client = static::getS3Client();
		$result = $s3Client->completeMultipartUpload([
			'Bucket'          => static::$bucketConfig['bucket'],
			'Key'             => $fullKey,
			'UploadId'        => $uploadId,
			'MultipartUpload' => [
				'Parts' => $parts,
			],
			...$options,
		]);
		return isset($result['Location']);
	}

	/**
	 * Uploads a file to an S3 bucket.
	 * @param string $keyFile     The key file for the uploaded file.
	 * @param mixed  $fileContent The content of the file to be uploaded. Can be a resource, string, or file path.
	 * @param string $keyPath     [optional] The key path for the uploaded file. Default is an empty string.
	 * @param array  $options     [optional] Additional options for the upload process. Default is an empty array.
	 * @return bool|string          Returns the URL of the uploaded file if successful, false otherwise.
	 */
	public static function upload(string $keyFile, mixed $fileContent, string $keyPath = '', array $options = []): bool|string
	{
		$keyFile = static::getKeyFile($keyFile, $keyPath);
		try {
			if (is_string($fileContent) && (str_starts_with($fileContent, 'http://') || str_starts_with($fileContent, 'https://'))) {
				$tempFile = WsLibUtilities::getTempFile();
				WsLibRpc::get($fileContent, [
					RequestOptions::SINK => $tempFile,
				]);
				$response = WsLibRpc::getLastResponse();
				if (empty($options['ContentType'])) {
					$options['ContentType'] = $response->getHeader('Content-Type');
					if (empty($options['ContentType']))
						$options['ContentType'] = WsLibUtilities::guessMimeTypeByFile($tempFile);
					if (empty($options['ContentType']) || !is_string($options['ContentType']))
						$options['ContentType'] = 'application/octet-stream';
				}
				$fileContent = fopen($tempFile, 'r');
			}
			if (empty($options['ContentType'])) {
				if (is_resource($fileContent)) {
					rewind($fileContent);
					$options['ContentType'] = WsLibUtilities::guessMimeTypeByContent(stream_get_contents($fileContent));
				}
				elseif (is_string($fileContent)) {
					if (file_exists($fileContent))
						$options['ContentType'] = WsLibUtilities::guessMimeTypeByFile($fileContent);
					else
						$options['ContentType'] = WsLibUtilities::guessMimeTypeByContent($fileContent);
				}
			}
			if (!empty($options['ContentType']) && $options['ContentType'] === 'application/octet-stream')
				unset($options['ContentType']);
			$tmpFile = is_resource($fileContent)
				? $fileContent
				: (file_exists($fileContent) && is_file($fileContent) ? fopen($fileContent, 'r')
					: tmpfile());
			if (is_resource($tmpFile) && is_string($fileContent) && !file_exists($fileContent))
				fwrite($tmpFile, file_exists($fileContent) && is_file($fileContent) ? file_get_contents($fileContent) : $fileContent);
			if (is_resource($tmpFile))
				rewind($tmpFile);

			if (!empty(static::$bucketConfig['acl']))
				$options['acl'] = static::$bucketConfig['acl'];
			$options = array_merge($options, [
				'Bucket'          => static::$bucketConfig['bucket'],
				'Key'             => $keyFile,
				'before_initiate' => function (Command $command) use ($options) {
					if (!empty($options['ContentType']))
						$command['ContentType'] = $options['ContentType'];
					if (!empty($options['ContentDisposition']))
						$command['ContentDisposition'] = $options['ContentDisposition'];
				},
			]);
			WsLibProfiler::addCheckPoint('s3::upload');

			//Recover from errors
			$s3Client = static::getS3Client();
			$fileExist = static::isExists($keyFile);
			$uploader = new MultipartUploader($s3Client, $tmpFile, $options);
			do {
				try {
					$result = $uploader->upload();
				} catch (MultipartUploadException $e) {
					$uploader = new MultipartUploader($s3Client, $tmpFile, ['state' => $e->getState()]);
				}
			} while (!isset($result));
			if ($fileExist)
				static::invalidate($keyFile);
			WsLibProfiler::addLog('s3::upload', "S3:upload $keyFile");
			$objUrl = (string)$result->get('ObjectURL');
			return !empty(static::$cfConfig['domain']) ? static::getLink(parse_url($objUrl, PHP_URL_PATH)) : $objUrl;
		} catch (Throwable $ex) {
			static::handleException($ex, "s3::upload($keyFile)");
			return FALSE;
		}
	}

	/**
	 * Invalidates the specified file in the CloudFront distribution if the configuration is set.
	 * @param string $keyFile The file to invalidate, relative to the CloudFront distribution.
	 */
	protected static function invalidate(string $keyFile, array $options = []): void
	{
		try {
			if (!empty(static::$cfConfig) && !empty(static::$cfConfig['cfDistributionId'])) {
				$cloudFrontClient = new CloudFrontClient([
					'version'     => 'latest',
					'region'      => static::$bucketConfig['region'],
					'credentials' => new Credentials(static::$cfConfig['access']['key'], static::$cfConfig['access']['secret']),
				]);

				$keyFile = str_starts_with($keyFile, '/') ? $keyFile : "/$keyFile";
				WsLibProfiler::addCheckPoint('s3::invalidate');
				$cloudFrontClient->createInvalidation([
					'DistributionId'    => static::$cfConfig['cfDistributionId'],
					'InvalidationBatch' => [
						'CallerReference' => microtime(TRUE) * 1e+9,
						'Paths'           => [
							'Items'    => [$keyFile],
							'Quantity' => 1,
						],
					],
					...$options,
				]);
				WsLibProfiler::addLog('s3::invalidate', "S3:invalidation $keyFile");
				WsLibProfiler::delCheckPoint('s3::invalidate');
			}
		} catch (Throwable $ex) {
			static::handleException($ex, "s3::invalidate($keyFile)");
			return;
		}
	}

	/**
	 * Retrieves information about an object from the S3 bucket specified in the config.
	 * @param string $keyFile The key file of the object to retrieve information about.
	 * @param string $keyPath The path to the key file (optional).
	 * @return Result|bool The result of the headObject operation if successful, otherwise FALSE.
	 * @throws ServerInternalError
	 */
	public static function info(string $keyFile, string $keyPath = '', array $options = []): Result|bool
	{
		$baseKeyName = static::getKeyFile($keyFile, $keyPath);
		$s3Client = static::getS3Client();
		try {
			WsLibProfiler::addCheckPoint('s3::info');
			if (!($result = $s3Client->headObject([
				'Bucket' => static::$bucketConfig['bucket'],
				'Key'    => $baseKeyName,
				...$options,
			])))
				return FALSE;
			WsLibProfiler::addLog('s3::info', "S3:info $baseKeyName");
			WsLibProfiler::delCheckPoint('s3::info');
			return $result;
		} catch (Throwable $ex) {
			static::handleException($ex, "s3::info($keyFile)");
			return FALSE;
		}
	}

	/**
	 * Retrieves an object from the S3 bucket specified in the config.
	 * @param string $keyFile The key file of the object to be retrieved.
	 * @param string $keyPath The path to the key file (optional).
	 * @return Result|bool The result of the getObject operation if successful, otherwise FALSE.
	 * @throws ServerInternalError
	 */
	public static function get(string $keyFile, string $keyPath = '', array $options = []): Result|bool
	{
		$baseKeyName = static::getKeyFile($keyFile, $keyPath);
		$s3Client = static::getS3Client();
		try {
			WsLibProfiler::addCheckPoint('s3::get');
			if (!($result = $s3Client->getObject([
				'Bucket' => static::$bucketConfig['bucket'],
				'Key'    => $baseKeyName,
				...$options,
			])))
				return FALSE;
			WsLibProfiler::addLog('s3::get', "S3:get $baseKeyName");
			WsLibProfiler::delCheckPoint('s3::get');
			return $result;
		} catch (Throwable $ex) {
			static::handleException($ex, "s3::get($keyFile)");
			return FALSE;
		}
	}

	/**
	 * Copies a file from one location to another in Amazon S3.
	 * @param string $oldKeyFile The key of the file to be copied.
	 * @param string $newKeyFile The new key for the copied file.
	 * @return bool Returns TRUE if the file was copied successfully, otherwise FALSE.
	 * @throws ServerInternalError
	 */
	public static function copy(string $oldKeyFile, string $newKeyFile, array $options = []): bool
	{
		$s3Client = static::getS3Client();
		try {
			WsLibProfiler::addCheckPoint('s3::copy');
			if (!($result = $s3Client->copyObject([
				'Bucket' => static::$bucketConfig['bucket'] . "$oldKeyFile",
				'Key'    => $newKeyFile,
				...$options,
			])))
				return FALSE;
			WsLibProfiler::addLog('s3::copy', "S3:delete $newKeyFile");
			WsLibProfiler::delCheckPoint('s3::copy');    // Check if the ETag is present in the result
			return isset($result['ETag']);
		} catch (Throwable $ex) {
			static::handleException($ex, "s3::copy($oldKeyFile, $newKeyFile)");
			return FALSE;
		}
	}

	/**
	 * Moves a file from one location to another in the S3 bucket.
	 * @param string $oldKeyFile The path of the file to move.
	 * @param string $newKeyFile The new path for the file.
	 * @return bool True if the move is successful, false otherwise.
	 */
	public static function move(string $oldKeyFile, string $newKeyFile, array $options = []): bool
	{
		$result = FALSE;
		try {
			WsLibProfiler::addCheckPoint('s3::move');
			if (static::copy($oldKeyFile, $newKeyFile, $options) && static::delete($oldKeyFile))
				$result = TRUE;
			WsLibProfiler::addLog('s3::move', "S3:move $oldKeyFile -> $newKeyFile");
			WsLibProfiler::delCheckPoint('s3::move');
			return $result;
		} catch (Throwable $ex) {
			static::handleException($ex, "s3::move($oldKeyFile, $newKeyFile)");
			return FALSE;
		}
	}

	/**
	 * Deletes an object from Amazon S3.
	 * @param string $keyFile The file key of the object to delete.
	 * @param string $keyPath The path of the object to delete (optional).
	 * @return bool  True if the object is successfully deleted, false otherwise.
	 * @throws ServerInternalError
	 */
	public static function delete(string $keyFile, string $keyPath = '', array $options = []): bool
	{
		$baseKeyName = static::getKeyFile($keyFile, $keyPath);
		$s3Client = static::getS3Client();
		try {
			WsLibProfiler::addCheckPoint('s3::delete');
			if (!($result = $s3Client->deleteObject([
				'Bucket' => static::$bucketConfig['bucket'],
				'Key'    => $baseKeyName,
				...$options,
			])))
				return FALSE;
			WsLibProfiler::addLog('s3::delete', "S3:delete $baseKeyName");
			WsLibProfiler::delCheckPoint('s3::delete');
			return !empty($result['DeleteMarker']);
		} catch (Throwable $ex) {
			static::handleException($ex, "s3::delete($keyFile)");
			return FALSE;
		}
	}
}