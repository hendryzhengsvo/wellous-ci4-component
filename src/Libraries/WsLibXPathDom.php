<?php
declare(strict_types=1);

namespace Wellous\Ci4Component\Libraries;

use DOMDocument;
use DOMElement;
use DOMNode;
use DOMNodeList;
use DOMXPath;

class WsLibXPathDom
{

	/**
	 * @var DOMXPath Dom XPath Element Library
	 */
	protected DOMXPath $domXPath;
	/**
	 * @var DOMNodeList
	 */
	protected DOMNodeList $domNodeList;

	/**
	 * @var DOMDocument Dom Document Library
	 */
	protected DOMDocument $dom;

	/**
	 * Constructor method for the class
	 */
	public function __construct()
	{
		libxml_use_internal_errors(TRUE);
		$this->dom = new DOMDocument();
	}

	/**
	 * Load content into the DOM
	 * @param string $content The content to load into the DOM
	 * @return self
	 */
	public function loadContent(string $content): self
	{
		if ($this->dom->loadHTML($content)) {
			unset($this->domXPath);
			$this->domXPath = new DOMXPath($this->dom);
			$this->domXPath->registerNamespace("php", "http://php.net/xpath");
			$this->domXPath->registerPhpFunctions();
		}
		return $this;
	}

	/**
	 * Searches for DOM nodes with a specific tag and attribute.
	 * @param string       $queryTag    The tag to query for.
	 * @param string       $attribute   The attribute to search for.
	 * @param DOMNode|null $contextNode (Optional) The context node to search within. If omitted, the search will be performed on the entire DOM.
	 * @return DOMNodeList|null
	 */
	public function searchTagAttr(string $queryTag, string $attribute, ?DOMNode $contextNode = NULL): ?DOMNodeList
	{
		return $this->queryDom("{$queryTag}[@$attribute]", $contextNode);
	}

	/**
	 * Searches for DOM nodes with a specific tag and attribute*/
	public function queryDom(string $queryString, DOMNode $contextNode = NULL): ?DOMNodeList
	{
		$this->domNodeList = $this->domXPath->query($queryString, $contextNode);
		return $this->domNodeList;
	}

	/**
	 * Searches for DOM nodes with a specific tag, attribute, and attribute value.
	 * @param string       $queryString The query string to specify the tag and attribute.
	 * @param string       $attribute
	 * @param string       $content
	 * @param DOMNode|null $contextNode
	 * @return DOMNodeList|null
	 */
	public function searchTagAttrValue(string  $queryString,
	                                   string  $attribute,
	                                   string  $content,
	                                   DOMNode $contextNode = NULL): ?DOMNodeList
	{
		$content = str_replace("'", "\\'", $content);
		return $this->queryDom("{$queryString}[contains(@$attribute,'$content')]", $contextNode);
	}

	/**
	 * Retrieves the value of a DOM element.
	 * If the provided $dom parameter is a string, the method will attempt to retrieve the DOM element using the $dom parameter as a selector. If $contextNode is provided, the search will be performed within the context of the provided DOMNode. Otherwise, the search will be performed on the entire DOM.
	 * If the provided $dom parameter is an instance of DOMNode, the method will directly retrieve the value of the DOMNode's nodeValue property.
	 * @param string|DOMNode $dom         The DOM element or selector to retrieve the value from.
	 * @param DOMNode|null   $contextNode (Optional) The context node to search within when $dom is a string selector. If omitted, the search will be performed on the entire DOM.
	 * @return string|null                 The value of the DOM element, or null if the element does not exist or the DOM parameter is invalid.
	 */
	public function getElementValue(string|DOMNode $dom, DOMNode $contextNode = NULL): ?string
	{
		if (is_string($dom) && !empty($dom))
			$dom = $this->oneDom($dom, 'ASC', $contextNode);
		if ($dom instanceof DOMNode) {
			return $dom->nodeValue;
		}
		else
			return NULL;
	}

	/**
	 * Retrieves a single DOM node based on a specified query string.
	 * @param string       $queryString The query string used to select the DOM node.
	 * @param string       $order       (Optional) The order in which the matching nodes should be considered. Defaults to 'ASC'.
	 * @param DOMNode|null $contextNode (Optional) The context node to search within. If omitted, the search will be performed on the entire DOM.
	 * @return DOMNode|null              The selected DOM node, or null if no node was found.
	 */
	public function oneDom(string  $queryString,
	                       string  $order = 'ASC',
	                       DOMNode $contextNode = NULL): ?DOMNode
	{
		$domNodeList = $this->domXPath->query($queryString, $contextNode);
		$position = 0;
		if (strtoupper($order) === 'DESC')
			$position = $domNodeList->length - 1;
		return $domNodeList instanceof DOMNodeList ? $domNodeList->item($position) : NULL;
	}

	/**
	 * Retrieves the value of a specific attribute from a DOM element.
	 * @param string|DOMNode $dom         The DOM element or the ID of the DOM element to retrieve the attribute value from.
	 * @param string         $attr        The attribute to retrieve the value of.
	 * @param DOMNode|null   $contextNode (Optional) The context node to search within. If omitted, the search will be performed on the entire DOM.
	 * @return string|null    The value of the attribute, or null if the attribute does not exist or the input is not a valid DOM element.
	 */
	public function getElementAttr(string|DOMNode $dom,
	                               string         $attr,
	                               DOMNode        $contextNode = NULL): ?string
	{
		if (is_string($dom) && !empty($dom))
			$dom = $this->oneDom($dom, 'ASC', $contextNode);
		if ($dom instanceof DOMElement) {
			return trim($dom->getAttribute($attr));
		}
		else
			return NULL;
	}

	/**
	 * Retrieves the values of DOM nodes from a given DOMNodeList or a CSS-like selector string.
	 * If the input is a string, it will be treated as a CSS-like selector string and an internal
	 * queryDom() method will be used to retrieve the DOMNodeList. If the input is a DOMNodeList,
	 * the method will directly process the list.
	 * @param string|DOMNodeList $domList     The CSS-like selector string or the DOMNodeList to retrieve values from.
	 * @param DOMNode|null       $contextNode (Optional) The context node to search within when using a CSS-like selector string. If omitted, the search will be performed on the entire DOM.
	 * @return mixed The values of the DOM nodes. If a single value is found, it will be returned as a single string. If multiple values are found, they will be returned as an array of strings. If no values are found, NULL will be returned.
	 */
	public function getElementListValues(string|DOMNodeList $domList, DOMNode $contextNode = NULL): mixed
	{
		if (is_string($domList) && !empty($domList))
			$domList = $this->queryDom($domList, $contextNode);
		if ($domList instanceof DOMNodeList === FALSE)
			$domList = $this->domNodeList;
		if ($domList instanceof DOMNodeList === TRUE) {
			$values = [];
			foreach ($domList as $dom)
				$values[] = trim($dom->nodeValue);
			if (count($values) === 1)
				return reset($values);
			else
				return $values;
		}
		else
			return NULL;
	}

	/**
	 * Retrieves a list of attribute values from elements in the DOM.
	 * This method allows you to extract attribute values from specific elements
	 * in the DOM based on the provided query string. You can narrow down the
	 * search by passing a query string or a previously obtained DOMNodeList.
	 * If no query string or DOMNodeList is provided, the search will be performed
	 * on the entire DOM.
	 * @param string                  $name         The name of the attribute to retrieve.
	 * @param string|DOMNodeList|null $queryString  (Optional) The query string or DOMNodeList to search within.
	 *                                              If omitted, the search will be performed on the entire DOM.
	 * @param DOMNode|null            $contextNode  (Optional) The context node to search within.
	 *                                              If omitted, the search will be performed on the entire DOM.
	 * @return mixed The retrieved attribute values. If a single value is found, it will be returned as a string.
	 *                                              If multiple values are found, they will be returned as an array. If no values are found, NULL will be returned.
	 */
	public function getElementsListAttrValues(string                  $name,
	                                          string|DOMNodeList|null $queryString = NULL,
	                                          ?DOMNode                $contextNode = NULL): mixed
	{
		if (is_string($queryString) && !empty($queryString))
			$queryString = $this->queryDom($queryString, $contextNode);
		if ($queryString instanceof DOMNodeList === FALSE)
			$queryString = $this->domNodeList;
		$values = [];
		if ($queryString instanceof DOMNodeList === TRUE) {
			foreach ($queryString as $Dom) {
				if ($Dom instanceof DOMElement)
					$values[] = trim($Dom->getAttribute($name));
			}
			if (count($values) === 1)
				return reset($values);
			else
				return $values;
		}
		else
			return NULL;
	}

	/**
	 * Retrieves all DOM nodes with a specific tag name.
	 * @param string $tagName The name of the tag to search for.
	 * @return DOMNodeList|null A list of DOM nodes with the specified tag name. Returns null if no matching nodes are found.
	 */
	public function getElementByTagName(string $tagName): ?DOMNodeList
	{
		return $this->dom->getElementsByTagName($tagName);
	}

	/**
	 * Retrieves an element from the DOM using its ID.
	 * @param string $id The ID of the element to retrieve.
	 * @return DOMElement|null The element with the specified ID, or null if no such element exists.
	 */
	public function getElementById(string $id): ?DOMElement
	{
		return $this->dom->getElementById($id);
	}

	/**
	 * Counts the number of DOM nodes that match the given query string.
	 * @param string       $queryString The query string to match the nodes.
	 * @param DOMNode|null $contextNode The optional context node to search within. If omitted, the search will be performed on the entire DOM.
	 * @return mixed The number of nodes that match the query string.
	 */
	public function countDom(string $queryString, DOMNode $contextNode = NULL): mixed
	{
		return $this->domXPath->evaluate("count($queryString)", $contextNode);
	}

	/**
	 * Calculates the sum of values from DOM nodes using XPath.
	 * @param string       $xPath       The XPath expression to select DOM nodes.
	 * @param DOMNode|null $contextNode (Optional) The context node to start the evaluation from. If omitted, the evaluation will start from the root of the DOM.
	 * @return mixed  The sum of the selected values, or null if no values are found.
	 */
	public function sumDomValue(string $xPath, DOMNode $contextNode = NULL): mixed
	{
		return $this->domXPath->evaluate("sum($xPath)", $contextNode);
	}

	/**
	 * Evaluates an XPath expression and returns the result.
	 * @param string       $xPath       The XPath expression to evaluate.
	 * @param DOMNode|null $contextNode (Optional) The context node for the XPath evaluation. If omitted, the evaluation will be performed on the entire DOM.
	 * @return mixed    The result of the XPath evaluation.
	 */
	public function evaluate(string $xPath, DOMNode $contextNode = NULL): mixed
	{
		return $this->domXPath->evaluate($xPath, $contextNode);
	}

}